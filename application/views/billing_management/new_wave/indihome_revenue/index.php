<form method="post" action="<?= base_url($path."index") ?>">
  <div class="row">
    <div class="col-md-12">
      <div id="table-search">
        <div class="wrap">
          <table  class="table table-search table-striped">
            <thead>
              <tr>
                <th colspan="6" v-align="middle">Parameter Pencarian Data</th>
              </tr>
            </thead>
            <tbody>
              <tr>                   
                <td>Divisi</td>
                <td>
                  <select name="search[division][]" id="division" multiple class="form-control">
                    <?php 
                      foreach($division_lists as $d){
                        echo "<option value='".$d->DIVISION_CODE."' ".($search['division']!="" && in_array($d->DIVISION_CODE,$search['division']) ? "selected" : "").">".$d->DIVISION_CODE."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Paket Indihome</td>
                <td>
                  <select name="search[packet_indihome][]" multiple class="form-control" id="packet_indihome">
                    <?php 
                      foreach($packet_indihome_lists as $d){
                        echo "<option value='".$d->PACKET_INDIHOME_ID."' ".($search['packet_indihome']!="" && in_array($d->PACKET_INDIHOME_ID,$search['packet_indihome']) ? "selected" : "").">".$d->PACKET_INDIHOME_DESCRIPTION."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Ubis</td>
                <td>
                  <select name="search[ubis][]" multiple class="form-control" id="ubis">
                    <?php 
                      foreach($ubis_lists as $d){
                        echo "<option value='".$d->UBIS_ID."' ".($search['ubis']!="" && in_array($d->UBIS_ID,$search['ubis']) ? "selected" : "").">".$d->UBIS."</option>";
                      }
                    ?>
                  </select>
                </td>
              </tr>
              <tr>                   
                <td>Witel</td>
                <td>
                  <select name="search[witel][]" id="witel" multiple class="form-control">
                    <?php 
                      foreach($witel_lists as $w){
                        echo "<option value='".$w->WITEL_CODE."' ".($search['witel']!="" && in_array($w->WITEL_CODE,$search['witel']) ? "selected" : "").">".$w->WITEL_DESCRIPTION."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Paket Speedy</td>
                <td>
                  <select name="search[packet_speedy][]" multiple class="form-control" id="packet_speedy">
                    <?php 
                      foreach($packet_speedy_lists as $d){
                        echo "<option value='".$d->PACKET_SPEEDY_ID."' ".($search['packet_speedy']!="" && in_array($d->PACKET_SPEEDY_ID,$search['packet_speedy']) ? "selected" : "").">".$d->PACKET_SPEEDY_DESCRIPTION."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Segmen</td>
                <td>
                  <select name="search[ubis_segment][]" id="ubis_segment" multiple class="form-control">
                    <?php 
                      foreach($ubis_segment_lists as $r){
                        echo "<option value='".$r->UBIS_SEGMENT_ID."' ".($search['ubis_segment']!="" && in_array($r->UBIS_SEGMENT_ID,$search['ubis_segment']) ? "selected" : "").">".$r->UBIS_SEGMENT."</option>";
                      }
                    ?>
                  </select>
                </td>
              </tr>
              <tr>                   
                <td>Datel</td>
                <td>
                  <select name="search[datel][]" id="datel" multiple class="form-control">
                    <?php 
                      foreach($datel_lists as $w){
                        echo "<option value='".$w->DATEL_CODE."' ".($search['datel']!="" && in_array($w->DATEL_CODE,$search['datel']) ? "selected" : "").">".$w->DATEL_DESCRIPTION."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Profile Tagihan</td>
                <td>
                  <select name="search[profile_pelanggan][]" id="profile_pelanggan" multiple class="form-control">
                    <?php 
                      foreach($profile_pelanggan_lists as $w){
                        echo "<option value='".$w->PROFILE_PELANGGAN_ID."' ".($search['profile_pelanggan']!="" && in_array($w->PROFILE_PELANGGAN_ID,$search['profile_pelanggan']) ? "selected" : "").">".$w->PROFILE_PELANGGAN_DESCRIPTION."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Umur Pelanggan</td>
                <td>
                  <select name="search[umur_pelanggan][]" id="umur_pelanggan" multiple class="form-control">
                    <?php 
                      foreach($umur_pelanggan_lists as $r){
                        echo "<option value='".$r->UMUR_PELANGGAN_ID."' ".($search['umur_pelanggan']!="" && in_array($r->UMUR_PELANGGAN_ID,$search['umur_pelanggan']) ? "selected" : "").">".$r->UMUR_PELANGGAN_DESCRIPTION."</option>";
                      }
                    ?>
                  </select>
                </td>
              </tr>
              <tr>                   
                <td>Periode</td>
                <td>
                  <div class="col-md-9" style="padding-left:0px;">
                    <div class="col-md-7" style="padding-left:0px;">
                      <select name="search[month]" class="form-control">
                        <?php 
                          foreach($month_lists as $key=>$d){
                            echo "<option value='".$key."' ".($search['month']==$key ? "selected" : "").">".$d."</option>";
                          }
                        ?>
                      </select>
                    </div>
                    <div class="col-md-5" style="padding-left:0px;">
                      <select name="search[year]" class="form-control">
                        <?php 
                          for($x=date("Y");$x>(date("Y")-10);$x--){
                            echo "<option value='".$x."' ".($search['year']==$x ? "selected" : "").">".$x."</option>";
                          }
                        ?>
                      </select>
                    </div>
                  </div>
                </td>
                <td colspan="4"></td>
              </tr>
              <tr class="background-filter">                   
                <td>Baris</td>
                <td>
                  <select name="search[row]" class="form-control" id="row">
                    <?php 
                      foreach($row_lists as $key=>$d){
                        echo "<option value='".$key."' ".($search['view_by']=="L11" && $key=="billing_component" ? "disabled" : "")." ".($search['row']==$key ? "selected" : "").">".$d."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Kolom</td>
                <td>
                  <select name="search[column]" class="form-control" id="column">
                    <?php 
                      foreach($row_lists as $key=>$d){
                        echo "<option value='".$key."' ".($search['view_by']=="L11" && $key=="billing_component" ? "disabled" : "")." ".($search['column']==$key ? "selected" : "").">".$d."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>View By</td>
                <td>
                  <select name="search[view_by]" class="form-control" id="view_by">
                    <?php 
                      foreach($view_by_lists as $r){
                        echo "<option value='".$r->VIEW_BY."' ".($search['view_by']==$r->VIEW_BY ? "selected" : "").">".$r->VIEW_BY_DESCRIPTION."</option>";
                      }
                    ?>
                  </select>
                </td>
              </tr>
            </tbody>
            <tfoot>
              <tr>
                <td>Search</td>
                <td><input type="submit" name="search[button]" class="btn btn-search" value="Search"></td>
                <td>To Excel</td>
                <td><input type="submit" name="search[button]" class="btn btn-search" value="Download"></td>
                <td colspan="2"></td>
              </tr>
            </tfoot>
          </table>
        </div>
        <a href="javascript:void(0)" class="showhide"></a>
      </div>
    </div>
  </div> 
</form>
<div class="row">
  <div class="col-md-12">
    <h4 class="title-menu">Indihome Revenue</h4>
    <div class="billing-detail">
      <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered" id="table_indihome_revenue">
        <thead>
          <?php 
            echo "<tr>";
            echo "<th class='text-center'>".$row_lists[$search['row']]."</th>";
            $columns=array();
            foreach($lists as $l){
              if(!in_array($l->{$field[$search['column']]},$columns)){
                array_push($columns,$l->{$field[$search['column']]});
                echo "<th class='text-center'>".($l->{$field[$search['column']]}!="" && $l->{$field[$search['column']]}!=NULL ? $l->{$field[$search['column']]} : $no_mapping)."</th>";
              }
            }
            echo "<th class='text-center'>Total</th>";
            echo "</tr>";
          ?>
        </thead>
        <tbody>
          <?php   
            $rows=array();
            $total_column=array();
            $total_column['total']=0;
            foreach($lists as $l){
              if(!in_array($l->{$field[$search['row']]},$rows)){
                array_push($rows,$l->{$field[$search['row']]});
                echo "<tr>";
                echo "<td>".($l->{$field[$search['row']]}!="" && $l->{$field[$search['row']]}!=NULL ? $l->{$field[$search['row']]} : $no_mapping)."</td>";
                $total=0;
                $counter=0;
                foreach($columns as $c){
                  $check=0;
                  foreach($lists as $l2){
                    if($l->{$field[$search['row']]}==$l2->{$field[$search['row']]})
                    {
                      if($c==$l2->{$field[$search['column']]}){
                        if($search['view_by']=="L11"){
                          $value=$l2->SST;
                        }elseif($search['view_by']=="AMOUNT"){
                          $value=$l2->TAGIHAN;
                        }
                        $total+=$value;
                        if(!isset($total_column[$c]))$total_column[$c]=0;
                        $total_column[$c]+=$value;
                        $counter++;
                        echo "<td class='text-right'>".number_format($value,0,".",",")."</td>";
                        $check=1;
                      }
                    }
                  }
                  if($check==0){
                    echo "<td class='text-right'>0</td>";
                    if(!isset($total_column[$c]))$total_column[$c]=0;
                    $total_column[$c]+=0;
                  }
                }
                if(!isset($total_column['total']))$total_column['total']=0;
                $total_column['total']+=$total;
                echo "<td class='text-right'>".number_format($total,0,".",",")."</td>";
                echo "</tr>";
              }
            }
          ?>
        </tbody>
        <tfoot>
        <?php
          echo "<tr>";
          echo "<td style='padding:8px;'>Total (".sizeof($rows)." item)</td>";
          foreach($columns as $c){
            foreach($total_column as $key=>$t){
              if($c==$key){
                echo "<td class='text-right' style='padding:8px;'>".number_format($t,0,".",",")."</td>";
              }
            }
          }
          echo "<td class='text-right' style='padding:8px;'>".number_format($total_column['total'],0,".",",")."</td>";
          echo "</tr>";
        ?>
        </tfoot>
      </table>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    $("#table_indihome_revenue").dataTable({
      "bSort" : true,
      "bFilter" : false,
      "bPaginate": false,
      "info": false,
    });
    $("#division").multiselect({
      includeSelectAllOption: true,
      numberDisplayed:2
    }).change(function(){
      var divisions=$(this).val();
      $.ajax({
        url:"<?= base_url("ajax/getWitel") ?>",
        dataType:"JSON",
        data:{divisions:divisions},
        success:function(response){
          $("#witel").multiselect("destroy");
          $("#witel").html(response.content);
          $("#witel").multiselect({
            includeSelectAllOption: true,
            maxHeight: 212,
            enableFiltering: true
          }).change();
        }
      });
    });
    $("#ubis").multiselect({
      dropRight: true,
      includeSelectAllOption: true,
      numberDisplayed:2
    }).change(function(){
      var ubis=$(this).val();
      $.ajax({
        url:"<?= base_url("ajax/getUbisSegments") ?>",
        dataType:"JSON",
        data:{ubis:ubis},
        success:function(response){
          $("#ubis_segment").multiselect("destroy");
          $("#ubis_segment").html(response.content);
          $("#ubis_segment").multiselect({
            dropRight: true,
            includeSelectAllOption: true,
            maxHeight: 212,
            enableFiltering: true
          });
        }
      });
    });
    $("#witel,#datel,#packet_indihome,#packet_speedy,#umur_pelanggan,#profile_pelanggan").multiselect({numberDisplayed:2,maxHeight: 212,includeSelectAllOption: true,enableFiltering: true});
    $("#ubis_segment").multiselect({dropRight: true,numberDisplayed:2,maxHeight: 212,includeSelectAllOption: true,enableFiltering: true});
    $("#view_by").change(function(){
      var view_by=$(this).val();
      switch(view_by){
        case "AMOUNT":
          $("#row option[value='billing_component']").removeAttr("disabled");
          $("#column option[value='billing_component']").removeAttr("disabled");
          if($("#row").val()=="billing_component" || $("#column").val()=="billing_component"){
            $("#billing_component").attr("disabled","disabled");
          }else{
            $("#billing_component").removeAttr("disabled");
          }
          break;
        case "L11":
          if($("#row").val()=="billing_component")$("#row").val(0);
          if($("#column").val()=="billing_component")$("#column").val(0);
          $("#billing_component").attr("disabled","disabled");
          $("#row option[value='billing_component']").attr("disabled","disabled");
          $("#column option[value='billing_component']").attr("disabled","disabled");
          break;
      }
    });
  });
  $("#witel").live("change",function(){
    var divisions=$("#division").val();
    var witels=$(this).val();
    $.ajax({
      url:"<?= base_url("ajax/getDatel") ?>",
      dataType:"JSON",
      data:{witels:witels,divisions:divisions},
      success:function(response){
        $("#datel").multiselect("destroy");
        $("#datel").html(response.content);
        $("#datel").multiselect({
          includeSelectAllOption: true,
          maxHeight: 212,
          enableFiltering: true,
          numberDisplayed:2
        }).change();
      }
    });
  });
</script>