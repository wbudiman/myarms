<form method="post" action="<?= base_url($path."index") ?>">
  <div class="row">
    <div class="col-md-12">
      <div id="table-search">
        <div class="wrap">
          <table class="table table-search table-striped">
            <thead>
              <tr>
                <th colspan="8" v-align="middle">Parameter Pencarian Data</th>
              </tr>
            </thead>
            <tbody>
              <tr>                   
                <td>Divisi</td>
                <td>
                  <select name="search[division][]" id="division" multiple class="form-control">
                    <?php 
                      foreach($division_lists as $d){
                        echo "<option value='".$d->DIVISION_CODE."' ".($search['division']!="" && in_array($d->DIVISION_CODE,$search['division']) ? "selected" : "").">".$d->DIVISION_CODE."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Paket IME</td>
                <td>
                  <select name="search[packet_ime][]" multiple class="form-control" id="packet_ime">
                    <?php 
                      foreach($packet_ime_lists as $d){
                        echo "<option value='".$d->PACKET_IME."' ".($search['packet_ime']!="" && in_array($d->PACKET_IME,$search['packet_ime']) ? "selected" : "").">".$d->PACKET_IME."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Ubis</td>
                <td>
                  <select name="search[ubis][]" multiple class="form-control" id="ubis">
                    <?php 
                      foreach($ubis_lists as $d){
                        echo "<option value='".$d->UBIS_ID."' ".($search['ubis']!="" && in_array($d->UBIS_ID,$search['ubis']) ? "selected" : "").">".$d->UBIS."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Periode 1</td>
                <td>
                  <div class="col-md-9">
                    <div class="col-sm-7">
                      <select name="search[month_1]" class="form-control" id="month_1">
                        <?php 
                          foreach($month_lists as $key=>$d){
                            echo "<option value='".$key."' ".($search['month_1']==$key ? "selected" : "").">".$d."</option>";
                          }
                        ?>
                      </select>
                    </div>
                    <div class="col-sm-5">
                      <select name="search[year_1]" class="form-control" id="year_1">
                        <?php 
                          for($x=date("Y");$x>(date("Y")-10);$x--){
                            echo "<option value='".$x."' ".($search['year_1']==$x ? "selected" : "").">".$x."</option>";
                          }
                        ?>
                      </select>
                    </div>
                  </div>
                </td>
              </tr>
              <tr>                   
                <td>Witel</td>
                <td>
                  <select name="search[witel][]" id="witel" multiple class="form-control">
                    <?php 
                      foreach($witel_lists as $w){
                        echo "<option value='".$w->WITEL_CODE."' ".($search['witel']!="" && in_array($w->WITEL_CODE,$search['witel']) ? "selected" : "").">".$w->WITEL_DESCRIPTION."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Deskripsi IME</td>
                <td>
                  <select name="search[ime_description][]" multiple class="form-control" id="ime_description">
                    <?php 
                      foreach($ime_description_lists as $d){
                        echo "<option value='".$d->PACKET_IME_ID."' ".($search['ime_description']!="" && in_array($d->PACKET_IME_ID,$search['ime_description']) ? "selected" : "").">".$d->PACKET_IME_DESCRIPTION."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Segmen</td>
                <td>
                  <select name="search[ubis_segment][]" id="ubis_segment" multiple class="form-control">
                    <?php 
                      foreach($ubis_segment_lists as $r){
                        echo "<option value='".$r->UBIS_SEGMENT_ID."' ".($search['ubis_segment']!="" && in_array($r->UBIS_SEGMENT_ID,$search['ubis_segment']) ? "selected" : "").">".$r->UBIS_SEGMENT."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Periode 2</td>
                <td>
                  <div class="col-sm-9">
                    <div class="col-sm-7">
                      <select name="search[month_2]" class="form-control" id="month_2">
                        <?php 
                          foreach($month_lists as $key=>$d){
                            echo "<option value='".$key."' ".($search['month_2']==$key ? "selected" : "").">".$d."</option>";
                          }
                        ?>
                      </select>
                    </div>
                    <div class="col-sm-5">
                      <select name="search[year_2]" class="form-control" id="year_2">
                        <?php 
                          for($x=date("Y");$x>(date("Y")-10);$x--){
                            echo "<option value='".$x."' ".($search['year_2']==$x ? "selected" : "").">".$x."</option>";
                          }
                        ?>
                      </select>
                    </div>
                  </div>
                </td>
              </tr>
              <tr>                   
                <td>Datel</td>
                <td>
                  <select name="search[datel][]" id="datel" multiple class="form-control">
                    <?php 
                      foreach($datel_lists as $w){
                        echo "<option value='".$w->DATEL_CODE."' ".($search['datel']!="" && in_array($w->DATEL_CODE,$search['datel']) ? "selected" : "").">".$w->DATEL_DESCRIPTION."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Status Bayar</td>
                <td>
                  <select name="search[payment_status][]" id="payment_status" multiple class="form-control">
                    <?php 
                      foreach($payment_status_lists as $w){
                        echo "<option value='".$w->PAYMENT_STATUS_ID."' ".($search['payment_status']!="" && in_array($w->PAYMENT_STATUS_ID,$search['payment_status']) ? "selected" : "").">".$w->PAYMENT_STATUS_NAME."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Umur Pelanggan</td>
                <td>
                  <select name="search[umur_pelanggan][]" id="umur_pelanggan" multiple class="form-control">
                    <?php 
                      foreach($umur_pelanggan_lists as $r){
                        echo "<option value='".$r->UMUR_PELANGGAN_ID."' ".($search['umur_pelanggan']!="" && in_array($r->UMUR_PELANGGAN_ID,$search['umur_pelanggan']) ? "selected" : "").">".$r->UMUR_PELANGGAN_DESCRIPTION."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Periode 3</td>
                <td>
                  <div class="col-sm-9">
                    <div class="col-sm-7">
                      <select name="search[month_3]" class="form-control" id="month_3">
                        <?php 
                          foreach($month_lists as $key=>$d){
                            echo "<option value='".$key."' ".($search['month_3']==$key ? "selected" : "").">".$d."</option>";
                          }
                        ?>
                      </select>
                    </div>
                    <div class="col-sm-5">
                      <select name="search[year_3]" class="form-control" id="year_3">
                        <?php 
                          for($x=date("Y");$x>(date("Y")-10);$x--){
                            echo "<option value='".$x."' ".($search['year_3']==$x ? "selected" : "").">".$x."</option>";
                          }
                        ?>
                      </select>
                    </div>
                  </div>
                </td>
              </tr>
              <tr class="background-filter">                   
                <td>Show Data</td>
                <td>
                  <select name="search[show_data]" class="form-control" id="show_data">
                    <?php 
                      foreach($row_lists as $key=>$d){
                        echo "<option value='".$key."' ".($search['show_data']==$key ? "selected" : "").">".$d."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>View By</td>
                <td>
                  <select name="search[view_by]" class="form-control" id="view_by">
                    <?php 
                      foreach($view_by_lists as $r){
                        echo "<option value='".$r->VIEW_BY."' ".($search['view_by']==$r->VIEW_BY ? "selected" : "").">".$r->VIEW_BY_DESCRIPTION."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td colspan="4"></td>
              </tr>
            </tbody>
            <tfoot>
              <tr>
                <td>Search</td>
                <td><input type="submit" name="search[button]" class="btn btn-search" value="Search"></td>
                <td>To Excel</td>
                <td><input type="submit" name="search[button]" class="btn btn-search" value="Download"></td>
                <td colspan="4"></td>
              </tr>
            </tfoot>
          </table>
        </div>
        <a href="javascript:void(0)" class="showhide"></a>
      </div>
    </div>
  </div> 
</form>
<div class="row">
  <div class="col-md-12">
    <h4 class="title-menu">IME Growth</h4>
    <div class="billing-detail">
      <table class="table table-bordered" id="table_ime_growth">
        <thead>
          <?php 
            $total_column=array();
            $total_column['total']=0;
            echo "<tr>";
            echo "<th class='text-center'>".$row_lists[$search['show_data']]."</th>";
            $periodes=array($search['year_1'].$search['month_1'],$search['year_2'].$search['month_2'],$search['year_3'].$search['month_3']);
            foreach($periodes as $l){
              $total_column[$l]=0;
              echo "<th class='text-center'>".$l."</th>";
            }
            echo "<th class='text-center'>Bulan 1 ke 2</th>";
            echo "<th class='text-center'>Growth 1 ke 2</th>";
            echo "<th class='text-center'>Bulan 2 ke 3</th>";
            echo "<th class='text-center'>Growth 2 ke 3</th>";
            echo "</tr>";
          ?>
        </thead>
        <tbody>
          <?php
            $rows=array();
            $rows2=array();
            foreach($lists as $l){
              if(!in_array($l->{$field[$search['show_data']]},$rows))
              {
                array_push($rows,$l->{$field[$search['show_data']]});
              }
            }
            $counter=0;
            foreach($rows as $r){
              echo "<tr>";
              echo "<td>".($r!="" && $r!=NULL ? $r : $no_mapping)."</td>";
              $values=array();
              foreach($periodes as $p){
                $check=0;
                foreach($lists as $l){
                  if($l->PERIODE==$p && $r==$l->{$field[$search['show_data']]}){
                    if($search['view_by']=="L11"){
                      $value=$l->SST;
                    }elseif($search['view_by']=="AMOUNT"){
                      $value=$l->TAGIHAN;
                    }
                    if(!isset($total_column[$p]))$total_column[$p]=0;
                    $total_column[$p]+=$value;
                    array_push($values,$value);
                    echo "<td class='text-right'>".number_format($value,0,".",",")."</td>";
                    $check=1;
                  }
                }
                if($check==0){
                  if(!isset($total_column[$p]))$total_column[$p]=0;
                  $total_column[$p]+=0;
                  array_push($values,0);
                  echo "<td class='text-right'>0</td>";
                }
              }
              $bulan_1_to_2=$values[1]-$values[0];
              $growth_1_to_2=($bulan_1_to_2!=0 ? ($values[0]!=0 ? $bulan_1_to_2/$values[0]*100 : 100) : 0);
              $bulan_2_to_3=$values[2]-$values[1];
              $growth_2_to_3=($bulan_2_to_3!=0 ? ($values[1]!=0 ? $bulan_2_to_3/$values[1]*100 : 100) : 0);
              echo "<td class='text-right'>".number_format($bulan_1_to_2,0,".",",")."</td>";
              echo "<td class='text-right'>".number_format($growth_1_to_2,2)." %</td>";
              echo "<td class='text-right'>".number_format($bulan_2_to_3,0,".",",")."</td>";
              echo "<td class='text-right'>".number_format($growth_2_to_3,2)." %</td>";
              echo "</tr>";
              if(!isset($total_column['bulan_1_to_2']))$total_column['bulan_1_to_2']=0;
              if(!isset($total_column['growth_1_to_2']))$total_column['growth_1_to_2']=0;
              if(!isset($total_column['bulan_2_to_3']))$total_column['bulan_2_to_3']=0;
              if(!isset($total_column['growth_2_to_3']))$total_column['growth_2_to_3']=0;
              $total_column['bulan_1_to_2']+=$bulan_1_to_2;
              $total_column['growth_1_to_2']+=$growth_1_to_2;
              $total_column['bulan_2_to_3']+=$bulan_2_to_3;
              $total_column['growth_2_to_3']+=$growth_2_to_3;
              $counter++;
            }
          ?>
        </tbody>
        <tfoot>
        <?php
          echo "<tr>";
          echo "<td style='padding:8px;'>Total (".sizeof($rows)." item)</td>";
          foreach($periodes as $p){
            foreach($total_column as $key=>$t){
              if($p==$key){
                echo "<td class='text-right' style='padding:8px;'>".number_format($t,0,".",",")."</td>";
              }
            }
          }
          $bulan_1_to_2=$total_column[$search['year_2'].$search['month_2']]-$total_column[$search['year_1'].$search['month_1']];
          echo "<td class='text-right' style='padding:8px;'>".number_format($bulan_1_to_2,0,".",",")."</td>";
          $growth_1_to_2=($bulan_1_to_2!=0 ? ($total_column[$search['year_1'].$search['month_1']]!=0 ? $bulan_1_to_2/$total_column[$search['year_1'].$search['month_1']]*100 : 100) : 0);
          echo "<td class='text-right' style='padding:8px;'>".number_format($growth_1_to_2,2)." %</td>";
          $bulan_2_to_3=$total_column[$search['year_3'].$search['month_3']]-$total_column[$search['year_2'].$search['month_2']];
          echo "<td class='text-right' style='padding:8px;'>".number_format($bulan_2_to_3,0,".",",")."</td>";
          $growth_2_to_3=($bulan_2_to_3!=0 ? ($total_column[$search['year_2'].$search['month_2']]!=0 ? $bulan_2_to_3/$total_column[$search['year_2'].$search['month_2']]*100 : 100) : 0);
          echo "<td class='text-right' style='padding:8px;'>".number_format($growth_2_to_3,2)." %</td>";
          echo "</tr>";
        ?>
        </tfoot>
      </table>
    </div>
  </div>
</div>
<script type="text/javascript">
  height="290px";
  $(document).ready(function(){
    $("#table_ime_growth").dataTable({
      "bSort" : true,
      "bFilter" : false,
      "bPaginate": false,
      "info": false,
    });
    $("#division").multiselect({
      includeSelectAllOption: true,
      numberDisplayed:2
    }).change(function(){
      var divisions=$(this).val();
      $.ajax({
        url:"<?= base_url("ajax/getWitel") ?>",
        dataType:"JSON",
        data:{divisions:divisions},
        success:function(response){
          $("#witel").multiselect("destroy");
          $("#witel").html(response.content);
          $("#witel").multiselect({
            includeSelectAllOption: true,
            maxHeight: 212,
            enableFiltering: true
          }).change();
        }
      });
    });
    $("#ubis").multiselect({
      includeSelectAllOption: true,
      numberDisplayed:2
    }).change(function(){
      var ubis=$(this).val();
      $.ajax({
        url:"<?= base_url("ajax/getUbisSegments") ?>",
        dataType:"JSON",
        data:{ubis:ubis},
        success:function(response){
          $("#ubis_segment").multiselect("destroy");
          $("#ubis_segment").html(response.content);
          $("#ubis_segment").multiselect({
            includeSelectAllOption: true,
            maxHeight: 212,
            enableFiltering: true
          });
        }
      });
    });
    $("#witel,#datel,#ubis_segment,#packet_ime,#ime_description,#umur_pelanggan,#payment_status").multiselect({numberDisplayed:1,maxHeight: 212,includeSelectAllOption: true,enableFiltering: true});
    $("form").submit(function(){
      var periode1=$("#year_1").val()+$("#month_1").val();
      var periode2=$("#year_2").val()+$("#month_2").val();
      var periode3=$("#year_3").val()+$("#month_3").val();
      if(periode3<=periode2 || periode2<=periode1){
        main.notification("bottomRight","warning","Peringatan","Silahkan pilih periode 1,2,3 dengan benar!");
        return false;
      }
    });
  });
  $("#witel").live("change",function(){
    var divisions=$("#division").val();
    var witels=$(this).val();
    $.ajax({
      url:"<?= base_url("ajax/getDatel") ?>",
      dataType:"JSON",
      data:{witels:witels,divisions:divisions},
      success:function(response){
        $("#datel").multiselect("destroy");
        $("#datel").html(response.content);
        $("#datel").multiselect({
          includeSelectAllOption: true,
          maxHeight: 212,
          enableFiltering: true,
          numberDisplayed:2
        }).change();
      }
    });
  });
  $("#packet_ime").live("change",function(){
    var packet_imes=$(this).val();
    $.ajax({
      url:"<?= base_url("ajax/getImeDescriptions") ?>",
      dataType:"JSON",
      data:{packet_imes:packet_imes},
      success:function(response){
        $("#ime_description").multiselect("destroy");
        $("#ime_description").html(response.content);
        $("#ime_description").multiselect({
          includeSelectAllOption: true,
          maxHeight: 212,
          enableFiltering: true,
          numberDisplayed:2
        })
      }
    });
  });
</script>