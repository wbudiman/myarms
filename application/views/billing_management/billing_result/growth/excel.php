<h5><?= $header_title ?> ( <?= $header_parameter ?> )</h5>
<table border="1">
  <thead>
    <?php 
      $total_column=array();
      $total_column['total']=0;
      echo "<tr>";
      echo "<th class='text-center'>".$row_lists[$search['show_data']]."</th>";
      $periodes=array($search['year_1'].$search['month_1'],$search['year_2'].$search['month_2'],$search['year_3'].$search['month_3']);
      foreach($periodes as $l){
        $total_column[$l]=0;
        echo "<th class='text-center'>".$l."</th>";
      }
      echo "<th class='text-center'>Bulan 1 ke 2</th>";
      echo "<th class='text-center'>Growth 1 ke 2</th>";
      echo "<th class='text-center'>Bulan 2 ke 3</th>";
      echo "<th class='text-center'>Growth 2 ke 3</th>";
      echo "</tr>";
    ?>
  </thead>
  <tbody>
    <?php
      $rows=array();
      $rows2=array();
      foreach($lists as $l){
        if(is_array($field[$search['show_data']]))
        {
          foreach($field[$search['show_data']] as $key=>$f){
            if(!in_array($f,$rows))
            {
              array_push($rows,$f);
              array_push($rows2,$key);
            }
          }
        }else{
          if(!in_array($l->{$field[$search['show_data']]},$rows))
          {
            array_push($rows,$l->{$field[$search['show_data']]});
          }
        }
      }
      $counter=0;
      foreach($rows as $r){
        echo "<tr>";
        echo "<td>".($r!="" && $r!=NULL ? $r : $no_mapping)."</td>";
        $values=array();
        foreach($periodes as $p){
          $check=0;
          foreach($lists as $l){
            if(is_array($field[$search['show_data']]))
            {
              if($l->PERIODE==$p){
                $value=$l->AMOUNT;
                if(!isset($total_column[$p]))$total_column[$p]=0;
                $total_column[$p]+=$value;
                array_push($values,$value);
                echo "<td class='text-right'>".$value."</td>";
                $check=1;
              }
            }else{
              if($l->PERIODE==$p && $r==$l->{$field[$search['show_data']]}){
                $value=$l->AMOUNT;
                if(!isset($total_column[$p]))$total_column[$p]=0;
                $total_column[$p]+=$value;
                array_push($values,$value);
                echo "<td class='text-right'>".$value."</td>";
                $check=1;
              }
            }
          }
          if($check==0){
            if(!isset($total_column[$p]))$total_column[$p]=0;
            $total_column[$p]+=0;
            array_push($values,0);
            echo "<td class='text-right'>0</td>";
          }
        }
        $bulan_1_to_2=$values[1]-$values[0];
        $growth_1_to_2=($bulan_1_to_2!=0 ? ($values[0]!=0 ? $bulan_1_to_2/$values[0]*100 : 100) : 0);
        $bulan_2_to_3=$values[2]-$values[1];
        $growth_2_to_3=($bulan_2_to_3!=0 ? ($values[1]!=0 ? $bulan_2_to_3/$values[1]*100 : 100) : 0);
        echo "<td class='text-right'>".$bulan_1_to_2."</td>";
        echo "<td class='text-right'>".number_format($growth_1_to_2,2)." %</td>";
        echo "<td class='text-right'>".$bulan_2_to_3."</td>";
        echo "<td class='text-right'>".number_format($growth_2_to_3,2)." %</td>";
        echo "</tr>";
        if(!isset($total_column['bulan_1_to_2']))$total_column['bulan_1_to_2']=0;
        if(!isset($total_column['growth_1_to_2']))$total_column['growth_1_to_2']=0;
        if(!isset($total_column['bulan_2_to_3']))$total_column['bulan_2_to_3']=0;
        if(!isset($total_column['growth_2_to_3']))$total_column['growth_2_to_3']=0;
        $total_column['bulan_1_to_2']+=$bulan_1_to_2;
        $total_column['growth_1_to_2']+=$growth_1_to_2;
        $total_column['bulan_2_to_3']+=$bulan_2_to_3;
        $total_column['growth_2_to_3']+=$growth_2_to_3;
        $counter++;
      }
    ?>
  </tbody>
  <tfoot>
  <?php
    // if($search['show_data']!="billing_component"){
      echo "<tr>";
      echo "<td style='padding:8px;'>Total (".sizeof($rows)." item)</td>";
      foreach($periodes as $p){
        foreach($total_column as $key=>$t){
          if($p==$key){
            echo "<td class='text-right' style='padding:8px;'>".$t."</td>";
          }
        }
      }
      $bulan_1_to_2=$total_column[$search['year_2'].$search['month_2']]-$total_column[$search['year_1'].$search['month_1']];
      echo "<td class='text-right' style='padding:8px;'>".$bulan_1_to_2."</td>";
      $growth_1_to_2=($bulan_1_to_2!=0 ? ($total_column[$search['year_1'].$search['month_1']]!=0 ? $bulan_1_to_2/$total_column[$search['year_1'].$search['month_1']]*100 : 100) : 0);
      echo "<td class='text-right' style='padding:8px;'>".number_format($growth_1_to_2,2)." %</td>";
      $bulan_2_to_3=$total_column[$search['year_3'].$search['month_3']]-$total_column[$search['year_2'].$search['month_2']];
      echo "<td class='text-right' style='padding:8px;'>".$bulan_2_to_3."</td>";
      $growth_2_to_3=($bulan_2_to_3!=0 ? ($total_column[$search['year_2'].$search['month_2']]!=0 ? $bulan_2_to_3/$total_column[$search['year_2'].$search['month_2']]*100 : 100) : 0);
      echo "<td class='text-right' style='padding:8px;'>".number_format($growth_2_to_3,2)." %</td>";
      echo "</tr>";
    // }
  ?>
  </tfoot>
</table>