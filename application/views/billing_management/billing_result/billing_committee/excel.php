<table border="1">
  <thead>
    <?php 
      echo "<tr>";
      echo "<th align='center'>".$row_lists[$search['header']]."</th>";
      $colspan=8;
      $periodes=array($search['year_1'].$search['month_1'],$search['year_2'].$search['month_2'],$search['year_3'].$search['month_3']);
      foreach($periodes as $l){
        echo "<th align='center'>".$l."</th>";
      }
      echo "<th align='center'>Bulan 1 ke 2</th>";
      echo "<th align='center'>Growth 1 ke 2</th>";
      echo "<th align='center'>Bulan 2 ke 3</th>";
      echo "<th align='center'>Growth 2 ke 3</th>";
      echo "</tr>";
    ?>
  </thead>
  <tbody>
    <?php
      $grand_total=array();
      $headers=array();
      foreach($lists as $l){
        if(is_array($field[$search['header']]))
        {
          foreach($field[$search['header']] as $key=>$h)
          {
            if(!in_array($h->BILLING_COMPONENT_FIELD,$headers)){
              $headers[$h->BILLING_COMPONENT_FIELD]=$h->BILLING_COMPONENT_DESCRIPTION;
            }
          }
        }else{
          if(!in_array($l->{$field[$search['header']]},$headers)){
            array_push($headers,$l->{$field[$search['header']]});
          }
        }
      }
      foreach($headers as $key=>$h){
        echo "<tr>";
        echo "<td colspan='".$colspan."' ><b>".($h!="" && $h!=NULL ? $h : $no_mapping)."</b></td>";
        echo "</tr>";
        $details=array();
        $values=array();
        $total_column=array();
        $total_column['total']=0;
        $total_per_row=0;
        if(is_array($field[$search['detail']]))
        {
          foreach($field[$search['detail']] as $key2=>$d){
            echo "<tr>";
            echo "<td>".($d->BILLING_COMPONENT_DESCRIPTION!="" && $d->BILLING_COMPONENT_DESCRIPTION!=NULL ? $d->BILLING_COMPONENT_DESCRIPTION : $no_mapping)."</td>";
            $total_per_row++;
            $values=array();
            foreach($periodes as $p){
              $check=0;
              foreach($lists as $l){
                if(is_array($field[$search['header']]))
                {
                  foreach($field[$search['header']] as $key3=>$h2){
                    if($l->PERIODE==$p && $h==$h2->BILLING_COMPONENT_DESCRIPTION && $d->BILLING_COMPONENT_FIELD==$h2->BILLING_COMPONENT_FIELD){
                      $value=$l->{$h2->BILLING_COMPONENT_FIELD};
                      echo "<td align='right'>".$value."</td>";
                      if(!isset($total_column[$p]))$total_column[$p]=0;
                      $total_column[$p]+=$value;
                      array_push($values,$value);
                      $check=1;
                    }
                  }
                }else{
                  if($l->PERIODE==$p && $h==$l->{$field[$search['header']]}){
                    if(!isset($search['billing_component']))
                    {
                      $value=$l->{$d->BILLING_COMPONENT_FIELD};
                    
                    }else{
                      $value=$l->{$search['billing_component']};
                    }
                    echo "<td align='right'>".$value."</td>";
                    if(!isset($total_column[$p]))$total_column[$p]=0;
                    $total_column[$p]+=$value;
                    array_push($values,$value);
                    $check=1;
                  }
                }
              }
              if($check==0){
                echo "<td align='right'>0</td>";
                array_push($values,0);
                if(!isset($total_column[$p]))$total_column[$p]=0;
                $total_column[$p]+=0;
              }
            }
            $bulan_1_to_2=$values[1]-$values[0];
            $growth_1_to_2=($bulan_1_to_2!=0 ? ($values[0]!=0 ? $bulan_1_to_2/$values[0]*100 : 100) : 0);
            $bulan_2_to_3=$values[2]-$values[1];
            $growth_2_to_3=($bulan_2_to_3!=0 ? ($values[1]!=0 ? $bulan_2_to_3/$values[1]*100 : 100) : 0);
            echo "<td align='right'>".$bulan_1_to_2."</td>";
            echo "<td align='right'>".number_format($growth_1_to_2,2)." %</td>";
            echo "<td align='right'>".$bulan_2_to_3."</td>";
            echo "<td align='right'>".number_format($growth_2_to_3,2)." %</td>";
            if(!isset($total_column['bulan_1_to_2']))$total_column['bulan_1_to_2']=0;
            if(!isset($total_column['growth_1_to_2']))$total_column['growth_1_to_2']=0;
            if(!isset($total_column['bulan_2_to_3']))$total_column['bulan_2_to_3']=0;
            if(!isset($total_column['growth_2_to_3']))$total_column['growth_2_to_3']=0;
            $total_column['bulan_1_to_2']+=$bulan_1_to_2;
            $total_column['growth_1_to_2']+=$growth_1_to_2;
            $total_column['bulan_2_to_3']+=$bulan_2_to_3;
            $total_column['growth_2_to_3']+=$growth_2_to_3;
            echo "</tr>";
          }
        }else{
          foreach($lists as $l){
            if(!in_array($l->{$field[$search['detail']]},$details) && $h==$l->{$field[$search['header']]} ){
              array_push($details,$l->{$field[$search['detail']]});
              echo "<tr>";
              echo "<td>".($l->{$field[$search['detail']]}!="" && $l->{$field[$search['detail']]}!=NULL ? $l->{$field[$search['detail']]} : $no_mapping)."</td>";
              $total_per_row++;
              $values=array();
              foreach($periodes as $p){
                $counter2=0;
                foreach($lists as $l2){
                  if($l->{$field[$search['detail']]}==$l2->{$field[$search['detail']]} && $h==$l2->{$field[$search['header']]} && $l2->PERIODE==$p)
                  {
                    if(!isset($search['billing_component']))
                    {
                      $value=$l2->AMOUNT;
                    }else{
                      $value=$l2->{$search['billing_component']};
                    }
                    if(!isset($total_column[$p]))$total_column[$p]=0;
                    $total_column[$p]+=$value;
                    array_push($values,$value);
                    echo "<td align='right'>".$value."</td>";
                    $counter2++;
                  }
                }
                if($counter2==0){
                  array_push($values,0);
                  if(!isset($total_column[$p]))$total_column[$p]=0;
                  $total_column[$p]+=0;
                  echo "<td align='right'>0</td>";
                }
              }
              $bulan_1_to_2=$values[1]-$values[0];
              $growth_1_to_2=($bulan_1_to_2!=0 ? ($values[0]!=0 ? $bulan_1_to_2/$values[0]*100 : 100) : 0);
              $bulan_2_to_3=$values[2]-$values[1];
              $growth_2_to_3=($bulan_2_to_3!=0 ? ($values[1]!=0 ? $bulan_2_to_3/$values[1]*100 : 100) : 0);
              echo "<td align='right'>".$bulan_1_to_2."</td>";
              echo "<td align='right'>".number_format($growth_1_to_2,2)." %</td>";
              echo "<td align='right'>".$bulan_2_to_3."</td>";
              echo "<td align='right'>".number_format($growth_2_to_3,2)." %</td>";
              if(!isset($total_column['bulan_1_to_2']))$total_column['bulan_1_to_2']=0;
              if(!isset($total_column['growth_1_to_2']))$total_column['growth_1_to_2']=0;
              if(!isset($total_column['bulan_2_to_3']))$total_column['bulan_2_to_3']=0;
              if(!isset($total_column['growth_2_to_3']))$total_column['growth_2_to_3']=0;
              $total_column['bulan_1_to_2']+=$bulan_1_to_2;
              $total_column['growth_1_to_2']+=$growth_1_to_2;
              $total_column['bulan_2_to_3']+=$bulan_2_to_3;
              $total_column['growth_2_to_3']+=$growth_2_to_3;
              echo "</tr>";
            }
          }
        }
        echo "<tr class='background-filter'>";
        echo "<td  align='center'><b>Total ".($h!="" && $h!=NULL ? $h : $no_mapping)." (".$total_per_row." item)</b></td>";
        foreach($periodes as $p){
          foreach($total_column as $key=>$t){
            if($p==$key){
              if(!isset($grand_total[$p]))$grand_total[$p]=0;
              $grand_total[$p]+=$t;
              echo "<td align='right' ><b>".$t."</b></td>";
            }
          }
        }
        $bulan_1_to_2=$total_column[$search['year_2'].$search['month_2']]-$total_column[$search['year_1'].$search['month_1']];
        echo "<td align='right' ><b>".$bulan_1_to_2."</b></td>";
        $growth_1_to_2=($bulan_1_to_2!=0 ? ($total_column[$search['year_1'].$search['month_1']]!=0 ? $bulan_1_to_2/$total_column[$search['year_1'].$search['month_1']]*100 : 100) : 0);
        echo "<td align='right' ><b>".number_format($growth_1_to_2,2)." %</b></td>";
        $bulan_2_to_3=$total_column[$search['year_3'].$search['month_3']]-$total_column[$search['year_2'].$search['month_2']];
        echo "<td align='right' ><b>".$bulan_2_to_3."</b></td>";
        $growth_2_to_3=($bulan_2_to_3!=0 ? ($total_column[$search['year_2'].$search['month_2']]!=0 ? $bulan_2_to_3/$total_column[$search['year_2'].$search['month_2']]*100 : 100) : 0);
        echo "<td align='right' ><b>".number_format($growth_2_to_3,2)." %</b></td>";
        echo "</tr>";
      }
    ?>
  </tbody>
  <tfoot>
    <?php
      echo "<tr>";
      echo "<td align='center'><b>TOTAL ALL</b></td>";
      foreach($periodes as $p){
        foreach($grand_total as $key=>$t){
          if($p==$key){
            echo "<td align='right'><b>".$t."</b></td>";
          }
        }
      }
      $bulan_1_to_2=$grand_total[$search['year_2'].$search['month_2']]-$grand_total[$search['year_1'].$search['month_1']];
      echo "<td align='right'><b>".$bulan_1_to_2."</b></td>";
      $growth_1_to_2=($bulan_1_to_2!=0 ? ($grand_total[$search['year_1'].$search['month_1']]!=0 ? $bulan_1_to_2/$grand_total[$search['year_1'].$search['month_1']]*100 : 100) : 0);
      echo "<td align='right'><b>".number_format($growth_1_to_2,2)." %</b></td>";
      $bulan_2_to_3=$grand_total[$search['year_3'].$search['month_3']]-$grand_total[$search['year_2'].$search['month_2']];
      echo "<td align='right'><b>".$bulan_2_to_3."</b></td>";
      $growth_2_to_3=($bulan_2_to_3!=0 ? ($grand_total[$search['year_2'].$search['month_2']]!=0 ? $bulan_2_to_3/$grand_total[$search['year_2'].$search['month_2']]*100 : 100) : 0);
      echo "<td align='right'><b>".number_format($growth_2_to_3,2)." %</b></td>";
      echo "</tr>";
    ?>
  </tfoot>
</table>