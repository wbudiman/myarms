<form method="post" action="<?= base_url($path."index") ?>">
  <div class="row">
    <div class="col-md-12">
      <div id="table-search">
        <div class="wrap">
          <table class="table table-search table-striped">
            <thead>
              <tr>
                <th colspan="8" v-align="middle">Parameter Pencarian Data</th>
              </tr>
            </thead>
            <tbody>
              <tr>                   
                <td>Divisi</td>
                <td>
                  <select name="search[division][]" id="division" multiple class="form-control">
                    <?php 
                      foreach($division_lists as $d){
                        echo "<option value='".$d->DIVISION_CODE."' ".($search['division']!="" && in_array($d->DIVISION_CODE,$search['division']) ? "selected" : "").">".$d->DIVISION_CODE."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Summary Product</td>
                <td>
                  <select name="search[summary_product][]" multiple class="form-control" id="summary_product">
                    <?php 
                      foreach($summary_product_lists as $d){
                        echo "<option value='".$d->SUMMARY_PRODUCT."' ".($search['summary_product']!="" && in_array($d->SUMMARY_PRODUCT,$search['summary_product']) ? "selected" : "").">".$d->SUMMARY_PRODUCT."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Jenis</td>
                <td>
                  <select name="search[component_type][]" id="component_type" multiple class="form-control">
                    <?php 
                      foreach($component_type_lists as $r){
                        echo "<option value='".$r->COMPONENT_TYPE."' ".($search['component_type']!="" && in_array($r->COMPONENT_TYPE,$search['component_type']) ? "selected" : "").">".$r->COMPONENT_TYPE_DESCRIPTION."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Periode 1</td>
                <td>
                  <div class="col-md-9">
                    <div class="col-sm-7">
                      <select name="search[month_1]" class="form-control" id="month_1">
                        <?php 
                          foreach($month_lists as $key=>$d){
                            echo "<option value='".$key."' ".($search['month_1']==$key ? "selected" : "").">".$d."</option>";
                          }
                        ?>
                      </select>
                    </div>
                    <div class="col-sm-5">
                      <select name="search[year_1]" class="form-control" id="year_1">
                        <?php 
                          for($x=date("Y");$x>(date("Y")-10);$x--){
                            echo "<option value='".$x."' ".($search['year_1']==$x ? "selected" : "").">".$x."</option>";
                          }
                        ?>
                      </select>
                    </div>
                  </div>
                </td>
              </tr>
              <tr>                   
                <td>Witel</td>
                <td>
                  <select name="search[witel][]" id="witel" multiple class="form-control">
                    <?php 
                      foreach($witel_lists as $w){
                        echo "<option value='".$w->WITEL_CODE."' ".($search['witel']!="" && in_array($w->WITEL_CODE,$search['witel']) ? "selected" : "").">".$w->WITEL_DESCRIPTION."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Produk</td>
                <td>
                  <select name="search[product][]" multiple class="form-control" id="product">
                    <?php 
                      foreach($product_lists as $r){
                        echo "<option value='".$r->PRODUCT_ID."' ".($search['product']!="" && in_array($r->PRODUCT_ID,$search['product']) ? "selected" : "").">".$r->PRODUCT_NAME."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Customer Category</td>
                <td>
                  <select name="search[customer_category][]" id="customer_category" multiple class="form-control">
                    <?php 
                      foreach($customer_category_lists as $r){
                        echo "<option value='".$r->CUSTOMER_CATEGORY_ID."' ".($search['customer_category']!="" && in_array($r->CUSTOMER_CATEGORY_ID,$search['customer_category']) ? "selected" : "").">".$r->CUSTOMER_CATEGORY_DESCRIPTION."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Periode 2</td>
                <td>
                  <div class="col-sm-9">
                    <div class="col-sm-7">
                      <select name="search[month_2]" class="form-control" id="month_2">
                        <?php 
                          foreach($month_lists as $key=>$d){
                            echo "<option value='".$key."' ".($search['month_2']==$key ? "selected" : "").">".$d."</option>";
                          }
                        ?>
                      </select>
                    </div>
                    <div class="col-sm-5">
                      <select name="search[year_2]" class="form-control" id="year_2">
                        <?php 
                          for($x=date("Y");$x>(date("Y")-10);$x--){
                            echo "<option value='".$x."' ".($search['year_2']==$x ? "selected" : "").">".$x."</option>";
                          }
                        ?>
                      </select>
                    </div>
                  </div>
                </td>
              </tr>
              <tr>                   
                <td>Datel</td>
                <td>
                  <select name="search[datel][]" id="datel" multiple class="form-control">
                    <?php 
                      foreach($datel_lists as $w){
                        echo "<option value='".$w->DATEL_CODE."' ".($search['datel']!="" && in_array($w->DATEL_CODE,$search['datel']) ? "selected" : "").">".$w->DATEL_DESCRIPTION."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Tagihan</td>
                <td>
                  <select name="search[billing_type][]" multiple class="form-control" id="billing_type">
                    <?php 
                      foreach($billing_type_lists as $r){
                        echo "<option value='".$r->BILLING_TYPE_ID."' ".($search['billing_type']!="" && in_array($r->BILLING_TYPE_ID,$search['billing_type']) ? "selected" : "").">".$r->BILLING_TYPE_DESCRIPTION."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td colspan="2"></td>
                <td>Periode 3</td>
                <td>
                  <div class="col-sm-9">
                    <div class="col-sm-7">
                      <select name="search[month_3]" class="form-control" id="month_3">
                        <?php 
                          foreach($month_lists as $key=>$d){
                            echo "<option value='".$key."' ".($search['month_3']==$key ? "selected" : "").">".$d."</option>";
                          }
                        ?>
                      </select>
                    </div>
                    <div class="col-sm-5">
                      <select name="search[year_3]" class="form-control" id="year_3">
                        <?php 
                          for($x=date("Y");$x>(date("Y")-10);$x--){
                            echo "<option value='".$x."' ".($search['year_3']==$x ? "selected" : "").">".$x."</option>";
                          }
                        ?>
                      </select>
                    </div>
                  </div>
                </td>
              </tr>
              <tr class="background-filter">                   
                <td>Header</td>
                <td>
                  <select name="search[header]" class="form-control" id="header">
                    <?php 
                      foreach($row_lists as $key=>$d){
                        echo "<option value='".$key."' ".($search['header']==$key ? "selected" : "").">".$d."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Detail</td>
                <td>
                  <select name="search[detail]" class="form-control" id="detail">
                    <?php 
                      foreach($row_lists as $key=>$d){
                        echo "<option value='".$key."' ".($search['detail']==$key ? "selected" : "").">".$d."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td colspan="4"></td>
              </tr>
            </tbody>
            <tfoot>
              <tr>
                <td>Search</td>
                <td><input type="submit" name="search[button]" class="btn btn-search" value="Search"></td>
                <td>To Excel</td>
                <td><input type="submit" name="search[button]" class="btn btn-search" value="Download"></td>
                <td colspan="4"></td>
              </tr>
            </tfoot>
          </table>
        </div>
        <a href="javascript:void(0)" class="showhide"></a>
      </div>
    </div>
  </div> 
</form>
<div class="row">
  <div class="col-md-12">
    <h4 class="title-menu">Komite Billing</h4>
    <div class="billing-detail">
      <table class="table table-bordered table-search">
        <thead>
          <?php 
            $grand_total=array();
            echo "<tr>";
            echo "<th class='text-center'>".$row_lists[$search['header']]."</th>";
            $colspan=8;
            $periodes=array($search['year_1'].$search['month_1'],$search['year_2'].$search['month_2'],$search['year_3'].$search['month_3']);
            foreach($periodes as $l){
              $grand_total[$l]=0;
              echo "<th class='text-center'>".$l."</th>";
            }
            echo "<th class='text-center'>Bulan 1 ke 2</th>";
            echo "<th class='text-center'>Growth 1 ke 2</th>";
            echo "<th class='text-center'>Bulan 2 ke 3</th>";
            echo "<th class='text-center'>Growth 2 ke 3</th>";
            echo "</tr>";
          ?>
        </thead>
        <tbody>
          <?php
            $headers=array();
            foreach($lists as $l){
              if(is_array($field[$search['header']]))
              {
                foreach($field[$search['header']] as $key=>$h)
                {
                  if(!in_array($h->BILLING_COMPONENT_FIELD,$headers)){
                    $headers[$h->BILLING_COMPONENT_FIELD]=$h->BILLING_COMPONENT_DESCRIPTION;
                  }
                }
              }else{
                if(!in_array($l->{$field[$search['header']]},$headers)){
                  array_push($headers,$l->{$field[$search['header']]});
                }
              }
            }
            foreach($headers as $key=>$h){
              echo "<tr>";
              echo "<td colspan='".$colspan."' style='background:lightblue'><b>".($h!="" && $h!=NULL ? $h : $no_mapping)."</b></td>";
              echo "</tr>";
              $details=array();
              $values=array();
              $total_column=array();
              $total_column['total']=0;
              $total_per_row=0;
              if(is_array($field[$search['detail']]))
              {
                foreach($field[$search['detail']] as $key2=>$d){
                  echo "<tr>";
                  echo "<td>".($d->BILLING_COMPONENT_DESCRIPTION!="" && $d->BILLING_COMPONENT_DESCRIPTION!=NULL ? $d->BILLING_COMPONENT_DESCRIPTION : $no_mapping)."</td>";
                  $total_per_row++;
                  $values=array();
                  foreach($periodes as $p){
                    $check=0;
                    foreach($lists as $l){
                      if(is_array($field[$search['header']]))
                      {
                        foreach($field[$search['header']] as $key3=>$h2){
                          if($l->PERIODE==$p && $h==$h2->BILLING_COMPONENT_DESCRIPTION && $d->BILLING_COMPONENT_FIELD==$h2->BILLING_COMPONENT_FIELD){
                            $value=$l->{$h2->BILLING_COMPONENT_FIELD};
                            echo "<td class='text-right'>".number_format($value,0,".",",")."</td>";
                            if(!isset($total_column[$p]))$total_column[$p]=0;
                            $total_column[$p]+=$value;
                            array_push($values,$value);
                            $check=1;
                          }
                        }
                      }else{
                        if($l->PERIODE==$p && $h==$l->{$field[$search['header']]}){
                          if(!isset($search['billing_component']))
                          {
                            $value=$l->{$d->BILLING_COMPONENT_FIELD};
                          
                          }else{
                            $value=$l->{$search['billing_component']};
                          }
                          echo "<td class='text-right'>".number_format($value,0,".",",")."</td>";
                          if(!isset($total_column[$p]))$total_column[$p]=0;
                          $total_column[$p]+=$value;
                          array_push($values,$value);
                          $check=1;
                        }
                      }
                    }
                    if($check==0){
                      echo "<td class='text-right'>0</td>";
                      array_push($values,0);
                      if(!isset($total_column[$p]))$total_column[$p]=0;
                      $total_column[$p]+=0;
                    }
                  }
                  $bulan_1_to_2=$values[1]-$values[0];
                  $growth_1_to_2=($bulan_1_to_2!=0 ? ($values[0]!=0 ? $bulan_1_to_2/$values[0]*100 : 100) : 0);
                  $bulan_2_to_3=$values[2]-$values[1];
                  $growth_2_to_3=($bulan_2_to_3!=0 ? ($values[1]!=0 ? $bulan_2_to_3/$values[1]*100 : 100) : 0);
                  echo "<td class='text-right'>".number_format($bulan_1_to_2,0,".",",")."</td>";
                  echo "<td class='text-right'>".number_format($growth_1_to_2,2)." %</td>";
                  echo "<td class='text-right'>".number_format($bulan_2_to_3,0,".",",")."</td>";
                  echo "<td class='text-right'>".number_format($growth_2_to_3,2)." %</td>";
                  if(!isset($total_column['bulan_1_to_2']))$total_column['bulan_1_to_2']=0;
                  if(!isset($total_column['growth_1_to_2']))$total_column['growth_1_to_2']=0;
                  if(!isset($total_column['bulan_2_to_3']))$total_column['bulan_2_to_3']=0;
                  if(!isset($total_column['growth_2_to_3']))$total_column['growth_2_to_3']=0;
                  $total_column['bulan_1_to_2']+=$bulan_1_to_2;
                  $total_column['growth_1_to_2']+=$growth_1_to_2;
                  $total_column['bulan_2_to_3']+=$bulan_2_to_3;
                  $total_column['growth_2_to_3']+=$growth_2_to_3;
                  echo "</tr>";
                }
              }else{
                foreach($lists as $l){
                  if(!in_array($l->{$field[$search['detail']]},$details) && $h==$l->{$field[$search['header']]} ){
                    array_push($details,$l->{$field[$search['detail']]});
                    echo "<tr>";
                    echo "<td>".($l->{$field[$search['detail']]}!="" && $l->{$field[$search['detail']]}!=NULL ? $l->{$field[$search['detail']]} : $no_mapping)."</td>";
                    $total_per_row++;
                    $values=array();
                    foreach($periodes as $p){
                      $counter2=0;
                      foreach($lists as $l2){
                        if($l->{$field[$search['detail']]}==$l2->{$field[$search['detail']]} && $h==$l2->{$field[$search['header']]} && $l2->PERIODE==$p)
                        {
                          if(!isset($search['billing_component']))
                          {
                            $value=$l2->AMOUNT;
                          }else{
                            $value=$l2->{$search['billing_component']};
                          }
                          if(!isset($total_column[$p]))$total_column[$p]=0;
                          $total_column[$p]+=$value;
                          array_push($values,$value);
                          echo "<td class='text-right'>".number_format($value,0,".",",")."</td>";
                          $counter2++;
                        }
                      }
                      if($counter2==0){
                        array_push($values,0);
                        if(!isset($total_column[$p]))$total_column[$p]=0;
                        $total_column[$p]+=0;
                        echo "<td class='text-right'>0</td>";
                      }
                    }
                    $bulan_1_to_2=$values[1]-$values[0];
                    $growth_1_to_2=($bulan_1_to_2!=0 ? ($values[0]!=0 ? $bulan_1_to_2/$values[0]*100 : 100) : 0);
                    $bulan_2_to_3=$values[2]-$values[1];
                    $growth_2_to_3=($bulan_2_to_3!=0 ? ($values[1]!=0 ? $bulan_2_to_3/$values[1]*100 : 100) : 0);
                    echo "<td class='text-right'>".number_format($bulan_1_to_2,0,".",",")."</td>";
                    echo "<td class='text-right'>".number_format($growth_1_to_2,2)." %</td>";
                    echo "<td class='text-right'>".number_format($bulan_2_to_3,0,".",",")."</td>";
                    echo "<td class='text-right'>".number_format($growth_2_to_3,2)." %</td>";
                    if(!isset($total_column['bulan_1_to_2']))$total_column['bulan_1_to_2']=0;
                    if(!isset($total_column['growth_1_to_2']))$total_column['growth_1_to_2']=0;
                    if(!isset($total_column['bulan_2_to_3']))$total_column['bulan_2_to_3']=0;
                    if(!isset($total_column['growth_2_to_3']))$total_column['growth_2_to_3']=0;
                    $total_column['bulan_1_to_2']+=$bulan_1_to_2;
                    $total_column['growth_1_to_2']+=$growth_1_to_2;
                    $total_column['bulan_2_to_3']+=$bulan_2_to_3;
                    $total_column['growth_2_to_3']+=$growth_2_to_3;
                    echo "</tr>";
                  }
                }
              }
              echo "<tr class='background-filter'>";
              echo "<td style='padding:8px;' class='text-center'><b>Total ".($h!="" && $h!=NULL ? $h : $no_mapping)." (".$total_per_row." item)</b></td>";
              foreach($periodes as $p){
                foreach($total_column as $key=>$t){
                  if($p==$key){
                    if(!isset($grand_total[$p]))$grand_total[$p]=0;
                    $grand_total[$p]+=$t;
                    echo "<td class='text-right' style='padding:8px;'><b>".number_format($t,0,".",",")."</b></td>";
                  }
                }
              }
              $bulan_1_to_2=$total_column[$search['year_2'].$search['month_2']]-$total_column[$search['year_1'].$search['month_1']];
              echo "<td class='text-right' style='padding:8px;'><b>".number_format($bulan_1_to_2,0,".",",")."</b></td>";
              $growth_1_to_2=($bulan_1_to_2!=0 ? ($total_column[$search['year_1'].$search['month_1']]!=0 ? $bulan_1_to_2/$total_column[$search['year_1'].$search['month_1']]*100 : 100) : 0);
              echo "<td class='text-right' style='padding:8px;'><b>".number_format($growth_1_to_2,2)." %</b></td>";
              $bulan_2_to_3=$total_column[$search['year_3'].$search['month_3']]-$total_column[$search['year_2'].$search['month_2']];
              echo "<td class='text-right' style='padding:8px;'><b>".number_format($bulan_2_to_3,0,".",",")."</b></td>";
              $growth_2_to_3=($bulan_2_to_3!=0 ? ($total_column[$search['year_2'].$search['month_2']]!=0 ? $bulan_2_to_3/$total_column[$search['year_2'].$search['month_2']]*100 : 100) : 0);
              echo "<td class='text-right' style='padding:8px;'><b>".number_format($growth_2_to_3,2)." %</b></td>";
              echo "</tr>";
              
            }
          ?>
        </tbody>
        <tfoot>
          <?php
            echo "<tr >";
            echo "<td style='padding:8px;' class='text-center'><b>TOTAL ALL</b></td>";
            foreach($periodes as $p){
              foreach($grand_total as $key=>$t){
                if($p==$key){
                  echo "<td class='text-right' style='padding:8px;'><b>".number_format($t,0,".",",")."</b></td>";
                }
              }
            }
            $bulan_1_to_2=$grand_total[$search['year_2'].$search['month_2']]-$grand_total[$search['year_1'].$search['month_1']];
            echo "<td class='text-right' style='padding:8px;'><b>".number_format($bulan_1_to_2,0,".",",")."</b></td>";
            $growth_1_to_2=($bulan_1_to_2!=0 ? ($grand_total[$search['year_1'].$search['month_1']]!=0 ? $bulan_1_to_2/$grand_total[$search['year_1'].$search['month_1']]*100 : 100) : 0);
            echo "<td class='text-right' style='padding:8px;'><b>".number_format($growth_1_to_2,2)." %</b></td>";
            $bulan_2_to_3=$grand_total[$search['year_3'].$search['month_3']]-$grand_total[$search['year_2'].$search['month_2']];
            echo "<td class='text-right' style='padding:8px;'><b>".number_format($bulan_2_to_3,0,".",",")."</b></td>";
            $growth_2_to_3=($bulan_2_to_3!=0 ? ($grand_total[$search['year_2'].$search['month_2']]!=0 ? $bulan_2_to_3/$grand_total[$search['year_2'].$search['month_2']]*100 : 100) : 0);
            echo "<td class='text-right' style='padding:8px;'><b>".number_format($growth_2_to_3,2)." %</b></td>";
            echo "</tr>";
          ?>
        </tfoot>
      </table>
    </div>
  </div>
</div>
<script type="text/javascript">
  height="290px";
  $(document).ready(function(){
    $("#division").multiselect({
      includeSelectAllOption: true,
      numberDisplayed:2
    }).change(function(){
      var divisions=$(this).val();
      $.ajax({
        url:"<?= base_url("ajax/getWitel") ?>",
        dataType:"JSON",
        data:{divisions:divisions},
        success:function(response){
          $("#witel").multiselect("destroy");
          $("#witel").html(response.content);
          $("#witel").multiselect({
            includeSelectAllOption: true,
            maxHeight: 212,
            enableFiltering: true
          }).change();
        }
      });
    });
    $("#summary_product").multiselect({
      includeSelectAllOption: true,
      numberDisplayed:2
    }).change(function(){
      var summary_products=$(this).val();
      $.ajax({
        url:"<?= base_url("ajax/getProducts") ?>",
        dataType:"JSON",
        data:{summary_products:summary_products},
        success:function(response){
          $("#product").multiselect("destroy");
          $("#product").html(response.content);
          $("#product").multiselect({
            includeSelectAllOption: true,
            maxHeight: 212
          });
        }
      });
    });
    $("#witel,#datel,#product,#customer_category,#billing_type,#component_type").multiselect({numberDisplayed:2,maxHeight: 212,includeSelectAllOption: true,enableFiltering: true});
    $("#header,#detail").change(function(){
      if($("#header").val()=="billing_component" || $("#detail").val()=="billing_component"){
        $("#billing_component").attr("disabled","disabled");
      }else{
        $("#billing_component").removeAttr("disabled");
      }
    });
    $("form").submit(function(){
      var periode1=$("#year_1").val()+$("#month_1").val();
      var periode2=$("#year_2").val()+$("#month_2").val();
      var periode3=$("#year_3").val()+$("#month_3").val();
      if(periode3<=periode2 || periode2<=periode1){
        main.notification("bottomRight","warning","Peringatan","Silahkan pilih periode 1,2,3 dengan benar!");
        return false;
      }
    });
  });
  $("#witel").live("change",function(){
    var divisions=$("#division").val();
    var witels=$(this).val();
    $.ajax({
      url:"<?= base_url("ajax/getDatel") ?>",
      dataType:"JSON",
      data:{witels:witels,divisions:divisions},
      success:function(response){
        $("#datel").multiselect("destroy");
        $("#datel").html(response.content);
        $("#datel").multiselect({
          includeSelectAllOption: true,
          maxHeight: 212,
          enableFiltering: true,
          numberDisplayed:2
        }).change();
      }
    });
  });
</script>