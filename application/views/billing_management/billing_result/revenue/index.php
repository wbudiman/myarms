<form method="post" action="<?= base_url($path."index") ?>">
  <div class="row">
    <div class="col-md-12">
      <div id="table-search">
        <div class="wrap">
          <table  class="table table-search table-striped">
            <thead>
              <tr>
                <th colspan="6" v-align="middle">Parameter Pencarian Data</th>
              </tr>
            </thead>
            <tbody>
              <tr>                   
                <td>Divisi</td>
                <td>
                  <select name="search[division][]" id="division" multiple class="form-control">
                    <?php 
                      foreach($division_lists as $d){
                        echo "<option value='".$d->DIVISION_CODE."' ".($search['division']!="" && in_array($d->DIVISION_CODE,$search['division']) ? "selected" : "").">".$d->DIVISION_CODE."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Summary Product</td>
                <td>
                  <select name="search[summary_product][]" multiple class="form-control" id="summary_product">
                    <?php 
                      foreach($summary_product_lists as $d){
                        echo "<option value='".$d->SUMMARY_PRODUCT."' ".($search['summary_product']!="" && in_array($d->SUMMARY_PRODUCT,$search['summary_product']) ? "selected" : "").">".$d->SUMMARY_PRODUCT."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Customer Category</td>
                <td>
                  <select name="search[customer_category][]" id="segment" multiple class="form-control">
                    <?php 
                      foreach($customer_category_lists as $r){
                        echo "<option value='".$r->CUSTOMER_CATEGORY_ID."' ".($search['customer_category']!="" && in_array($r->CUSTOMER_CATEGORY_ID,$search['customer_category']) ? "selected" : "").">".$r->CUSTOMER_CATEGORY_DESCRIPTION."</option>";
                      }
                    ?>
                  </select>
                </td>
              </tr>
              <tr>                   
                <td>Witel</td>
                <td>
                  <select name="search[witel][]" id="witel" multiple class="form-control">
                    <?php 
                      foreach($witel_lists as $w){
                        echo "<option value='".$w->WITEL_CODE."' ".($search['witel']!="" && in_array($w->WITEL_CODE,$search['witel']) ? "selected" : "").">".$w->WITEL_DESCRIPTION."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Produk</td>
                <td>
                  <select name="search[product][]" multiple class="form-control" id="product">
                    <?php 
                      foreach($product_lists as $r){
                        echo "<option value='".$r->PRODUCT_NAME."' ".($search['product']!="" && in_array($r->PRODUCT_NAME,$search['product']) ? "selected" : "").">".$r->PRODUCT_NAME."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Jenis</td>
                <td>
                  <select name="search[component_type][]" id="component_type" multiple class="form-control">
                    <?php 
                      foreach($component_type_lists as $r){
                        echo "<option value='".$r->COMPONENT_TYPE."' ".($search['component_type']!="" && in_array($r->COMPONENT_TYPE,$search['component_type']) ? "selected" : "").">".$r->COMPONENT_TYPE_DESCRIPTION."</option>";
                      }
                    ?>
                  </select>
                </td>
              </tr>
              <tr>                   
                <td>Datel</td>
                <td>
                  <select name="search[datel][]" id="datel" multiple class="form-control">
                    <?php 
                      foreach($datel_lists as $w){
                        echo "<option value='".$w->DATEL_CODE."' ".($search['datel']!="" && in_array($w->DATEL_CODE,$search['datel']) ? "selected" : "").">".$w->DATEL_DESCRIPTION."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Tagihan</td>
                <td>
                  <select name="search[billing_type][]" multiple class="form-control" id="billing_type">
                    <?php 
                      foreach($billing_type_lists as $r){
                        echo "<option value='".$r->BILLING_TYPE_ID."' ".($search['billing_type']!="" && in_array($r->BILLING_TYPE_ID,$search['billing_type']) ? "selected" : "").">".$r->BILLING_TYPE_DESCRIPTION."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Billing Komponen</td>
                <td>
                  <select name="search[billing_component]" class="form-control">
                    <option value="">All</option>
                    <?php 
                      foreach($billing_component_lists as $r){
                        echo "<option value='".$r->KOMPONEN_BILL."' ".($search['billing_component']==$r->KOMPONEN_BILL ? "selected" : "").">".$r->KOMPONEN_BILL."</option>";
                      }
                    ?>
                  </select>
                </td>
              </tr>
              <tr>                   
                <td colspan="4"></td>
                <td>Periode</td>
                <td>
                  <div class="col-md-9" style="padding-left:0px;">
                    <div class="col-md-7" style="padding-left:0px;">
                      <select name="search[month]" class="form-control">
                        <?php 
                          foreach($month_lists as $key=>$d){
                            echo "<option value='".$key."' ".($search['month']==$key ? "selected" : "").">".$d."</option>";
                          }
                        ?>
                      </select>
                    </div>
                    <div class="col-md-5" style="padding-left:0px;">
                      <select name="search[year]" class="form-control">
                        <?php 
                          for($x=date("Y");$x>(date("Y")-10);$x--){
                            echo "<option value='".$x."' ".($search['year']==$x ? "selected" : "").">".$x."</option>";
                          }
                        ?>
                      </select>
                    </div>
                  </div>
                </td>
              </tr>
              <tr class="background-filter">                   
                <td>Baris</td>
                <td>
                  <select name="search[row]" class="form-control" id="row">
                    <?php 
                      foreach($row_lists as $key=>$d){
                        echo "<option value='".$key."' ".($search['row']==$key ? "selected" : "").">".$d."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Kolom</td>
                <td>
                  <select name="search[column]" class="form-control" id="column">
                    <?php 
                      foreach($row_lists as $key=>$d){
                        if($key!="b.URAIAN"){
                          echo "<option value='".$key."' ".($search['column']==$key ? "selected" : "").">".$d."</option>";
                        }
                      }
                    ?>
                  </select>
                </td>
                <td colspan="2"></td>
              </tr>
            </tbody>
            <tfoot>
              <tr>
                <td>Search</td>
                <td><input type="submit" name="search[button]" class="btn btn-search" value="Search"></td>
                <td>To Excel</td>
                <td><input type="submit" name="search[button]" class="btn btn-search" value="Download"></td>
                <td colspan="2"></td>
              </tr>
            </tfoot>
          </table>
        </div>
        <a href="javascript:void(0)" class="showhide"></a>
      </div>
    </div>
  </div> 
</form>
<div class="row">
  <div class="col-md-12">
    <h4 class="title-menu">Revenue</h4>
    <div class="billing-detail">
      <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered" id="table_revenue">
        <thead>
        <?php 
          echo "<tr>";
          echo "<th class='text-center'>".$row_lists[$search['row']]."</th>";
          $columns=array();
          foreach($lists as $l){
            if(is_array($field[$search['column']]))
            {
              foreach($field[$search['column']] as $c){
                if(!in_array($c,$columns)){
                  array_push($columns,$c);
                  echo "<th class='text-center'>".($c!="" && $c!=NULL ? $c : $no_mapping)."</th>";
                }
              }
            }else{
              if(!in_array($l->{$field[$search['column']]},$columns)){
                array_push($columns,$l->{$field[$search['column']]});
                echo "<th class='text-center'>".($l->{$field[$search['column']]}!="" && $l->{$field[$search['column']]}!=NULL ? $l->{$field[$search['column']]} : $no_mapping)."</th>";
              }
            }
          }
          if($search['column']!="billing_component"){
            echo "<th class='text-center'>Total</th>";
          }
          echo "</tr>";
        ?>
        </thead>
        <tbody>
          <?php 
            $rows=array();
            $total_column=array();
            $total_column['total']=0;
            foreach($lists as $l){
              if(is_array($field[$search['row']]))
              {
                foreach($field[$search['row']] as $key=>$r){
                  if(!in_array($r,$rows)){
                    array_push($rows,$r);
                    echo "<tr>";
                    echo "<td>".($r!="" && $r!=NULL ? $r : $no_mapping)."</td>";
                    $total=0;
                    $counter=0;
                    foreach($columns as $c){
                      $check=0;
                      foreach($lists as $l2){
                        
                        if(is_array($field[$search['column']]))
                        {
                          
                            foreach($field[$search['column']] as $key2=>$f)
                            {
                              if($l->{$key}==$l2->{$key2}){
                                if($f==$c){
                                  $value=$l2->AMOUNT;
                                  $total+=$value;
                                  if(!isset($total_column[$c]))$total_column[$c]=0;
                                  $total_column[$c]+=$value;
                                  $counter++;
                                  echo "<td class='text-right'>".number_format($value,0,".",",")."</td>";
                                  $check=1;
                                }
                              }
                            }
                        }else{
                          if($c==$l2->{$field[$search['column']]}){
                            $value=$l2->AMOUNT;
                            $total+=$value;
                            if(!isset($total_column[$c]))$total_column[$c]=0;
                            $total_column[$c]+=$value;
                            $counter++;
                            echo "<td class='text-right'>".number_format($value,0,".",",")."</td>";
                            $check=1;
                          }
                        }
                        
                      }
                      if($check==0){
                        if(!isset($total_column[$c]))$total_column[$c]=0;
                        $total_column[$c]+=0;
                        echo "<td class='text-right'>0</td>";
                      }
                    }
                    if($search['column']!="billing_component"){
                      if(!isset($total_column['total']))$total_column['total']=0;
                      $total_column['total']+=$total;
                      echo "<td class='text-right'>".number_format($total,0,".",",")."</td>";
                    }
                    echo "</tr>";
                  }
                }
              }else{
                if(!in_array($l->{$field[$search['row']]},$rows)){
                  if(is_array($field[$search['column']]))
                  {
                    echo "<tr>";
                    echo "<td>".($l->{$field[$search['row']]}!="" && $l->{$field[$search['row']]}!=NULL ? $l->{$field[$search['row']]} : $no_mapping)."</td>";
                    array_push($rows,$l->{$field[$search['row']]});
                    $total=0;
                    $counter=0;
                    foreach($field[$search['column']] as $key=>$c){
                      foreach($lists as $l2){
                        if($l->{$field[$search['row']]}==$l2->{$field[$search['row']]}){
                          $value=$l2->AMOUNT;
                          $total+=$value;
                          if(!isset($total_column[$c]))$total_column[$c]=0;
                          $total_column[$c]+=$value;
                          $counter++;
                          echo "<td class='text-right'>".number_format($value,0,".",",")."</td>";
                          $check=1;
                        }
                      }
                    }
                    if($search['column']!="billing_component"){
                      if(!isset($total_column['total']))$total_column['total']=0;
                      $total_column['total']+=$total;
                      echo "<td class='text-right'>".number_format($total,0,".",",")."</td>";
                    }
                    echo "</tr>";
                  }else{
                    array_push($rows,$l->{$field[$search['row']]});
                    echo "<tr>";
                    echo "<td>".($l->{$field[$search['row']]}!="" && $l->{$field[$search['row']]}!=NULL ? $l->{$field[$search['row']]} : $no_mapping)."</td>";
                    $total=0;
                    $counter=0;
                    foreach($columns as $c){
                      $check=0;
                      foreach($lists as $l2){
                        if($l->{$field[$search['row']]}==$l2->{$field[$search['row']]})
                        {
                          if($c==$l2->{$field[$search['column']]}){
                            $value=$l2->AMOUNT;
                            $total+=$value;
                            if(!isset($total_column[$c]))$total_column[$c]=0;
                            $total_column[$c]+=$value;
                            $counter++;
                            echo "<td class='text-right'>".number_format($value,0,".",",")."</td>";
                            $check=1;
                          }
                        }
                      }
                      if($check==0){
                        echo "<td class='text-right'>0</td>";
                        if(!isset($total_column[$c]))$total_column[$c]=0;
                        $total_column[$c]+=0;
                      }
                    }
                    if($search['column']!="billing_component"){
                      if(!isset($total_column['total']))$total_column['total']=0;
                      $total_column['total']+=$total;
                      echo "<td class='text-right'>".number_format($total,0,".",",")."</td>";
                    }
                    echo "</tr>";
                  }
                }
              }
            }
          ?>
        </tbody>
        <tfoot>
        <?php
          if($search['row']!="billing_component"){
            echo "<tr>";
            echo "<td style='padding:8px;'>Total (".sizeof($rows)." item)</td>";
            foreach($columns as $c){
              foreach($total_column as $key=>$t){
                if($c==$key){
                  echo "<td class='text-right' style='padding:8px;'>".number_format($t,0,".",",")."</td>";
                }
              }
            }
            if($search['column']!="billing_component"){
              echo "<td class='text-right' style='padding:8px;'>".number_format($total_column['total'],0,".",",")."</td>";
            }
            echo "</tr>";
          }
        ?>
        </tfoot>
      </table>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    $("#table_revenue").dataTable({
      "bSort" : true,
      "bFilter" : false,
      "bPaginate": false,
      "info": false,
    });
    $("#division").multiselect({
      includeSelectAllOption: true,
      numberDisplayed:2
    }).change(function(){
      var divisions=$(this).val();
      $.ajax({
        url:"<?= base_url("ajax/getWitel") ?>",
        dataType:"JSON",
        data:{divisions:divisions},
        success:function(response){
          $("#witel").multiselect("destroy");
          $("#witel").html(response.content);
          $("#witel").multiselect({
            includeSelectAllOption: true,
            maxHeight: 212,
            enableFiltering: true
          }).change();
        }
      });
    });
    $("#summary_product").multiselect({
      includeSelectAllOption: true,
      numberDisplayed:2
    }).change(function(){
      var summary_products=$(this).val();
      $.ajax({
        url:"<?= base_url("ajax/getProducts") ?>",
        dataType:"JSON",
        data:{summary_products:summary_products},
        success:function(response){
          $("#product").multiselect("destroy");
          $("#product").html(response.content);
          $("#product").multiselect({
            includeSelectAllOption: true,
            maxHeight: 212
          });
        }
      });
    });
    $("#witel,#datel,#product,#segment,#billing_type,#component_type").multiselect({numberDisplayed:2,maxHeight: 212,includeSelectAllOption: true,enableFiltering: true});
    $("#row,#column").change(function(){
      if($("#row").val()=="billing_component" || $("#column").val()=="billing_component"){
        $("#billing_component").attr("disabled","disabled");
      }else{
        $("#billing_component").removeAttr("disabled");
      }
    });
  });
  $("#witel").live("change",function(){
    var divisions=$("#division").val();
    var witels=$(this).val();
    $.ajax({
      url:"<?= base_url("ajax/getDatel") ?>",
      dataType:"JSON",
      data:{witels:witels,divisions:divisions},
      success:function(response){
        $("#datel").multiselect("destroy");
        $("#datel").html(response.content);
        $("#datel").multiselect({
          includeSelectAllOption: true,
          maxHeight: 212,
          enableFiltering: true,
          numberDisplayed:2
        }).change();
      }
    });
  });
</script>