<form method="post" action="<?= base_url($path."index") ?>">
  <div class="row">
    <div class="col-md-12">
      <div id="table-search">
        <div class="wrap">
          <table class="table table-search table-striped">
            <thead>
              <tr>
                <th colspan="8" v-align="middle">Parameter Pencarian Data</th>
              </tr>
            </thead>
            <tbody>
              <tr>                   
                <td>Ubis</td>
                <td>
                  <select name="search[ubis][]" multiple class="form-control" id="ubis">
                    <?php 
                      foreach($ubis_lists as $d){
                        echo "<option value='".$d->UBIS_ID."' ".($search['ubis']!="" && in_array($d->UBIS_ID,$search['ubis']) ? "selected" : "").">".$d->UBIS."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Customer Category</td>
                <td>
                  <select name="search[ubis_segment][]" id="ubis_segment" multiple class="form-control">
                    <?php 
                      foreach($ubis_segment_lists as $r){
                        echo "<option value='".$r->UBIS_SEGMENT_ID."' ".($search['ubis_segment']!="" && in_array($r->UBIS_SEGMENT_ID,$search['ubis_segment']) ? "selected" : "").">".$r->UBIS_SEGMENT."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Periode Aktif</td>
                <td>
                  <div class="col-md-9" style="padding-left:0px;">
                    <div class="col-md-7" style="padding-left:0px;">
                      <select name="search[month_1]" class="form-control">
                        <option value=""></option>
                        <?php 
                          foreach($month_lists as $key=>$d){
                            echo "<option value='".$key."' ".($search['month_1']==$key ? "selected" : "").">".$d."</option>";
                          }
                        ?>
                      </select>
                    </div>
                    <div class="col-md-5" style="padding-left:0px;">
                      <select name="search[year_1]" class="form-control">
                        <option value=""></option>
                        <?php 
                          for($x=date("Y");$x>(date("Y")-10);$x--){
                            echo "<option value='".$x."' ".($search['year_1']==$x ? "selected" : "").">".$x."</option>";
                          }
                        ?>
                      </select>
                    </div>
                  </div>
                </td>
              </tr>
              <tr>
                <td>Kata Kunci</td>
                <td>
          <input type="text" name="search[keyword]" value="<?php if(isset($search['keyword'])) echo $search['keyword'];?>">
                </td>
                <td>Product</td>
                <td>
          <select name="search[product][]" multiple class="form-control" id="product">
                    <?php 
                      foreach($product_lists as $r){
                        echo "<option value='".$r->PRODUCT_ID."' ".($search['product']!="" && in_array($r->PRODUCT_ID,$search['product']) ? "selected" : "").">".$r->PRODUCT_NAME."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td></td>
                <td>
					
                </td>
              </tr>
			  
            </tbody>
            <tfoot>
              <tr>                   
                <td>Search</td>
                <td>
                  <input type="submit" name="search[button]" class="btn btn-search" value="Search">
                </td>
                <td>To Excel</td>
                <td>
                  <input type="submit" name="search[button]" class="btn btn-search" value="Download">
                </td>
                <td colspan="2"></td>
              </tr>
			  
            </tfoot>
          </table>
        </div>
        <a href="javascript:void(0)" class="showhide"></a>
      </div>
    </div>
  </div> 
</form>
<div class="row">
  <div class="col-md-12">
    <h4 class="title-menu">Gimmick Aktif Per Periode</h4>
    <div class="billing-detail">
      <div class="clearfix"></div>
      <table class="table table-bordered" id="table_extrim_usage">
        <thead>
          <?php
            echo "<tr>";
            echo "<th>Gimmick</th>";
            echo "<th>Start Promo</th>";
            echo "<th>End Promo</th>";
            echo "<th>Deskripsi</th>";
            echo "</tr>";
          ?>
        </thead>
        <tbody>
          <?php
            $total=array("ssl"=>0,"record"=>0,"duration"=>0);
			if($lists){
				foreach($lists as $l){
				  echo "<tr>";
				  echo "<td><a href='javascript:void(0);' data-remote-modal='".base_url($path."detail/00?".$param_get)."&id_pack=".$l->ID_PACK."&judul=".$l->DESCRIPTION."' target='_blank'>".$l->DESCRIPTION."</a></td>";
				  echo "<td>".$l->START_DATE."</td>";
				  echo "<td>".$l->START_END."</td>";
				  echo "<td>".$l->DETAILLED_SUM."</td>";
				  echo "</tr>";
				}
			}
          ?>
        </tbody>
        <tfoot>
          <?php
            // echo "<tr>";
            // echo "<td class='text-center' colspan='2'><b>Total</b></td>";
            // echo "<td class='text-right'>".number_format($total['ssl'],0,".",",")."</td>";
            // echo "<td class='text-right'>".number_format($total['record'],0,".",",")."</td>";
            // echo "<td class='text-right'>".number_format($total['duration'],0,".",",")."</td>";
            // echo "<td></td>";
            // echo "</tr>";
          ?>
        </tfoot>
      </table>
    </div>
  </div>
</div>
<script type="text/javascript">
  height=335;
  $(document).ready(function(){
    $("#table_extrim_usage").dataTable({
      "bSort" : true,
      "bFilter" : false,
      "bPaginate": false,
      "info": false,
    });
    $("#division").multiselect({
      includeSelectAllOption: true,
      numberDisplayed:2
    }).change(function(){
      var divisions=$(this).val();
      $.ajax({
        url:"<?= base_url("ajax/getWitel") ?>",
        dataType:"JSON",
        data:{divisions:divisions},
        success:function(response){
          $("#witel").multiselect("destroy");
          $("#witel").html(response.content);
          $("#witel").multiselect({
            includeSelectAllOption: true,
            maxHeight: 212,
            enableFiltering: true
          }).change();
        }
      });
    });
    $("#summary_product").multiselect({
      includeSelectAllOption: true,
      numberDisplayed:2
    }).change(function(){
      var summary_products=$(this).val();
      $.ajax({
        url:"<?= base_url("ajax/getProducts") ?>",
        dataType:"JSON",
        data:{summary_products:summary_products},
        success:function(response){
          $("#product").multiselect("destroy");
          $("#product").html(response.content);
          $("#product").multiselect({
            includeSelectAllOption: true,
            maxHeight: 212
          });
        }
      });
    });
    $("#ubis").multiselect({
      dropRight: true,
      includeSelectAllOption: true,
      numberDisplayed:2
    }).change(function(){
      var ubis=$(this).val();
      $.ajax({
        url:"<?= base_url("ajax/getUbisSegments") ?>",
        dataType:"JSON",
        data:{ubis:ubis},
        success:function(response){
          $("#ubis_segment").multiselect("destroy");
          $("#ubis_segment").html(response.content);
          $("#ubis_segment").multiselect({
            dropRight: true,
            includeSelectAllOption: true,
            maxHeight: 212,
            enableFiltering: true
          });
        }
      });
    });
    $("#ubis_segment").multiselect({
      dropRight: true,
      includeSelectAllOption: true,
      numberDisplayed:2
    }).change(function(){
      var ubis_segment=$(this).val();
      $.ajax({
        url:"<?= base_url("ajax/getBisnisArea") ?>",
        dataType:"JSON",
        data:{ubis_segment:ubis_segment},
        success:function(response){
          $("#bisnis_area").multiselect("destroy");
          $("#bisnis_area").html(response.content);
          $("#bisnis_area").multiselect({
            dropRight: true,
            includeSelectAllOption: true,
            maxHeight: 212,
            enableFiltering: true
          });
        }
      });
    });
    $("#witel,#datel,#product,#bisnis_area").multiselect({numberDisplayed:2,maxHeight: 212,includeSelectAllOption: true,enableFiltering: true});
  });
  $("#witel").live("change",function(){
    var divisions=$("#division").val();
    var witels=$(this).val();
    $.ajax({
      url:"<?= base_url("ajax/getDatel") ?>",
      dataType:"JSON",
      data:{witels:witels,divisions:divisions},
      success:function(response){
        $("#datel").multiselect("destroy");
        $("#datel").html(response.content);
        $("#datel").multiselect({
          includeSelectAllOption: true,
          maxHeight: 212,
          enableFiltering: true,
          numberDisplayed:2
        }).change();
      }
    });
  });
</script>