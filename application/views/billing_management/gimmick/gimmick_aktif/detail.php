<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title">Detail Gimmick Aktif Per Periode</h4>
</div>
<div class="row">
  <div class="col-md-12">
    <h4 class="title-menu"><center>Gimmick : <?=$search['judul']?></center></h4>
    <div class="billing-detail">
      <div class="clearfix"></div>
      <?php if(sizeof($lists)>0): ?>
      <br>
      <form method="post" action="<?= base_url($path."detail/".$search['param']."?".$param_get) ?>">
        <div class="pull-right">
          <input type="submit" name="search[button]" class="btn btn-search" value="Download">
        </div>
      </form>
      <?php endif; ?>
      <br>
      <table class="table table-bordered" id="table_l251_detail">
        <thead>
          <?php 
            echo "<tr>";
            echo "<th class='text-center'>Paket Saluran</th>";
            echo "<th class='text-center'>Deskripsi Tarif</th>";
            echo "<th class='text-center'>Mulai Berlaku</th>";
            echo "<th class='text-center'>Masa Periode</th>";
            echo "<th class='text-center'>Masa Berakhir</th>";
            echo "<th class='text-center'>Diskon</th>";
            echo "<th class='text-center'>Unit</th>";
            echo "</tr>";
          ?>
        </thead>
        <tbody>
          <?php
            if(sizeof($lists)>0){
              $total=array("record"=>0,"duration"=>0);
              foreach($lists as $l){
                echo "<tr>";
                echo "<td>".$l->DESC_PACK_LINE."</td>";
                echo "<td>".$l->DESCRIPTION."</td>";
                echo "<td>".$l->PROMO_START."</td>";
                echo "<td>".$l->PROMOTIONAL_PERIOD."</td>";
                echo "<td>".$l->END_PROMOTIONAL_DATE."</td>";
                echo "<td>".$l->PROMO_DISCOUNT."</td>";
                echo "<td>".$l->PROMO_UNIT."</td>";
                echo "</tr>";
              }
            } 
          ?>
        </tbody>
        <tfoot>
        </tfoot>
      </table>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    $("#table_l251_detail").dataTable({
      "bSort" : true,
      "bFilter" : false,
      "bPaginate": false,
      "info": false,
    });
  });
</script>