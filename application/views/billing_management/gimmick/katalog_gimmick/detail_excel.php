<h5><?= $header_title ?></h5>
<table border="1">
<thead>
  <?php 
    $total_column=array();
    $total_column['total']=0;
    echo "<tr>";
    echo "<td align='center'>Jam</th>";
    $periodes=array($search['year_1'].$search['month_1'],$search['year_2'].$search['month_2'],$search['year_3'].$search['month_3']);
    foreach($periodes as $l){
      $total_column[$l]=0;
      echo "<td align='center'>".$l."</th>";
    }
    echo "<td align='center'>Bulan 1 ke 2</th>";
    echo "<td align='center'>Growth 1 ke 2</th>";
    echo "<td align='center'>Bulan 2 ke 3</th>";
    echo "<td align='center'>Growth 2 ke 3</th>";
    echo "</tr>";
  ?>
</thead>
<tbody>
  <?php
    if(sizeof($lists)>0){
      for($x=0;$x<=23;$x++){
        $time=($x<10 ? "0".$x : $x);
        echo "<tr>";
        echo "<td>".$time."</td>";
        $values=array();
        foreach($periodes as $p){
          $check=0;
          foreach($lists as $l){
            if($l->PERIODE==$p && $l->BY_TIME==$time){
              if($search['view_by']=="TOTAL_RECORD"){
                $value=$l->TOTAL_RECORD;
              }elseif($search['view_by']=="TOTAL_DURATION"){
                $value=$l->TOTAL_DURATION;
              }else{
                $value=$l->TOTAL;
              }
              echo "<td align='right'>".$value."</td>";
              if(!isset($total_column[$p]))$total_column[$p]=0;
              $total_column[$p]+=$value;
              array_push($values,$value);
              $check++;
            }
          }
          if($check==0){
            $value=0;
            if(!isset($total_column[$p]))$total_column[$p]=0;
            $total_column[$p]+=$value;
            array_push($values,$value);
            echo "<td align='right'>".$value."</td>";
          }
        }
        $bulan_1_to_2=$values[1]-$values[0];
        $growth_1_to_2=($bulan_1_to_2!=0 ? ($values[0]!=0 ? $bulan_1_to_2/$values[0]*100 : 100) : 0);
        $bulan_2_to_3=$values[2]-$values[1];
        $growth_2_to_3=($bulan_2_to_3!=0 ? ($values[1]!=0 ? $bulan_2_to_3/$values[1]*100 : 100) : 0);
        echo "<td align='right'>".$bulan_1_to_2."</td>";
        echo "<td align='right'>".number_format($growth_1_to_2,2)." %</td>";
        echo "<td align='right'>".$bulan_2_to_3."</td>";
        echo "<td align='right'>".number_format($growth_2_to_3,2)." %</td>";
        echo "</tr>";
        if(!isset($total_column['bulan_1_to_2']))$total_column['bulan_1_to_2']=0;
        if(!isset($total_column['growth_1_to_2']))$total_column['growth_1_to_2']=0;
        if(!isset($total_column['bulan_2_to_3']))$total_column['bulan_2_to_3']=0;
        if(!isset($total_column['growth_2_to_3']))$total_column['growth_2_to_3']=0;
        $total_column['bulan_1_to_2']+=$bulan_1_to_2;
        $total_column['growth_1_to_2']+=$growth_1_to_2;
        $total_column['bulan_2_to_3']+=$bulan_2_to_3;
        $total_column['growth_2_to_3']+=$growth_2_to_3;
      }
    }
  ?>
</tbody>
<tfoot>
  <?php
    echo "<tr>";
    echo "<td >Total (".(sizeof($lists)>0 ? 24 : 0)." item)</td>";
    foreach($periodes as $p){
      foreach($total_column as $key=>$t){
        if($p==$key){
          echo "<td align='right' >".$t."</td>";
        }
      }
    }
    $bulan_1_to_2=$total_column[$search['year_2'].$search['month_2']]-$total_column[$search['year_1'].$search['month_1']];
    echo "<td align='right' >".$bulan_1_to_2."</td>";
    $growth_1_to_2=($bulan_1_to_2!=0 ? ($total_column[$search['year_1'].$search['month_1']]!=0 ? $bulan_1_to_2/$total_column[$search['year_1'].$search['month_1']]*100 : 100) : 0);
    echo "<td align='right' >".number_format($growth_1_to_2,2)." %</td>";
    $bulan_2_to_3=$total_column[$search['year_3'].$search['month_3']]-$total_column[$search['year_2'].$search['month_2']];
    echo "<td align='right' >".$bulan_2_to_3."</td>";
    $growth_2_to_3=($bulan_2_to_3!=0 ? ($total_column[$search['year_2'].$search['month_2']]!=0 ? $bulan_2_to_3/$total_column[$search['year_2'].$search['month_2']]*100 : 100) : 0);
    echo "<td align='right' >".number_format($growth_2_to_3,2)." %</td>";
    echo "</tr>";
  ?>
</tfoot>
</table>