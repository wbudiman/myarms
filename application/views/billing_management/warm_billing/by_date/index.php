<form method="post" action="<?= base_url($path."index") ?>">
  <div class="row">
    <div class="col-md-12">
      <div id="table-search">
        <div class="wrap">
          <table class="table table-search table-striped">
            <thead>
              <tr>
                <th colspan="8" v-align="middle">Parameter Pencarian Data</th>
              </tr>
            </thead>
            <tbody>
              <tr>                   
                <td>Divisi</td>
                <td>
                  <select name="search[division][]" id="division" multiple class="form-control">
                    <?php 
                      foreach($division_lists as $d){
                        echo "<option value='".$d->DIVISION_CODE."' ".($search['division']!="" && in_array($d->DIVISION_CODE,$search['division']) ? "selected" : "").">".$d->DIVISION_CODE."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Periode 1</td>
                <td>
                  <div class="col-sm-6 padding-left-0">
                    <div class="col-sm-5 padding-left-0">
                      <select name="search[month_1]" class="form-control" id="month_1">
                        <?php 
                          foreach($month_lists as $key=>$d){
                            echo "<option value='".$key."' ".($search['month_1']==$key ? "selected" : "").">".$d."</option>";
                          }
                        ?>
                      </select>
                    </div>
                    <div class="col-sm-5">
                      <select name="search[year_1]" class="form-control" id="year_1">
                        <?php 
                          for($x=date("Y");$x>(date("Y")-10);$x--){
                            echo "<option value='".$x."' ".($search['year_1']==$x ? "selected" : "").">".$x."</option>";
                          }
                        ?>
                      </select>
                    </div>
                  </div>
                </td>
              </tr>
              <tr>                   
                <td>STO</td>
                <td>
                  <select name="search[sto][]" multiple class="form-control" id="sto">
                    <?php 
                      foreach($sto_lists as $r){
                        echo "<option value='".$r->STO."' ".($search['sto']!="" && in_array($r->STO,$search['sto']) ? "selected" : "").">".$r->STO."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Periode 2</td>
                <td>
                  <div class="col-sm-6 padding-left-0">
                    <div class="col-sm-5 padding-left-0">
                      <select name="search[month_2]" class="form-control" id="month_2">
                        <?php 
                          foreach($month_lists as $key=>$d){
                            echo "<option value='".$key."' ".($search['month_2']==$key ? "selected" : "").">".$d."</option>";
                          }
                        ?>
                      </select>
                    </div>
                    <div class="col-sm-5">
                      <select name="search[year_2]" class="form-control" id="year_2">
                        <?php 
                          for($x=date("Y");$x>(date("Y")-10);$x--){
                            echo "<option value='".$x."' ".($search['year_2']==$x ? "selected" : "").">".$x."</option>";
                          }
                        ?>
                      </select>
                    </div>
                  </div>
                </td>
              </tr>
              <tr>                   
                <td>Komponen Billing</td>
                <td>
                  <select name="search[billing_component][]" multiple class="form-control" id="billing_component">
                    <?php 
                      foreach($billing_component_lists as $d){
                        echo "<option value='".$d->KOMPONEN_BILLING."' ".($search['billing_component']!="" && in_array($d->KOMPONEN_BILLING,$search['billing_component']) ? "selected" : "").">".$d->KOMPONEN_BILLING."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Periode 3</td>
                <td>
                  <div class="col-sm-6 padding-left-0">
                    <div class="col-sm-5 padding-left-0">
                      <select name="search[month_3]" class="form-control" id="month_3">
                        <?php 
                          foreach($month_lists as $key=>$d){
                            echo "<option value='".$key."' ".($search['month_3']==$key ? "selected" : "").">".$d."</option>";
                          }
                        ?>
                      </select>
                    </div>
                    <div class="col-sm-5">
                      <select name="search[year_3]" class="form-control" id="year_3">
                        <?php 
                          for($x=date("Y");$x>(date("Y")-10);$x--){
                            echo "<option value='".$x."' ".($search['year_3']==$x ? "selected" : "").">".$x."</option>";
                          }
                        ?>
                      </select>
                    </div>
                  </div>
                </td>
              </tr>
              <tr class="background-filter">
                <td>View By</td>
                <td>
                  <select name="search[view_by]" class="form-control" id="view_by">
                    <?php 
                      foreach($view_by_lists as $d){
                        echo "<option value='".$d->VIEW_BY."' ".($search['view_by']==$d->VIEW_BY ? "selected" : "").">".$d->VIEW_BY_DESCRIPTION."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td colspan="2"></td>
              </tr>
            </tbody>
            <tfoot>
              <tr>                   
                <td>Search</td>
                <td>
                  <input type="submit" name="search[button]" class="btn btn-search" value="Search">
                </td>
                <td>To Excel</td>
                <td>
                  <input type="submit" name="search[button]" class="btn btn-search" value="Download">
                </td>
              </tr>
            </tfoot>
          </table>
        </div>
        <a href="javascript:void(0)" class="showhide"></a>
      </div>
    </div>
  </div> 
</form>
<div class="row">
  <div class="col-md-12">
    <h4 class="title-menu">By Tanggal</h4>
    <div class="billing-detail">
      <div id="container_by_date"></div>
      <div class="clearfix"></div>
      <br>
      <table class="table table-bordered" id="table_by_date">
        <thead>
          <?php 
            $total_column=array();
            $total_column['total']=0;
            echo "<tr>";
            echo "<th class='text-center'>Tanggal</th>";
            $periodes=array($search['year_1'].$search['month_1'],$search['year_2'].$search['month_2'],$search['year_3'].$search['month_3']);
            foreach($periodes as $l){
              $total_column[$l]=0;
              echo "<th class='text-center'>".$l."</th>";
            }
            echo "<th class='text-center'>Bulan 1 ke 2</th>";
            echo "<th class='text-center'>Growth 1 ke 2</th>";
            echo "<th class='text-center'>Bulan 2 ke 3</th>";
            echo "<th class='text-center'>Growth 2 ke 3</th>";
            echo "<th class='text-center'>Detail</th>";
            echo "</tr>";
          ?>
        </thead>
        <tbody>
          <?php
            $chart_data=array($search['year_1'].$search['month_1']=>"",$search['year_2'].$search['month_2']=>"",$search['year_3'].$search['month_3']=>"");
            if(sizeof($recycle_lists)>0){
              echo "<tr>";
              echo "<td>00(Recycle)</td>";
              $values=array();
              foreach($periodes as $p){
                $check=0;
                foreach($recycle_lists as $l){
                  if($l->PERIODE==$p){
                    if($search['view_by']=="TOTAL_RECORD"){
                      $value=$l->TOTAL_RECORD;
                    }elseif($search['view_by']=="TOTAL_DURATION"){
                      $value=$l->TOTAL_DURATION;
                    }else{
                      $value=$l->TOTAL;
                    }
                    echo "<td class='text-right'>".number_format($value,0,".",",")."</td>";
                    if(!isset($total_column[$p]))$total_column[$p]=0;
                    $total_column[$p]+=$value;
                    array_push($values,$value);
                    $chart_data[$p].=$value.",";
                    $check++;
                  }
                }
                if($check==0){
                  $value=0;
                  if(!isset($total_column[$p]))$total_column[$p]=0;
                  $total_column[$p]+=$value;
                  array_push($values,$value);
                  $chart_data[$p].=$value.",";
                  echo "<td class='text-right'>".number_format($value,0,".",",")."</td>";
                }
              }
              $bulan_1_to_2=$values[1]-$values[0];
              $growth_1_to_2=($bulan_1_to_2!=0 ? ($values[0]!=0 ? $bulan_1_to_2/$values[0]*100 : 100) : 0);
              $bulan_2_to_3=$values[2]-$values[1];
              $growth_2_to_3=($bulan_2_to_3!=0 ? ($values[1]!=0 ? $bulan_2_to_3/$values[1]*100 : 100) : 0);
              echo "<td class='text-right'>".number_format($bulan_1_to_2,0,".",",")."</td>";
              echo "<td class='text-right'>".number_format($growth_1_to_2,2)." %</td>";
              echo "<td class='text-right'>".number_format($bulan_2_to_3,0,".",",")."</td>";
              echo "<td class='text-right'>".number_format($growth_2_to_3,2)." %</td>";
              echo "<td class='text-center'><a href='javascript:void(0);' data-remote-modal='".base_url($path."detail/00?".$param_get)."' target='_blank'>Detail</a></td>";
              echo "</tr>";
              if(!isset($total_column['bulan_1_to_2']))$total_column['bulan_1_to_2']=0;
              if(!isset($total_column['growth_1_to_2']))$total_column['growth_1_to_2']=0;
              if(!isset($total_column['bulan_2_to_3']))$total_column['bulan_2_to_3']=0;
              if(!isset($total_column['growth_2_to_3']))$total_column['growth_2_to_3']=0;
              $total_column['bulan_1_to_2']+=$bulan_1_to_2;
              $total_column['growth_1_to_2']+=$growth_1_to_2;
              $total_column['bulan_2_to_3']+=$bulan_2_to_3;
              $total_column['growth_2_to_3']+=$growth_2_to_3;
            }
            if(sizeof($lists)>0){
              for($x=1;$x<=31;$x++){
                $date=($x<10 ? "0".$x : $x);
                echo "<tr>";
                echo "<td>".$date."</td>";
                $values=array();
                foreach($periodes as $p){
                  $check=0;
                  foreach($lists as $l){
                    if($l->PERIODE==$p && substr($l->BY_DATE,6,2)==$date){
                      if($search['view_by']=="TOTAL_RECORD"){
                        $value=$l->TOTAL_RECORD;
                      }elseif($search['view_by']=="TOTAL_DURATION"){
                        $value=$l->TOTAL_DURATION;
                      }else{
                        $value=$l->TOTAL;
                      }
                      echo "<td class='text-right'>".number_format($value,0,".",",")."</td>";
                      if(!isset($total_column[$p]))$total_column[$p]=0;
                      $total_column[$p]+=$value;
                      array_push($values,$value);
                      $chart_data[$p].=$value.",";
                      $check++;
                    }
                  }
                  if($check==0){
                    $value=0;
                    if(!isset($total_column[$p]))$total_column[$p]=0;
                    $total_column[$p]+=$value;
                    array_push($values,$value);
                    $chart_data[$p].=$value.",";
                    echo "<td class='text-right'>".number_format($value,0,".",",")."</td>";
                  }
                }
                $bulan_1_to_2=$values[1]-$values[0];
                $growth_1_to_2=($bulan_1_to_2!=0 ? ($values[0]!=0 ? $bulan_1_to_2/$values[0]*100 : 100) : 0);
                $bulan_2_to_3=$values[2]-$values[1];
                $growth_2_to_3=($bulan_2_to_3!=0 ? ($values[1]!=0 ? $bulan_2_to_3/$values[1]*100 : 100) : 0);
                echo "<td class='text-right'>".number_format($bulan_1_to_2,0,".",",")."</td>";
                echo "<td class='text-right'>".number_format($growth_1_to_2,2)." %</td>";
                echo "<td class='text-right'>".number_format($bulan_2_to_3,0,".",",")."</td>";
                echo "<td class='text-right'>".number_format($growth_2_to_3,2)." %</td>";
                echo "<td class='text-center'>";
                if($values[0]!=0 || $values[1]!=0 || $values[2]!=0)echo "<a href='javascript:void(0);'  data-remote-modal='".base_url($path."detail/".$date."?".$param_get)."' target='_blank'>Detail</a>";
                echo "</td>";
                echo "</tr>";
                if(!isset($total_column['bulan_1_to_2']))$total_column['bulan_1_to_2']=0;
                if(!isset($total_column['growth_1_to_2']))$total_column['growth_1_to_2']=0;
                if(!isset($total_column['bulan_2_to_3']))$total_column['bulan_2_to_3']=0;
                if(!isset($total_column['growth_2_to_3']))$total_column['growth_2_to_3']=0;
                $total_column['bulan_1_to_2']+=$bulan_1_to_2;
                $total_column['growth_1_to_2']+=$growth_1_to_2;
                $total_column['bulan_2_to_3']+=$bulan_2_to_3;
                $total_column['growth_2_to_3']+=$growth_2_to_3;
              }
            }
          ?>
        </tbody>
        <tfoot>
          <?php
            echo "<tr>";
            echo "<td style='padding:8px;'>Total (".((sizeof($lists)>0 ? 31 : 0)+sizeof($recycle_lists))." item)</td>";
            foreach($periodes as $p){
              foreach($total_column as $key=>$t){
                if($p==$key){
                  echo "<td class='text-right' style='padding:8px;'>".number_format($t,0,".",",")."</td>";
                }
              }
            }
            $bulan_1_to_2=$total_column[$search['year_2'].$search['month_2']]-$total_column[$search['year_1'].$search['month_1']];
            echo "<td class='text-right' style='padding:8px;'>".number_format($bulan_1_to_2,0,".",",")."</td>";
            $growth_1_to_2=($bulan_1_to_2!=0 ? ($total_column[$search['year_1'].$search['month_1']]!=0 ? $bulan_1_to_2/$total_column[$search['year_1'].$search['month_1']]*100 : 100) : 0);
            echo "<td class='text-right' style='padding:8px;'>".number_format($growth_1_to_2,2)." %</td>";
            $bulan_2_to_3=$total_column[$search['year_3'].$search['month_3']]-$total_column[$search['year_2'].$search['month_2']];
            echo "<td class='text-right' style='padding:8px;'>".number_format($bulan_2_to_3,0,".",",")."</td>";
            $growth_2_to_3=($bulan_2_to_3!=0 ? ($total_column[$search['year_2'].$search['month_2']]!=0 ? $bulan_2_to_3/$total_column[$search['year_2'].$search['month_2']]*100 : 100) : 0);
            echo "<td class='text-right' style='padding:8px;'>".number_format($growth_2_to_3,2)." %</td>";
            echo "<td></td>";
            echo "</tr>";
          ?>
        </tfoot>
      </table>
    </div>
  </div>
</div>
<script type="text/javascript">
  height=285;
  var periode1="<?= date("F Y",strtotime($search['year_1']."-".$search['month_1']."-01")) ?>";
  var periode2="<?= date("F Y",strtotime($search['year_2']."-".$search['month_2']."-01")) ?>";
  var periode3="<?= date("F Y",strtotime($search['year_3']."-".$search['month_3']."-01")) ?>";
  var dates=new Array();
  <?php if(sizeof($recycle_lists)>0): ?>
    dates.push("00");
  <?php endif; ?>
  <?php if(sizeof($lists)>0): ?>
    for(x=1;x<=31;x++){
      dates.push((x<10 ? "0"+x : x));
    }
  <?php endif; ?>
  $(document).ready(function(){
    var chart = new Highcharts.Chart({
      chart: {
        renderTo: 'container_by_date'
      },
      exporting: { 
        buttons: {
          contextButton: {
            menuItems: [{
              text: 'Cetak',
              onclick: function () {
                chart.print()
              }
            }]
          }
        }
      },
      title: {
        text: 'Warm Billing By Tanggal',
        x: -20 
      },
      subtitle: {
        text: "( "+periode1+", "+periode2+", "+periode3+" )",
        x: -20
      },
      xAxis: {
        categories: dates
      },
      yAxis: {
        labels: {
          format: '{value:,.0f}'
        },
        title: {
          text: ''
        },
        plotLines: [{
          value: 0,
          width: 1,
          color: '#808080'
        }]
      },
      tooltip: {
        headerFormat: '<div style="font-size:10px">Tanggal : {point.key}</div>',
        useHTML: true,
        valuePrefix: ''
      },
      legend: {
        layout: 'horizontal',
        align: 'center',
        verticalAlign: 'bottom',
        borderWidth: 0
      },
      series: [{
        name: periode1,
        data: [<?= $chart_data[$search['year_1'].$search['month_1']] ?>]
      }, {
        name: periode2,
        data: [<?= $chart_data[$search['year_2'].$search['month_2']] ?>]
      }, {
        name: periode3,
        data: [<?= $chart_data[$search['year_3'].$search['month_3']] ?>]
      }]
    });
    $("#table_by_date").dataTable({
      "bSort" : true,
      "bFilter" : false,
      "bPaginate": false,
      "info": false,
    });
    $("#division,#billing_component,#sto").multiselect({numberDisplayed:2,maxHeight: 212,includeSelectAllOption: true,enableFiltering: true});
    $("form").submit(function(){
      var periode1=$("#year_1").val()+$("#month_1").val();
      var periode2=$("#year_2").val()+$("#month_2").val();
      var periode3=$("#year_3").val()+$("#month_3").val();
      if(periode3<=periode2 || periode2<=periode1){
        main.notification("bottomRight","warning","Peringatan","Silahkan pilih periode 1,2,3 dengan benar!");
        return false;
      }
    });
  });
</script>