<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title">By Time</h4>
</div>
<div class="modal-body">
  <div class="row">
    <div class="col-md-12">
      <h4 class="title-menu"></h4>
      <div class="billing-detail">
        <div id="container_by_time" style="width:850px;"></div>
        <div class="clearfix"></div>
        <?php if(sizeof($lists)>0): ?>
        <br>
        <form method="post" action="<?= base_url($path."detail/".$search['day']."?".$param_get) ?>">
          <div class="pull-right">
            <input type="submit" name="search[button]" class="btn btn-search" value="Download">
          </div>
        </form>
        <?php endif; ?>
        <br>
        <table class="table table-bordered" id="table_by_time">
          <thead>
            <?php 
              $total_column=array();
              $total_column['total']=0;
              echo "<tr>";
              echo "<th class='text-center'>Jam</th>";
              $periodes=array($search['year_1'].$search['month_1'],$search['year_2'].$search['month_2'],$search['year_3'].$search['month_3']);
              foreach($periodes as $l){
                $total_column[$l]=0;
                echo "<th class='text-center'>".$l."</th>";
              }
              echo "<th class='text-center'>Bulan 1 ke 2</th>";
              echo "<th class='text-center'>Growth 1 ke 2</th>";
              echo "<th class='text-center'>Bulan 2 ke 3</th>";
              echo "<th class='text-center'>Growth 2 ke 3</th>";
              echo "</tr>";
            ?>
          </thead>
          <tbody>
            <?php
              $chart_data=array($search['year_1'].$search['month_1']=>"",$search['year_2'].$search['month_2']=>"",$search['year_3'].$search['month_3']=>"");
              if(sizeof($lists)>0){
                for($x=0;$x<=23;$x++){
                  $time=($x<10 ? "0".$x : $x);
                  echo "<tr>";
                  echo "<td>".$time."</td>";
                  $values=array();
                  foreach($periodes as $p){
                    $check=0;
                    foreach($lists as $l){
                      if($l->PERIODE==$p && $l->BY_TIME==$time){
                        if($search['view_by']=="TOTAL_RECORD"){
                          $value=$l->TOTAL_RECORD;
                        }elseif($search['view_by']=="TOTAL_DURATION"){
                          $value=$l->TOTAL_DURATION;
                        }else{
                          $value=$l->TOTAL;
                        }
                        echo "<td class='text-right'>".number_format($value,0,".",",")."</td>";
                        if(!isset($total_column[$p]))$total_column[$p]=0;
                        $total_column[$p]+=$value;
                        array_push($values,$value);
                        $chart_data[$p].=$value.",";
                        $check++;
                      }
                    }
                    if($check==0){
                      $value=0;
                      if(!isset($total_column[$p]))$total_column[$p]=0;
                      $total_column[$p]+=$value;
                      array_push($values,$value);
                      $chart_data[$p].=$value.",";
                      echo "<td class='text-right'>".number_format($value,0,".",",")."</td>";
                    }
                  }
                  $bulan_1_to_2=$values[1]-$values[0];
                  $growth_1_to_2=($bulan_1_to_2!=0 ? ($values[0]!=0 ? $bulan_1_to_2/$values[0]*100 : 100) : 0);
                  $bulan_2_to_3=$values[2]-$values[1];
                  $growth_2_to_3=($bulan_2_to_3!=0 ? ($values[1]!=0 ? $bulan_2_to_3/$values[1]*100 : 100) : 0);
                  echo "<td class='text-right'>".number_format($bulan_1_to_2,0,".",",")."</td>";
                  echo "<td class='text-right'>".number_format($growth_1_to_2,2)." %</td>";
                  echo "<td class='text-right'>".number_format($bulan_2_to_3,0,".",",")."</td>";
                  echo "<td class='text-right'>".number_format($growth_2_to_3,2)." %</td>";
                  echo "</tr>";
                  if(!isset($total_column['bulan_1_to_2']))$total_column['bulan_1_to_2']=0;
                  if(!isset($total_column['growth_1_to_2']))$total_column['growth_1_to_2']=0;
                  if(!isset($total_column['bulan_2_to_3']))$total_column['bulan_2_to_3']=0;
                  if(!isset($total_column['growth_2_to_3']))$total_column['growth_2_to_3']=0;
                  $total_column['bulan_1_to_2']+=$bulan_1_to_2;
                  $total_column['growth_1_to_2']+=$growth_1_to_2;
                  $total_column['bulan_2_to_3']+=$bulan_2_to_3;
                  $total_column['growth_2_to_3']+=$growth_2_to_3;
                }
              }
            ?>
          </tbody>
          <tfoot>
            <?php
              echo "<tr>";
              echo "<td style='padding:8px;'>Total (".(sizeof($lists)>0 ? 24 : 0)." item)</td>";
              foreach($periodes as $p){
                foreach($total_column as $key=>$t){
                  if($p==$key){
                    echo "<td class='text-right' style='padding:8px;'>".number_format($t,0,".",",")."</td>";
                  }
                }
              }
              $bulan_1_to_2=$total_column[$search['year_2'].$search['month_2']]-$total_column[$search['year_1'].$search['month_1']];
              echo "<td class='text-right' style='padding:8px;'>".number_format($bulan_1_to_2,0,".",",")."</td>";
              $growth_1_to_2=($bulan_1_to_2!=0 ? ($total_column[$search['year_1'].$search['month_1']]!=0 ? $bulan_1_to_2/$total_column[$search['year_1'].$search['month_1']]*100 : 100) : 0);
              echo "<td class='text-right' style='padding:8px;'>".number_format($growth_1_to_2,2)." %</td>";
              $bulan_2_to_3=$total_column[$search['year_3'].$search['month_3']]-$total_column[$search['year_2'].$search['month_2']];
              echo "<td class='text-right' style='padding:8px;'>".number_format($bulan_2_to_3,0,".",",")."</td>";
              $growth_2_to_3=($bulan_2_to_3!=0 ? ($total_column[$search['year_2'].$search['month_2']]!=0 ? $bulan_2_to_3/$total_column[$search['year_2'].$search['month_2']]*100 : 100) : 0);
              echo "<td class='text-right' style='padding:8px;'>".number_format($growth_2_to_3,2)." %</td>";
              echo "</tr>";
            ?>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  var periode1="<?= date("F Y",strtotime($search['year_1']."-".$search['month_1']."-01")) ?>";
  var periode2="<?= date("F Y",strtotime($search['year_2']."-".$search['month_2']."-01")) ?>";
  var periode3="<?= date("F Y",strtotime($search['year_3']."-".$search['month_3']."-01")) ?>";
  var times=new Array();
  <?php if(sizeof($lists)>0): ?>
    for(x=0;x<=23;x++){
      times.push((x<10 ? "0"+x : x));
    }
  <?php endif; ?>
  $(document).ready(function(){
    var chart = new Highcharts.Chart({
      chart: {
        renderTo: 'container_by_time'
      },
      exporting: { 
        buttons: {
          contextButton: {
            menuItems: [{
              text: 'Cetak',
              onclick: function () {
                chart.print()
              }
            }]
          }
        }
      },
      title: {
        text: 'Warm Billing Hari : '+'<?= $search['day'] ?> By Time',
        x: -20 
      },
      subtitle: {
        text: "( "+periode1+", "+periode2+", "+periode3+" )",
        x: -20
      },
      xAxis: {
        categories: times
      },
      yAxis: {
        labels: {
          format: '{value:,.0f}'
        },
        title: {
          text: ''
        },
        plotLines: [{
          value: 0,
          width: 1,
          color: '#808080'
        }]
      },
      tooltip: {
        headerFormat: '<div style="font-size:10px">Jam : {point.key}</div>',
        useHTML: true,
        valuePrefix: ''
      },
      legend: {
        layout: 'horizontal',
        align: 'center',
        verticalAlign: 'bottom',
        borderWidth: 0
      },
      series: [{
        name: periode1,
        data: [<?= $chart_data[$search['year_1'].$search['month_1']] ?>]
      }, {
        name: periode2,
        data: [<?= $chart_data[$search['year_2'].$search['month_2']] ?>]
      }, {
        name: periode3,
        data: [<?= $chart_data[$search['year_3'].$search['month_3']] ?>]
      }]
    });
    $("#table_by_time").dataTable({
      "bSort" : true,
      "bFilter" : false,
      "bPaginate": false,
      "info": false,
    });
  });
</script>