<h5><?= $header_title ?> ( <?= $header_parameter ?> )</h5>
<table border="1">
<thead>
  <?php 
    $total_column=array();
    $total_column['total']=0;
    echo "<tr>";
    echo "<th align='center'>Hari</th>";
    $periodes=array($search['year_1'].$search['month_1'],$search['year_2'].$search['month_2'],$search['year_3'].$search['month_3']);
    foreach($periodes as $key=>$l){
      $total_column[$l]=0;
      echo "<th align='center' width='80'>Tgl ".($key+1)."</th>";
      echo "<th align='center'>".$l."</th>";
    }
    echo "<th align='center'>Bulan 1 ke 2</th>";
    echo "<th align='center'>Growth 1 ke 2</th>";
    echo "<th align='center'>Bulan 2 ke 3</th>";
    echo "<th align='center'>Growth 2 ke 3</th>";
    echo "</tr>";
  ?>
</thead>
<tbody>
  <?php
    $chart_data=array($search['year_1'].$search['month_1']=>"",$search['year_2'].$search['month_2']=>"",$search['year_3'].$search['month_3']=>"");
    $periode_1=date("l",strtotime($search['year_1']."-".$search['month_1']."-01"));
    $periode_2=date("l",strtotime($search['year_2']."-".$search['month_2']."-01"));
    $periode_3=date("l",strtotime($search['year_3']."-".$search['month_3']."-01"));
    $first=$day[$periode_1];
    $more=0;
    if($day[$periode_2]<$day[$periode_1]){
      $first=$day[$periode_2];
      $more=$day[$periode_1]-$day[$periode_2];
    }
    if($day[$periode_3]<$first){
      $first=$day[$periode_3];
      $more=$day[$first]-$day[$periode_3];
    }
    $counter=array($search['year_1'].$search['month_1']=>1,$search['year_2'].$search['month_2']=>1,$search['year_3'].$search['month_3']=>1);
    if(sizeof($recycle_lists)>0){
      echo "<tr>";
      echo "<td>RECYCLE</td>";
      $values=array();
      foreach($periodes as $p){
        $check=0;
        foreach($recycle_lists as $l){
          if($l->PERIODE==$p){
            if($search['view_by']=="TOTAL_RECORD"){
              $value=$l->TOTAL_RECORD;
            }elseif($search['view_by']=="TOTAL_DURATION"){
              $value=$l->TOTAL_DURATION;
            }else{
              $value=$l->TOTAL;
            }
            echo "<td align='right'>00(RCYL)</td>";
            echo "<td align='right'>".number_format($value,0,".",",")."</td>";
            if(!isset($total_column[$p]))$total_column[$p]=0;
            $total_column[$p]+=$value;
            array_push($values,$value);
            $chart_data[$p].=$value.",";
            $check++;
          }
        }
        if($check==0){
          $value=0;
          if(!isset($total_column[$p]))$total_column[$p]=0;
          $total_column[$p]+=$value;
          array_push($values,$value);
          $chart_data[$p].=$value.",";
          echo "<td align='right'>00(RCYL)</td>";
          echo "<td align='right'>".number_format($value,0,".",",")."</td>";
        }
      }
      $bulan_1_to_2=$values[1]-$values[0];
      $growth_1_to_2=($bulan_1_to_2!=0 ? ($values[0]!=0 ? $bulan_1_to_2/$values[0]*100 : 100) : 0);
      $bulan_2_to_3=$values[2]-$values[1];
      $growth_2_to_3=($bulan_2_to_3!=0 ? ($values[1]!=0 ? $bulan_2_to_3/$values[1]*100 : 100) : 0);
      echo "<td align='right'>".number_format($bulan_1_to_2,0,".",",")."</td>";
      echo "<td align='right'>".number_format($growth_1_to_2,2)." %</td>";
      echo "<td align='right'>".number_format($bulan_2_to_3,0,".",",")."</td>";
      echo "<td align='right'>".number_format($growth_2_to_3,2)." %</td>";
      echo "</tr>";
      if(!isset($total_column['bulan_1_to_2']))$total_column['bulan_1_to_2']=0;
      if(!isset($total_column['growth_1_to_2']))$total_column['growth_1_to_2']=0;
      if(!isset($total_column['bulan_2_to_3']))$total_column['bulan_2_to_3']=0;
      if(!isset($total_column['growth_2_to_3']))$total_column['growth_2_to_3']=0;
      $total_column['bulan_1_to_2']+=$bulan_1_to_2;
      $total_column['growth_1_to_2']+=$growth_1_to_2;
      $total_column['bulan_2_to_3']+=$bulan_2_to_3;
      $total_column['growth_2_to_3']+=$growth_2_to_3;
    }
    if(sizeof($lists)>0){
      for($x=1;$x<=(31+$more);$x++){
        $date2=($x<10? "0".$x : $x);
        $z=($x+$first-2)%7;
        $hari=$day2[$z];
        echo "<tr>";
        echo "<td>".$hari."</td>";
        $values=array();
        $per="";
        foreach($data as $key=>$d) {
          if(isset($d[$counter[$key]]) && $d[$counter[$key]]['BY_DAY'] == $hari){
            $per.=$counter[$key].",";
            echo "<td align='center'>".$counter[$key]."</td>";
            if($search['view_by']=="TOTAL_RECORD"){
              $value=$d[$counter[$key]]['TOTAL_RECORD'];
            }elseif($search['view_by']=="TOTAL_DURATION"){
              $value=$d[$counter[$key]]['TOTAL_DURATION'];
            }else{
              $value=$d[$counter[$key]]['TOTAL'];
            }
            echo "<td align='right'>".number_format($value,0,".",",")."</td>";
            if(!isset($total_column[$key]))$total_column[$key]=0;
            $total_column[$key]+=$value;
            array_push($values,$value);
            $chart_data[$key].=$value.",";
            $counter[$key]++;
          }else{
            if(!isset($total_column[$key]))$total_column[$key]=0;
            $total_column[$key]+=0;
            array_push($values,0);
            $chart_data[$key].="0,";
            echo "<td></td>";
            echo "<td></td>";
          }
        }
        $bulan_1_to_2=$values[1]-$values[0];
        $growth_1_to_2=($bulan_1_to_2!=0 ? ($values[0]!=0 ? $bulan_1_to_2/$values[0]*100 : 100) : 0);
        $bulan_2_to_3=$values[2]-$values[1];
        $growth_2_to_3=($bulan_2_to_3!=0 ? ($values[1]!=0 ? $bulan_2_to_3/$values[1]*100 : 100) : 0);
        echo "<td align='right'>".number_format($bulan_1_to_2,0,".",",")."</td>";
        echo "<td align='right'>".number_format($growth_1_to_2,2)." %</td>";
        echo "<td align='right'>".number_format($bulan_2_to_3,0,".",",")."</td>";
        echo "<td align='right'>".number_format($growth_2_to_3,2)." %</td>";
        echo "</tr>";
        if(!isset($total_column['bulan_1_to_2']))$total_column['bulan_1_to_2']=0;
        if(!isset($total_column['growth_1_to_2']))$total_column['growth_1_to_2']=0;
        if(!isset($total_column['bulan_2_to_3']))$total_column['bulan_2_to_3']=0;
        if(!isset($total_column['growth_2_to_3']))$total_column['growth_2_to_3']=0;
        $total_column['bulan_1_to_2']+=$bulan_1_to_2;
        $total_column['growth_1_to_2']+=$growth_1_to_2;
        $total_column['bulan_2_to_3']+=$bulan_2_to_3;
        $total_column['growth_2_to_3']+=$growth_2_to_3;
        echo "</tr>";
      }
    }
  ?>
</tbody>
<tfoot>
  <?php
    echo "<tr>";
    echo "<td >Total (".((sizeof($lists)>0 ? 31+$more : 0)+sizeof($recycle_lists))." item)</td>";
    foreach($periodes as $p){
      foreach($total_column as $key=>$t){
        if($p==$key){
          echo "<td></td>";
          echo "<td align='right' >".number_format($t,0,".",",")."</td>";
        }
      }
    }
    $bulan_1_to_2=$total_column[$search['year_2'].$search['month_2']]-$total_column[$search['year_1'].$search['month_1']];
    echo "<td align='right' >".number_format($bulan_1_to_2,0,".",",")."</td>";
    $growth_1_to_2=($bulan_1_to_2!=0 ? ($total_column[$search['year_1'].$search['month_1']]!=0 ? $bulan_1_to_2/$total_column[$search['year_1'].$search['month_1']]*100 : 100) : 0);
    echo "<td align='right' >".number_format($growth_1_to_2,2)." %</td>";
    $bulan_2_to_3=$total_column[$search['year_3'].$search['month_3']]-$total_column[$search['year_2'].$search['month_2']];
    echo "<td align='right' >".number_format($bulan_2_to_3,0,".",",")."</td>";
    $growth_2_to_3=($bulan_2_to_3!=0 ? ($total_column[$search['year_2'].$search['month_2']]!=0 ? $bulan_2_to_3/$total_column[$search['year_2'].$search['month_2']]*100 : 100) : 0);
    echo "<td align='right' >".number_format($growth_2_to_3,2)." %</td>";
    echo "</tr>";
  ?>
</tfoot>
</table>