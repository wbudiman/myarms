<h5><?= $header_title ?></h5>
<table border="1">
<thead>
  <tr>
    <th class="text-center">CCA</th>
    <th class="text-center">SND</th>
    <th class="text-center">Nama</th>
    <th class="text-center">Bisnis Area</th>
    <th class="text-center">Produk</th>
    <th class="text-center">Segmen</th>
    <th class="text-center">Call</th>
    <th class="text-center">Min Call</th>
    <th class="text-center">Max Call</th>
    <th class="text-center">Durasi</th>
    <th class="text-center">Usage Current</th>
    <th class="text-center">Usage Last</th>
    <th class="text-center">Amount Last</th>
    <th class="text-center">Selisih Usage</th>
  </tr>
</thead>
<tbody>
  <?php
    if(sizeof($lists)>0){
      foreach($lists as $l){
        $current_usage=$l->LOKAL+$l->SLJJ+$l->STB+$l->SLI007+$l->JAPATI+$l->USAGE_LAINNYA;
        $selisih=$l->USAGE_LALU-$current_usage;
        echo "<tr>";
        echo "<td>".$l->CCA."</td>";
        echo "<td>".$l->ND."</td>";
        echo "<td>".$l->NAMA."</td>";
        echo "<td>".$l->BUSINESS_AREA_DESCRIPTION."</td>";
        echo "<td>".$l->PRODUCT_NAME."</td>";
        echo "<td>".$l->UBIS_SEGMENT."</td>";
        echo "<td>".$l->CALL."</td>";
        echo "<td>".$l->MIN_CALL."</td>";
        echo "<td>".$l->MAX_CALL."</td>";
        echo "<td class='text-right'>".number_format($l->DURATION,2,".",",")."</td>";
        echo "<td class='text-right'>".number_format($current_usage,2,".",",")."</td>";
        echo "<td class='text-right'>".number_format($l->USAGE_LALU,2,".",",")."</td>";
        echo "<td class='text-right'>".number_format($l->TAG_LALU,2,".",",")."</td>";
        echo "<td class='text-right'>".number_format($selisih,2,".",",")."</td>";
        echo "</tr>";
      }
    }
  ?>
</tbody>
</table>