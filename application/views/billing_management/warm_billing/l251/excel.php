<h5><?= $header_title ?> ( <?= $header_parameter ?> )</h5>
<table border="1">  
  <thead>
    <?php
      echo "<tr>";
      echo "<th>Divre</th>";
      echo "<th>".$row_lists[$search['show_data']]."</th>";
      echo "<th>SSL</th>";
      echo "<th>Record</th>";
      echo "<th>Duration</th>";
      echo "</tr>";
    ?>
  </thead>
  <tbody>
    <?php
      $total=array("ssl"=>0,"record"=>0,"duration"=>0);
      foreach($lists as $l){
        echo "<tr>";
        echo "<td>".$l->DIVISION_CODE."</td>";
        echo "<td>".$l->{$field[$search['show_data']]}."</td>";
        echo "<td align='right'>".$l->SSL."</td>";
        echo "<td align='right'>".$l->TOTAL_CALL."</td>";
        echo "<td align='right'>".$l->TOTAL_DURATION."</td>";
        echo "</tr>";
        $total['ssl']+=$l->SSL;
        $total['record']+=$l->TOTAL_CALL;
        $total['duration']+=$l->TOTAL_DURATION;
      }
    ?>
  </tbody>
  <tfoot>
    <?php
      echo "<tr>";
      echo "<td></td>";
      echo "<td align='center'><b>Total</b></td>";
      echo "<td align='right'>".$total['ssl']."</td>";
      echo "<td align='right'>".$total['record']."</td>";
      echo "<td align='right'>".$total['duration']."</td>";
      echo "</tr>";
    ?>
  </tfoot>
</table>