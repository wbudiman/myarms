<div class="row">
  <div class="col-md-12">
    <div class="billing-detail">
      <div class="clearfix"></div>
      <?php if(sizeof($lists)>0): ?>
      <br>
      <form method="post" action="<?= base_url($path."detail?".$param_get) ?>">
        <div class="pull-right">
          <input type="submit" name="search[button]" class="btn btn-search" value="Download">
        </div>
      </form>
      <?php endif; ?>
      <br>
      <table class="table table-bordered" id="table_l251_detail">
        <thead>
          <?php 
            $total_column=array();
            $total_column['total']=0;
            echo "<tr>";
            echo "<th class='text-center'>Wilayah</th>";
            echo "<th class='text-center'>Divre</th>";
            echo "<th class='text-center'>STO</th>";
            echo "<th class='text-center'>Notel</th>";
            echo "<th class='text-center'>First Call</th>";
            echo "<th class='text-center'>Last Call</th>";
            echo "<th class='text-center'>Total Call</th>";
            echo "<th class='text-center'>Total Duration</th>";
            echo "<th class='text-center'>Error</th>";
            echo "</tr>";
          ?>
        </thead>
        <tbody>
          <?php
            $total=array("record"=>0,"duration"=>0);
            if(sizeof($lists)>0){
              foreach($lists as $l){
                echo "<tr>";
                echo "<td>".$l->TERITORY."</td>";
                echo "<td>".$l->DIVISION_CODE."</td>";
                echo "<td>".$l->STO_ID."</td>";
                echo "<td>".$l->ND."</td>";
                echo "<td>".$l->FIRST_CALL."</td>";
                echo "<td>".$l->LAST_CALL."</td>";
                echo "<td class='text-right'>".number_format($l->TOTAL_CALL,0,".",",")."</td>";
                echo "<td class='text-right'>".number_format($l->TOTAL_DURATION,0,".",",")."</td>";
                echo "<td>".$l->ERROR_DESCRIPTION."</td>";
                echo "</tr>";
                $total['record']+=$l->TOTAL_CALL;
                $total['duration']+=$l->TOTAL_DURATION;
              }
            }
          ?>
        </tbody>
        <tfoot>
          <?php
            echo "<tr>";
            echo "<td class='text-center' colspan='6'><b>Total</b></td>";
            echo "<td class='text-right'>".number_format($total['record'],0,".",",")."</td>";
            echo "<td class='text-right'>".number_format($total['duration'],0,".",",")."</td>";
            echo "<td></td>";
            echo "</tr>";
          ?>
        </tfoot>
      </table>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    $("#table_l251_detail").dataTable({
      "bSort" : true,
      "bFilter" : false,
      "bPaginate": false,
      "info": false,
    });
  });
</script>