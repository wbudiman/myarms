<form method="post" action="<?= base_url($path."index") ?>">
  <div class="row">
    <div class="col-md-12">
      <div id="table-search">
        <div class="wrap">
          <table class="table table-search table-striped">
            <thead>
              <tr>
                <th colspan="8" v-align="middle">Parameter Pencarian Data</th>
              </tr>
            </thead>
            <tbody>
              <tr>                   
                <td>Divisi</td>
                <td>
                  <select name="search[division][]" id="division" multiple class="form-control">
                    <?php 
                      foreach($division_lists as $d){
                        echo "<option value='".$d->DIVISION_CODE."' ".($search['division']!="" && in_array($d->DIVISION_CODE,$search['division']) ? "selected" : "").">".$d->DIVISION_CODE."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Periode</td>
                <td>
                  <div class="col-sm-6 padding-left-0">
                    <div class="col-sm-5 padding-left-0">
                      <select name="search[month]" class="form-control" id="month">
                        <?php 
                          foreach($month_lists as $key=>$d){
                            echo "<option value='".$key."' ".($search['month']==$key ? "selected" : "").">".$d."</option>";
                          }
                        ?>
                      </select>
                    </div>
                    <div class="col-sm-5">
                      <select name="search[year]" class="form-control" id="year">
                        <?php 
                          for($x=date("Y");$x>(date("Y")-10);$x--){
                            echo "<option value='".$x."' ".($search['year']==$x ? "selected" : "").">".$x."</option>";
                          }
                        ?>
                      </select>
                    </div>
                  </div>
                </td>
              </tr>
              <tr>                   
                <td>STO</td>
                <td>
                  <select name="search[sto][]" multiple class="form-control" id="sto">
                    <?php 
                      foreach($sto_lists as $r){
                        echo "<option value='".$r->STO_ID."' ".($search['sto']!="" && in_array($r->STO_ID,$search['sto']) ? "selected" : "").">".$r->STO_ID."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Error</td>
                <td>
                  <select name="search[error][]" multiple class="form-control" id="error">
                    <?php 
                      foreach($error_lists as $d){
                        echo "<option value='".$d->ERROR_ID."' ".($search['error']!="" && in_array($d->ERROR_ID,$search['error']) ? "selected" : "").">".$d->ERROR_DESCRIPTION."</option>";
                      }
                    ?>
                  </select>
                </td>
              </tr>
              <tr class="background-filter">
                <td>Show Data</td>
                <td>
                  <select name="search[show_data]" class="form-control">
                    <?php 
                      foreach($row_lists as $key=>$d){
                        echo "<option value='".$key."' ".($search['show_data']==$key ? "selected" : "").">".$d."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td colspan="2"></td>
              </tr>
            </tbody>
            <tfoot>
              <tr>                   
                <td>Search</td>
                <td>
                  <input type="submit" name="search[button]" class="btn btn-search" value="Search">
                </td>
                <td>To Excel</td>
                <td>
                  <input type="submit" name="search[button]" class="btn btn-search" value="Download">
                </td>
              </tr>
            </tfoot>
          </table>
        </div>
        <a href="javascript:void(0)" class="showhide"></a>
      </div>
    </div>
  </div> 
</form>
<div class="row">
  <div class="col-md-12">
    <h4 class="title-menu">L251</h4>
    <div class="billing-detail">
      <div class="clearfix"></div>
      <table class="table table-bordered" id="table_l251">
        <thead>
          <?php
            echo "<tr>";
            echo "<th>Divre</th>";
            echo "<th>".$row_lists[$search['show_data']]."</th>";
            echo "<th>SSL</th>";
            echo "<th>Record</th>";
            echo "<th>Duration</th>";
            echo "<th>Detail</th>";
            echo "</tr>";
          ?>
        </thead>
        <tbody>
          <?php
            $total=array("ssl"=>0,"record"=>0,"duration"=>0);
            foreach($lists as $l){
              echo "<tr>";
              echo "<td>".$l->DIVISION_CODE."</td>";
              echo "<td>".$l->{$field[$search['show_data']]}."</td>";
              echo "<td class='text-right'>".number_format($l->SSL,0,".",",")."</td>";
              echo "<td class='text-right'>".number_format($l->TOTAL_CALL,0,".",",")."</td>";
              echo "<td class='text-right'>".number_format($l->TOTAL_DURATION,0,".",",")."</td>";
              echo "<td class='text-center'><a href='".base_url($path."detail?".$param_get."&BY=".$field[$search['show_data']]."&ID=".$l->{$field[$search['show_data']]}."&BY_DIV=".$l->DIVISION_CODE)."' target='_blank'>Detail</a></td>";
              echo "</tr>";
              $total['ssl']+=$l->SSL;
              $total['record']+=$l->TOTAL_CALL;
              $total['duration']+=$l->TOTAL_DURATION;
            }
          ?>
        </tbody>
        <tfoot>
          <?php
            echo "<tr>";
            echo "<td class='text-center' colspan='2'><b>Total</b></td>";
            echo "<td class='text-right'>".number_format($total['ssl'],0,".",",")."</td>";
            echo "<td class='text-right'>".number_format($total['record'],0,".",",")."</td>";
            echo "<td class='text-right'>".number_format($total['duration'],0,".",",")."</td>";
            echo "<td></td>";
            echo "</tr>";
          ?>
        </tfoot>
      </table>
    </div>
  </div>
</div>
<script type="text/javascript">
  height=235;
  $(document).ready(function(){
    $("#table_l251").dataTable({
      "bSort" : true,
      "bFilter" : false,
      "bPaginate": false,
      "info": false,
    });
    $("#division,#error,#sto").multiselect({numberDisplayed:2,maxHeight: 212,includeSelectAllOption: true,enableFiltering: true});
  });
</script>