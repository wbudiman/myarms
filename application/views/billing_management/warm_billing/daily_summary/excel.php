<h5><?= $header_title ?> ( <?= $header_parameter ?> )</h5>
<table border="1">  
  <thead>
    <?php
      $total_column=array();
      $total_column['total']=0;
      $end_of_month=date("t",strtotime($search['year']."-".$search['month']."-01"));
      echo "<tr>";
      echo "<td>Divre</td>";
      echo "<td>".$row_lists[$search['group_by']]."</td>";
      if(sizeof($recycle_lists)>0){
        $total_column["00"]=0;
        echo "<td>Rec</td>";
      }
      for($x=1;$x<=$end_of_month;$x++){
        $total_column[$x]=0;
        $date=($x<10 ? "0".$x : $x);
        echo "<td>".$date."</td>";
      }
      echo "<td>Total</td>";
      echo "</tr>";
    ?>
  </thead>
  <tbody>
    <?php
      foreach($lists as $key=>$l){
        $total=0;
        echo "<tr>";
        echo "<td>".$l['data']->DIVISION_CODE."</td>";
        echo "<td>".$key."</td>";
        if(sizeof($recycle_lists)>0){
          $check=0;
          foreach($recycle_lists as $r){
            if($key==$r->{$field[$search['group_by']]}){
              if($search['view_by']=="TOTAL_RECORD"){
                $value=$r->TOTAL_RECORD;
              }elseif($search['view_by']=="TOTAL_DURATION"){
                $value=$r->TOTAL_DURATION;
              }else{
                $value=$r->TOTAL;
              }
              $check=1;
              if(!isset($total_column["00"]))$total_column["00"]=0;
              $total_column["00"]+=$value;
              $total+=$value;
              echo "<td align='right'>".$value."</td>";
            }
          }
          if($check==0){
            $value=0;
            if(!isset($total_column["00"]))$total_column["00"]=0;
            $total_column["00"]+=$value;
            $total+=$value;
            echo "<td align='right'>".$value."</td>";
          }
        }
        for($x=1;$x<=$end_of_month;$x++){
          $temp=array();
          foreach($l['detail'] as $d){
            $date=($x<10 ? "0".$x : $x);
            if($d->BY_DATE==$search['year'].$search['month'].$date){
              $temp=$d;
              break;
            }
          }
          if(sizeof($temp)>0){
            if($search['view_by']=="TOTAL_RECORD"){
              $value=$temp->TOTAL_RECORD;
            }elseif($search['view_by']=="TOTAL_DURATION"){
              $value=$temp->TOTAL_DURATION;
            }else{
              $value=$temp->TOTAL;
            }
          }else{
            $value=0;
          }
          if(!isset($total_column[$x]))$total_column[$x]=0;
          $total_column[$x]+=$value;
          $total+=$value;
          echo "<td align='right'>".$value."</td>";
        }
        $total_column['total']+=$total;
        echo "<td align='right'>".$total."</td>";
        echo "</tr>";
      }
    ?>
  </tbody>
  <tfoot>
    <?php
      echo "<tr>";
      echo "<td align='center'><b>Total</b></td>";
      echo "<td></td>";
      if(sizeof($recycle_lists)>0){
        echo "<td align='right'>".$total_column["00"]."</td>";
      }
      for($x=1;$x<=$end_of_month;$x++){
        echo "<td align='right'>".$total_column[$x]."</td>";
      }
      echo "<td align='right'>".$total_column['total']."</td>";
      echo "</tr>";
    ?>
  </tfoot>
</table>