<form method="post" action="<?= base_url($path."index") ?>">
  <div class="row">
    <div class="col-md-12">
      <div id="table-search">
        <div class="wrap">
          <table class="table table-search table-striped">
            <thead>
              <tr>
                <th colspan="8" v-align="middle">Parameter Pencarian Data</th>
              </tr>
            </thead>
            <tbody>
              <tr>                   
                <td>Divisi</td>
                <td>
                  <select name="search[division][]" id="division" multiple class="form-control">
                    <?php 
                      foreach($division_lists as $d){
                        echo "<option value='".$d->DIVISION_CODE."' ".($search['division']!="" && in_array($d->DIVISION_CODE,$search['division']) ? "selected" : "").">".$d->DIVISION_CODE."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Periode</td>
                <td>
                  <div class="col-sm-6 padding-left-0">
                    <div class="col-sm-5 padding-left-0">
                      <select name="search[month]" class="form-control" id="month">
                        <?php 
                          foreach($month_lists as $key=>$d){
                            echo "<option value='".$key."' ".($search['month']==$key ? "selected" : "").">".$d."</option>";
                          }
                        ?>
                      </select>
                    </div>
                    <div class="col-sm-5">
                      <select name="search[year]" class="form-control" id="year">
                        <?php 
                          for($x=date("Y");$x>(date("Y")-10);$x--){
                            echo "<option value='".$x."' ".($search['year']==$x ? "selected" : "").">".$x."</option>";
                          }
                        ?>
                      </select>
                    </div>
                  </div>
                </td>
              </tr>
              <tr>                   
                <td>STO</td>
                <td>
                  <select name="search[sto][]" multiple class="form-control" id="sto">
                    <?php 
                      foreach($sto_lists as $r){
                        echo "<option value='".$r->STO."' ".($search['sto']!="" && in_array($r->STO,$search['sto']) ? "selected" : "").">".$r->STO."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Komponen Billing</td>
                <td>
                  <select name="search[billing_component][]" multiple class="form-control" id="billing_component">
                    <?php 
                      foreach($billing_component_lists as $d){
                        echo "<option value='".$d->KOMPONEN_BILLING."' ".($search['billing_component']!="" && in_array($d->KOMPONEN_BILLING,$search['billing_component']) ? "selected" : "").">".$d->KOMPONEN_BILLING."</option>";
                      }
                    ?>
                  </select>
                </td>
              </tr>
              <tr class="background-filter">
                <td>Group By</td>
                <td>
                  <select name="search[group_by]" class="form-control" id="group_by">
                    <?php 
                      foreach($row_lists as $key=>$d){
                        echo "<option value='".$key."' ".($search['group_by']==$key ? "selected" : "").">".$d."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>View By</td>
                <td>
                  <select name="search[view_by]" class="form-control" id="view_by">
                    <?php 
                      foreach($view_by_lists as $d){
                        echo "<option value='".$d->VIEW_BY."' ".($search['view_by']==$d->VIEW_BY ? "selected" : "").">".$d->VIEW_BY_DESCRIPTION."</option>";
                      }
                    ?>
                  </select>
                </td>
              </tr>
            </tbody>
            <tfoot>
              <tr>                   
                <td>Search</td>
                <td>
                  <input type="submit" name="search[button]" class="btn btn-search" value="Search">
                </td>
                <td>To Excel</td>
                <td>
                  <input type="submit" name="search[button]" class="btn btn-search" value="Download">
                </td>
              </tr>
            </tfoot>
          </table>
        </div>
        <a href="javascript:void(0)" class="showhide"></a>
      </div>
    </div>
  </div> 
</form>
<div class="row">
  <div class="col-md-12">
    <h4 class="title-menu">Rekap Harian</h4>
    <div class="billing-detail">
      <div class="clearfix"></div>
      <table class="table table-bordered" id="table_daily_summary">
        <thead>
          <?php
            $total_column=array();
            $total_column['total']=0;
            $end_of_month=date("t",strtotime($search['year']."-".$search['month']."-01"));
            echo "<tr>";
            echo "<td>Divre</td>";
            echo "<td>".$row_lists[$search['group_by']]."</td>";
            if(sizeof($recycle_lists)>0){
              $total_column["00"]=0;
              echo "<td>Rec</td>";
            }
            for($x=1;$x<=$end_of_month;$x++){
              $total_column[$x]=0;
              $date=($x<10 ? "0".$x : $x);
              echo "<td>".$date."</td>";
            }
            echo "<td>Total</td>";
            echo "</tr>";
          ?>
        </thead>
        <tbody>
          <?php
            foreach($lists as $key=>$l){
              $total=0;
              echo "<tr>";
              echo "<td>".$l['data']->DIVISION_CODE."</td>";
              echo "<td>".$key."</td>";
              if(sizeof($recycle_lists)>0){
                $check=0;
                foreach($recycle_lists as $r){
                  if($key==$r->{$field[$search['group_by']]}){
                    if($search['view_by']=="TOTAL_RECORD"){
                      $value=$r->TOTAL_RECORD;
                    }elseif($search['view_by']=="TOTAL_DURATION"){
                      $value=$r->TOTAL_DURATION;
                    }else{
                      $value=$r->TOTAL;
                    }
                    $check=1;
                    if(!isset($total_column["00"]))$total_column["00"]=0;
                    $total_column["00"]+=$value;
                    $total+=$value;
                    echo "<td class='text-right'>".number_format($value,0,".",",")."</td>";
                  }
                }
                if($check==0){
                  $value=0;
                  if(!isset($total_column["00"]))$total_column["00"]=0;
                  $total_column["00"]+=$value;
                  $total+=$value;
                  echo "<td class='text-right'>".number_format($value,0,".",",")."</td>";
                }
              }
              for($x=1;$x<=$end_of_month;$x++){
                $temp=array();
                foreach($l['detail'] as $d){
                  $date=($x<10 ? "0".$x : $x);
                  if($d->BY_DATE==$search['year'].$search['month'].$date){
                    $temp=$d;
                    break;
                  }
                }
                if(sizeof($temp)>0){
                  if($search['view_by']=="TOTAL_RECORD"){
                    $value=$temp->TOTAL_RECORD;
                  }elseif($search['view_by']=="TOTAL_DURATION"){
                    $value=$temp->TOTAL_DURATION;
                  }else{
                    $value=$temp->TOTAL;
                  }
                }else{
                  $value=0;
                }
                if(!isset($total_column[$x]))$total_column[$x]=0;
                $total_column[$x]+=$value;
                $total+=$value;
                echo "<td class='text-right'>".number_format($value,0,".",",")."</td>";
              }
              $total_column['total']+=$total;
              echo "<td class='text-right'>".number_format($total,0,".",",")."</td>";
              echo "</tr>";
            }
          ?>
        </tbody>
        <tfoot>
          <?php
            echo "<tr>";
            echo "<td colspan='2' class='text-center'><b>Total</b></td>";
            if(sizeof($recycle_lists)>0){
              echo "<td class='text-right'>".number_format($total_column["00"],0,".",",")."</td>";
            }
            for($x=1;$x<=$end_of_month;$x++){
              echo "<td class='text-right'>".number_format($total_column[$x],0,".",",")."</td>";
            }
            echo "<td class='text-right'>".number_format($total_column['total'],0,".",",")."</td>";
            echo "</tr>";
          ?>
        </tfoot>
      </table>
    </div>
  </div>
</div>
<script type="text/javascript">
  height=235;
  $(document).ready(function(){
    $("#table_daily_summary").dataTable({
      "bSort" : true,
      "bFilter" : false,
      "bPaginate": false,
      "info": false,
    });
    $("#division,#billing_component,#sto").multiselect({numberDisplayed:2,maxHeight: 212,includeSelectAllOption: true,enableFiltering: true});
  });
</script>