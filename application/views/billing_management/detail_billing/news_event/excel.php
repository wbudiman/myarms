<h5><?= $header_title ?></h5>
<?php
  $summaries=array();
  foreach($lists as $g):
  if(!in_array($g->SUMMARY_PRODUCT,$summaries)):
    echo "<h3 class='title'>Revenue ".$g->SUMMARY_PRODUCT."</h3>";
    array_push($summaries,$g->SUMMARY_PRODUCT);
  
?>
<table border="1">
  <thead>
    <?php 
      echo "<tr>";
      echo "<th align='center'>".$row_lists[$search['show_data']]."</th>";
      $periodes=array($search['year_1'].$search['month_1'],$search['year_2'].$search['month_2'],$search['year_3'].$search['month_3']);
      foreach($periodes as $l){
        echo "<th align='center'>".$l."</th>";
      }
      echo "<th align='center'>Bulan 1 ke 2</th>";
      echo "<th align='center'>Growth 1 ke 2</th>";
      echo "<th align='center'>Bulan 2 ke 3</th>";
      echo "<th align='center'>Growth 2 ke 3</th>";
      echo "</tr>";
    ?>
  </thead>
  <tbody>
    <?php
      $rows=array();
      $rows2=array();
      $total_column=array();
      $total_column['total']=0;
      foreach($lists as $l){
        if(!in_array($l->{$field[$search['show_data']]},$rows))
        {
          array_push($rows,$l->{$field[$search['show_data']]});
        }
      }
      $counter=0;
      foreach($rows as $r){
        echo "<tr>";
        echo "<td>".($r!="" && $r!=NULL ? $r : $no_mapping)."</td>";
        $values=array();
        foreach($periodes as $p){
          $check=0;
          foreach($lists as $l){
            if($l->PERIODE==$p && $r==$l->{$field[$search['show_data']]} && $g->SUMMARY_PRODUCT==$l->SUMMARY_PRODUCT){
              $value=$l->TAGIHAN;
              if(!isset($total_column[$p]))$total_column[$p]=0;
              $total_column[$p]+=$value;
              array_push($values,$value);
              echo "<td align='right'>".$value."</td>";
              $check=1;
            }
          }
          if($check==0){
            array_push($values,0);
            if(!isset($total_column[$p]))$total_column[$p]=0;
            $total_column[$p]+=0;
            echo "<td align='right'>0</td>";
          }
        }
        $bulan_1_to_2=$values[1]-$values[0];
        $growth_1_to_2=($bulan_1_to_2!=0 ? ($values[0]!=0 ? $bulan_1_to_2/$values[0]*100 : 100) : 0);
        $bulan_2_to_3=$values[2]-$values[1];
        $growth_2_to_3=($bulan_2_to_3!=0 ? ($values[1]!=0 ? $bulan_2_to_3/$values[1]*100 : 100) : 0);
        echo "<td align='right'>".$bulan_1_to_2."</td>";
        echo "<td align='right'>".$growth_1_to_2." %</td>";
        echo "<td align='right'>".$bulan_2_to_3."</td>";
        echo "<td align='right'>".$growth_2_to_3." %</td>";
        echo "</tr>";
        if(!isset($total_column['bulan_1_to_2']))$total_column['bulan_1_to_2']=0;
        if(!isset($total_column['growth_1_to_2']))$total_column['growth_1_to_2']=0;
        if(!isset($total_column['bulan_2_to_3']))$total_column['bulan_2_to_3']=0;
        if(!isset($total_column['growth_2_to_3']))$total_column['growth_2_to_3']=0;
        $total_column['bulan_1_to_2']+=$bulan_1_to_2;
        $total_column['growth_1_to_2']+=$growth_1_to_2;
        $total_column['bulan_2_to_3']+=$bulan_2_to_3;
        $total_column['growth_2_to_3']+=$growth_2_to_3;
        $counter++;
      }
    ?>
  </tbody>
  <tfoot>
  <?php
    echo "<tr>";
    echo "<td style='padding:8px;'>Total (".sizeof($rows)." item)</td>";
    foreach($periodes as $p){
      foreach($total_column as $key=>$t){
        if($p==$key){
          echo "<td align='right' style='padding:8px;'>".$t."</td>";
        }
      }
    }
    $bulan_1_to_2=$total_column[$search['year_2'].$search['month_2']]-$total_column[$search['year_1'].$search['month_1']];
    echo "<td align='right' style='padding:8px;'>".$bulan_1_to_2."</td>";
    $growth_1_to_2=($bulan_1_to_2!=0 ? ($total_column[$search['year_1'].$search['month_1']]!=0 ? $bulan_1_to_2/$total_column[$search['year_1'].$search['month_1']]*100 : 100) : 0);
    echo "<td align='right' style='padding:8px;'>".$growth_1_to_2." %</td>";
    $bulan_2_to_3=$total_column[$search['year_3'].$search['month_3']]-$total_column[$search['year_2'].$search['month_2']];
    echo "<td align='right' style='padding:8px;'>".$bulan_2_to_3."</td>";
    $growth_2_to_3=($bulan_2_to_3!=0 ? ($total_column[$search['year_2'].$search['month_2']]!=0 ? $bulan_2_to_3/$total_column[$search['year_2'].$search['month_2']]*100 : 100) : 0);
    echo "<td align='right' style='padding:8px;'>".$growth_2_to_3." %</td>";
    echo "</tr>";
  ?>
  </tfoot>
</table>
<?php endif; ?>
<?php endforeach; ?>

<?php
  $summaries=array();
  foreach($lists as $g):
  if(!in_array($g->SUMMARY_PRODUCT,$summaries)):
    echo "<h3 class='title'>L11 ".$g->SUMMARY_PRODUCT."</h3>";
    array_push($summaries,$g->SUMMARY_PRODUCT);
  
?>
<table border="1">
  <thead>
    <?php 
      echo "<tr>";
      echo "<th align='center'>".$row_lists[$search['show_data']]."</th>";
      $periodes=array($search['year_1'].$search['month_1'],$search['year_2'].$search['month_2'],$search['year_3'].$search['month_3']);
      foreach($periodes as $l){
        echo "<th align='center'>".$l."</th>";
      }
      echo "<th align='center'>Bulan 1 ke 2</th>";
      echo "<th align='center'>Growth 1 ke 2</th>";
      echo "<th align='center'>Bulan 2 ke 3</th>";
      echo "<th align='center'>Growth 2 ke 3</th>";
      echo "</tr>";
    ?>
  </thead>
  <tbody>
    <?php
      $rows=array();
      $rows2=array();
      $total_column=array();
      $total_column['total']=0;
      foreach($lists as $l){
        if(!in_array($l->{$field[$search['show_data']]},$rows))
        {
          array_push($rows,$l->{$field[$search['show_data']]});
        }
      }
      $counter=0;
      foreach($rows as $r){
        echo "<tr>";
        echo "<td>".($r!="" && $r!=NULL ? $r : $no_mapping)."</td>";
        $values=array();
        foreach($periodes as $p){
          $check=0;
          foreach($lists as $l){
            if($l->PERIODE==$p && $r==$l->{$field[$search['show_data']]} && $g->SUMMARY_PRODUCT==$l->SUMMARY_PRODUCT){
              $value=$l->SST;
              if(!isset($total_column[$p]))$total_column[$p]=0;
              $total_column[$p]+=$value;
              array_push($values,$value);
              echo "<td align='right'>".$value."</td>";
              $check=1;
            }
          }
          if($check==0){
            array_push($values,0);
            if(!isset($total_column[$p]))$total_column[$p]=0;
            $total_column[$p]+=0;
            echo "<td align='right'>0</td>";
          }
        }
        $bulan_1_to_2=$values[1]-$values[0];
        $growth_1_to_2=($bulan_1_to_2!=0 ? ($values[0]!=0 ? $bulan_1_to_2/$values[0]*100 : 100) : 0);
        $bulan_2_to_3=$values[2]-$values[1];
        $growth_2_to_3=($bulan_2_to_3!=0 ? ($values[1]!=0 ? $bulan_2_to_3/$values[1]*100 : 100) : 0);
        echo "<td align='right'>".$bulan_1_to_2."</td>";
        echo "<td align='right'>".$growth_1_to_2." %</td>";
        echo "<td align='right'>".$bulan_2_to_3."</td>";
        echo "<td align='right'>".$growth_2_to_3." %</td>";
        echo "</tr>";
        if(!isset($total_column['bulan_1_to_2']))$total_column['bulan_1_to_2']=0;
        if(!isset($total_column['growth_1_to_2']))$total_column['growth_1_to_2']=0;
        if(!isset($total_column['bulan_2_to_3']))$total_column['bulan_2_to_3']=0;
        if(!isset($total_column['growth_2_to_3']))$total_column['growth_2_to_3']=0;
        $total_column['bulan_1_to_2']+=$bulan_1_to_2;
        $total_column['growth_1_to_2']+=$growth_1_to_2;
        $total_column['bulan_2_to_3']+=$bulan_2_to_3;
        $total_column['growth_2_to_3']+=$growth_2_to_3;
        $counter++;
      }
    ?>
  </tbody>
  <tfoot>
  <?php
    echo "<tr>";
    echo "<td style='padding:8px;'>Total (".sizeof($rows)." item)</td>";
    foreach($periodes as $p){
      foreach($total_column as $key=>$t){
        if($p==$key){
          echo "<td align='right' style='padding:8px;'>".$t."</td>";
        }
      }
    }
    $bulan_1_to_2=$total_column[$search['year_2'].$search['month_2']]-$total_column[$search['year_1'].$search['month_1']];
    echo "<td align='right' style='padding:8px;'>".$bulan_1_to_2."</td>";
    $growth_1_to_2=($bulan_1_to_2!=0 ? ($total_column[$search['year_1'].$search['month_1']]!=0 ? $bulan_1_to_2/$total_column[$search['year_1'].$search['month_1']]*100 : 100) : 0);
    echo "<td align='right' style='padding:8px;'>".$growth_1_to_2." %</td>";
    $bulan_2_to_3=$total_column[$search['year_3'].$search['month_3']]-$total_column[$search['year_2'].$search['month_2']];
    echo "<td align='right' style='padding:8px;'>".$bulan_2_to_3."</td>";
    $growth_2_to_3=($bulan_2_to_3!=0 ? ($total_column[$search['year_2'].$search['month_2']]!=0 ? $bulan_2_to_3/$total_column[$search['year_2'].$search['month_2']]*100 : 100) : 0);
    echo "<td align='right' style='padding:8px;'>".$growth_2_to_3." %</td>";
    echo "</tr>";
  ?>
  </tfoot>
</table>
<?php endif; ?>
<?php endforeach; ?>

<?php
  $summaries=array();
  foreach($bc_lists as $g):
  if(!in_array($g->SUMMARY_PRODUCT,$summaries)):
    echo "<h3 class='title'>Komponen Billing ".$g->SUMMARY_PRODUCT."</h3>";
    array_push($summaries,$g->SUMMARY_PRODUCT);
  
?>
<table border="1">
  <thead>
    <?php 
      echo "<tr>";
      echo "<th align='center'>Billing Komponen</th>";
      $periodes=array($search['year_1'].$search['month_1'],$search['year_2'].$search['month_2'],$search['year_3'].$search['month_3']);
      foreach($periodes as $l){
        echo "<th align='center'>".$l."</th>";
      }
      echo "<th align='center'>Bulan 1 ke 2</th>";
      echo "<th align='center'>Growth 1 ke 2</th>";
      echo "<th align='center'>Bulan 2 ke 3</th>";
      echo "<th align='center'>Growth 2 ke 3</th>";
      echo "</tr>";
    ?>
  </thead>
  <tbody>
    <?php
      $rows=array();
      $rows2=array();
      $total_column=array();
      $total_column['total']=0;
      foreach($bc_lists as $l){
        foreach($field["billing_component"] as $key=>$f){
          if(!in_array($f->BILLING_COMPONENT_DESCRIPTION,$rows))
          {
            array_push($rows,$f->BILLING_COMPONENT_DESCRIPTION);
            array_push($rows2,$f->BILLING_COMPONENT_FIELD);
          }
        }
      }
      $counter=0;
      foreach($rows as $r){
        echo "<tr>";
        echo "<td>".($r!="" && $r!=NULL ? $r : $no_mapping)."</td>";
        $values=array();
        foreach($periodes as $p){
          $check=0;
          foreach($bc_lists as $l){
            if($l->PERIODE==$p && $g->SUMMARY_PRODUCT==$l->SUMMARY_PRODUCT){
              $value=$l->{$rows2[$counter]};
              if(!isset($total_column[$p]))$total_column[$p]=0;
              $total_column[$p]+=$value;
              array_push($values,$value);
              echo "<td align='right'>".$value."</td>";
              $check=1;
            }
          }
          if($check==0){
            array_push($values,0);
            if(!isset($total_column[$p]))$total_column[$p]=0;
            $total_column[$p]+=0;
            echo "<td align='right'>0</td>";
          }
        }
        $bulan_1_to_2=$values[1]-$values[0];
        $growth_1_to_2=($bulan_1_to_2!=0 ? ($values[0]!=0 ? $bulan_1_to_2/$values[0]*100 : 100) : 0);
        $bulan_2_to_3=$values[2]-$values[1];
        $growth_2_to_3=($bulan_2_to_3!=0 ? ($values[1]!=0 ? $bulan_2_to_3/$values[1]*100 : 100) : 0);
        echo "<td align='right'>".$bulan_1_to_2."</td>";
        echo "<td align='right'>".$growth_1_to_2." %</td>";
        echo "<td align='right'>".$bulan_2_to_3."</td>";
        echo "<td align='right'>".$growth_2_to_3." %</td>";
        echo "</tr>";
        if(!isset($total_column['bulan_1_to_2']))$total_column['bulan_1_to_2']=0;
        if(!isset($total_column['growth_1_to_2']))$total_column['growth_1_to_2']=0;
        if(!isset($total_column['bulan_2_to_3']))$total_column['bulan_2_to_3']=0;
        if(!isset($total_column['growth_2_to_3']))$total_column['growth_2_to_3']=0;
        $total_column['bulan_1_to_2']+=$bulan_1_to_2;
        $total_column['growth_1_to_2']+=$growth_1_to_2;
        $total_column['bulan_2_to_3']+=$bulan_2_to_3;
        $total_column['growth_2_to_3']+=$growth_2_to_3;
        $counter++;
      }
    ?>
  </tbody>
</table>
<?php endif; ?>
<?php endforeach; ?>