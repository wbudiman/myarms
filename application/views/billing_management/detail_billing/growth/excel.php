<h5><?= $header_title ?></h5>
<table border="1">
  <thead>
    <?php 
      $total_column=array();
      $total_column['total']=0;
      echo "<tr>";
      echo "<th class='text-center'>".$row_lists[$search['show_data']]."</th>";
      $periodes=array($search['year_1'].$search['month_1'],$search['year_2'].$search['month_2'],$search['year_3'].$search['month_3']);
      foreach($periodes as $l){
        echo "<th class='text-center'>".$l."</th>";
      }
      echo "<th class='text-center'>Bulan 1 ke 2</th>";
      echo "<th class='text-center'>Growth 1 ke 2</th>";
      echo "<th class='text-center'>Bulan 2 ke 3</th>";
      echo "<th class='text-center'>Growth 2 ke 3</th>";
      echo "</tr>";
    ?>
  </thead>
  <tbody>
    <?php
      $rows=array();
      $rows2=array();
      foreach($lists as $l){
        if(is_array($field[$search['show_data']]))
        {
          foreach($field[$search['show_data']] as $f){
            if(!in_array($f->BILLING_COMPONENT_DESCRIPTION,$rows))
            {
              array_push($rows,$f->BILLING_COMPONENT_DESCRIPTION);
              array_push($rows2,$f->BILLING_COMPONENT_FIELD);
            }
          }
        }else{
          if(!in_array($l->{$field[$search['show_data']]},$rows))
          {
            array_push($rows,$l->{$field[$search['show_data']]});
          }
        }
      }
      $counter=0;
      foreach($rows as $r){
        $checking=false;
        foreach($periodes as $p){
          foreach($lists as $l){
            if(is_array($field[$search['show_data']]))
            {
              if($l->PERIODE==$p){
                if($search['view_by']=="L11"){
                  $value=$l->SST;
                }elseif($search['view_by']=="ARPU"){
                  $value=$l->{$rows2[$counter]}/$l->SST;
                }else{
                  $value=$l->{$rows2[$counter]};
                }
                if($value>0)$checking=true;
              }
            }else{
              if($l->PERIODE==$p && $r==$l->{$field[$search['show_data']]}){
                if($search['view_by']=="L11"){
                  $value=$l->SST;
                }elseif($search['view_by']=="ARPU"){
                  $value=$l->{$search['billing_component']}/$l->SST;
                }else{
                  $value=$l->{$search['billing_component']};
                }
                if($value>0)$checking=true;
              }
            }
          }
        }
        if($checking==true){
          echo "<tr>";
          echo "<td>".($r!="" && $r!=NULL ? $r : $no_mapping)."</td>";
          $values=array();
          foreach($periodes as $p){
            $check=0;
            foreach($lists as $l){
              if(is_array($field[$search['show_data']]))
              {
                if($l->PERIODE==$p){
                  if($search['view_by']=="L11"){
                    if(!isset($total_column[$p]))$total_column[$p]=0;
                    $value=$l->SST;
                    $total_column[$p]+=$value;
                  }elseif($search['view_by']=="ARPU"){
                    $value=$l->{$rows2[$counter]}/$l->SST;
                     if(!isset($total_column[$p]))$total_column[$p]=0;
                     $total_column[$p]+=$value;
                  }else{
                    if(!isset($total_column[$p]))$total_column[$p]=0;
                    $value=$l->{$rows2[$counter]};
                    $total_column[$p]+=$value;
                  }
                  array_push($values,$value);
                  echo "<td class='text-right'>".number_format($value,0,".",",")."</td>";
                  $check=1;
                }
              }else{
                if($l->PERIODE==$p && $r==$l->{$field[$search['show_data']]}){
                  if($search['view_by']=="L11"){
                    $value=$l->SST;
                    if(!isset($total_column[$p]))$total_column[$p]=0;
                    $total_column[$p]+=$value;
                  }elseif($search['view_by']=="ARPU"){
                    $value=$l->{$search['billing_component']}/$l->SST;
                    if(!isset($total_column[$p]))$total_column[$p]=array("AMOUNT"=>0,"L11"=>0);
                    $total_column[$p]['AMOUNT']+=$l->{$search['billing_component']};
                    $total_column[$p]['L11']+=$l->SST;
                  }else{
                    $value=$l->{$search['billing_component']};
                    if(!isset($total_column[$p]))$total_column[$p]=0;
                    $total_column[$p]+=$value;
                  }
                  array_push($values,$value);
                  echo "<td class='text-right'>".number_format($value,0,".",",")."</td>";
                  $check=1;
                }
              }
            }
            if($check==0){
              if(!is_array($field[$search['show_data']]) && $search['view_by']=="ARPU"){
                if(!isset($total_column[$p]))$total_column[$p]=array("AMOUNT"=>0,"L11"=>0);
              }else{
                if(!isset($total_column[$p]))$total_column[$p]=0;
              }
              array_push($values,0);
              echo "<td class='text-right'>0</td>";
            }
          }
          $bulan_1_to_2=$values[1]-$values[0];
          $growth_1_to_2=($bulan_1_to_2!=0 ? ($values[0]!=0 ? $bulan_1_to_2/$values[0]*100 : 100) : 0);
          $bulan_2_to_3=$values[2]-$values[1];
          $growth_2_to_3=($bulan_2_to_3!=0 ? ($values[1]!=0 ? $bulan_2_to_3/$values[1]*100 : 100) : 0);
          echo "<td class='text-right'>".number_format($bulan_1_to_2,0,".",",")."</td>";
          echo "<td class='text-right'>".number_format($growth_1_to_2,2)." %</td>";
          echo "<td class='text-right'>".number_format($bulan_2_to_3,0,".",",")."</td>";
          echo "<td class='text-right'>".number_format($growth_2_to_3,2)." %</td>";
          echo "</tr>";
          if(!isset($total_column['bulan_1_to_2']))$total_column['bulan_1_to_2']=0;
          if(!isset($total_column['growth_1_to_2']))$total_column['growth_1_to_2']=0;
          if(!isset($total_column['bulan_2_to_3']))$total_column['bulan_2_to_3']=0;
          if(!isset($total_column['growth_2_to_3']))$total_column['growth_2_to_3']=0;
          $total_column['bulan_1_to_2']+=$bulan_1_to_2;
          $total_column['growth_1_to_2']+=$growth_1_to_2;
          $total_column['bulan_2_to_3']+=$bulan_2_to_3;
          $total_column['growth_2_to_3']+=$growth_2_to_3;
        }
        $counter++;
      }
    ?>
  </tbody>
  <tfoot>
  <?php
    if($search['show_data']!="billing_component"){
      echo "<tr>";
      echo "<td style='padding:8px;'>Total (".sizeof($rows)." item)</td>";
      foreach($periodes as $p){
        if(isset($total_column[$p])){
          foreach($total_column as $key=>$t){
            if($p==$key){
              if($search['view_by']=="ARPU"){
                if($t['AMOUNT']!=0 && $t['L11']!=0){
                  echo "<td class='text-right' style='padding:8px;'>".number_format($t['AMOUNT']/$t['L11'],0,".",",")."</td>";
                }else{
                  echo "<td class='text-right' style='padding:8px;'>".number_format(0,0,".",",")."</td>";
                }
              }else{
                echo "<td class='text-right' style='padding:8px;'>".number_format($t,0,".",",")."</td>";
              }
            }
          }
        }else{
          echo "<td class='text-right'>0</td>";
          if($search['view_by']=="ARPU"){
            $total_column[$p]=array("AMOUNT"=>0,"L11"=>0);
          }else{
            $total_column[$p]=0;
          }
        }
      }
      if($search['view_by']=="ARPU"){
        $month2=0;
        if($total_column[$search['year_2'].$search['month_2']]['AMOUNT']!=0 && $total_column[$search['year_2'].$search['month_2']]['L11']!=0){
          $month2=$total_column[$search['year_2'].$search['month_2']]['AMOUNT']/$total_column[$search['year_2'].$search['month_2']]['L11'];
        }
        $month1=0;
        if($total_column[$search['year_1'].$search['month_1']]['AMOUNT']!=0 && $total_column[$search['year_1'].$search['month_1']]['L11']!=0){
          $month1=$total_column[$search['year_1'].$search['month_1']]['AMOUNT']/$total_column[$search['year_1'].$search['month_1']]['L11'];
        }
        $month3=0;
        if($total_column[$search['year_3'].$search['month_3']]['AMOUNT']!=0 && $total_column[$search['year_3'].$search['month_3']]['L11']!=0){
          $month3=$total_column[$search['year_3'].$search['month_3']]['AMOUNT']/$total_column[$search['year_3'].$search['month_3']]['L11'];
        }
        $bulan_1_to_2=$month2-$month1;
        echo "<td class='text-right' style='padding:8px;'>".number_format($bulan_1_to_2,0,".",",")."</td>";
        $growth_1_to_2=($bulan_1_to_2!=0 ? ($month1!=0 ? $bulan_1_to_2/$month1*100 : 100) : 0);
        echo "<td class='text-right' style='padding:8px;'>".number_format($growth_1_to_2,2)." %</td>";
        $bulan_2_to_3=$month3-$month2;
        echo "<td class='text-right' style='padding:8px;'>".number_format($bulan_2_to_3,0,".",",")."</td>";
        $growth_2_to_3=($bulan_2_to_3!=0 ? ($month2!=0 ? $bulan_2_to_3/$month2*100 : 100) : 0);
        echo "<td class='text-right' style='padding:8px;'>".number_format($growth_2_to_3,2)." %</td>";
      }else{
        $bulan_1_to_2=$total_column[$search['year_2'].$search['month_2']]-$total_column[$search['year_1'].$search['month_1']];
        echo "<td class='text-right' style='padding:8px;'>".number_format($bulan_1_to_2,0,".",",")."</td>";
        $growth_1_to_2=($bulan_1_to_2!=0 ? ($total_column[$search['year_1'].$search['month_1']]!=0 ? $bulan_1_to_2/$total_column[$search['year_1'].$search['month_1']]*100 : 100) : 0);
        echo "<td class='text-right' style='padding:8px;'>".number_format($growth_1_to_2,2)." %</td>";
        $bulan_2_to_3=$total_column[$search['year_3'].$search['month_3']]-$total_column[$search['year_2'].$search['month_2']];
        echo "<td class='text-right' style='padding:8px;'>".number_format($bulan_2_to_3,0,".",",")."</td>";
        $growth_2_to_3=($bulan_2_to_3!=0 ? ($total_column[$search['year_2'].$search['month_2']]!=0 ? $bulan_2_to_3/$total_column[$search['year_2'].$search['month_2']]*100 : 100) : 0);
        echo "<td class='text-right' style='padding:8px;'>".number_format($growth_2_to_3,2)." %</td>";
      }
      echo "</tr>";
    }else{
      echo "<tr>";
      echo "<td>Tagihan</td>";
      $total=0;
      $values=array();
      foreach($periodes as $c){
        if(isset($total_column[$p])){
          foreach($total_column as $key=>$t){
            if($c==$key){
              if($search['view_by']=="ARPU" && !is_array($field[$search['show_data']])){
                $total+=($t['AMOUNT']/$t['L11']);
                array_push($values,($t['AMOUNT']/$t['L11']));
                echo "<td class='text-right' style='padding:8px;'>".number_format($t['AMOUNT']/$t['L11'],0,".",",")."</td>";
              }else{
                array_push($values,$t);
                echo "<td class='text-right' style='padding:8px;'>".number_format($t,0,".",",")."</td>";
              }
            }
          }
        }else{
          echo "<td class='text-right'>0</td>";
          if($search['view_by']=="ARPU"){
            $total_column[$p]=array("AMOUNT"=>0,"L11"=>0);
          }else{
            $total_column[$p]=0;
          }
        }
      }
      $bulan_1_to_2=$values[1]-$values[0];
      $growth_1_to_2=($bulan_1_to_2!=0 ? ($values[0]!=0 ? $bulan_1_to_2/$values[0]*100 : 100) : 0);
      $bulan_2_to_3=$values[2]-$values[1];
      $growth_2_to_3=($bulan_2_to_3!=0 ? ($values[1]!=0 ? $bulan_2_to_3/$values[1]*100 : 100) : 0);
      echo "<td class='text-right'>".number_format($bulan_1_to_2,0,".",",")."</td>";
      echo "<td class='text-right'>".number_format($growth_1_to_2,2)." %</td>";
      echo "<td class='text-right'>".number_format($bulan_2_to_3,0,".",",")."</td>";
      echo "<td class='text-right'>".number_format($growth_2_to_3,2)." %</td>";
      echo "</tr>";
      echo "<tr>";
      echo "<td>Ppn</td>";
      $total=0;
      $values=array();
      foreach($periodes as $c){
        foreach($total_column as $key=>$t){
          if($c==$key){
            if($search['view_by']=="ARPU" && !is_array($field[$search['show_data']]) ){
              $total+=($t['AMOUNT']/$t['L11'])*0.1;
              array_push($values,($t['AMOUNT']/$t['L11'])*0.1);
              echo "<td class='text-right' style='padding:8px;'>".number_format(($t['AMOUNT']/$t['L11'])*0.1,0,".",",")."</td>";
            }else{
              array_push($values,$t*0.1);
              echo "<td class='text-right' style='padding:8px;'>".number_format($t*0.1,0,".",",")."</td>";
            }
          }
        }
      }
      $bulan_1_to_2=$values[1]-$values[0];
      $growth_1_to_2=($bulan_1_to_2!=0 ? ($values[0]!=0 ? $bulan_1_to_2/$values[0]*100 : 100) : 0);
      $bulan_2_to_3=$values[2]-$values[1];
      $growth_2_to_3=($bulan_2_to_3!=0 ? ($values[1]!=0 ? $bulan_2_to_3/$values[1]*100 : 100) : 0);
      echo "<td class='text-right'>".number_format($bulan_1_to_2,0,".",",")."</td>";
      echo "<td class='text-right'>".number_format($growth_1_to_2,2)." %</td>";
      echo "<td class='text-right'>".number_format($bulan_2_to_3,0,".",",")."</td>";
      echo "<td class='text-right'>".number_format($growth_2_to_3,2)." %</td>";
      echo "</tr>";
      echo "<tr>";
      echo "<td>Tagihan + Ppn</td>";
      $total=0;
      $values=array();
      foreach($periodes as $c){
        foreach($total_column as $key=>$t){
          if($c==$key){
            if($search['view_by']=="ARPU" && !is_array($field[$search['show_data']])){
              $total+=($t['AMOUNT']/$t['L11'])+(($t['AMOUNT']/$t['L11'])*0.1);
              array_push($values,($t['AMOUNT']/$t['L11'])+(($t['AMOUNT']/$t['L11'])*0.1));
              echo "<td class='text-right' style='padding:8px;'>".number_format(($t['AMOUNT']/$t['L11'])+(($t['AMOUNT']/$t['L11'])*0.1),0,".",",")."</td>";
            }else{
              array_push($values,$t+($t*0.1));
              echo "<td class='text-right' style='padding:8px;'>".number_format($t+($t*0.1),0,".",",")."</td>";
            }
          }
        }
      }
      $bulan_1_to_2=$values[1]-$values[0];
      $growth_1_to_2=($bulan_1_to_2!=0 ? ($values[0]!=0 ? $bulan_1_to_2/$values[0]*100 : 100) : 0);
      $bulan_2_to_3=$values[2]-$values[1];
      $growth_2_to_3=($bulan_2_to_3!=0 ? ($values[1]!=0 ? $bulan_2_to_3/$values[1]*100 : 100) : 0);
      echo "<td class='text-right'>".number_format($bulan_1_to_2,0,".",",")."</td>";
      echo "<td class='text-right'>".number_format($growth_1_to_2,2)." %</td>";
      echo "<td class='text-right'>".number_format($bulan_2_to_3,0,".",",")."</td>";
      echo "<td class='text-right'>".number_format($growth_2_to_3,2)." %</td>";
      echo "</tr>";
    }
  ?>
  </tfoot>
</table>