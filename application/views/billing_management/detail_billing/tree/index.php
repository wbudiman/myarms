<form method="post" action="<?= base_url($path."index") ?>" id="form_tree">
  <div class="row">
    <div class="col-md-12">
      <div id="table-search">
        <div class="wrap">
          <table class="table table-search table-striped">
            <thead>
              <tr>
                <th colspan="8" v-align="middle">Parameter Pencarian Data</th>
              </tr>
            </thead>
            <tbody>
              <tr>                   
                <td>Divisi</td>
                <td>
                  <select name="search[division][]" id="division" multiple class="form-control">
                    <?php 
                      foreach($division_lists as $d){
                        echo "<option value='".$d->DIVISION_CODE."' ".($search['division']!="" && in_array($d->DIVISION_CODE,$search['division']) ? "selected" : "").">".$d->DIVISION_CODE."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Summary Product</td>
                <td>
                  <select name="search[summary_product][]" multiple class="form-control" id="summary_product">
                    <?php 
                      foreach($summary_product_lists as $d){
                        echo "<option value='".$d->SUMMARY_PRODUCT."' ".($search['summary_product']!="" && in_array($d->SUMMARY_PRODUCT,$search['summary_product']) ? "selected" : "").">".$d->SUMMARY_PRODUCT."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Top Customer</td>
                <td>
                  <select name="search[top_customer][]" multiple class="form-control" id="top_customer">
                    <?php 
                      echo '<optgroup label="Top 20">';
                      foreach($top_customer_lists as $r){
                        if($r->PORTOFOLIO_GROUP=="Top 20")
                        {
                          echo "<option value='".$r->PORTOFOLIO_ID."' ".($search['top_customer']!="" && in_array($r->PORTOFOLIO_ID,$search['top_customer']) ? "selected" : "").">".$r->PORTOFOLIO_NAME."</option>";
                        }
                      }
                      echo '</optgroup>';
                      echo '<optgroup label="Non Top 20">';
                      foreach($top_customer_lists as $r){
                        if($r->PORTOFOLIO_GROUP=="Non Top 20")
                        {
                          echo "<option value='".$r->PORTOFOLIO_ID."' ".($search['top_customer']!="" && in_array($r->PORTOFOLIO_ID,$search['top_customer']) ? "selected" : "").">".$r->PORTOFOLIO_NAME."</option>";
                        }
                      }
                      echo '</optgroup>';
                    ?>
                  </select>
                </td>
              </tr>
              <tr>                   
                <td>Witel</td>
                <td>
                  <select name="search[witel][]" id="witel" multiple class="form-control">
                    <?php 
                      foreach($witel_lists as $w){
                        echo "<option value='".$w->WITEL_CODE."' ".($search['witel']!="" && in_array($w->WITEL_CODE,$search['witel']) ? "selected" : "").">".$w->WITEL_DESCRIPTION."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Produk</td>
                <td>
                  <select name="search[product][]" multiple class="form-control" id="product">
                    <?php 
                      foreach($product_lists as $r){
                        echo "<option value='".$r->PRODUCT_NAME."' ".($search['product']!="" && in_array($r->PRODUCT_NAME,$search['product']) ? "selected" : "").">".$r->PRODUCT_NAME."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Indihome</td>
                <td>
                  <select name="search[indihome]" class="form-control">
                    <option value="">All</option>
                    <?php 
                      foreach($indihome_lists as $r){
                        echo "<option value='".$r->INDIHOME_ID."' ".($search['indihome']==$r->INDIHOME_ID ? "selected" : "").">".$r->INDIHOME_DESCRIPTION."</option>";
                      }
                    ?>
                  </select>
                </td>
              </tr>
              <tr>                   
                <td>Datel</td>
                <td>
                  <select name="search[datel][]" id="datel" multiple class="form-control">
                    <?php 
                      foreach($datel_lists as $w){
                        echo "<option value='".$w->DATEL_CODE."' ".($search['datel']!="" && in_array($w->DATEL_CODE,$search['datel']) ? "selected" : "").">".$w->DATEL_DESCRIPTION."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Ubis</td>
                <td>
                  <select name="search[ubis][]" multiple class="form-control" id="ubis">
                    <?php 
                      foreach($ubis_lists as $d){
                        echo "<option value='".$d->UBIS_ID."' ".($search['ubis']!="" && in_array($d->UBIS_ID,$search['ubis']) ? "selected" : "").">".$d->UBIS."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Paket Speedy</td>
                <td>
                  <select name="search[packet_speedy]" class="form-control" style="width:200px;">
                    <option value="">All</option>
                    <?php 
                      foreach($packet_speedy_lists as $d){
                        echo "<option value='".$d->PACKET_SPEEDY_ID."' ".($search['packet_speedy']==$d->PACKET_SPEEDY_ID ? "selected" : "").">".$d->PACKET_SPEEDY_ID." - ".$d->PACKET_SPEEDY_DESCRIPTION."</option>";
                      }
                    ?>
                  </select>
                </td>
              </tr>
              <tr>                   
                <td>Periode</td>
                <td>
                  <div class="col-md-9" style="padding-left:0px;">
                    <div class="col-md-7" style="padding-left:0px;">
                      <select name="search[month]" class="form-control">
                        <?php 
                          foreach($month_lists as $key=>$d){
                            echo "<option value='".$key."' ".($search['month']==$key ? "selected" : "").">".$d."</option>";
                          }
                        ?>
                      </select>
                    </div>
                    <div class="col-md-5" style="padding-left:0px;">
                      <select name="search[year]" class="form-control">
                        <?php 
                          for($x=date("Y");$x>(date("Y")-10);$x--){
                            echo "<option value='".$x."' ".($search['year']==$x ? "selected" : "").">".$x."</option>";
                          }
                        ?>
                      </select>
                    </div>
                  </div>
                </td>
                <td>Customer Category</td>
                <td>
                  <select name="search[ubis_segment][]" id="ubis_segment" multiple class="form-control">
                    <?php 
                      foreach($ubis_segment_lists as $r){
                        echo "<option value='".$r->UBIS_SEGMENT."' ".($search['ubis_segment']!="" && in_array($r->UBIS_SEGMENT,$search['ubis_segment']) ? "selected" : "").">".$r->UBIS_SEGMENT."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Usage</td>
                <td>
                  <select name="search[usage]" class="form-control">
                    <option value="">All</option>
                    <?php foreach($usage_status_lists as $u): ?>
                      <option value="<?= $u->USAGE_ID ?>" <?= ($search['usage']!="" && $search['usage']==$u->USAGE_ID ? "selected" : "") ?>><?= $u->USAGE_STATUS ?></option>
                    <?php endforeach; ?>
                  </select>
                </td>
              </tr>
              <tr class="background-filter">                   
                <td>Show Data</td>
                <td>
                  <select name="search[show_data]" class="form-control" id="show_data">
                    <?php 
                      foreach($row_lists as $key=>$d){
                        echo "<option value='".$key."' ".($search['show_data']==$key ? "selected" : "").">".$d."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>View</td>
                <td>
                  <select name="search[view]" class="form-control">
                    <option value="">All</option>
                    <option value="summary" <?= ($search['view']=="summary" ? "selected" : "") ?>>Summary</option>
                    <option value="detail" <?= ($search['view']=="detail" ? "selected" : "") ?>>Detail</option>
                  </select>
                </td>
                <td colspan="2"></td>
              </tr>
            </tbody>
            <tfoot>
              <tr>
                <td>Search</td>
                <td><input type="submit" name="search[button]" class="btn btn-search" value="Search"></td>
                <td>To Image</td>
                <td>
                  <input type="button" id="render" class="btn btn-search" value="Render">
                  <a href="" id="download" target="_blank" class="btn btn-search hide">Download</a>
                </td>
                <td colspan="4"></td>
              </tr>
            </tfoot>
          </table>
        </div>
        <a href="javascript:void(0)" class="showhide"></a>
      </div>
    </div>
  </div> 
</form>
<div class="row">
  <div class="col-md-12">
    <div class="billing-detail">
      <ul id="tree" class="hide">
        <li>
          <h3 class="orgChart-header-title">Pohon Revenue</h3>
          <ul>
            <li>
              <div class="table_tree_tag" data-table_type="TOTAL_REVENUE">
                <h5 class="orgChart-title">A. Total Revenue</h5>
                <table class="table table-bordered table_tree" style="margin-bottom:0px !important;min-width:300px;">
                  <thead>
                    <?php 
                      echo "<tr>";
                      echo "<th class='text-center'>".$row_lists[$search['show_data']]."</th>";
                      echo "<th class='text-center'>L11</th>";
                      echo "<th class='text-center'>Revenue</th>";
                      echo "</tr>";
                    ?>
                  </thead>
                  <tbody>
                    <?php
                      $total_A=array("L11"=>0,"revenue"=>0);
                      foreach($lists as $l){
                        echo "<tr>";
                        echo "<td>".($l->{$field[$search['show_data']]}!="" && $l->{$field[$search['show_data']]}!=NULL ? $l->{$field[$search['show_data']]} : $no_mapping)."</td>";
                        echo "<td class='text-right'>".number_format($l->SST,0,".",",")."</td>";
                        echo "<td class='text-right'>".number_format($l->TAGIHAN,0,".",",")."</td>";
                        echo "</tr>";
                        $total_A['L11']+=$l->SST;
                        $total_A['revenue']+=$l->TAGIHAN;
                      }
                    ?>
                  </tbody>
                  <tfoot>
                    <tr>
                      <td>Total</td>
                      <td class="text-right"><?= number_format($total_A['L11'],0,".",",") ?></td>
                      <td class="text-right"><?= number_format($total_A['revenue'],0,".",",") ?></td>
                    </tr>
                  </tfoot>
                </table>
                <div style="display:none;" class="tree_detail">
                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th>CCA</th>
                        <th>SND</th>
                        <th>SND Group</th>
                        <th>Produk</th>
                        <th>Tagihan Bulan Lalu</th>
                        <th>Tagihan Sekarang</th>
                        <th>Bisnis Area</th>
                        <th>Umur</th>
                        <th>Usage</th>
                        <th>Paket Speedy</th>
                        <th>Paket FPIP</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                        $total_detail_A=array("now"=>0,"last"=>0);
                        foreach($detail_lists as $l){
                          echo "<tr>";
                          echo "<td>".$l->CCA."</td>";
                          echo "<td>".$l->SND."</td>";
                          echo "<td>".$l->SND_GROUP."</td>";
                          echo "<td>".$l->PRODUCT_NAME."</td>";
                          echo "<td class='text-right'>".number_format($l->TOTAL_LALU,0,".",",")."</td>";
                          echo "<td class='text-right'>".number_format($l->TOTAL,0,".",",")."</td>";
                          echo "<td>".$l->BUSINESS_AREA_DESCRIPTION."</td>";
                          echo "<td>".$l->UMUR_PELANGGAN_DESCRIPTION."</td>";
                          echo "<td>".$l->USAGE_STATUS."</td>";
                          echo "<td>".$l->PACKET_SPEEDY_DESCRIPTION."</td>";
                          echo "<td>".$l->PACKET_FBIP_DESCRIPTION."</td>";
                          echo "</tr>";
                          $total_detail_A['now']+=$l->TOTAL;
                          $total_detail_A['last']+=$l->TOTAL_LALU;
                        }
                      ?>
                    </tbody>
                    <tfoot>
                      <tr>
                        <td colspan="4">Total</td>
                        <td class="text-right"><?= number_format($total_detail_A['last'],0,".",",") ?></td>
                        <td class="text-right"><?= number_format($total_detail_A['now'],0,".",",") ?></td>
                        <td colspan="5"></td>
                      </tr>
                    </tfoot>
                  </table>
                </div>
              </div>
              <ul>
                <li>
                  <div class="table_tree_tag" data-table_type="REVENUE_EXISTING">
                    <h5 class="orgChart-title">B. Revenue Existing</h5>
                    <table class="table table-bordered table_tree" style="margin-bottom:0px !important;min-width:480px;">
                      <thead>
                        <?php 
                          echo "<tr>";
                          echo "<th class='text-center'>".$row_lists[$search['show_data']]."</th>";
                          echo "<th class='text-center'>L11</th>";
                          echo "<th class='text-center'>Revenue</th>";
                          echo "<th class='text-center'>Revenue Bln Lalu</th>";
                          echo "</tr>";
                        ?>
                      </thead>
                      <tbody>
                        <?php
                          $total_B=array("L11"=>0,"revenue"=>0,"revenue_lalu"=>0);
                          foreach($lists as $l){
                            foreach($last_1_lists as $l2){
                              if($l->{$field[$search['show_data']]}==$l2->{$field[$search['show_data']]})
                              {
                                echo "<tr>";
                                echo "<td>".($l->{$field[$search['show_data']]}!="" && $l->{$field[$search['show_data']]}!=NULL ? $l->{$field[$search['show_data']]} : $no_mapping)."</td>";
                                echo "<td class='text-right'>".number_format($l->SST,0,".",",")."</td>";
                                echo "<td class='text-right'>".number_format($l->TAGIHAN,0,".",",")."</td>";
                                echo "<td class='text-right'>".number_format($l2->TAGIHAN,0,".",",")."</td>";
                                echo "</tr>";
                                $total_B['L11']+=$l->SST;
                                $total_B['revenue']+=$l->TAGIHAN;
                                $total_B['revenue_lalu']+=$l2->TAGIHAN;
                              }
                            }
                          }
                        ?>
                      </tbody>
                      <tfoot>
                        <tr>
                          <td>Total</td>
                          <td class="text-right"><?= number_format($total_B['L11'],0,".",",") ?></td>
                          <td class="text-right"><?= number_format($total_B['revenue'],0,".",",") ?></td>
                          <td class="text-right"><?= number_format($total_B['revenue_lalu'],0,".",",") ?></td>
                        </tr>
                      </tfoot>
                    </table>
                    <div style="display:none;" class="tree_detail">
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>CCA</th>
                            <th>SND</th>
                            <th>SND Group</th>
                            <th>Produk</th>
                            <th>Tagihan Bulan Lalu</th>
                            <th>Tagihan Sekarang</th>
                            <th>Bisnis Area</th>
                            <th>Umur</th>
                            <th>Usage</th>
                            <th>Paket Speedy</th>
                            <th>Paket FPIP</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                            $total_detail_B=array("now"=>0,"last"=>0);
                            foreach($detail_lists as $l){
                              foreach($last_1_detail_lists as $l2){
                                if($l->{$field[$search['show_data']]}==$l2->{$field[$search['show_data']]})
                                {
                                  echo "<tr>";
                                  echo "<td>".$l->CCA."</td>";
                                  echo "<td>".$l->SND."</td>";
                                  echo "<td>".$l->SND_GROUP."</td>";
                                  echo "<td>".$l->PRODUCT_NAME."</td>";
                                  echo "<td class='text-right'>".number_format($l->TOTAL_LALU,0,".",",")."</td>";
                                  echo "<td class='text-right'>".number_format($l->TOTAL,0,".",",")."</td>";
                                  echo "<td>".$l->BUSINESS_AREA_DESCRIPTION."</td>";
                                  echo "<td>".$l->UMUR_PELANGGAN_DESCRIPTION."</td>";
                                  echo "<td>".$l->USAGE_STATUS."</td>";
                                  echo "<td>".$l->PACKET_SPEEDY_DESCRIPTION."</td>";
                                  echo "<td>".$l->PACKET_FBIP_DESCRIPTION."</td>";
                                  echo "</tr>";
                                  $total_detail_B['now']+=$l->TOTAL;
                                  $total_detail_B['last']+=$l->TOTAL_LALU;
                                }
                              }
                            }
                          ?>
                        </tbody>
                        <tfoot>
                          <tr>
                            <td colspan="4">Total</td>
                            <td class="text-right"><?= number_format($total_detail_B['last'],0,".",",") ?></td>
                            <td class="text-right"><?= number_format($total_detail_B['now'],0,".",",") ?></td>
                            <td colspan="5"></td>
                          </tr>
                        </tfoot>
                      </table>
                    </div>
                  </div>
                  <ul>
                    <li>
                      <div class="table_tree_tag" data-table_type="REVENUE_EXISTING_TAGIHAN_NAIK">
                        <h5 class="orgChart-title">B.1 Revenue Existing - Tagihan Naik</h5>
                        <table class="table table-bordered table_tree" style="margin-bottom:0px !important;min-width:480px;">
                          <thead>
                            <?php 
                              echo "<tr>";
                              echo "<th class='text-center'>".$row_lists[$search['show_data']]."</th>";
                              echo "<th class='text-center'>L11</th>";
                              echo "<th class='text-center'>Revenue</th>";
                              echo "<th class='text-center'>L11 Bln Lalu</th>";
                              echo "<th class='text-center'>Revenue Bln Lalu</th>";
                              echo "</tr>";
                            ?>
                          </thead>
                          <tbody>
                            <?php
                              $total_B1=array("L11"=>0,"revenue"=>0,"L11_lalu"=>0,"revenue_lalu"=>0);
                              foreach($lists as $l){
                                foreach($last_1_lists as $l2){
                                  if($l->{$field[$search['show_data']]}==$l2->{$field[$search['show_data']]} && $l->TAGIHAN>$l2->TAGIHAN)
                                  {
                                    echo "<tr>";
                                    echo "<td>".($l->{$field[$search['show_data']]}!="" && $l->{$field[$search['show_data']]}!=NULL ? $l->{$field[$search['show_data']]} : $no_mapping)."</td>";
                                    echo "<td class='text-right'>".number_format($l->SST,0,".",",")."</td>";
                                    echo "<td class='text-right'>".number_format($l->TAGIHAN,0,".",",")."</td>";
                                    echo "<td class='text-right'>".number_format($l2->SST,0,".",",")."</td>";
                                    echo "<td class='text-right'>".number_format($l2->TAGIHAN,0,".",",")."</td>";
                                    echo "</tr>";
                                    $total_B1['L11']+=$l->SST;
                                    $total_B1['revenue']+=$l->TAGIHAN;
                                    $total_B1['L11_lalu']+=$l2->SST;
                                    $total_B1['revenue_lalu']+=$l2->TAGIHAN;
                                  }
                                }
                              }
                            ?>
                          </tbody>
                          <tfoot>
                            <tr>
                              <td>Total</td>
                              <td class="text-right"><?= number_format($total_B1['L11'],0,".",",") ?></td>
                              <td class="text-right"><?= number_format($total_B1['revenue'],0,".",",") ?></td>
                              <td class="text-right"><?= number_format($total_B1['L11_lalu'],0,".",",") ?></td>
                              <td class="text-right"><?= number_format($total_B1['revenue_lalu'],0,".",",") ?></td>
                            </tr>
                          </tfoot>
                        </table>
                        <div style="display:none;" class="tree_detail">
                          <table class="table table-bordered">
                            <thead>
                              <tr>
                                <th>CCA</th>
                                <th>SND</th>
                                <th>SND Group</th>
                                <th>Produk</th>
                                <th>Tagihan Bulan Lalu</th>
                                <th>Tagihan Sekarang</th>
                                <th>Bisnis Area</th>
                                <th>Umur</th>
                                <th>Usage</th>
                                <th>Paket Speedy</th>
                                <th>Paket FPIP</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php
                                $total_detail_B1=array("now"=>0,"last"=>0);
                                foreach($detail_lists as $l){
                                  foreach($last_1_detail_lists as $l2){
                                    if($l->{$field[$search['show_data']]}==$l2->{$field[$search['show_data']]} && $l->TAGIHAN>$l2->TAGIHAN)
                                    {
                                      echo "<tr>";
                                      echo "<td>".$l->CCA."</td>";
                                      echo "<td>".$l->SND."</td>";
                                      echo "<td>".$l->SND_GROUP."</td>";
                                      echo "<td>".$l->PRODUCT_NAME."</td>";
                                      echo "<td class='text-right'>".number_format($l->TOTAL_LALU,0,".",",")."</td>";
                                      echo "<td class='text-right'>".number_format($l->TOTAL,0,".",",")."</td>";
                                      echo "<td>".$l->BUSINESS_AREA_DESCRIPTION."</td>";
                                      echo "<td>".$l->UMUR_PELANGGAN_DESCRIPTION."</td>";
                                      echo "<td>".$l->USAGE_STATUS."</td>";
                                      echo "<td>".$l->PACKET_SPEEDY_DESCRIPTION."</td>";
                                      echo "<td>".$l->PACKET_FBIP_DESCRIPTION."</td>";
                                      echo "</tr>";
                                      $total_detail_B1['now']+=$l->TOTAL;
                                      $total_detail_B1['last']+=$l->TOTAL_LALU;
                                    }
                                  }
                                }
                              ?>
                            </tbody>
                            <tfoot>
                              <tr>
                                <td colspan="4">Total</td>
                                <td class="text-right"><?= number_format($total_detail_B1['last'],0,".",",") ?></td>
                                <td class="text-right"><?= number_format($total_detail_B1['now'],0,".",",") ?></td>
                                <td colspan="5"></td>
                              </tr>
                            </tfoot>
                          </table>
                        </div>
                      </div>
                      <ul>
                        <?php
                          $status_b=array("status"=>array(),"status_name"=>array());
                          foreach($lists as $l){
                            foreach($last_1x_lists as $l2){
                              if($l->{$field[$search['show_data']]}==$l2->{$field[$search['show_data']]} && $l->TAGIHAN>$l2->TAGIHAN)
                              {
                                if(!in_array($l2->STATUS,$status_b['status'])){
                                  array_push($status_b['status'],$l2->STATUS);
                                  array_push($status_b['status_name'],$l2->TREE_STATUS_NAME);
                                }
                              }
                            }
                          }
                        ?>
                        <?php foreach($status_b['status'] as $key=>$s): ?>
                          <li>
                            <div class="table_tree_tag" data-table_type="REVENUE_EXISTING_TAGIHAN_NAIK_<?= ($key+1) ?>">
                              <h5 class="orgChart-title">B.1.<?= $key+1 ?> Revenue Existing - Tagihan Naik - <?= ($status_b['status_name'][$key]!="" && $status_b['status_name'][$key]!=NULL ? $status_b['status_name'][$key] : $no_mapping) ?></h5>
                              <table class="table table-bordered table_tree" style="margin-bottom:0px !important;min-width:480px;">
                                <thead>
                                  <?php 
                                    echo "<tr>";
                                    echo "<th class='text-center'>".$row_lists[$search['show_data']]."</th>";
                                    echo "<th class='text-center'>L11</th>";
                                    echo "<th class='text-center'>Revenue</th>";
                                    echo "<th class='text-center'>L11 Bln Lalu</th>";
                                    echo "<th class='text-center'>Revenue Bln Lalu</th>";
                                    echo "</tr>";
                                  ?>
                                </thead>
                                <tbody>
                                  <?php
                                    $total_B1_x=array("L11"=>0,"revenue"=>0,"L11_lalu"=>0,"revenue_lalu"=>0);
                                    foreach($lists as $l){
                                      foreach($last_1x_lists as $l2){
                                        if($l->{$field[$search['show_data']]}==$l2->{$field[$search['show_data']]} && $l->TAGIHAN>$l2->TAGIHAN && $l2->STATUS==$s)
                                        {
                                          echo "<tr>";
                                          echo "<td>".($l->{$field[$search['show_data']]}!="" && $l->{$field[$search['show_data']]}!=NULL ? $l->{$field[$search['show_data']]} : $no_mapping)."</td>";
                                          echo "<td class='text-right'>".number_format($l->SST,0,".",",")."</td>";
                                          echo "<td class='text-right'>".number_format($l->TAGIHAN,0,".",",")."</td>";
                                          echo "<td class='text-right'>".number_format($l2->SST,0,".",",")."</td>";
                                          echo "<td class='text-right'>".number_format($l2->TAGIHAN,0,".",",")."</td>";
                                          echo "</tr>";
                                          $total_B1_x['L11']+=$l->SST;
                                          $total_B1_x['revenue']+=$l->TAGIHAN;
                                          $total_B1_x['L11_lalu']+=$l2->SST;
                                          $total_B1_x['revenue_lalu']+=$l2->TAGIHAN;
                                        }
                                      }
                                    }
                                  ?>
                                </tbody>
                                <tfoot>
                                  <tr>
                                    <td>Total</td>
                                    <td class="text-right"><?= number_format($total_B1_x['L11'],0,".",",") ?></td>
                                    <td class="text-right"><?= number_format($total_B1_x['revenue'],0,".",",") ?></td>
                                    <td class="text-right"><?= number_format($total_B1_x['L11_lalu'],0,".",",") ?></td>
                                    <td class="text-right"><?= number_format($total_B1_x['revenue_lalu'],0,".",",") ?></td>
                                  </tr>
                                </tfoot>
                              </table>
                              <div style="display:none;" class="tree_detail">
                                <table class="table table-bordered">
                                  <thead>
                                    <tr>
                                      <th>CCA</th>
                                      <th>SND</th>
                                      <th>SND Group</th>
                                      <th>Produk</th>
                                      <th>Tagihan Bulan Lalu</th>
                                      <th>Tagihan Sekarang</th>
                                      <th>Bisnis Area</th>
                                      <th>Umur</th>
                                      <th>Usage</th>
                                      <th>Paket Speedy</th>
                                      <th>Paket FPIP</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <?php
                                      $total_detail_B1_x=array("now"=>0,"last"=>0);
                                      foreach($detail_lists as $l){
                                        foreach($last_1x_detail_lists as $l2){
                                          if($l->{$field[$search['show_data']]}==$l2->{$field[$search['show_data']]} && $l->TAGIHAN>$l2->TAGIHAN && $l2->STATUS==$s)
                                          {
                                            echo "<tr>";
                                            echo "<td>".$l->CCA."</td>";
                                            echo "<td>".$l->SND."</td>";
                                            echo "<td>".$l->SND_GROUP."</td>";
                                            echo "<td>".$l->PRODUCT_NAME."</td>";
                                            echo "<td class='text-right'>".number_format($l->TOTAL_LALU,0,".",",")."</td>";
                                            echo "<td class='text-right'>".number_format($l->TOTAL,0,".",",")."</td>";
                                            echo "<td>".$l->BUSINESS_AREA_DESCRIPTION."</td>";
                                            echo "<td>".$l->UMUR_PELANGGAN_DESCRIPTION."</td>";
                                            echo "<td>".$l->USAGE_STATUS."</td>";
                                            echo "<td>".$l->PACKET_SPEEDY_DESCRIPTION."</td>";
                                            echo "<td>".$l->PACKET_FBIP_DESCRIPTION."</td>";
                                            echo "</tr>";
                                            $total_detail_B1_x['now']+=$l->TOTAL;
                                            $total_detail_B1_x['last']+=$l->TOTAL_LALU;
                                          }
                                        }
                                      }
                                    ?>
                                  </tbody>
                                  <tfoot>
                                    <tr>
                                      <td colspan="4">Total</td>
                                      <td class="text-right"><?= number_format($total_detail_B1_x['last'],0,".",",") ?></td>
                                      <td class="text-right"><?= number_format($total_detail_B1_x['now'],0,".",",") ?></td>
                                      <td colspan="5"></td>
                                    </tr>
                                  </tfoot>
                                </table>
                              </div>
                            </div>
                          </li>
                        <?php endforeach; ?>
                      </ul>
                    </li>
                    <li>
                      <div class="table_tree_tag" data-table_type="REVENUE_EXISTING_TAGIHAN_TETAP">
                        <h5 class="orgChart-title">B.2 Revenue Existing - Tagihan Tetap</h5>
                        <table class="table table-bordered table_tree" style="margin-bottom:0px !important;min-width:480px;">
                          <thead>
                            <?php 
                              echo "<tr>";
                              echo "<th class='text-center'>".$row_lists[$search['show_data']]."</th>";
                              echo "<th class='text-center'>L11</th>";
                              echo "<th class='text-center'>Revenue</th>";
                              echo "<th class='text-center'>L11 Bln Lalu</th>";
                              echo "<th class='text-center'>Revenue Bln Lalu</th>";
                              echo "</tr>";
                            ?>
                          </thead>
                          <tbody>
                            <?php
                              $total_B2=array("L11"=>0,"revenue"=>0,"L11_lalu"=>0,"revenue_lalu"=>0);
                              foreach($lists as $l){
                                foreach($last_1_lists as $l2){
                                  if($l->{$field[$search['show_data']]}==$l2->{$field[$search['show_data']]} && $l->TAGIHAN==$l2->TAGIHAN)
                                  {
                                    echo "<tr>";
                                    echo "<td>".($l->{$field[$search['show_data']]}!="" && $l->{$field[$search['show_data']]}!=NULL ? $l->{$field[$search['show_data']]} : $no_mapping)."</td>";
                                    echo "<td class='text-right'>".number_format($l->SST,0,".",",")."</td>";
                                    echo "<td class='text-right'>".number_format($l->TAGIHAN,0,".",",")."</td>";
                                    echo "<td class='text-right'>".number_format($l2->SST,0,".",",")."</td>";
                                    echo "<td class='text-right'>".number_format($l2->TAGIHAN,0,".",",")."</td>";
                                    echo "</tr>";
                                    $total_B2['L11']+=$l->SST;
                                    $total_B2['revenue']+=$l->TAGIHAN;
                                    $total_B2['L11_lalu']+=$l2->SST;
                                    $total_B2['revenue_lalu']+=$l2->TAGIHAN;
                                  }
                                }
                              }
                            ?>
                          </tbody>
                          <tfoot>
                            <tr>
                              <td>Total</td>
                              <td class="text-right"><?= number_format($total_B2['L11'],0,".",",") ?></td>
                              <td class="text-right"><?= number_format($total_B2['revenue'],0,".",",") ?></td>
                              <td class="text-right"><?= number_format($total_B2['L11_lalu'],0,".",",") ?></td>
                              <td class="text-right"><?= number_format($total_B2['revenue_lalu'],0,".",",") ?></td>
                            </tr>
                          </tfoot>
                        </table>
                        <div style="display:none;" class="tree_detail">
                          <table class="table table-bordered">
                            <thead>
                              <tr>
                                <th>CCA</th>
                                <th>SND</th>
                                <th>SND Group</th>
                                <th>Produk</th>
                                <th>Tagihan Bulan Lalu</th>
                                <th>Tagihan Sekarang</th>
                                <th>Bisnis Area</th>
                                <th>Umur</th>
                                <th>Usage</th>
                                <th>Paket Speedy</th>
                                <th>Paket FPIP</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php
                                $total_detail_B2=array("now"=>0,"last"=>0);
                                foreach($detail_lists as $l){
                                  foreach($last_1_detail_lists as $l2){
                                    if($l->{$field[$search['show_data']]}==$l2->{$field[$search['show_data']]} && $l->TAGIHAN==$l2->TAGIHAN)
                                    {
                                      echo "<tr>";
                                      echo "<td>".$l->CCA."</td>";
                                      echo "<td>".$l->SND."</td>";
                                      echo "<td>".$l->SND_GROUP."</td>";
                                      echo "<td>".$l->PRODUCT_NAME."</td>";
                                      echo "<td class='text-right'>".number_format($l->TOTAL_LALU,0,".",",")."</td>";
                                      echo "<td class='text-right'>".number_format($l->TOTAL,0,".",",")."</td>";
                                      echo "<td>".$l->BUSINESS_AREA_DESCRIPTION."</td>";
                                      echo "<td>".$l->UMUR_PELANGGAN_DESCRIPTION."</td>";
                                      echo "<td>".$l->USAGE_STATUS."</td>";
                                      echo "<td>".$l->PACKET_SPEEDY_DESCRIPTION."</td>";
                                      echo "<td>".$l->PACKET_FBIP_DESCRIPTION."</td>";
                                      echo "</tr>";
                                      $total_detail_B2['now']+=$l->TOTAL;
                                      $total_detail_B2['last']+=$l->TOTAL_LALU;
                                    }
                                  }
                                }
                              ?>
                            </tbody>
                            <tfoot>
                              <tr>
                                <td colspan="4">Total</td>
                                <td class="text-right"><?= number_format($total_detail_B2['last'],0,".",",") ?></td>
                                <td class="text-right"><?= number_format($total_detail_B2['now'],0,".",",") ?></td>
                                <td colspan="5"></td>
                              </tr>
                            </tfoot>
                          </table>
                        </div>
                      </div>
                      <ul>
                        <?php
                          $status_b=array("status"=>array(),"status_name"=>array());
                          foreach($lists as $l){
                            foreach($last_1x_lists as $l2){
                              if($l->{$field[$search['show_data']]}==$l2->{$field[$search['show_data']]} && $l->TAGIHAN==$l2->TAGIHAN)
                              {
                                if(!in_array($l2->STATUS,$status_b['status'])){
                                  array_push($status_b['status'],$l2->STATUS);
                                  array_push($status_b['status_name'],$l2->TREE_STATUS_NAME);
                                }
                              }
                            }
                          }
                        ?>
                        <?php foreach($status_b['status'] as $key=>$s): ?>
                          <li>
                            <div class="table_tree_tag" data-table_type="REVENUE_EXISTING_TAGIHAN_TETAP_<?= ($key+1) ?>">
                              <h5 class="orgChart-title">B.2.<?= $key+1 ?> Revenue Existing - Tagihan Tetap - <?= ($status_b['status_name'][$key]!="" && $status_b['status_name'][$key]!=NULL ? $status_b['status_name'][$key] : $no_mapping) ?></h5>
                              <table class="table table-bordered table_tree" style="margin-bottom:0px !important;min-width:480px;">
                                <thead>
                                  <?php 
                                    echo "<tr>";
                                    echo "<th class='text-center'>".$row_lists[$search['show_data']]."</th>";
                                    echo "<th class='text-center'>L11</th>";
                                    echo "<th class='text-center'>Revenue</th>";
                                    echo "<th class='text-center'>L11 Bln Lalu</th>";
                                    echo "<th class='text-center'>Revenue Bln Lalu</th>";
                                    echo "</tr>";
                                  ?>
                                </thead>
                                <tbody>
                                  <?php
                                    $total_B2_x=array("L11"=>0,"revenue"=>0,"L11_lalu"=>0,"revenue_lalu"=>0);
                                    foreach($lists as $l){
                                      foreach($last_1x_lists as $l2){
                                        if($l->{$field[$search['show_data']]}==$l2->{$field[$search['show_data']]} && $l->TAGIHAN==$l2->TAGIHAN && $l2->STATUS==$s)
                                        {
                                          echo "<tr>";
                                          echo "<td>".($l->{$field[$search['show_data']]}!="" && $l->{$field[$search['show_data']]}!=NULL ? $l->{$field[$search['show_data']]} : $no_mapping)."</td>";
                                          echo "<td class='text-right'>".number_format($l->SST,0,".",",")."</td>";
                                          echo "<td class='text-right'>".number_format($l->TAGIHAN,0,".",",")."</td>";
                                          echo "<td class='text-right'>".number_format($l2->SST,0,".",",")."</td>";
                                          echo "<td class='text-right'>".number_format($l2->TAGIHAN,0,".",",")."</td>";
                                          echo "</tr>";
                                          $total_B2_x['L11']+=$l->SST;
                                          $total_B2_x['revenue']+=$l->TAGIHAN;
                                          $total_B2_x['L11_lalu']+=$l2->SST;
                                          $total_B2_x['revenue_lalu']+=$l2->TAGIHAN;
                                        }
                                      }
                                    }
                                  ?>
                                </tbody>
                                <tfoot>
                                  <tr>
                                    <td>Total</td>
                                    <td class="text-right"><?= number_format($total_B2_x['L11'],0,".",",") ?></td>
                                    <td class="text-right"><?= number_format($total_B2_x['revenue'],0,".",",") ?></td>
                                    <td class="text-right"><?= number_format($total_B2_x['L11_lalu'],0,".",",") ?></td>
                                    <td class="text-right"><?= number_format($total_B2_x['revenue_lalu'],0,".",",") ?></td>
                                  </tr>
                                </tfoot>
                              </table>
                              <div style="display:none;" class="tree_detail">
                                <table class="table table-bordered">
                                  <thead>
                                    <tr>
                                      <th>CCA</th>
                                      <th>SND</th>
                                      <th>SND Group</th>
                                      <th>Produk</th>
                                      <th>Tagihan Bulan Lalu</th>
                                      <th>Tagihan Sekarang</th>
                                      <th>Bisnis Area</th>
                                      <th>Umur</th>
                                      <th>Usage</th>
                                      <th>Paket Speedy</th>
                                      <th>Paket FPIP</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <?php
                                      $total_detail_B2_x=array("now"=>0,"last"=>0);
                                      foreach($detail_lists as $l){
                                        foreach($last_1x_detail_lists as $l2){
                                          if($l->{$field[$search['show_data']]}==$l2->{$field[$search['show_data']]} && $l->TAGIHAN==$l2->TAGIHAN && $l2->STATUS==$s)
                                          {
                                            echo "<tr>";
                                            echo "<td>".$l->CCA."</td>";
                                            echo "<td>".$l->SND."</td>";
                                            echo "<td>".$l->SND_GROUP."</td>";
                                            echo "<td>".$l->PRODUCT_NAME."</td>";
                                            echo "<td class='text-right'>".number_format($l->TOTAL_LALU,0,".",",")."</td>";
                                            echo "<td class='text-right'>".number_format($l->TOTAL,0,".",",")."</td>";
                                            echo "<td>".$l->BUSINESS_AREA_DESCRIPTION."</td>";
                                            echo "<td>".$l->UMUR_PELANGGAN_DESCRIPTION."</td>";
                                            echo "<td>".$l->USAGE_STATUS."</td>";
                                            echo "<td>".$l->PACKET_SPEEDY_DESCRIPTION."</td>";
                                            echo "<td>".$l->PACKET_FBIP_DESCRIPTION."</td>";
                                            echo "</tr>";
                                            $total_detail_B2_x['now']+=$l->TOTAL;
                                            $total_detail_B2_x['last']+=$l->TOTAL_LALU;
                                          }
                                        }
                                      }
                                    ?>
                                  </tbody>
                                  <tfoot>
                                    <tr>
                                      <td colspan="4">Total</td>
                                      <td class="text-right"><?= number_format($total_detail_B2_x['last'],0,".",",") ?></td>
                                      <td class="text-right"><?= number_format($total_detail_B2_x['now'],0,".",",") ?></td>
                                      <td colspan="5"></td>
                                    </tr>
                                  </tfoot>
                                </table>
                              </div>
                            </div>
                          </li>
                        <?php endforeach; ?>
                      </ul>
                    </li>
                    <li>
                      <div class="table_tree_tag" data-table_type="REVENUE_EXISTING_TAGIHAN_TURUN">
                        <h5 class="orgChart-title">B.3 Revenue Existing - Tagihan Turun</h5>
                        <table class="table table-bordered table_tree" style="margin-bottom:0px !important;min-width:480px;">
                          <thead>
                            <?php 
                              echo "<tr>";
                              echo "<th class='text-center'>".$row_lists[$search['show_data']]."</th>";
                              echo "<th class='text-center'>L11</th>";
                              echo "<th class='text-center'>Revenue</th>";
                              echo "<th class='text-center'>L11 Bln Lalu</th>";
                              echo "<th class='text-center'>Revenue Bln Lalu</th>";
                              echo "</tr>";
                            ?>
                          </thead>
                          <tbody>
                            <?php
                              $total_B3=array("L11"=>0,"revenue"=>0,"L11_lalu"=>0,"revenue_lalu"=>0);
                              foreach($lists as $l){
                                foreach($last_1_lists as $l2){
                                  if($l->{$field[$search['show_data']]}==$l2->{$field[$search['show_data']]} && $l->TAGIHAN<$l2->TAGIHAN)
                                  {
                                    echo "<tr>";
                                    echo "<td>".($l->{$field[$search['show_data']]}!="" && $l->{$field[$search['show_data']]}!=NULL ? $l->{$field[$search['show_data']]} : $no_mapping)."</td>";
                                    echo "<td class='text-right'>".number_format($l->SST,0,".",",")."</td>";
                                    echo "<td class='text-right'>".number_format($l->TAGIHAN,0,".",",")."</td>";
                                    echo "<td class='text-right'>".number_format($l2->SST,0,".",",")."</td>";
                                    echo "<td class='text-right'>".number_format($l2->TAGIHAN,0,".",",")."</td>";
                                    echo "</tr>";
                                    $total_B3['L11']+=$l->SST;
                                    $total_B3['revenue']+=$l->TAGIHAN;
                                    $total_B3['L11_lalu']+=$l2->SST;
                                    $total_B3['revenue_lalu']+=$l2->TAGIHAN;
                                  }
                                }
                              }
                            ?>
                          </tbody>
                          <tfoot>
                            <tr>
                              <td>Total</td>
                              <td class="text-right"><?= number_format($total_B3['L11'],0,".",",") ?></td>
                              <td class="text-right"><?= number_format($total_B3['revenue'],0,".",",") ?></td>
                              <td class="text-right"><?= number_format($total_B3['L11_lalu'],0,".",",") ?></td>
                              <td class="text-right"><?= number_format($total_B3['revenue_lalu'],0,".",",") ?></td>
                            </tr>
                          </tfoot>
                        </table>
                        <div style="display:none;" class="tree_detail">
                          <table class="table table-bordered">
                            <thead>
                              <tr>
                                <th>CCA</th>
                                <th>SND</th>
                                <th>SND Group</th>
                                <th>Produk</th>
                                <th>Tagihan Bulan Lalu</th>
                                <th>Tagihan Sekarang</th>
                                <th>Bisnis Area</th>
                                <th>Umur</th>
                                <th>Usage</th>
                                <th>Paket Speedy</th>
                                <th>Paket FPIP</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php
                                $total_detail_B3=array("now"=>0,"last"=>0);
                                foreach($detail_lists as $l){
                                  foreach($last_1_detail_lists as $l2){
                                    if($l->{$field[$search['show_data']]}==$l2->{$field[$search['show_data']]} && $l->TAGIHAN<$l2->TAGIHAN)
                                    {
                                      echo "<tr>";
                                      echo "<td>".$l->CCA."</td>";
                                      echo "<td>".$l->SND."</td>";
                                      echo "<td>".$l->SND_GROUP."</td>";
                                      echo "<td>".$l->PRODUCT_NAME."</td>";
                                      echo "<td class='text-right'>".number_format($l->TOTAL_LALU,0,".",",")."</td>";
                                      echo "<td class='text-right'>".number_format($l->TOTAL,0,".",",")."</td>";
                                      echo "<td>".$l->BUSINESS_AREA_DESCRIPTION."</td>";
                                      echo "<td>".$l->UMUR_PELANGGAN_DESCRIPTION."</td>";
                                      echo "<td>".$l->USAGE_STATUS."</td>";
                                      echo "<td>".$l->PACKET_SPEEDY_DESCRIPTION."</td>";
                                      echo "<td>".$l->PACKET_FBIP_DESCRIPTION."</td>";
                                      echo "</tr>";
                                      $total_detail_B3['now']+=$l->TOTAL;
                                      $total_detail_B3['last']+=$l->TOTAL_LALU;
                                    }
                                  }
                                }
                              ?>
                            </tbody>
                            <tfoot>
                              <tr>
                                <td colspan="4">Total</td>
                                <td class="text-right"><?= number_format($total_detail_B3['last'],0,".",",") ?></td>
                                <td class="text-right"><?= number_format($total_detail_B3['now'],0,".",",") ?></td>
                                <td colspan="5"></td>
                              </tr>
                            </tfoot>
                          </table>
                        </div>
                      </div>
                      <ul>
                        <?php
                          $status_b=array("status"=>array(),"status_name"=>array());
                          foreach($lists as $l){
                            foreach($last_1x_lists as $l2){
                              if($l->{$field[$search['show_data']]}==$l2->{$field[$search['show_data']]} && $l->TAGIHAN<$l2->TAGIHAN)
                              {
                                if(!in_array($l2->STATUS,$status_b['status'])){
                                  array_push($status_b['status'],$l2->STATUS);
                                  array_push($status_b['status_name'],$l2->TREE_STATUS_NAME);
                                }
                              }
                            }
                          }
                        ?>
                        <?php foreach($status_b['status'] as $key=>$s): ?>
                          <li>
                            <div class="table_tree_tag" data-table_type="REVENUE_EXISTING_TAGIHAN_TURUN_<?= ($key+1) ?>">
                              <h5 class="orgChart-title">B.3.<?= $key+1 ?> Revenue Existing - Tagihan Turun - <?= ($status_b['status_name'][$key]!="" && $status_b['status_name'][$key]!=NULL ? $status_b['status_name'][$key] : $no_mapping) ?></h5>
                              <table class="table table-bordered table_tree" style="margin-bottom:0px !important;min-width:480px;">
                                <thead>
                                  <?php 
                                    echo "<tr>";
                                    echo "<th class='text-center'>".$row_lists[$search['show_data']]."</th>";
                                    echo "<th class='text-center'>L11</th>";
                                    echo "<th class='text-center'>Revenue</th>";
                                    echo "<th class='text-center'>L11 Bln Lalu</th>";
                                    echo "<th class='text-center'>Revenue Bln Lalu</th>";
                                    echo "</tr>";
                                  ?>
                                </thead>
                                <tbody>
                                  <?php
                                    $total_B3_x=array("L11"=>0,"revenue"=>0,"L11_lalu"=>0,"revenue_lalu"=>0);
                                    foreach($lists as $l){
                                      foreach($last_1x_lists as $l2){
                                        if($l->{$field[$search['show_data']]}==$l2->{$field[$search['show_data']]} && $l->TAGIHAN<$l2->TAGIHAN && $l2->STATUS==$s)
                                        {
                                          echo "<tr>";
                                          echo "<td>".($l->{$field[$search['show_data']]}!="" && $l->{$field[$search['show_data']]}!=NULL ? $l->{$field[$search['show_data']]} : $no_mapping)."</td>";
                                          echo "<td class='text-right'>".number_format($l->SST,0,".",",")."</td>";
                                          echo "<td class='text-right'>".number_format($l->TAGIHAN,0,".",",")."</td>";
                                          echo "<td class='text-right'>".number_format($l2->SST,0,".",",")."</td>";
                                          echo "<td class='text-right'>".number_format($l2->TAGIHAN,0,".",",")."</td>";
                                          echo "</tr>";
                                          $total_B3_x['L11']+=$l->SST;
                                          $total_B3_x['revenue']+=$l->TAGIHAN;
                                          $total_B3_x['L11_lalu']+=$l2->SST;
                                          $total_B3_x['revenue_lalu']+=$l2->TAGIHAN;
                                        }
                                      }
                                    }
                                  ?>
                                </tbody>
                                <tfoot>
                                  <tr>
                                    <td>Total</td>
                                    <td class="text-right"><?= number_format($total_B3_x['L11'],0,".",",") ?></td>
                                    <td class="text-right"><?= number_format($total_B3_x['revenue'],0,".",",") ?></td>
                                    <td class="text-right"><?= number_format($total_B3_x['L11_lalu'],0,".",",") ?></td>
                                    <td class="text-right"><?= number_format($total_B3_x['revenue_lalu'],0,".",",") ?></td>
                                  </tr>
                                </tfoot>
                              </table>
                              <div style="display:none;" class="tree_detail">
                                <table class="table table-bordered">
                                  <thead>
                                    <tr>
                                      <th>CCA</th>
                                      <th>SND</th>
                                      <th>SND Group</th>
                                      <th>Produk</th>
                                      <th>Tagihan Bulan Lalu</th>
                                      <th>Tagihan Sekarang</th>
                                      <th>Bisnis Area</th>
                                      <th>Umur</th>
                                      <th>Usage</th>
                                      <th>Paket Speedy</th>
                                      <th>Paket FPIP</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <?php
                                      $total_detail_B3_x=array("now"=>0,"last"=>0);
                                      foreach($detail_lists as $l){
                                        foreach($last_1x_detail_lists as $l2){
                                          if($l->{$field[$search['show_data']]}==$l2->{$field[$search['show_data']]} && $l->TAGIHAN<$l2->TAGIHAN && $l2->STATUS==$s)
                                          {
                                            echo "<tr>";
                                            echo "<td>".$l->CCA."</td>";
                                            echo "<td>".$l->SND."</td>";
                                            echo "<td>".$l->SND_GROUP."</td>";
                                            echo "<td>".$l->PRODUCT_NAME."</td>";
                                            echo "<td class='text-right'>".number_format($l->TOTAL_LALU,0,".",",")."</td>";
                                            echo "<td class='text-right'>".number_format($l->TOTAL,0,".",",")."</td>";
                                            echo "<td>".$l->BUSINESS_AREA_DESCRIPTION."</td>";
                                            echo "<td>".$l->UMUR_PELANGGAN_DESCRIPTION."</td>";
                                            echo "<td>".$l->USAGE_STATUS."</td>";
                                            echo "<td>".$l->PACKET_SPEEDY_DESCRIPTION."</td>";
                                            echo "<td>".$l->PACKET_FBIP_DESCRIPTION."</td>";
                                            echo "</tr>";
                                            $total_detail_B3_x['now']+=$l->TOTAL;
                                            $total_detail_B3_x['last']+=$l->TOTAL_LALU;
                                          }
                                        }
                                      }
                                    ?>
                                  </tbody>
                                  <tfoot>
                                    <tr>
                                      <td colspan="4">Total</td>
                                      <td class="text-right"><?= number_format($total_detail_B3_x['last'],0,".",",") ?></td>
                                      <td class="text-right"><?= number_format($total_detail_B3_x['now'],0,".",",") ?></td>
                                      <td colspan="5"></td>
                                    </tr>
                                  </tfoot>
                                </table>
                              </div>
                            </div>
                          </li>
                        <?php endforeach; ?>
                      </ul>
                    </li>
                  </ul>
                </li>
                <li>
                  <div class="table_tree_tag" data-table_type="NEW_BILLING">
                    <h5 class="orgChart-title">C. New Billing</h5>
                    <table class="table table-bordered table_tree" style="margin-bottom:0px !important;min-width:300px;">
                      <thead>
                        <?php 
                          echo "<tr>";
                          echo "<th class='text-center'>".$row_lists[$search['show_data']]."</th>";
                          echo "<th class='text-center'>L11</th>";
                          echo "<th class='text-center'>Revenue</th>";
                          echo "</tr>";
                        ?>
                      </thead>
                      <tbody>
                        <?php
                          $total_C=array("L11"=>0,"revenue"=>0);
                          foreach($lists as $l){
                            $check=0;
                            foreach($last_1_lists as $l2){
                              if($l->{$field[$search['show_data']]}==$l2->{$field[$search['show_data']]})
                              {
                                $check++;
                              }
                            }
                            if($check==0){
                              echo "<tr>";
                              echo "<td>".($l->{$field[$search['show_data']]}!="" && $l->{$field[$search['show_data']]}!=NULL ? $l->{$field[$search['show_data']]} : $no_mapping)."</td>";
                              echo "<td class='text-right'>".number_format($l->SST,0,".",",")."</td>";
                              echo "<td class='text-right'>".number_format($l->TAGIHAN,0,".",",")."</td>";
                              echo "</tr>";
                              $total_C['L11']+=$l->SST;
                              $total_C['revenue']+=$l->TAGIHAN;
                            }
                          }
                        ?>
                      </tbody>
                      <tfoot>
                        <tr>
                          <td>Total</td>
                          <td class="text-right"><?= number_format($total_C['L11'],0,".",",") ?></td>
                          <td class="text-right"><?= number_format($total_C['revenue'],0,".",",") ?></td>
                        </tr>
                      </tfoot>
                    </table>
                    <div style="display:none;" class="tree_detail">
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>CCA</th>
                            <th>SND</th>
                            <th>SND Group</th>
                            <th>Produk</th>
                            <th>Tagihan Bulan Lalu</th>
                            <th>Tagihan Sekarang</th>
                            <th>Bisnis Area</th>
                            <th>Umur</th>
                            <th>Usage</th>
                            <th>Paket Speedy</th>
                            <th>Paket FPIP</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                            $total_detail_C=array("now"=>0,"last"=>0);
                            foreach($detail_lists as $l){
                              $check=0;
                              foreach($last_1_detail_lists as $l2){
                                if($l->{$field[$search['show_data']]}==$l2->{$field[$search['show_data']]})
                                {
                                  $check++;
                                }
                              }
                              if($check==0){
                                echo "<tr>";
                                echo "<td>".$l->CCA."</td>";
                                echo "<td>".$l->SND."</td>";
                                echo "<td>".$l->SND_GROUP."</td>";
                                echo "<td>".$l->PRODUCT_NAME."</td>";
                                echo "<td class='text-right'>".number_format($l->TOTAL_LALU,0,".",",")."</td>";
                                echo "<td class='text-right'>".number_format($l->TOTAL,0,".",",")."</td>";
                                echo "<td>".$l->BUSINESS_AREA_DESCRIPTION."</td>";
                                echo "<td>".$l->UMUR_PELANGGAN_DESCRIPTION."</td>";
                                echo "<td>".$l->USAGE_STATUS."</td>";
                                echo "<td>".$l->PACKET_SPEEDY_DESCRIPTION."</td>";
                                echo "<td>".$l->PACKET_FBIP_DESCRIPTION."</td>";
                                echo "</tr>";
                                $total_detail_C['now']+=$l->TOTAL;
                                $total_detail_C['last']+=$l->TOTAL_LALU;
                              }
                            }
                          ?>
                        </tbody>
                        <tfoot>
                          <tr>
                            <td colspan="4">Total</td>
                            <td class="text-right"><?= number_format($total_detail_C['last'],0,".",",") ?></td>
                            <td class="text-right"><?= number_format($total_detail_C['now'],0,".",",") ?></td>
                            <td colspan="5"></td>
                          </tr>
                        </tfoot>
                      </table>
                    </div>
                  </div>
                  <ul>
                    <?php
                      $data_new_billing2=array();
                      $data_new_billing2_detail=array();
                      $status_c=array("status"=>array(),"status_name"=>array());
                      foreach($x_lists as $l){
                        $check=0;
                        foreach($last_1_lists as $l2){
                          if($l->{$field[$search['show_data']]}==$l2->{$field[$search['show_data']]})
                          {
                            $check++;
                          }
                        }
                        if($check==0){
                          if(!in_array($l->STATUS,$status_c['status'])){
                            array_push($status_c['status'],$l->STATUS);
                            array_push($status_c['status_name'],$l->TREE_STATUS_NAME);
                          } 
                          array_push($data_new_billing2,$l);
                        }
                      }
                      foreach($x_detail_lists as $l){
                        $check=0;
                        foreach($last_1_detail_lists as $l2){
                          if($l->{$field[$search['show_data']]}==$l2->{$field[$search['show_data']]})
                          {
                            $check++;
                          }
                        }
                        if($check==0){
                          array_push($data_new_billing2_detail,$l);
                        }
                      }
                    ?>
                    <?php foreach($status_c['status'] as $key=>$s): ?>
                    <li>
                      <div class="table_tree_tag" data-table_type="NEW_BILLING_<?= ($key+1) ?>">
                        <h5 class="orgChart-title">C.<?= $key+1 ?> New Billing - <?= ($status_c['status_name'][$key]!="" && $status_c['status_name'][$key]!=NULL ? $status_c['status_name'][$key] : $no_mapping) ?></h5>
                        <table class="table table-bordered table_tree" style="margin-bottom:0px !important;min-width:300px;">
                          <thead>
                            <?php 
                              echo "<tr>";
                              echo "<th class='text-center'>".$row_lists[$search['show_data']]."</th>";
                              echo "<th class='text-center'>L11</th>";
                              echo "<th class='text-center'>Revenue</th>";
                              echo "</tr>";
                            ?>
                          </thead>
                          <tbody>
                            <?php
                              $total_C1=array("L11"=>0,"revenue"=>0);
                              foreach($data_new_billing2 as $l){
                                if($l->STATUS==$s){
                                  echo "<tr>";
                                  echo "<td>".($l->{$field[$search['show_data']]}!="" && $l->{$field[$search['show_data']]}!=NULL ? $l->{$field[$search['show_data']]} : $no_mapping)."</td>";
                                  echo "<td class='text-right'>".number_format($l->SST,0,".",",")."</td>";
                                  echo "<td class='text-right'>".number_format($l->TAGIHAN,0,".",",")."</td>";
                                  echo "</tr>";
                                  $total_C1['L11']+=$l->SST;
                                  $total_C1['revenue']+=$l->TAGIHAN;
                                }
                              }
                            ?>
                          </tbody>
                          <tfoot>
                            <tr>
                              <td>Total</td>
                              <td class="text-right"><?= number_format($total_C1['L11'],0,".",",") ?></td>
                              <td class="text-right"><?= number_format($total_C1['revenue'],0,".",",") ?></td>
                            </tr>
                          </tfoot>
                        </table>
                        <div style="display:none;" class="tree_detail">
                          <table class="table table-bordered">
                            <thead>
                              <tr>
                                <th>CCA</th>
                                <th>SND</th>
                                <th>SND Group</th>
                                <th>Produk</th>
                                <th>Tagihan Bulan Lalu</th>
                                <th>Tagihan Sekarang</th>
                                <th>Bisnis Area</th>
                                <th>Umur</th>
                                <th>Usage</th>
                                <th>Paket Speedy</th>
                                <th>Paket FPIP</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php
                                $total_detail_C1=array("now"=>0,"last"=>0);
                                foreach($data_new_billing2_detail as $l){
                                  if($l->STATUS==$s){
                                    echo "<tr>";
                                    echo "<td>".$l->CCA."</td>";
                                    echo "<td>".$l->SND."</td>";
                                    echo "<td>".$l->SND_GROUP."</td>";
                                    echo "<td>".$l->PRODUCT_NAME."</td>";
                                    echo "<td class='text-right'>".number_format($l->TOTAL_LALU,0,".",",")."</td>";
                                    echo "<td class='text-right'>".number_format($l->TOTAL,0,".",",")."</td>";
                                    echo "<td>".$l->BUSINESS_AREA_DESCRIPTION."</td>";
                                    echo "<td>".$l->UMUR_PELANGGAN_DESCRIPTION."</td>";
                                    echo "<td>".$l->USAGE_STATUS."</td>";
                                    echo "<td>".$l->PACKET_SPEEDY_DESCRIPTION."</td>";
                                    echo "<td>".$l->PACKET_FBIP_DESCRIPTION."</td>";
                                    echo "</tr>";
                                    $total_detail_C1['now']+=$l->TOTAL;
                                    $total_detail_C1['last']+=$l->TOTAL_LALU;
                                  }
                                }
                              ?>
                            </tbody>
                            <tfoot>
                              <tr>
                                <td colspan="4">Total</td>
                                <td class="text-right"><?= number_format($total_detail_C1['last'],0,".",",") ?></td>
                                <td class="text-right"><?= number_format($total_detail_C1['now'],0,".",",") ?></td>
                                <td colspan="5"></td>
                              </tr>
                            </tfoot>
                          </table>
                        </div>
                      </div>
                    </li>
                    <?php endforeach; ?>
                  </ul>
                </li>
              </ul>
            </li>
            <li>
              <div class="table_tree_tag" data-table_type="REVENUE_BULAN_LALU_TIDAK_BULAN_INI">
                <h5 class="orgChart-title">X. Revenue Bulan Lalu Tidak Bulan Ini</h5>
                <table class="table table-bordered table_tree" style="margin-bottom:0px !important;min-width:300px;">
                  <thead>
                    <?php 
                      echo "<tr>";
                      echo "<th class='text-center'>".$row_lists[$search['show_data']]."</th>";
                      echo "<th class='text-center'>L11</th>";
                      echo "<th class='text-center'>Revenue</th>";
                      echo "</tr>";
                    ?>
                  </thead>
                  <tbody>
                    <?php
                      $total_X=array("L11"=>0,"revenue"=>0);
                      foreach($last_1_lists as $l){
                        $check=0;
                        foreach($lists as $l2){
                          if($l->{$field[$search['show_data']]}==$l2->{$field[$search['show_data']]})
                          {
                            $check++;
                          }
                        }
                        if($check==0){
                          echo "<tr>";
                          echo "<td>".($l->{$field[$search['show_data']]}!="" && $l->{$field[$search['show_data']]}!=NULL ? $l->{$field[$search['show_data']]} : $no_mapping)."</td>";
                          echo "<td class='text-right'>".number_format($l->SST,0,".",",")."</td>";
                          echo "<td class='text-right'>".number_format($l->TAGIHAN,0,".",",")."</td>";
                          echo "</tr>";
                          $total_X['L11']+=$l->SST;
                          $total_X['revenue']+=$l->TAGIHAN;
                        }
                      }
                    ?>
                  </tbody>
                  <tfoot>
                    <tr>
                      <td>Total</td>
                      <td class="text-right"><?= number_format($total_X['L11'],0,".",",") ?></td>
                      <td class="text-right"><?= number_format($total_X['revenue'],0,".",",") ?></td>
                    </tr>
                  </tfoot>
                </table>
                <div style="display:none;" class="tree_detail">
                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th>CCA</th>
                        <th>SND</th>
                        <th>SND Group</th>
                        <th>Produk</th>
                        <th>Tagihan Bulan Lalu</th>
                        <th>Tagihan Sekarang</th>
                        <th>Bisnis Area</th>
                        <th>Umur</th>
                        <th>Usage</th>
                        <th>Paket Speedy</th>
                        <th>Paket FPIP</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                        $total_detail_X=array("now"=>0,"last"=>0);
                        foreach($last_1_detail_lists as $l){
                          $check=0;
                          foreach($detail_lists as $l2){
                            if($l->{$field[$search['show_data']]}==$l2->{$field[$search['show_data']]})
                            {
                              $check++;
                            }
                          }
                          if($check==0){
                            echo "<tr>";
                            echo "<td>".$l->CCA."</td>";
                            echo "<td>".$l->SND."</td>";
                            echo "<td>".$l->SND_GROUP."</td>";
                            echo "<td>".$l->PRODUCT_NAME."</td>";
                            echo "<td class='text-right'>".number_format($l->TOTAL_LALU,0,".",",")."</td>";
                            echo "<td class='text-right'>".number_format($l->TOTAL,0,".",",")."</td>";
                            echo "<td>".$l->BUSINESS_AREA_DESCRIPTION."</td>";
                            echo "<td>".$l->UMUR_PELANGGAN_DESCRIPTION."</td>";
                            echo "<td>".$l->USAGE_STATUS."</td>";
                            echo "<td>".$l->PACKET_SPEEDY_DESCRIPTION."</td>";
                            echo "<td>".$l->PACKET_FBIP_DESCRIPTION."</td>";
                            echo "</tr>";
                            $total_detail_X['now']+=$l->TOTAL;
                            $total_detail_X['last']+=$l->TOTAL_LALU;
                          }
                        }
                      ?>
                    </tbody>
                    <tfoot>
                      <tr>
                        <td colspan="4">Total</td>
                        <td class="text-right"><?= number_format($total_detail_X['last'],0,".",",") ?></td>
                        <td class="text-right"><?= number_format($total_detail_X['now'],0,".",",") ?></td>
                        <td colspan="5"></td>
                      </tr>
                    </tfoot>
                  </table>
                </div>
              </div>
              <ul>
                <?php
                  $status_x=array("status"=>array(),"status_name"=>array());
                  $data_x=array();
                  $data_x_detail=array();
                  foreach($last_1x_lists as $l){
                    $check=0;
                    foreach($lists as $l2){
                      if($l->{$field[$search['show_data']]}==$l2->{$field[$search['show_data']]})
                      {
                        $check++;
                      }
                    }
                    if($check==0){
                      if(!in_array($l->STATUS,$status_x['status'])){
                        array_push($status_x['status'],$l->STATUS);
                        array_push($status_x['status_name'],$l->TREE_STATUS_NAME);
                      }
                      array_push($data_x,$l);
                    }
                  }
                  foreach($last_1x_detail_lists as $l){
                    $check=0;
                    foreach($detail_lists as $l2){
                      if($l->{$field[$search['show_data']]}==$l2->{$field[$search['show_data']]})
                      {
                        $check++;
                      }
                    }
                    if($check==0){
                      array_push($data_x_detail,$l);
                    }
                  }
                ?>
                <?php foreach($status_x['status'] as $key=>$s): ?>
                <li>
                  <div class="table_tree_tag" data-table_type="REVENUE_BULAN_LALU_TIDAK_BULAN_INI_<?= ($key+1) ?>">
                    <h5 class="orgChart-title">X.<?= $key+1 ?> <?= ($status_x['status_name'][$key]!="" && $status_x['status_name'][$key]!=NULL ? $status_x['status_name'][$key] : $no_mapping) ?></h5>
                    <table class="table table-bordered table_tree" style="margin-bottom:0px !important;min-width:300px;">
                      <thead>
                        <?php 
                          echo "<tr>";
                          echo "<th class='text-center'>".$row_lists[$search['show_data']]."</th>";
                          echo "<th class='text-center'>L11</th>";
                          echo "<th class='text-center'>Revenue</th>";
                          echo "</tr>";
                        ?>
                      </thead>
                      <tbody>
                        <?php 
                          $total_X_1=array("L11"=>0,"revenue"=>0);
                          foreach($data_x as $d){
                            if($d->STATUS==$s){
                              echo "<tr>";
                              echo "<td>".($d->{$field[$search['show_data']]}!="" && $d->{$field[$search['show_data']]}!=NULL ? $d->{$field[$search['show_data']]} : $no_mapping)."</td>";
                              echo "<td class='text-right'>".number_format($d->SST,0,".",",")."</td>";
                              echo "<td class='text-right'>".number_format($d->TAGIHAN,0,".",",")."</td>";
                              echo "</tr>";
                              $total_X_1['L11']+=$d->SST;
                              $total_X_1['revenue']+=$d->TAGIHAN;
                            }
                          }
                        ?>
                      </tbody>
                      <tfoot>
                        <tr>
                          <td>Total</td>
                          <td class="text-right"><?= number_format($total_X_1['L11'],0,".",",") ?></td>
                          <td class="text-right"><?= number_format($total_X_1['revenue'],0,".",",") ?></td>
                        </tr>
                      </tfoot>
                    </table>
                    <div style="display:none;" class="tree_detail">
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>CCA</th>
                            <th>SND</th>
                            <th>SND Group</th>
                            <th>Produk</th>
                            <th>Tagihan Bulan Lalu</th>
                            <th>Tagihan Sekarang</th>
                            <th>Bisnis Area</th>
                            <th>Umur</th>
                            <th>Usage</th>
                            <th>Paket Speedy</th>
                            <th>Paket FPIP</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                            $total_detail_X1=array("now"=>0,"last"=>0);
                            foreach($data_x_detail as $d){
                              if($d->STATUS==$s){
                                echo "<tr>";
                                echo "<td>".$l->CCA."</td>";
                                echo "<td>".$l->SND."</td>";
                                echo "<td>".$l->SND_GROUP."</td>";
                                echo "<td>".$l->PRODUCT_NAME."</td>";
                                echo "<td class='text-right'>".number_format($l->TOTAL_LALU,0,".",",")."</td>";
                                echo "<td class='text-right'>".number_format($l->TOTAL,0,".",",")."</td>";
                                echo "<td>".$l->BUSINESS_AREA_DESCRIPTION."</td>";
                                echo "<td>".$l->UMUR_PELANGGAN_DESCRIPTION."</td>";
                                echo "<td>".$l->USAGE_STATUS."</td>";
                                echo "<td>".$l->PACKET_SPEEDY_DESCRIPTION."</td>";
                                echo "<td>".$l->PACKET_FBIP_DESCRIPTION."</td>";
                                echo "</tr>";
                                $total_detail_X1['now']+=$l->TOTAL;
                                $total_detail_X1['last']+=$l->TOTAL_LALU;
                              }
                            }
                          ?>
                        </tbody>
                        <tfoot>
                          <tr>
                            <td colspan="4">Total</td>
                            <td class="text-right"><?= number_format($total_detail_X1['last'],0,".",",") ?></td>
                            <td class="text-right"><?= number_format($total_detail_X1['now'],0,".",",") ?></td>
                            <td colspan="5"></td>
                          </tr>
                        </tfoot>
                      </table>
                    </div>
                  </div>
                </li>
                <?php endforeach; ?>
              </ul>
            </li>
          </ul>
        </li>
      </ul>
      <div id="tree_main"></div>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    $("#render").click(function(){
      $(".billing-detail").css('width',4000);
      html2canvas(document.getElementById("tree_main"), {
        background:"darkgray",
        onrendered: function(canvas) {
          $("#download").attr("href",canvas.toDataURL()).removeClass("hide");
          $(".billing-detail").css('width',"auto");
        }
      });
    });
    $("#tree").orgChart({container: $("#tree_main"), interactive: true, fade: true, speed: 'slow',showLevels:2});
    $(".table_tree").dataTable({
      "bSort" : true,
      "bFilter" : false,
      "bPaginate": false,
      "info": false,
    });
    $("#division").multiselect({
      includeSelectAllOption: true,
      numberDisplayed:2
    }).change(function(){
      var divisions=$(this).val();
      $.ajax({
        url:"<?= base_url("ajax/getWitel") ?>",
        dataType:"JSON",
        data:{divisions:divisions},
        success:function(response){
          $("#witel").multiselect("destroy");
          $("#witel").html(response.content);
          $("#witel").multiselect({
            includeSelectAllOption: true,
            maxHeight: 212,
            enableFiltering: true
          }).change();
        }
      });
    });
    $("#summary_product").multiselect({
      includeSelectAllOption: true,
      numberDisplayed:2
    }).change(function(){
      var summary_products=$(this).val();
      $.ajax({
        url:"<?= base_url("ajax/getProducts") ?>",
        dataType:"JSON",
        data:{summary_products:summary_products},
        success:function(response){
          $("#product").multiselect("destroy");
          $("#product").html(response.content);
          $("#product").multiselect({
            includeSelectAllOption: true,
            maxHeight: 212
          });
        }
      });
    });
    $("#ubis").multiselect({
      dropRight: true,
      includeSelectAllOption: true,
      numberDisplayed:2
    }).change(function(){
      var ubis=$(this).val();
      $.ajax({
        url:"<?= base_url("ajax/getUbisSegments") ?>",
        dataType:"JSON",
        data:{ubis:ubis},
        success:function(response){
          $("#ubis_segment").multiselect("destroy");
          $("#ubis_segment").html(response.content);
          $("#ubis_segment").multiselect({
            dropRight: true,
            includeSelectAllOption: true,
            maxHeight: 212,
            enableFiltering: true
          });
        }
      });
    });
    $("#witel,#datel,#product").multiselect({numberDisplayed:2,maxHeight: 212,includeSelectAllOption: true,enableFiltering: true});
    $("#ubis_segment").multiselect({dropRight: true,numberDisplayed:2,maxHeight: 212,includeSelectAllOption: true,enableFiltering: true});
    $("#top_customer").multiselect({
      onChange: function(element, checked) {
        if(checked==true){
          value=$(element).val();
          if(value=="SILVER"){
            $("#top_customer option:selected[value='GOLD']").removeAttr("selected");
            $("#top_customer option:selected[value='PLATIN']").removeAttr("selected");
            $("#top_customer option:selected[value='TITAN']").removeAttr("selected");
          }else{
            $("#top_customer option:selected[value='SILVER']").removeAttr("selected");
          }
          $("#top_customer").multiselect('refresh');
        }
      }
    });
  });
  $(".table_tree tbody tr[role='row'] td").live('click',function(){
    var table_type=$(this).parents(".table_tree_tag").data("table_type");
    var table=$(this).parents(".table_tree_tag").find(".tree_detail").html();
    var data={table_type:table_type,content:table,data:$("#form_tree").serializeObject()};
    $("#mainModal .modal-dialog").css("width","95%");
    main.showModal("<?= base_url("ajax/getDetailRevenueTree") ?>","#mainModal",".modal-content","POST",data);
    
  });
  $("#witel").live("change",function(){
    var divisions=$("#division").val();
    var witels=$(this).val();
    $.ajax({
      url:"<?= base_url("ajax/getDatel") ?>",
      dataType:"JSON",
      data:{witels:witels,divisions:divisions},
      success:function(response){
        $("#datel").multiselect("destroy");
        $("#datel").html(response.content);
        $("#datel").multiselect({
          includeSelectAllOption: true,
          maxHeight: 212,
          enableFiltering: true,
          numberDisplayed:2
        }).change();
      }
    });
  });
</script>