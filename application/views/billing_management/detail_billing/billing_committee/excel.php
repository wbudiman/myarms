<h5><?= $header_title ?></h5>
<table border="1">
  <thead>
    <?php 
      echo "<tr>";
      echo "<th class='text-center'>".$row_lists[$search['header']]."</th>";
      $colspan=8;
      $periodes=array($search['year_1'].$search['month_1'],$search['year_2'].$search['month_2'],$search['year_3'].$search['month_3']);
      foreach($periodes as $l){
        echo "<th class='text-center'>".$l."</th>";
      }
      echo "<th class='text-center'>Bulan 1 ke 2</th>";
      echo "<th class='text-center'>Growth 1 ke 2</th>";
      echo "<th class='text-center'>Bulan 2 ke 3</th>";
      echo "<th class='text-center'>Growth 2 ke 3</th>";
      echo "</tr>";
    ?>
  </thead>
  <tbody>
    <?php
      $headers=array();
      $grand_total=array();
      foreach($lists as $l){
        if(is_array($field[$search['header']]))
        {
          foreach($field[$search['header']] as $key=>$h)
          {
            if(!in_array($h->BILLING_COMPONENT_FIELD,$headers)){
              $headers[$h->BILLING_COMPONENT_FIELD]=$h->BILLING_COMPONENT_DESCRIPTION;
            }
          }
        }else{
          if(!in_array($l->{$field[$search['header']]},$headers)){
            array_push($headers,$l->{$field[$search['header']]});
          }
        }
      }
      foreach($headers as $key=>$h){
        echo "<tr>";
        echo "<td colspan='".$colspan."' style='background:lightblue'><b>".($h!="" && $h!=NULL ? $h : $no_mapping)."</b></td>";
        echo "</tr>";
        $details=array();
        $values=array();
        $total_column=array();
        $total_column['total']=0;
        $total_per_row=0;
        if(is_array($field[$search['detail']]))
        {
          foreach($field[$search['detail']] as $key2=>$d){
            echo "<tr>";
            echo "<td>".($d->BILLING_COMPONENT_DESCRIPTION!="" && $d->BILLING_COMPONENT_DESCRIPTION!=NULL ? $d->BILLING_COMPONENT_DESCRIPTION : $no_mapping)."</td>";
            $total_per_row++;
            $values=array();
            foreach($periodes as $p){
              $check=0;
              foreach($lists as $l){
                if(is_array($field[$search['header']]))
                {
                  foreach($field[$search['header']] as $key3=>$h2){
                    if($l->PERIODE==$p && $h==$h2->BILLING_COMPONENT_DESCRIPTION && $d->BILLING_COMPONENT_FIELD==$h2->BILLING_COMPONENT_FIELD){
                      if($search['view_by']=="L11"){
                        $value=$l->SST;
                        if(!isset($total_column[$p]))$total_column[$p]=0;
                        $total_column[$p]+=$value;
                      }elseif($search['view_by']=="ARPU"){
                        $value=$l->{$h2->BILLING_COMPONENT_FIELD}/$l->SST;
                        if(!isset($total_column[$p]))$total_column[$p]=0;
                        $total_column[$p]+=$value;
                      }else{
                        $value=$l->{$h2->BILLING_COMPONENT_FIELD};
                        if(!isset($total_column[$p]))$total_column[$p]=0;
                        $total_column[$p]+=$value;
                      }
                      echo "<td class='text-right'>".number_format($value,0,".",",")."</td>";
                      array_push($values,$value);
                      $check=1;
                    }
                  }
                }else{
                  if($l->PERIODE==$p && $h==$l->{$field[$search['header']]}){
                    if($search['view_by']=="L11"){
                      $value=$l->SST;
                      if(!isset($total_column[$p]))$total_column[$p]=0;
                      $total_column[$p]+=$value;
                    }elseif($search['view_by']=="ARPU"){
                      if(!isset($search['billing_component']))
                      {
                        $value=$l->{$d->BILLING_COMPONENT_FIELD}/$l->SST;
                        if(!isset($total_column[$p]))$total_column[$p]=array("AMOUNT"=>0,"L11"=>0);
                        $total_column[$p]['AMOUNT']+=$l->{$d->BILLING_COMPONENT_FIELD};
                        $total_column[$p]['L11']+=$l->SST;
                      }else{
                        $value=$l->{$search['billing_component']}/$l->SST;
                        if(!isset($total_column[$p]))$total_column[$p]=array("AMOUNT"=>0,"L11"=>0);
                        $total_column[$p]['AMOUNT']+=$l->{$search['billing_component']};
                        $total_column[$p]['L11']+=$l->SST;
                      }
                    }else{
                      if(!isset($search['billing_component']))
                      {
                        $value=$l->{$d->BILLING_COMPONENT_FIELD};
                      
                      }else{
                        $value=$l->{$search['billing_component']};
                      }
                      if(!isset($total_column[$p]))$total_column[$p]=0;
                      $total_column[$p]+=$value;
                    }
                    echo "<td class='text-right'>".number_format($value,0,".",",")."</td>";
                    array_push($values,$value);
                    $check=1;
                  }
                }
              }
              if($check==0){
                echo "<td class='text-right'>0</td>";
                if($search['view_by']=="ARPU"){
                  if(!isset($total_column[$p]))$total_column[$p]=array("AMOUNT"=>0,"L11"=>0);
                }else{
                  if(!isset($total_column[$p]))$total_column[$p]=0;
                }
                array_push($values,0);
              }
            }
            $bulan_1_to_2=$values[1]-$values[0];
            $growth_1_to_2=($bulan_1_to_2!=0 ? ($values[0]!=0 ? $bulan_1_to_2/$values[0]*100 : 100) : 0);
            $bulan_2_to_3=$values[2]-$values[1];
            $growth_2_to_3=($bulan_2_to_3!=0 ? ($values[1]!=0 ? $bulan_2_to_3/$values[1]*100 : 100) : 0);
            echo "<td class='text-right'>".number_format($bulan_1_to_2,0,".",",")."</td>";
            echo "<td class='text-right'>".number_format($growth_1_to_2,2)." %</td>";
            echo "<td class='text-right'>".number_format($bulan_2_to_3,0,".",",")."</td>";
            echo "<td class='text-right'>".number_format($growth_2_to_3,2)." %</td>";
            if(!isset($total_column['bulan_1_to_2']))$total_column['bulan_1_to_2']=0;
            if(!isset($total_column['growth_1_to_2']))$total_column['growth_1_to_2']=0;
            if(!isset($total_column['bulan_2_to_3']))$total_column['bulan_2_to_3']=0;
            if(!isset($total_column['growth_2_to_3']))$total_column['growth_2_to_3']=0;
            $total_column['bulan_1_to_2']+=$bulan_1_to_2;
            $total_column['growth_1_to_2']+=$growth_1_to_2;
            $total_column['bulan_2_to_3']+=$bulan_2_to_3;
            $total_column['growth_2_to_3']+=$growth_2_to_3;
            echo "</tr>";
          }
        }else{
          foreach($lists as $l){
            if(!in_array($l->{$field[$search['detail']]},$details) && (isset($search['billing_component']) ? $h==$l->{$field[$search['header']]} : 1==1) ){
              $checking=false;
              foreach($periodes as $p){
                foreach($lists as $l2){
                  if($l->{$field[$search['detail']]}==$l2->{$field[$search['detail']]} && $h==$l2->{$field[$search['header']]} && $l2->PERIODE==$p)
                  {
                    if($search['view_by']=="L11"){
                      $value=$l2->SST;
                    }elseif($search['view_by']=="ARPU"){
                      if(!isset($search['billing_component']))
                      {
                        $value=$l2->{$key}/$l2->SST;
                      }else{
                        $value=$l2->{$search['billing_component']}/$l2->SST;
                      }
                    }else{
                      if(!isset($search['billing_component']))
                      {
                        $value=$l2->{$key};
                      }else{
                        $value=$l2->{$search['billing_component']};
                      }
                    }
                    if($value>0)$checking=true;
                  }
                }
              }
              if($checking==true){
                array_push($details,$l->{$field[$search['detail']]});
                echo "<tr>";
                echo "<td>".($l->{$field[$search['detail']]}!="" && $l->{$field[$search['detail']]}!=NULL ? $l->{$field[$search['detail']]} : $no_mapping)."</td>";
                $total_per_row++;
                $values=array();
                foreach($periodes as $p){
                  $counter2=0;
                  foreach($lists as $l2){
                    if($l->{$field[$search['detail']]}==$l2->{$field[$search['detail']]} && $h==$l2->{$field[$search['header']]} && $l2->PERIODE==$p)
                    {
                      if($search['view_by']=="L11"){
                        $value=$l2->SST;
                        if(!isset($total_column[$p]))$total_column[$p]=0;
                        $total_column[$p]+=$value;
                      }elseif($search['view_by']=="ARPU"){
                        if(!isset($search['billing_component']))
                        {
                          $value=$l2->{$key}/$l2->SST;
                          if(!isset($total_column[$p]))$total_column[$p]=array("AMOUNT"=>0,"L11"=>0);
                          $total_column[$p]['AMOUNT']+=$l2->{$key};
                          $total_column[$p]['L11']+=$l2->SST;
                        }else{
                          $value=$l2->{$search['billing_component']}/$l2->SST;
                          if(!isset($total_column[$p]))$total_column[$p]=array("AMOUNT"=>0,"L11"=>0);
                          $total_column[$p]['AMOUNT']+=$l2->{$search['billing_component']};
                          $total_column[$p]['L11']+=$l2->SST;
                        }
                      }else{
                        if(!isset($search['billing_component']))
                        {
                          $value=$l2->{$key};
                        }else{
                          $value=$l2->{$search['billing_component']};
                        }
                        if(!isset($total_column[$p]))$total_column[$p]=0;
                        $total_column[$p]+=$value;
                      }
                      array_push($values,$value);
                      echo "<td class='text-right'>".number_format($value,0,".",",")."</td>";
                      $counter2++;
                    }
                  }
                  if($counter2==0){
                    if($search['view_by']=="ARPU"){
                      if(!isset($total_column[$p]))$total_column[$p]=array("AMOUNT"=>0,"L11"=>0);
                    }else{
                      if(!isset($total_column[$p]))$total_column[$p]=0;
                    }
                    array_push($values,0);
                    // if(!isset($total_column[$p]))$total_column[$p]=0;
                    // $total_column[$p]+=0;
                    echo "<td class='text-right'>0</td>";
                  }
                }
                $bulan_1_to_2=$values[1]-$values[0];
                $growth_1_to_2=($bulan_1_to_2!=0 ? ($values[0]!=0 ? $bulan_1_to_2/$values[0]*100 : 100) : 0);
                $bulan_2_to_3=$values[2]-$values[1];
                $growth_2_to_3=($bulan_2_to_3!=0 ? ($values[1]!=0 ? $bulan_2_to_3/$values[1]*100 : 100) : 0);
                echo "<td class='text-right'>".number_format($bulan_1_to_2,0,".",",")."</td>";
                echo "<td class='text-right'>".number_format($growth_1_to_2,2)." %</td>";
                echo "<td class='text-right'>".number_format($bulan_2_to_3,0,".",",")."</td>";
                echo "<td class='text-right'>".number_format($growth_2_to_3,2)." %</td>";
                if(!isset($total_column['bulan_1_to_2']))$total_column['bulan_1_to_2']=0;
                if(!isset($total_column['growth_1_to_2']))$total_column['growth_1_to_2']=0;
                if(!isset($total_column['bulan_2_to_3']))$total_column['bulan_2_to_3']=0;
                if(!isset($total_column['growth_2_to_3']))$total_column['growth_2_to_3']=0;
                $total_column['bulan_1_to_2']+=$bulan_1_to_2;
                $total_column['growth_1_to_2']+=$growth_1_to_2;
                $total_column['bulan_2_to_3']+=$bulan_2_to_3;
                $total_column['growth_2_to_3']+=$growth_2_to_3;
                echo "</tr>";
              }
            }
          }
        }
        echo "<tr class='background-filter'>";
        echo "<td style='padding:8px;' class='text-center'><b>Total ".($h!="" && $h!=NULL ? $h : $no_mapping)." (".$total_per_row." item)</b></td>";
        foreach($periodes as $p){
          foreach($total_column as $key=>$t){
            if($p==$key){
              if($search['view_by']=="ARPU"){
                if(!isset($grand_total[$p]))$grand_total[$p]=array("AMOUNT"=>0,"L11"=>0);
                $grand_total[$p]['AMOUNT']+=$t['AMOUNT'];
                $grand_total[$p]['L11']+=$t['L11'];
                if($t['AMOUNT']!=0 && $t['L11']!=0){
                  echo "<td class='text-right' style='padding:8px;'>".number_format($t['AMOUNT']/$t['L11'],0,".",",")."</td>";
                }else{
                  echo "<td class='text-right' style='padding:8px;'>".number_format(0,0,".",",")."</td>";
                }
              }else{
                if(!isset($grand_total[$p]))$grand_total[$p]=0;
                $grand_total[$p]+=$t;
                echo "<td class='text-right' style='padding:8px;'>".number_format($t,0,".",",")."</td>";
              }
            }
          }
        }
        if($search['view_by']=="ARPU"){
          $month2=0;
          if($total_column[$search['year_2'].$search['month_2']]['AMOUNT']!=0 && $total_column[$search['year_2'].$search['month_2']]['L11']!=0){
            $month2=$total_column[$search['year_2'].$search['month_2']]['AMOUNT']/$total_column[$search['year_2'].$search['month_2']]['L11'];
          }
          $month1=0;
          if($total_column[$search['year_1'].$search['month_1']]['AMOUNT']!=0 && $total_column[$search['year_1'].$search['month_1']]['L11']!=0){
            $month1=$total_column[$search['year_1'].$search['month_1']]['AMOUNT']/$total_column[$search['year_1'].$search['month_1']]['L11'];
          }
          $month3=0;
          if($total_column[$search['year_3'].$search['month_3']]['AMOUNT']!=0 && $total_column[$search['year_3'].$search['month_3']]['L11']!=0){
            $month3=$total_column[$search['year_3'].$search['month_3']]['AMOUNT']/$total_column[$search['year_3'].$search['month_3']]['L11'];
          }
          $bulan_1_to_2=$month2-$month1;
          echo "<td class='text-right' style='padding:8px;'>".number_format($bulan_1_to_2,0,".",",")."</td>";
          $growth_1_to_2=($bulan_1_to_2!=0 ? ($month1!=0 ? $bulan_1_to_2/$month1*100 : 100) : 0);
          echo "<td class='text-right' style='padding:8px;'>".number_format($growth_1_to_2,2)." %</td>";
          $bulan_2_to_3=$month3-$month2;
          echo "<td class='text-right' style='padding:8px;'>".number_format($bulan_2_to_3,0,".",",")."</td>";
          $growth_2_to_3=($bulan_2_to_3!=0 ? ($month2!=0 ? $bulan_2_to_3/$month2*100 : 100) : 0);
          echo "<td class='text-right' style='padding:8px;'>".number_format($growth_2_to_3,2)." %</td>";
        }else{
          $bulan_1_to_2=$total_column[$search['year_2'].$search['month_2']]-$total_column[$search['year_1'].$search['month_1']];
          echo "<td class='text-right' style='padding:8px;'><b>".number_format($bulan_1_to_2,0,".",",")."</b></td>";
          $growth_1_to_2=($bulan_1_to_2!=0 ? ($total_column[$search['year_1'].$search['month_1']]!=0 ? $bulan_1_to_2/$total_column[$search['year_1'].$search['month_1']]*100 : 100) : 0);
          echo "<td class='text-right' style='padding:8px;'><b>".number_format($growth_1_to_2,2)." %</b></td>";
          $bulan_2_to_3=$total_column[$search['year_3'].$search['month_3']]-$total_column[$search['year_2'].$search['month_2']];
          echo "<td class='text-right' style='padding:8px;'><b>".number_format($bulan_2_to_3,0,".",",")."</b></td>";
          $growth_2_to_3=($bulan_2_to_3!=0 ? ($total_column[$search['year_2'].$search['month_2']]!=0 ? $bulan_2_to_3/$total_column[$search['year_2'].$search['month_2']]*100 : 100) : 0);
          echo "<td class='text-right' style='padding:8px;'><b>".number_format($growth_2_to_3,2)." %</b></td>";
        }
        echo "</tr>";
      }
    ?>
  </tbody>
  <tfoot>
    <?php
      echo "<tr >";
      echo "<td style='padding:8px;' class='text-center'><b>TOTAL ALL</b></td>";
      foreach($periodes as $p){
        $check=false;
        foreach($grand_total as $key=>$t){
          if($p==$key){
            if($search['view_by']=="ARPU"){
              if($t['AMOUNT']!=0 && $t['L11']!=0){
                echo "<td class='text-right' style='padding:8px;'>".number_format($t['AMOUNT']/$t['L11'],0,".",",")."</td>";
              }else{
                echo "<td class='text-right' style='padding:8px;'>".number_format(0,0,".",",")."</td>";
              }
            }else{
              echo "<td class='text-right' style='padding:8px;'><b>".number_format($t,0,".",",")."</b></td>";
            }
            $check=true;
          }
        }
        if($check==false){
          echo "<td class='text-right' style='padding:8px;'><b>0</b></td>";
        }
      }
      if(sizeof($grand_total)>0){
        if($search['view_by']=="ARPU"){
          $month2=0;
          if($grand_total[$search['year_2'].$search['month_2']]['AMOUNT']!=0 && $grand_total[$search['year_2'].$search['month_2']]['L11']!=0){
            $month2=$grand_total[$search['year_2'].$search['month_2']]['AMOUNT']/$grand_total[$search['year_2'].$search['month_2']]['L11'];
          }
          $month1=0;
          if($grand_total[$search['year_1'].$search['month_1']]['AMOUNT']!=0 && $grand_total[$search['year_1'].$search['month_1']]['L11']!=0){
            $month1=$grand_total[$search['year_1'].$search['month_1']]['AMOUNT']/$grand_total[$search['year_1'].$search['month_1']]['L11'];
          }
          $month3=0;
          if($grand_total[$search['year_3'].$search['month_3']]['AMOUNT']!=0 && $grand_total[$search['year_3'].$search['month_3']]['L11']!=0){
            $month3=$grand_total[$search['year_3'].$search['month_3']]['AMOUNT']/$grand_total[$search['year_3'].$search['month_3']]['L11'];
          }
          $bulan_1_to_2=$month2-$month1;
          echo "<td class='text-right' style='padding:8px;'>".number_format($bulan_1_to_2,0,".",",")."</td>";
          $growth_1_to_2=($bulan_1_to_2!=0 ? ($month1!=0 ? $bulan_1_to_2/$month1*100 : 100) : 0);
          echo "<td class='text-right' style='padding:8px;'>".number_format($growth_1_to_2,2)." %</td>";
          $bulan_2_to_3=$month3-$month2;
          echo "<td class='text-right' style='padding:8px;'>".number_format($bulan_2_to_3,0,".",",")."</td>";
          $growth_2_to_3=($bulan_2_to_3!=0 ? ($month2!=0 ? $bulan_2_to_3/$month2*100 : 100) : 0);
          echo "<td class='text-right' style='padding:8px;'>".number_format($growth_2_to_3,2)." %</td>";
        }else{
          $bulan_1_to_2=$grand_total[$search['year_2'].$search['month_2']]-$grand_total[$search['year_1'].$search['month_1']];
          echo "<td class='text-right' style='padding:8px;'><b>".number_format($bulan_1_to_2,0,".",",")."</b></td>";
          $growth_1_to_2=($bulan_1_to_2!=0 ? ($grand_total[$search['year_1'].$search['month_1']]!=0 ? $bulan_1_to_2/$grand_total[$search['year_1'].$search['month_1']]*100 : 100) : 0);
          echo "<td class='text-right' style='padding:8px;'><b>".number_format($growth_1_to_2,2)." %</b></td>";
          $bulan_2_to_3=$grand_total[$search['year_3'].$search['month_3']]-$grand_total[$search['year_2'].$search['month_2']];
          echo "<td class='text-right' style='padding:8px;'><b>".number_format($bulan_2_to_3,0,".",",")."</b></td>";
          $growth_2_to_3=($bulan_2_to_3!=0 ? ($grand_total[$search['year_2'].$search['month_2']]!=0 ? $bulan_2_to_3/$grand_total[$search['year_2'].$search['month_2']]*100 : 100) : 0);
          echo "<td class='text-right' style='padding:8px;'><b>".number_format($growth_2_to_3,2)." %</b></td>";
        }
      }else{
        echo "<td class='text-right' style='padding:8px;'><b>0</b></td>";
        echo "<td class='text-right' style='padding:8px;'><b>0 %</b></td>";
        echo "<td class='text-right' style='padding:8px;'><b>0</b></td>";
        echo "<td class='text-right' style='padding:8px;'><b>0 %</b></td>";
      }
      echo "</tr>";
    ?>
  </tfoot>
</table>