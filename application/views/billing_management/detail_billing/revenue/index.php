<form method="post" action="<?= base_url($path."index") ?>">
  <div class="row">
    <div class="col-md-12">
      <div id="table-search">
        <div class="wrap">
          <table  class="table table-search table-striped">
            <thead>
              <tr>
                <th colspan="6" v-align="middle">Parameter Pencarian Data</th>
              </tr>
            </thead>
            <tbody>
              <tr>                   
                <td>Divisi</td>
                <td>
                  <select name="search[division][]" id="division" multiple class="form-control">
                    <?php 
                      foreach($division_lists as $d){
                        echo "<option value='".$d->DIVISION_CODE."' ".($search['division']!="" && in_array($d->DIVISION_CODE,$search['division']) ? "selected" : "").">".$d->DIVISION_CODE."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Top Customer</td>
                <td>
                  <select name="search[top_customer][]" multiple class="form-control" id="top_customer">
                    <?php 
                      echo '<optgroup label="Top 20">';
                      foreach($top_customer_lists as $r){
                        if($r->PORTOFOLIO_GROUP=="Top 20")
                        {
                          echo "<option value='".$r->PORTOFOLIO_ID."' ".($search['top_customer']!="" && in_array($r->PORTOFOLIO_ID,$search['top_customer']) ? "selected" : "").">".$r->PORTOFOLIO_NAME."</option>";
                        }
                      }
                      echo '</optgroup>';
                      echo '<optgroup label="Non Top 20">';
                      foreach($top_customer_lists as $r){
                        if($r->PORTOFOLIO_GROUP=="Non Top 20")
                        {
                          echo "<option value='".$r->PORTOFOLIO_ID."' ".($search['top_customer']!="" && in_array($r->PORTOFOLIO_ID,$search['top_customer']) ? "selected" : "").">".$r->PORTOFOLIO_NAME."</option>";
                        }
                      }
                      echo '</optgroup>';
                    ?>
                  </select>
                </td>
                <td>Ubis</td>
                <td>
                  <select name="search[ubis][]" multiple class="form-control" id="ubis">
                    <?php 
                      foreach($ubis_lists as $d){
                        echo "<option value='".$d->UBIS_ID."' ".($search['ubis']!="" && in_array($d->UBIS_ID,$search['ubis']) ? "selected" : "").">".$d->UBIS."</option>";
                      }
                    ?>
                  </select>
                </td>
              </tr>
              <tr>                   
                <td>Witel</td>
                <td>
                  <select name="search[witel][]" id="witel" multiple class="form-control">
                    <?php 
                      foreach($witel_lists as $w){
                        echo "<option value='".$w->WITEL_CODE."' ".($search['witel']!="" && in_array($w->WITEL_CODE,$search['witel']) ? "selected" : "").">".$w->WITEL_DESCRIPTION."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Summary Product</td>
                <td>
                  <select name="search[summary_product][]" multiple class="form-control" id="summary_product">
                    <?php 
                      foreach($summary_product_lists as $d){
                        echo "<option value='".$d->SUMMARY_PRODUCT."' ".($search['summary_product']!="" && in_array($d->SUMMARY_PRODUCT,$search['summary_product']) ? "selected" : "").">".$d->SUMMARY_PRODUCT."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Customer Category</td>
                <td>
                  <select name="search[ubis_segment][]" id="ubis_segment" multiple class="form-control">
                    <?php 
                      foreach($ubis_segment_lists as $r){
                        echo "<option value='".$r->UBIS_SEGMENT."' ".($search['ubis_segment']!="" && in_array($r->UBIS_SEGMENT,$search['ubis_segment']) ? "selected" : "").">".$r->UBIS_SEGMENT."</option>";
                      }
                    ?>
                  </select>
                </td>
              </tr>
              <tr>                   
                <td>Datel</td>
                <td>
                  <select name="search[datel][]" id="datel" multiple class="form-control">
                    <?php 
                      foreach($datel_lists as $w){
                        echo "<option value='".$w->DATEL_CODE."' ".($search['datel']!="" && in_array($w->DATEL_CODE,$search['datel']) ? "selected" : "").">".$w->DATEL_DESCRIPTION."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Produk</td>
                <td>
                  <select name="search[product][]" multiple class="form-control" id="product">
                    <?php 
                      foreach($product_lists as $r){
                        echo "<option value='".$r->PRODUCT_NAME."' ".($search['product']!="" && in_array($r->PRODUCT_NAME,$search['product']) ? "selected" : "").">".$r->PRODUCT_NAME."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Komponen Billing</td>
                <td>
                  <select name="search[billing_component]" class="form-control" id="billing_component" <?= ($search['view_by']=="L11" ? "disabled" : "") ?>>
                    <?php 
                      foreach($billing_component_lists as $r){
                        echo "<option value='".$r->BILLING_COMPONENT_FIELD."' ".(isset($search['billing_component']) && $search['billing_component']==$r->BILLING_COMPONENT_FIELD ? "selected" : "").">".$r->BILLING_COMPONENT_DESCRIPTION."</option>";
                      }
                    ?>
                  </select>
                </td>
              </tr>
              <tr>                   
                <td>Periode</td>
                <td>
                  <div class="col-md-9" style="padding-left:0px;">
                    <div class="col-md-7" style="padding-left:0px;">
                      <select name="search[month]" class="form-control">
                        <?php 
                          foreach($month_lists as $key=>$d){
                            echo "<option value='".$key."' ".($search['month']==$key ? "selected" : "").">".$d."</option>";
                          }
                        ?>
                      </select>
                    </div>
                    <div class="col-md-5" style="padding-left:0px;">
                      <select name="search[year]" class="form-control">
                        <?php 
                          for($x=date("Y");$x>(date("Y")-10);$x--){
                            echo "<option value='".$x."' ".($search['year']==$x ? "selected" : "").">".$x."</option>";
                          }
                        ?>
                      </select>
                    </div>
                  </div>
                </td>
                <td>Indihome</td>
                <td>
                  <select name="search[indihome]" class="form-control">
                    <option value="">All</option>
                    <?php 
                      foreach($indihome_lists as $r){
                        echo "<option value='".$r->INDIHOME_ID."' ".($search['indihome']==$r->INDIHOME_ID ? "selected" : "").">".$r->INDIHOME_DESCRIPTION."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>View By</td>
                <td>
                  <select name="search[view_by]" class="form-control" id="view_by">
                    <?php 
                      foreach($view_by_lists as $r){
                        echo "<option value='".$r->VIEW_BY."' ".($search['view_by']==$r->VIEW_BY ? "selected" : "").">".$r->VIEW_BY_DESCRIPTION."</option>";
                      }
                    ?>
                  </select>
                </td>
              </tr>
              <tr class="background-filter">                   
                <td>Baris</td>
                <td>
                  <select name="search[row]" class="form-control" id="row">
                    <?php 
                      foreach($row_lists as $key=>$d){
                        echo "<option value='".$key."' ".($search['view_by']=="L11" && $key=="billing_component" ? "disabled" : "")." ".($search['row']==$key ? "selected" : "").">".$d."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Kolom</td>
                <td>
                  <select name="search[column]" class="form-control" id="column">
                    <?php 
                      foreach($row_lists as $key=>$d){
                        echo "<option value='".$key."' ".($search['view_by']=="L11" && $key=="billing_component" ? "disabled" : "")." ".($search['column']==$key ? "selected" : "").">".$d."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td colspan="2"></td>
              </tr>
            </tbody>
            <tfoot>
              <tr>
                <td>Search</td>
                <td><input type="submit" name="search[button]" class="btn btn-search" value="Search"></td>
                <td>To Excel</td>
                <td><input type="submit" name="search[button]" class="btn btn-search" value="Download"></td>
                <td colspan="2"></td>
              </tr>
            </tfoot>
          </table>
        </div>
        <a href="javascript:void(0)" class="showhide"></a>
      </div>
    </div>
  </div> 
</form>
<div class="row">
  <div class="col-md-12">
    <h4 class="title-menu">Revenue</h4>
    <div class="billing-detail">
      <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered" id="table_revenue">
        <thead>
          <?php 
            echo "<tr>";
            echo "<th class='text-center'>".(is_array($row_lists[$search['row']]) ? "Komponen Billing" : $row_lists[$search['row']])."</th>";
            $columns=array();
            foreach($lists as $l){
              if(is_array($field[$search['column']]))
              {
                foreach($field[$search['column']] as $c){
                  if(!in_array($c->BILLING_COMPONENT_FIELD,$columns)){
                    array_push($columns,$c->BILLING_COMPONENT_FIELD);
                    echo "<th class='text-center'>".($c->BILLING_COMPONENT_DESCRIPTION!="" && $c->BILLING_COMPONENT_DESCRIPTION!=NULL ? $c->BILLING_COMPONENT_DESCRIPTION : $no_mapping)."</th>";
                    
                  }
                }
              }else{
                if(!in_array($l->{$field[$search['column']]},$columns)){
                  array_push($columns,$l->{$field[$search['column']]});
                  echo "<th class='text-center'>".($l->{$field[$search['column']]}!="" && $l->{$field[$search['column']]}!=NULL ? $l->{$field[$search['column']]} : $no_mapping)."</th>";
                }
              }
            }
            if($search['column']!="billing_component"){
              echo "<th class='text-center'>Total</th>";
            }
            echo "</tr>";
          ?>
        </thead>
        <tbody>
          <?php   
            $rows=array();
            $total_column=array();
            $total_column['total']=0;
            foreach($lists as $l){
              if(is_array($field[$search['row']]))
              {
                foreach($field[$search['row']] as $r){
                  if(!in_array($r->BILLING_COMPONENT_FIELD,$rows)){
                    $checking=false;
                    foreach($columns as $c){
                      foreach($lists as $l2){
                        if(is_array($field[$search['column']]))
                        { 
                          foreach($field[$search['column']] as $f)
                          {
                            if($l->{$r->BILLING_COMPONENT_FIELD}==$l2->{$f->BILLING_COMPONENT_FIELD}){
                              if($f->BILLING_COMPONENT_FIELD==$c){
                                $value=$l2->{$r->BILLING_COMPONENT_FIELD};
                                if($search['view_by']=="L11"){
                                  $value=$l2->SST;
                                }elseif($search['view_by']=="ARPU"){
                                  $value=$value/$l2->SST;
                                }
                                if($value>0)$checking=true;
                              }
                            }
                          }
                        }else{
                          if($c==$l2->{$field[$search['column']]}){
                            $value=$l2->{$r->BILLING_COMPONENT_FIELD};
                            if($search['view_by']=="L11"){
                              $value=$l2->SST;
                            }elseif($search['view_by']=="ARPU"){
                              $value=$value/$l2->SST;
                            }
                            if($value>0)$checking=true;
                          }
                        }
                      }
                    }
                    if($checking==true){
                    
                    
                      array_push($rows,$r->BILLING_COMPONENT_FIELD);
                      echo "<tr>";
                      echo "<td>".($r->BILLING_COMPONENT_DESCRIPTION!="" && $r->BILLING_COMPONENT_DESCRIPTION!=NULL ? $r->BILLING_COMPONENT_DESCRIPTION : $no_mapping)."</td>";
                      $total=0;
                      $counter=0;
                      foreach($columns as $c){
                        $check=0;
                        foreach($lists as $l2){
                          
                          if(is_array($field[$search['column']]))
                          {
                            
                              foreach($field[$search['column']] as $f)
                              {
                                if($l->{$r->BILLING_COMPONENT_FIELD}==$l2->{$f->BILLING_COMPONENT_FIELD}){
                                  if($f->BILLING_COMPONENT_FIELD==$c){
                                    $value=$l2->{$r->BILLING_COMPONENT_FIELD};
                                    if($search['view_by']=="L11"){
                                      if(!isset($total_column[$c]))$total_column[$c]=0;
                                      $value=$l2->SST;
                                      $total_column[$c]+=$value;
                                    }elseif($search['view_by']=="ARPU"){
                                      if(!isset($total_column[$c]))$total_column[$c]=array("AMOUNT"=>0,"L11"=>0);
                                      $total_column[$c]['AMOUNT']+=$value;
                                      $total_column[$c]['L11']+=$l2->SST;
                                    }else{
                                      if(!isset($total_column[$c]))$total_column[$c]=0;
                                      $total_column[$c]+=$value;
                                    }
                                    $total+=$value;
                                    $counter++;
                                    echo "<td class='text-right'>".number_format($value,0,".",",")."</td>";
                                    $check=1;
                                  }
                                }
                              }
                          }else{
                            if($c==$l2->{$field[$search['column']]}){
                              $value=$l2->{$r->BILLING_COMPONENT_FIELD};
                              if($search['view_by']=="L11"){
                                if(!isset($total_column[$c]))$total_column[$c]=0;
                                $value=$l2->SST;
                                $total_column[$c]+=$value;
                              }elseif($search['view_by']=="ARPU"){
                                if(!isset($total_column[$c]))$total_column[$c]=array("AMOUNT"=>0,"L11"=>0);
                                $total_column[$c]['AMOUNT']+=$value;
                                $total_column[$c]['L11']+=$l2->SST;
                              }else{
                                if(!isset($total_column[$c]))$total_column[$c]=0;
                                $total_column[$c]+=$value;
                              }
                              $total+=$value;
                              $counter++;
                              echo "<td class='text-right'>".number_format($value,0,".",",")."</td>";
                              $check=1;
                            }
                          }
                          
                        }
                        if($check==0){
                          if(!isset($total_column[$c]))$total_column[$c]=0;
                          $total_column[$c]+=0;
                          echo "<td class='text-right'>0</td>";
                        }
                      }
                      if($search['column']!="billing_component"){
                        if(!isset($total_column['total']))$total_column['total']=0;
                        $total_column['total']+=$total;
                        echo "<td class='text-right'>".number_format($total,0,".",",")."</td>";
                      }
                      echo "</tr>";
                    }
                  }
                }
              }else{
                if(!in_array($l->{$field[$search['row']]},$rows)){
                  if(is_array($field[$search['column']]))
                  {
                    $checking=false;
                    foreach($field[$search['column']] as $c){
                      foreach($lists as $l2){
                        if($l->{$field[$search['row']]}==$l2->{$field[$search['row']]}){
                          $value=$l2->{$c->BILLING_COMPONENT_FIELD};
                          if($search['view_by']=="L11"){
                            $value=$l2->SST;
                          }elseif($search['view_by']=="ARPU"){
                            $value=$value/$l2->SST;
                          }
                          if($value>0)$checking=true;
                        }
                      }
                    }
                    if($checking==true){
                      echo "<tr>";
                      echo "<td>".($l->{$field[$search['row']]}!="" && $l->{$field[$search['row']]}!=NULL ? $l->{$field[$search['row']]} : $no_mapping)."</td>";
                      array_push($rows,$l->{$field[$search['row']]});
                      $total=0;
                      $counter=0;
                      foreach($field[$search['column']] as $c){
                        foreach($lists as $l2){
                          if($l->{$field[$search['row']]}==$l2->{$field[$search['row']]}){
                            $value=$l2->{$c->BILLING_COMPONENT_FIELD};
                            if($search['view_by']=="L11"){
                              $value=$l2->SST;
                            }elseif($search['view_by']=="ARPU"){
                              $value=$value/$l2->SST;
                            }
                            $total+=$value;
                            if(!isset($total_column[$c->BILLING_COMPONENT_FIELD]))$total_column[$c->BILLING_COMPONENT_FIELD]=0;
                            $total_column[$c->BILLING_COMPONENT_FIELD]+=$value;
                            $counter++;
                            echo "<td class='text-right'>".number_format($value,0,".",",")."</td>";
                            $check=1;
                          }
                        }
                      }
                      if($search['column']!="billing_component"){
                        if(!isset($total_column['total']))$total_column['total']=0;
                        $total_column['total']+=$total;
                        echo "<td class='text-right'>".number_format($total,0,".",",")."</td>";
                      }
                      echo "</tr>";
                    }
                  }else{
                    $checking=false;
                    foreach($columns as $c){
                      foreach($lists as $l2){
                        if($l->{$field[$search['row']]}==$l2->{$field[$search['row']]})
                        {
                          if($c==$l2->{$field[$search['column']]}){
                            if($search['view_by']=="L11"){
                              $value=$l2->SST;
                            }elseif($search['view_by']=="ARPU"){
                              $value=$l2->{$search['billing_component']}/$l2->SST;
                            }else{
                              $value=$l2->{$search['billing_component']};
                            }
                            if($value>0)$checking=true;
                          }
                        }
                      }
                    }
                    if($checking==true){
                      array_push($rows,$l->{$field[$search['row']]});
                      echo "<tr>";
                      echo "<td>".($l->{$field[$search['row']]}!="" && $l->{$field[$search['row']]}!=NULL ? $l->{$field[$search['row']]} : $no_mapping)."</td>";
                      $total=0;
                      $counter=0;
                      foreach($columns as $c){
                        $check=0;
                        foreach($lists as $l2){
                          if($l->{$field[$search['row']]}==$l2->{$field[$search['row']]})
                          {
                            if($c==$l2->{$field[$search['column']]}){
                              if($search['view_by']=="L11"){
                                if(!isset($total_column[$c]))$total_column[$c]=0;
                                $value=$l2->SST;
                                $total_column[$c]+=$value;
                              }elseif($search['view_by']=="ARPU"){
                                $value=$l2->{$search['billing_component']}/$l2->SST;
                                if(!isset($total_column[$c]))$total_column[$c]=array("AMOUNT"=>0,"L11"=>0);
                                $total_column[$c]['AMOUNT']+=$l2->{$search['billing_component']};
                                $total_column[$c]['L11']+=$l2->SST;
                              }else{
                                if(!isset($total_column[$c]))$total_column[$c]=0;
                                $value=$l2->{$search['billing_component']};
                                $total_column[$c]+=$value;
                              }
                              $total+=$value;
                              $counter++;
                              echo "<td class='text-right'>".number_format($value,0,".",",")."</td>";
                              $check=1;
                            }
                          }
                        }
                        if($check==0){
                          echo "<td class='text-right'>0</td>";
                          if($search['view_by']=="ARPU"){
                            if(!isset($total_column[$c]))$total_column[$c]=array("AMOUNT"=>0,"L11"=>0);
                          }else{
                            if(!isset($total_column[$c]))$total_column[$c]=0;
                          }
                        }
                      }
                      if($search['column']!="billing_component"){
                        if(!isset($total_column['total']))$total_column['total']=0;
                        $total_column['total']+=$total;
                        echo "<td class='text-right'>".number_format($total,0,".",",")."</td>";
                      }
                      echo "</tr>";
                    }
                  }
                }
              }
            }
          ?>
        </tbody>
        <tfoot>
        <?php
          if($search['row']!="billing_component"){
            echo "<tr>";
            echo "<td style='padding:8px;'>Total (".sizeof($rows)." item)</td>";
            $total=0;
            foreach($columns as $c){
              foreach($total_column as $key=>$t){
                if($c==$key){
                  if($search['view_by']=="ARPU"){
                    $total+=($t['AMOUNT']/$t['L11']);
                    echo "<td class='text-right' style='padding:8px;'>".number_format($t['AMOUNT']/$t['L11'],0,".",",")."</td>";
                  }else{
                    echo "<td class='text-right' style='padding:8px;'>".number_format($t,0,".",",")."</td>";
                  }
                }
              }
            }
            if($search['column']!="billing_component"){
              if($search['view_by']=="ARPU"){
                echo "<td class='text-right' style='padding:8px;'>".number_format($total,0,".",",")."</td>";
              }else{
                echo "<td class='text-right' style='padding:8px;'>".number_format($total_column['total'],0,".",",")."</td>";
              }
            }
            echo "</tr>";
          }else{
            echo "<tr>";
            echo "<td>Tagihan</td>";
            foreach($columns as $c){
              foreach($total_column as $key=>$t){
                if($c==$key){
                  if($search['view_by']=="ARPU"){
                    $total+=($t['AMOUNT']/$t['L11']);
                    echo "<td class='text-right' style='padding:8px;'>".number_format($t['AMOUNT']/$t['L11'],0,".",",")."</td>";
                  }else{
                    echo "<td class='text-right' style='padding:8px;'>".number_format($t,0,".",",")."</td>";
                  }
                }
              }
            }
            if($search['column']!="billing_component"){
              if($search['view_by']=="ARPU"){
                echo "<td class='text-right' style='padding:8px;'>".number_format($total,0,".",",")."</td>";
              }else{
                echo "<td class='text-right' style='padding:8px;'>".number_format($total_column['total'],0,".",",")."</td>";
              }
            }
            echo "</tr>";
            echo "<tr>";
            echo "<td>Ppn</td>";
            foreach($columns as $c){
              foreach($total_column as $key=>$t){
                if($c==$key){
                  if($search['view_by']=="ARPU"){
                    $total+=($t['AMOUNT']/$t['L11'])*0.1;
                    echo "<td class='text-right' style='padding:8px;'>".number_format(($t['AMOUNT']/$t['L11'])*0.1,0,".",",")."</td>";
                  }else{
                    echo "<td class='text-right' style='padding:8px;'>".number_format($t*0.1,0,".",",")."</td>";
                  }
                }
              }
            }
            if($search['column']!="billing_component"){
              if($search['view_by']=="ARPU"){
                echo "<td class='text-right' style='padding:8px;'>".number_format($total,0,".",",")."</td>";
              }else{
                echo "<td class='text-right' style='padding:8px;'>".number_format($total_column['total']*0.1,0,".",",")."</td>";
              }
            }
            echo "</tr>";
            echo "<tr>";
            echo "<td>Tagihan + Ppn</td>";
            foreach($columns as $c){
              foreach($total_column as $key=>$t){
                if($c==$key){
                  if($search['view_by']=="ARPU"){
                    $total+=($t['AMOUNT']/$t['L11'])+(($t['AMOUNT']/$t['L11'])*0.1);
                    echo "<td class='text-right' style='padding:8px;'>".number_format(($t['AMOUNT']/$t['L11'])+(($t['AMOUNT']/$t['L11'])*0.1),0,".",",")."</td>";
                  }else{
                    echo "<td class='text-right' style='padding:8px;'>".number_format($t+($t*0.1),0,".",",")."</td>";
                  }
                }
              }
            }
            if($search['column']!="billing_component"){
              if($search['view_by']=="ARPU"){
                echo "<td class='text-right' style='padding:8px;'>".number_format($total,0,".",",")."</td>";
              }else{
                echo "<td class='text-right' style='padding:8px;'>".number_format($total_column['total']+($total_column['total']*0.1),0,".",",")."</td>";
              }
            }
            echo "</tr>";
          }
        ?>
        </tfoot>
      </table>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    $("#table_revenue").dataTable({
      "bSort" : true,
      "bFilter" : false,
      "bPaginate": false,
      "info": false,
    });
    $("#division").multiselect({
      includeSelectAllOption: true,
      numberDisplayed:2
    }).change(function(){
      var divisions=$(this).val();
      $.ajax({
        url:"<?= base_url("ajax/getWitel") ?>",
        dataType:"JSON",
        data:{divisions:divisions},
        success:function(response){
          $("#witel").multiselect("destroy");
          $("#witel").html(response.content);
          $("#witel").multiselect({
            includeSelectAllOption: true,
            maxHeight: 212,
            enableFiltering: true
          }).change();
        }
      });
    });
    $("#summary_product").multiselect({
      includeSelectAllOption: true,
      numberDisplayed:2
    }).change(function(){
      var summary_products=$(this).val();
      $.ajax({
        url:"<?= base_url("ajax/getProducts") ?>",
        dataType:"JSON",
        data:{summary_products:summary_products},
        success:function(response){
          $("#product").multiselect("destroy");
          $("#product").html(response.content);
          $("#product").multiselect({
            includeSelectAllOption: true,
            maxHeight: 212
          });
        }
      });
    });
    $("#ubis").multiselect({
      dropRight: true,
      includeSelectAllOption: true,
      numberDisplayed:2
    }).change(function(){
      var ubis=$(this).val();
      $.ajax({
        url:"<?= base_url("ajax/getUbisSegments") ?>",
        dataType:"JSON",
        data:{ubis:ubis},
        success:function(response){
          $("#ubis_segment").multiselect("destroy");
          $("#ubis_segment").html(response.content);
          $("#ubis_segment").multiselect({
            dropRight: true,
            includeSelectAllOption: true,
            maxHeight: 212,
            enableFiltering: true
          });
        }
      });
    });
    $("#witel,#datel,#regional,#product").multiselect({numberDisplayed:2,maxHeight: 212,includeSelectAllOption: true,enableFiltering: true});
    $("#ubis_segment").multiselect({dropRight: true,numberDisplayed:2,maxHeight: 212,includeSelectAllOption: true,enableFiltering: true});
    $("#top_customer").multiselect({
      onChange: function(element, checked) {
        if(checked==true){
          value=$(element).val();
          if(value=="SILVER"){
            $("#top_customer option:selected[value='GOLD']").removeAttr("selected");
            $("#top_customer option:selected[value='PLATIN']").removeAttr("selected");
            $("#top_customer option:selected[value='TITAN']").removeAttr("selected");
          }else{
            $("#top_customer option:selected[value='SILVER']").removeAttr("selected");
          }
          $("#top_customer").multiselect('refresh');
        }
      }
    });
    $("#view_by").change(function(){
      var view_by=$(this).val();
      switch(view_by){
        case "AMOUNT":
          $("#row option[value='billing_component']").removeAttr("disabled");
          $("#column option[value='billing_component']").removeAttr("disabled");
          if($("#row").val()=="billing_component" || $("#column").val()=="billing_component"){
            $("#billing_component").attr("disabled","disabled");
          }else{
            $("#billing_component").removeAttr("disabled");
          }
          break;
        case "L11":
          if($("#row").val()=="billing_component")$("#row").val(0);
          if($("#column").val()=="billing_component")$("#column").val(0);
          $("#billing_component").attr("disabled","disabled");
          $("#row option[value='billing_component']").attr("disabled","disabled");
          $("#column option[value='billing_component']").attr("disabled","disabled");
          break;
        case "ARPU":
          $("#row option[value='billing_component']").removeAttr("disabled");
          $("#column option[value='billing_component']").removeAttr("disabled");
          if($("#row").val()=="billing_component" || $("#column").val()=="billing_component"){
            $("#billing_component").attr("disabled","disabled");
          }else{
            $("#billing_component").removeAttr("disabled");
          }
          break;
      }
    });
    $("#row,#column").change(function(){
      if($("#row").val()=="billing_component" || $("#column").val()=="billing_component"){
        $("#billing_component").attr("disabled","disabled");
      }else{
        $("#billing_component").removeAttr("disabled");
      }
    });
  });
  $("#witel").live("change",function(){
    var divisions=$("#division").val();
    var witels=$(this).val();
    $.ajax({
      url:"<?= base_url("ajax/getDatel") ?>",
      dataType:"JSON",
      data:{witels:witels,divisions:divisions},
      success:function(response){
        $("#datel").multiselect("destroy");
        $("#datel").html(response.content);
        $("#datel").multiselect({
          includeSelectAllOption: true,
          maxHeight: 212,
          enableFiltering: true,
          numberDisplayed:2
        }).change();
      }
    });
  });
</script>