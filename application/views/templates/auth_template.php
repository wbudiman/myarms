<!DOCTYPE html>
<html lang="en">
	<head>
		<title><?= $PAGE_TITLE; ?></title>
    <?= $INCLUDES; ?>
	</head>
	<body>
    <header></header> 
    <section>
      <?= $VIEW ?>
    </section>
    <?= $FOOTER ?>
  </body>
</html>