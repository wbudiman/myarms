<!DOCTYPE html>
<html class="no-js" lang="en"> 
	<head>
		<title><?= $PAGE_TITLE; ?></title>
    <?= $INCLUDES; ?>
	</head>
	<body class="billing">
    <?= $HEADER ?>
    <section>
      <div class="container">  
        <?= $SUB_HEADER ?>
        <?= $VIEW ?>
      </div>
    </section>
    <?= $FOOTER ?>
    <div class="modal fade" id="mainModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
      <div class="modal-dialog" style="width:900px;">
        <div class="modal-content"></div>
      </div>
    </div>
    <div class="overlay-loading" style="display:none;"><img src="<?= base_url() . "assets/images/loading.gif" ?>"></div>
  </body>
</html>