<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="<?= base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">
<link href="<?= base_url() ?>assets/css/bootstrap-multiselect.css" rel="stylesheet">
<link href="<?= base_url() ?>assets/css/dataTables.bootstrap.css" rel="stylesheet">
<link href="<?= base_url() ?>assets/css/style.css" rel="stylesheet">
<link href="<?= base_url() ?>assets/css/mobile.css" rel="stylesheet">
<link href="<?= base_url() ?>assets/css/custom.css" rel="stylesheet">
<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/jquery-orgchart/jquery.orgchart.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/jquery-autocomplete/content/styles.css">
<!--[if lt IE 9]>
  <script src="<?= base_url() ?>assets/js/html5shiv.min.js"></script>
  <script src="<?= base_url() ?>assets/js/respond.min.js"></script>
<![endif]-->

<script src="<?= base_url() ?>assets/plugins/jquery/jquery-1.8.3.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/plugins/jquery-cookie/jquery.cookie.js"></script>
<script src="<?= base_url() ?>assets/js/bootstrap.min.js"></script>
<script src="<?= base_url() ?>assets/js/bootstrap-multiselect.js"></script>
<script src="<?= base_url() ?>assets/plugins/DataTables-1.10.2/media/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/plugins/jquery-validation/dist/jquery.validate.js"></script>
<script src="<?= base_url() ?>assets/plugins/noty-2.2.6/js/noty/packaged/jquery.noty.packaged.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/plugins/jquery-orgchart/jquery.orgchart.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/plugins/html2canvas/build/html2canvas.js"></script>
<script src="<?= base_url() ?>assets/plugins/jquery-autocomplete/scripts/jquery.mockjax.js"></script>
<script src="<?= base_url() ?>assets/plugins/jquery-autocomplete/src/jquery.autocomplete.js"></script>
<script src="<?= base_url() ?>assets/cores/apps.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/plugins/Highcharts-4.0.4/js/highcharts.js"></script>
<script src="<?= base_url() ?>assets/plugins/Highcharts-4.0.4/js/modules/exporting.js"></script>
<script type="text/javascript">
  <?php if($current_user!=NULL): ?>
  var menuArray=<?= $LIST_MENU_ACTIVE ?>;
  <?php endif; ?>
  $(document).ready(function(){
    <?php if($current_user!=NULL): ?>
    $('#search-menu-global').autocomplete({
      autoSelectFirst:true,
      lookup: menuArray,
      onSelect: function(suggestion) {
        var url=suggestion.url;
        window.location.href=url;
      }
    });
    <?php endif; ?>
    <?php if($this->session->flashdata("error")!=null){ ?>
      main.notification("bottomRight","error","Peringatan","<?php echo $this->session->flashdata("error"); ?>");
    <?php }elseif($this->session->flashdata("success")!=null){ ?>
      main.notification("bottomRight","success","Informasi","<?php echo $this->session->flashdata("success"); ?>");
    <?php } ?>
  });
</script>
<?php /*
<link href="<?= base_url() ?>assets/plugins/boostrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="<?= base_url() ?>assets/plugins/bootstrap-datepicker/css/datepicker.css" rel="stylesheet" type="text/css" />
<link href="<?= base_url() ?>assets/plugins/bootstrap-multiselect/css/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
<link href="<?= base_url() ?>assets/plugins/DataTables-1.10.2/media/css/jquery.dataTables.css" rel="stylesheet" type="text/css" />

<script src="<?= base_url() ?>assets/plugins/jquery/jquery-1.8.3.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/plugins/bootstrap-multiselect/js/bootstrap-multiselect.js" type="text/javascript"></script>
*/ ?>