<div class="wrapper">
  <form class="form-horizontal form-signin" role="form" method="post" action="<?= $url ?>">
    <img src="<?= base_url() ?>assets/images/logo.png" class="logo" alt=""/>
    <p>Silahkan Login dengan Username dan Password</p>
    <div class="form-group">
      <div class="input-group">
        <div class="input-group-addon email"></div>
        <input type="text" class="form-control" name="user[username]">
      </div>
    </div>
    <div class="form-group "> <!-- add class (has-error) -->
      <div class="input-group">
        <div class="input-group-addon password"></div>
        <input type="password" class="form-control" name="user[password]">
      </div>
    </div>
    <div class="form-group">
      <!--<div class="checkbox-re">
        <input type="checkbox" value="1" id="checkboxFiveInput" name="" />
        <label for="checkboxFiveInput"><span>Remember me</span></label>
      </div>-->
      <button type="submit" class="btn btn-orange">Login</button>
    </div>
  </form>
</div>