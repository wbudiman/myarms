<div class="row">
  <div class="col-sm-12">
    <h3 class="page-header-2">Profile</h3>
  </div>
</div>
<div class="row">
  <div class="col-sm-12">
    <div class="panel panel-default">
      <div class="panel-body">
        <form role="form" validate class="form-horizontal" method="post" action="<?= base_url("auth/profile") ?>">
          <input type="hidden" name="user[USER_ID]" value="<?= $user->USER_ID ?>">
          <div class="form-group">
            <label class="col-sm-3 control-label">Nama</label>
            <div class="col-sm-3">
              <input type="text" placeholder="Nama" class="form-control required" name="user[NAME]" value="<?= $user->NAME ?>">
              <span class="help-block"></span>
            </div>
          </div>
          <div class="form-group" style="margin-bottom: 20px;">
            <label class="col-sm-3 control-label">Username</label>
            <div class="col-sm-3">
              <div style="padding-top:7px;"><?= $user->USERNAME ?></div>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">Password</label>
            <div class="col-sm-3">
              <input type="password" placeholder="Password" id="password" class="form-control required" name="user[PASSWORD]">
              <span class="help-block"></span>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">Ulangi Password</label>
            <div class="col-sm-3">
              <input type="password" class="form-control required" placeholder="Ulangi Password" equalTo="#password">
              <span class="help-block"></span>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label"></label>
            <div class="col-sm-5">
              <button type="submit" class="btn btn-search">Simpan</button>
              <button type="reset" class="btn btn-default">Reset</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>