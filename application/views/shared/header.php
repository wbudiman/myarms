<header class="navbar navbar-static-top bs-docs-nav" id="top" role="banner">
  <nav role="navigation">        
      <div class="navbar-heade">
        <div class="container">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-nav">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>   
          <div class="row">
            <div class="logo navbar-left">
              <a href="<?= base_url() ?>" class="navbar-brand" ><img src="<?= base_url() ?>assets/images/logo-small.png" alt=""></a>
            </div> 
            <div class="col-md-6 navbar-form navbar-right search" role="search">
              <div class="form-group">
                <div class="input-group">
                  <input type="text" class="form-control" placeholder="Pencarian Menu" id="search-menu-global" style="width:420px;">
                  <a href="javascript:void(0)" class="btn btn-danger" id="zoom_in" title="Perbesar" data-placement="bottom" style="padding: 4px 9px;margin-left:2px;"><i class="fa fa-search-plus"></i></a>
                  <a href="javascript:void(0)" class="btn btn-danger" id="zoom_out" title="Perkecil" data-placement="bottom" style="padding: 4px 9px;margin-left:2px;"><i class="fa fa-search-minus"></i></a>
                  <a href="<?= base_url("manual/manual") ?>" class="btn btn-danger" id="question" title="Manual" data-placement="bottom" style="padding: 4px 9px;margin-left:2px;"><i class="fa fa-question"></i></a>
                </div>                  
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-nav">
        <div class="container">                          
          <ul class="nav navbar-nav navbar-left">
            <?php foreach($MENUS as $m): ?>
            <?php if($m['data']->IS_EXIST>0): ?>
            <li class="<?= $m['data']->MENU_ID==$MENU_ACTIVE ? "active" : "" ?>">  <!-- add class active -->                
              <a href="<?= ($m['data']->URL!="" ? base_url($m['data']->URL) : "javascript:void(0);" ) ?>" class="level1" data-menu_id="<?= $m['data']->MENU_ID ?>" title="<?= $m['data']->NAME ?>" data-placement="bottom">
                <span class="arrow-down"></span>
                <span class="<?= $m['data']->ICON ?>"></span>
              </a>
            </li>
            <?php endif; ?>
            <?php endforeach; ?>
          </ul>                           
          <ul class="nav navbar-nav navbar-right">                        
            <li class="desc"><a href="<?= base_url("auth/profile") ?>"><?= $current_user->USERNAME." | ".$current_user->NAME ?></a></li> 
            <li>                  
              <a href="<?= base_url("auth/logout") ?>" title="Logout" data-placement="bottom">
                <span class="arrow-down"></span>
                <span class="ico-nav logout"></span>
                Logout
              </a>
            </li>             
          </ul> 
        </div>
      </div>
      <div class="clearfix"></div>
  </nav>
  <div class="clearfix"></div>
</header>