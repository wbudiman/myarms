<div class="row">
  <div class="col-md-12">
    <div class="billing-tabs <?= ($MENU_ACTIVE=="" ? "hide" : "") ?>">
      <?php foreach($MENUS as $m): ?>
        <?php if($m['data']->IS_EXIST>0 && sizeof($m['childs'])>0): ?>
          <div class="level2 <?= $m['data']->MENU_ID==$MENU_ACTIVE ? "" : "hide" ?>" data-parent="<?= $m['data']->MENU_ID ?>">
            <ul class="nav nav-tabs" role="tablist">
              <?php foreach($m['childs'] as $l2): ?>
                <?php if($l2['data']->IS_EXIST>0): ?>
                <li class="<?= isset($MENU_PARENT[1]) && $MENU_PARENT[1]==$l2['data']->MENU_ID ? "active" : "" ?>">
                  <a href="<?= ($l2['data']->URL!="" ? base_url($l2['data']->URL) : "#tab".$l2['data']->MENU_ID) ?>" role="tab" data-toggle="tab"><?= $l2['data']->NAME ?></a>
                </li>
                <?php endif; ?>
              <?php endforeach; ?>
            </ul>
            <div class="tab-content">
              <?php foreach($m['childs'] as $l2): ?>
              <?php if($l2['data']->IS_EXIST>0): ?>
              <div style="<?= (isset($MENU_PARENT[4]) && $MENU_PARENT[4]!="" ? "height: 100px;" : "") ?>" class="tab-pane fade in <?= isset($MENU_PARENT[1]) && $MENU_PARENT[1]==$l2['data']->MENU_ID ? "active" : "" ?>" id="tab<?= $l2['data']->MENU_ID ?>">
                <ul class="nav-tab">
                  <?php foreach($l2['childs'] as $l3): ?>
                    <?php if($l3['data']->IS_EXIST>0): ?>
                    <li class="<?= (sizeof($l3['childs'])>0 ? "has_menu" : "") ?> <?= isset($MENU_PARENT[2]) && $MENU_PARENT[2]==$l3['data']->MENU_ID ? "active" : "" ?>">
                      <a href="<?= ($l3['data']->URL!="" ? base_url($l3['data']->URL) : "javascript:void(0)") ?>"><?= $l3['data']->NAME ?></a>
                      <?php if(sizeof($l3['childs'])>0 && $l3['data']->IS_EXIST>0): ?>
                      <ul class="sub-one">
                        <?php foreach($l3['childs'] as $l4): ?>
                          <?php if($l4['data']->IS_EXIST>0): ?>
                            <li class="<?= (sizeof($l4['childs'])>0 ? "has_menu_two" : "") ?> <?= isset($MENU_PARENT[3]) && $MENU_PARENT[3]==$l4['data']->MENU_ID ? "active" : "" ?>">
                              <a href="<?= ($l4['data']->URL!="" ? base_url($l4['data']->URL) : "javascript:void(0)") ?>"><?= $l4['data']->NAME ?></a>
                              <?php if(sizeof($l4['childs'])>0 && $l4['data']->IS_EXIST>0): ?>
                              <ul class="sub-two <?= isset($MENU_PARENT[3]) && $MENU_PARENT[3]==$l4['data']->MENU_ID ? "active" : "" ?>">
                                <?php foreach($l4['childs'] as $l5): ?>
                                  <?php if($l5['data']->IS_EXIST>0): ?>
                                    <li class="<?= isset($MENU_PARENT[4]) && $MENU_PARENT[4]==$l5['data']->MENU_ID ? "active" : "" ?>"><a href="<?= ($l5['data']->URL!="" ? base_url($l5['data']->URL) : "javascript:void(0)") ?>"><!--<span class="arrow-down"></span>--><?= $l5['data']->NAME ?></a></li>
                                  <?php endif; ?>
                                <?php endforeach; ?>
                              </ul>
                              <?php endif; ?>
                            </li>
                          <?php endif; ?>
                        <?php endforeach; ?>
                      </ul>
                      <?php endif; ?>
                    </li>
                    <?php endif; ?>
                  <?php endforeach; ?>
                </ul>                    
              </div>
              <?php endif; ?>
              <?php endforeach; ?>
            </div>
            <div class="head-title"><?= $m['data']->NAME ?></div>
          </div>
        <?php endif; ?>
      <?php endforeach; ?>
    </div>
  </div>
  <div class="clearfix"></div>
</div> 