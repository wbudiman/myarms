<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Server Requirements : CodeIgniter User Guide</title>

<style type='text/css' media='all'>@import url('../userguide.css');</style>
<link rel='stylesheet' type='text/css' media='all' href='../userguide.css' />

<script type="text/javascript" src="../nav/nav.js"></script>
<script type="text/javascript" src="../nav/prototype.lite.js"></script>
<script type="text/javascript" src="../nav/moo.fx.js"></script>
<script type="text/javascript" src="../nav/user_guide_menu.js"></script>

<meta http-equiv='expires' content='-1' />
<meta http-equiv= 'pragma' content='no-cache' />
<meta name='robots' content='all' />

</head>
<body>

<!-- START NAVIGATION -->
<div id="nav"><div id="nav_inner"><script type="text/javascript">create_menu('../');</script></div></div>
<div id="nav2"><a name="top"></a><a href="javascript:void(0);" onclick="myHeight.toggle();"><img src="../images/nav_toggle_darker.jpg" width="154" height="43" border="0" title="Toggle Table of Contents" alt="Toggle Table of Contents" /></a></div>
<div id="masthead">
<table cellpadding="0" cellspacing="0" border="0" style="width:100%">
<tr>
<td><h1>My Arms Aplication User Guide</h1></td>
<td id="breadcrumb_right"><a href="<?php echo site_url();?>./manual/toc">Pilih Manual</a></td>
</tr>
</table>
</div>
<!-- END NAVIGATION -->


<br clear="all" />


<!-- START CONTENT -->
<div id="content">

<h1>Billing Result - Revenue</h1>
<img src="../images/revenue.jpg">
<ul>
	<li>Tampilan Menu Revenue</li>
    <img src="../images/revenue_1.jpg">
<center>Gambar.1</center>
Pada Gambar.1, merupakan tampilan untuk memilih data apa yang akan ditampilkan.
<center>Gambar.2</center>
Pada Gambar.2, sama halnya dengan Gambar.1, yaitu untuk memilih data yang akan ditampilkan.
<center>Gambar.3</center>
Untuk Gambar.3 merupakan tombol yang berfungsi untuk menampilkan dan mendownload data.
	<li>Menampilkan Data Revenue</li>
Untuk menampilkan data pada menu Revenu dapat mengikuti langkah sebagai berikut :<br>
<p>1.	Memilih periode yang akan ditampilkan seperti gambar berikut :</p>
<p>2.	Kemudian pilih klik tombol Search untuk melihat tampilan periode yang telah dipilih</p>
<p>3.	Tampilan periode yang telah dipilih</p>
	<ul>
		<li>Fungsi tombol()pada tampilan data, yaitu untuk menampilkan data dari urutan kecil ke besar atau sebaliknya.</li>
		<li>Pada Baris paling bawah terdapat Total, yang menjumlahkan keseluruhan jumlah data sesuai dengan kolom masing-masing.</li>
		<li>Fungsi tombol    pada tampilan menu  , untuk menyembunyikan dan menampilankan Parameter Pencarian Data.</li>
	</ul>
<p>4.	Selanjutnya, data yang tampil dapat diDownload dengan klik tombol  , maka data akan didownload dalam bentuk . Excel.</p>
</ul>



</div>
<!-- END CONTENT -->



<div id="footer">
<p>
<a href="#top">Top of Page</a>&nbsp;&nbsp;&nbsp;&middot;&nbsp;&nbsp;
</div>

</body>
</html>