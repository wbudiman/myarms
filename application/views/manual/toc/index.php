<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<style type='text/css' media='all'>@import url('userguide.css');</style>
<link rel='stylesheet' type='text/css' media='all' href='userguide.css' />

<script type="text/javascript" src="nav/nav.js"></script>
<script type="text/javascript" src="nav/prototype.lite.js"></script>
<script type="text/javascript" src="nav/moo.fx.js"></script>
<script type="text/javascript" src="nav/user_guide_menu.js"></script>

<meta http-equiv='expires' content='-1' />
<meta http-equiv= 'pragma' content='no-cache' />
<meta name='robots' content='all' />
<meta name='author' content='ExpressionEngine Dev Team' />

</head>
<body>


<!-- START NAVIGATION -->
<div id="nav"><div id="nav_inner"><script type="text/javascript">create_menu('null');</script></div></div>
<div id="nav2"><a name="top"></a><a href="javascript:void(0);" onclick="myHeight.toggle();"><img src="images/nav_toggle_darker.jpg" width="154" height="43" border="0" title="Toggle Table of Contents" alt="Toggle Table of Contents" /></a></div>
<div id="masthead">
<table cellpadding="0" cellspacing="0" border="0" style="width:100%">
<tr>
<td><h1>My Arms Aplication User Guide</h1></td>
</tr>
</table>
</div>
<!-- END NAVIGATION -->


<!-- START BREADCRUMB -->

<table cellpadding="0" cellspacing="0" border="0" style="width:100%">
<tr>
<td id="breadcrumb">
<a href="#">My Arms Aplication</a> &nbsp;&#8250;&nbsp;
<a href="#">User Guide Home</a> &nbsp;&#8250;&nbsp;
Pilih Manual
</td>

</tr>
</table>
<!-- END BREADCRUMB -->

<br clear="all" />

<!-- START CONTENT -->
<div id="content">


<h1>Pilih Manual</h1>

<table cellpadding="0" cellspacing="10" border="0" width="100%">
<tr>
<td valign="top" width="25%">

<h3>Billing Result</h3>
<ul>
	<li><a href="<?php echo site_url();?>./manual/revenue">Revenue</a></li>
	<li><a href="<?php echo site_url();?>./manual/growth">Growth</a></li>
	<li><a href="<?php echo site_url();?>./manual/growthL11">Growth L11</a></li>
	<li><a href="<?php echo site_url();?>./manual/berita_acara">Berita Acara</a></li>
	<li><a href="<?php echo site_url();?>./manual/komite_billing">Komite Billing</a></li>
</ul>

<h3>Gimmick Mgt</h3>
<ul>
	<li><a href="#">Katalog Gimmick </a></li>
	<li><a href="#">Gimmick Aktif Per Periode</a></li>
	<li><a href="#">Pelanggan Dpt Gimmick</a></li>
	<li><a href="#">Pelanggan Berakhir Gimmick</a></li>
</ul>


</td>
<td valign="top" width="25%">

<h3>Detail Billing</h3>
<ul>
	<li><a href="#">Revenue</a></li>
	<li><a href="#">Growth</a></li>
	<li><a href="#">Berita Acara</a></li>
	<li><a href="#">KOmite Billing</a></li>
	<li><a href="#">Pohon Revenue</a></li>
</ul>

<h3>New Wave</h3>
<ul>
	<li><a href="#">Indihome</a></li>
	<li><a href="#">IME</a></li>
</ul>


</td>
<td valign="top" width="25%">

<h3>Warm Billing</h3>
<ul>
	<li><a href="#">By Tanggal</a></li>
	<li><a href="#">By Hari</a></li>
	<li><a href="#">Rekap Harian</a></li>
	<li><a href="#">Ekstrim Usage</a></li>
	<li><a href="#">L251</a></li>
</ul>


<h3>Administrator</h3>
<ul>
<li><a href="#">Hak Akses</a></li>
<li><a href="#">Otorisasi User</a></li>
</ul>


</td>
<td valign="top" width="25%">

<h3>Collection & Settlement - Retail</h3>
<ul>
<li><a href="#">IF Master TREMS</a></li>
<li><a href="#">Rekon Finnet</a></li>
<li><a href="#">Collection</a></li>
<li><a href="#">Laporan Loket</a></li>
<li><a href="#">Debt Management</a></li>
</ul>


</td>
</tr>
</table>

</div>
<!-- END CONTENT -->


<div id="footer">
<p><a href="#top">Top of Page</a></p>
</div>


</body>
</html>