<form method="post" action="<?= base_url($path."index") ?>">
Ini Halaman Manual
</form>
<script type="text/javascript">
  height=335;
  $(document).ready(function(){
    $("#table_gimmick_catalog").dataTable({
      "bSort" : true,
      "bFilter" : false,
      "bPaginate": false,
      "info": false,
    });
    $("#ubis").multiselect({
      dropRight: true,
      includeSelectAllOption: true,
      numberDisplayed:2
    }).change(function(){
      var ubis=$(this).val();
      $.ajax({
        url:"<?= base_url("ajax/getUbisSegments") ?>",
        dataType:"JSON",
        data:{ubis:ubis},
        success:function(response){
          $("#ubis_segment").multiselect("destroy");
          $("#ubis_segment").html(response.content);
          $("#ubis_segment").multiselect({
            dropRight: true,
            includeSelectAllOption: true,
            maxHeight: 212,
            enableFiltering: true
          });
        }
      });
    });
    $("#ubis_segment").multiselect({
      dropRight: true,
      includeSelectAllOption: true,
      numberDisplayed:2
    }).change(function(){
      var ubis_segment=$(this).val();
      $.ajax({
        url:"<?= base_url("ajax/getBisnisArea") ?>",
        dataType:"JSON",
        data:{ubis_segment:ubis_segment},
        success:function(response){
          $("#bisnis_area").multiselect("destroy");
          $("#bisnis_area").html(response.content);
          $("#bisnis_area").multiselect({
            dropRight: true,
            includeSelectAllOption: true,
            maxHeight: 212,
            enableFiltering: true
          });
        }
      });
    });
    $("#product,#bisnis_area").multiselect({numberDisplayed:2,maxHeight: 212,includeSelectAllOption: true,enableFiltering: true});
    $("form").submit(function(){
      var periode1=$("#year_1").val()+$("#month_1").val();
      var periode2=$("#year_2").val()+$("#month_2").val();
      /*if(periode2=='' && periode1==''){
        main.notification("bottomRight","warning","Peringatan","Silahkan pilih start promo dan end promo dengan benar!");
        return false;
      }*/
    });
  });
</script>