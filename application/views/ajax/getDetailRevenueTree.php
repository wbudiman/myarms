<form method="post" action="<?= base_url("billing_management/detail_billing/tree/index") ?>">
  <input type="hidden" name="search[table_type]" value="<?= $table_type ?>">
  <?php foreach($data as $key=>$d): ?>
    <input type="hidden" name="search[<?= $key ?>]" value="<?= $d ?>">
  <?php endforeach; ?>
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
    <h4 class="semi-bold">Pohon Revenue Detail (100 First Records)</h4>
  </div>
  <div class="modal-body">
    <div style="height:400px;overflow-y:auto" id="table_tree_detail">
      <?= $content ?>
    </div>
  </div>
  <div class="modal-footer">
    <input type="submit" name="search[button]" class="btn btn-primary" value="Download to Txt">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  </div>
</form>
<script type="text/javascript">
  $(document).ready(function(){
    $("#table_tree_detail table").dataTable({
      "bSort" : true,
      "bFilter" : false,
      "bPaginate": false,
      "info": false
    });
  });
</script>
