<h5><?= $header_title ?> ( <?= $header_parameter ?> )</h5>
<table border="1">
  <thead>
          <?php
            echo "<tr>";
            echo "<th width='80'>Witel</th>";
            echo "<th width='80'>Area</th>";
            echo "<th width='80'>Lokasi</th>";
            echo "<th>Kode Loket</th>";
            echo "<th>Loket</th>";
            echo "<th>Tahapan</th>";
            echo "<th>Petugas</th>";
            echo "<th>Transaksi</th>";
            echo "<th>Buka Tahapan</th>";
            echo "<th>Tutup Tahapan</th>";
            echo "<th>Saldo</th>";
            echo "<th>Jumlah</th>";
            echo "<th>Validasi</th>";
            echo "<th>Tgl Validasi</th>";
            echo "</tr>";
          ?>
        </thead>
        <tbody>
          <?php
            foreach($lists as $l){
              echo "<tr>";
              echo "<td width='80'>".$l->WITEL_DESCRIPTION."</td>";
              echo "<td width='80'>".$l->DATEL_DESCRIPTION."</td>";
              echo "<td width='80'>".$l->LOCATION."</td>";
              echo "<td>".$l->CODE_LOCKET."</td>";
			  echo "<td>".$l->LOCKET_NAME."</td>";
			  echo "<td>".$l->STEP."</td>";
			  echo "<td>".$l->PTGS_CASH_DESK."</td>";
			  echo "<td>".$l->TRANSACTION_TYPE."</td>";
			  echo "<td>".$l->STEP_OPEN_DATE."</td>";
			  echo "<td>".$l->STEP_CLOSE_DATE."</td>";
			  echo "<td>".$l->AMOUNT."</td>";
			  echo "<td>".$l->SST."</td>";
			  echo "<td>".$l->STATUS_VALIDATION."</td>";
			  echo "<td>".$l->VALIDATION_DATE."</td>";
              echo "</tr>";
            }
          ?>
        </tbody>
</table>