<form method="post" action="<?= base_url($path."index") ?>">
  <div class="row">
    <div class="col-md-12">
      <div id="table-search">
        <div class="wrap">
          <table class="table table-search table-striped">
            <thead>
              <tr>
                <th colspan="8" v-align="middle">Parameter Pencarian Data</th>
              </tr>
            </thead>
            <tbody>
              <tr>  f                 
                <td>Nomor Jastel</td>
                <td>
                  <input type="text" name="search[no_jastel]" value="<?php if(isset($search['no_jastel'])) echo $search['no_jastel'];?>">
                </td>
                <td>Periode</td>
                <td>
                  <div class="col-md-9" style="padding-left:0px;">
                    <div class="col-md-7" style="padding-left:0px;">
                      <select name="search[month_1]" class="form-control" id="month_1">
                        <?php 
                          foreach($month_lists as $key=>$d){
                            echo "<option value='".$key."' ".($search['month_1']==$key ? "selected" : "").">".$d."</option>";
                          }
                        ?>
                      </select>
                    </div>
                    <div class="col-md-5" style="padding-left:0px;">
                      <select name="search[year_1]" class="form-control" id="year_1">
                        <?php 
                          for($x=date("Y");$x>(date("Y")-5);$x--){
                            echo "<option value='".$x."' ".($search['year_1']==$x ? "selected" : "").">".$x."</option>";
                          }
                        ?>
                      </select>
                    </div>
                  </div>
              </tr>
			  </tbody>
            <tfoot>
              <tr>                   
                <td>Search</td>
                <td>
                  <input type="submit" name="search[button]" class="btn btn-search" value="Search">
                </td>
                <td>To Excel</td>
                <td>
                  <input type="submit" name="search[button]" class="btn btn-search" value="Download">
                </td>
              </tr>
            </tfoot>
          </table>
        </div>
        <a href="javascript:void(0)" class="showhide"></a>
      </div>
    </div>
  </div> 
</form>
<div class="row">
  <div class="col-md-12">
    <h4 class="title-menu">PSB</h4> 
    <div class="billing-detail">
      <div class="clearfix"></div>
      <table class="table table-bordered" id="table_gimmick_catalog">
        <thead>
          <?php
            echo "<tr>";
            echo "<th width='80'>CCA</th>";
            echo "<th width='80'>NCLI</th>";
            echo "<th>NDOS</th>";
			      echo "<th width='80'>No Jastel</th>";
            echo "<th>Produk ID</th>";
            echo "<th>Nama</th>";
            echo "<th>Alamat</th>";
            echo "<th>PS Date</th>";
            echo "<th>CA Date</th>";
            echo "<th>Kategori</th>";
            echo "<th>Trans Type</th>";
            echo "</tr>";
          ?>
        </thead>
        <tbody>
          <?php
            foreach($lists as $l){
              echo "<tr>";
              echo "<td width='80'>".$l->CCA."</td>";
              echo "<td width='80'>".$l->NCLI."</td>";
              echo "<td>".$l->NDOS."</td>";
			  echo "<td width='80'>".$l->ND."</td>";
              echo "<td>".$l->CPROD."</td>";
			  echo "<td>".$l->NAME."</td>";
			  echo "<td>".$l->ADDRESS."</td>";
			  echo "<td>".$l->PSDATE."</td>";
			  echo "<td>".$l->CADATE."</td>";
			  echo "<td>".$l->CATEGORY."</td>";
			  echo "<td>".$l->TRANS_TYPE."</td>";
              echo "</tr>";
            }
          ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<script type="text/javascript">
  height=335;
  $(document).ready(function(){
    $("#table_gimmick_catalog").dataTable({
      "bSort" : true,
      "bFilter" : false,
      "bPaginate": false,
      "info": false,
    });
    $("#division").multiselect({
      includeSelectAllOption: true,
      numberDisplayed:2
    }).change(function(){
      var divisions=$(this).val();
      $.ajax({
        url:"<?= base_url("ajax/getWitel") ?>",
        dataType:"JSON",
        data:{divisions:divisions},
        success:function(response){
          $("#witel").multiselect("destroy");
          $("#witel").html(response.content);
          $("#witel").multiselect({
            includeSelectAllOption: true,
            maxHeight: 212,
            enableFiltering: true
          }).change();
        }
      });
    });
	$("#witel").live("change",function(){
    var divisions=$("#division").val();
    var witels=$(this).val();
    $.ajax({
      url:"<?= base_url("ajax/getDatel") ?>",
      dataType:"JSON",
      data:{witels:witels,divisions:divisions},
      success:function(response){
        $("#datel").multiselect("destroy");
        $("#datel").html(response.content);
        $("#datel").multiselect({
          includeSelectAllOption: true,
          maxHeight: 212,
          enableFiltering: true,
          numberDisplayed:2
        }).change();
      }
    });
  });
    $("#witel,#datel,#locket").multiselect({numberDisplayed:2,maxHeight: 212,includeSelectAllOption: true,enableFiltering: true});
    $("#ubis_segment").multiselect({dropRight: true,numberDisplayed:2,maxHeight: 212,includeSelectAllOption: true,enableFiltering: true});
    $("form").submit(function(){
      var periode1=$("#year_1").val()+$("#month_1").val();
      var periode2=$("#year_2").val()+$("#month_2").val();
      if(periode2<periode1){
        main.notification("bottomRight","warning","Peringatan","Silahkan pilih start promo dan end promo dengan benar!");
        return false;
      }
    });
  });
</script>