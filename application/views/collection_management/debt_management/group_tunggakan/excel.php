<h5><?= $header_title ?> ( <?= $header_parameter ?> )</h5>
<table border="1">
  <thead>
    <?php 
      echo "<tr>";
      echo "<th class='text-center'>".(is_array($row_lists[$search['row']]) ? "Komponen Billing" : $row_lists[$search['row']])."</th>";
      $columns=array();
      foreach($lists as $l){
        if(is_array($field[$search['column']]))
        {
          foreach($field[$search['column']] as $c){
            if(!in_array($c->BILLING_COMPONENT_FIELD,$columns)){
              array_push($columns,$c->BILLING_COMPONENT_FIELD);
              echo "<th class='text-center'>".($c->BILLING_COMPONENT_DESCRIPTION!="" && $c->BILLING_COMPONENT_DESCRIPTION!=NULL ? $c->BILLING_COMPONENT_DESCRIPTION : $no_mapping)."</th>";
              
            }
          }
        }else{
          if(!in_array($l->{$field[$search['column']]},$columns)){
            array_push($columns,$l->{$field[$search['column']]});
            echo "<th class='text-center'>".($l->{$field[$search['column']]}!="" && $l->{$field[$search['column']]}!=NULL ? $l->{$field[$search['column']]} : $no_mapping)."</th>";
          }
        }
      }
      if($search['column']!="billing_component"){
        echo "<th class='text-center'>Total</th>";
      }
      echo "</tr>";
    ?>
  </thead>
  <tbody>
    <?php   
      $rows=array();
      $total_column=array();
      $total_column['total']=0;
      foreach($lists as $l){
        if(is_array($field[$search['row']]))
        {
          foreach($field[$search['row']] as $r){
            if(!in_array($r->BILLING_COMPONENT_FIELD,$rows)){
              array_push($rows,$r->BILLING_COMPONENT_FIELD);
              echo "<tr>";
              echo "<td>".($r->BILLING_COMPONENT_DESCRIPTION!="" && $r->BILLING_COMPONENT_DESCRIPTION!=NULL ? $r->BILLING_COMPONENT_DESCRIPTION : $no_mapping)."</td>";
              $total=0;
              $counter=0;
              foreach($columns as $c){
                $check=0;
                foreach($lists as $l2){
                  
                  if(is_array($field[$search['column']]))
                  {
                    
                      foreach($field[$search['column']] as $f)
                      {
                        if($l->{$r->BILLING_COMPONENT_FIELD}==$l2->{$f->BILLING_COMPONENT_FIELD}){
                          if($f->BILLING_COMPONENT_FIELD==$c){
                            $value=$l2->{$r->BILLING_COMPONENT_FIELD};
                            if($search['view_by']=="L11"){
                              $value=$l2->SST;
                            }elseif($search['view_by']=="ARPU"){
                              $value=$value/$l2->SST;
                            }
                            $total+=$value;
                            if(!isset($total_column[$c]))$total_column[$c]=0;
                            $total_column[$c]+=$value;
                            $counter++;
                            echo "<td class='text-right'>".$value."</td>";
                            $check=1;
                          }
                        }
                      }
                  }else{
                    if($c==$l2->{$field[$search['column']]}){
                      $value=$l2->{$r->BILLING_COMPONENT_FIELD};
                      if($search['view_by']=="L11"){
                        $value=$l2->SST;
                      }elseif($search['view_by']=="ARPU"){
                        $value=$value/$l2->SST;
                      }
                      $total+=$value;
                      if(!isset($total_column[$c]))$total_column[$c]=0;
                      $total_column[$c]+=$value;
                      $counter++;
                      echo "<td class='text-right'>".$value."</td>";
                      $check=1;
                    }
                  }
                  
                }
                if($check==0){
                  if(!isset($total_column[$c]))$total_column[$c]=0;
                  $total_column[$c]+=0;
                  echo "<td class='text-right'>0</td>";
                }
              }
              if($search['column']!="billing_component"){
                if(!isset($total_column['total']))$total_column['total']=0;
                $total_column['total']+=$total;
                echo "<td class='text-right'>".$total."</td>";
              }
              echo "</tr>";
            }
          }
        }else{
          if(!in_array($l->{$field[$search['row']]},$rows)){
            if(is_array($field[$search['column']]))
            {
              echo "<tr>";
              echo "<td>".($l->{$field[$search['row']]}!="" && $l->{$field[$search['row']]}!=NULL ? $l->{$field[$search['row']]} : $no_mapping)."</td>";
              array_push($rows,$l->{$field[$search['row']]});
              $total=0;
              $counter=0;
              foreach($field[$search['column']] as $c){
                foreach($lists as $l2){
                  if($l->{$field[$search['row']]}==$l2->{$field[$search['row']]}){
                    $value=$l2->{$c->BILLING_COMPONENT_FIELD};
                    if($search['view_by']=="L11"){
                      $value=$l2->SST;
                    }elseif($search['view_by']=="ARPU"){
                      $value=$value/$l2->SST;
                    }
                    $total+=$value;
                    if(!isset($total_column[$c->BILLING_COMPONENT_FIELD]))$total_column[$c->BILLING_COMPONENT_FIELD]=0;
                    $total_column[$c->BILLING_COMPONENT_FIELD]+=$value;
                    $counter++;
                    echo "<td class='text-right'>".$value."</td>";
                    $check=1;
                  }
                }
              }
              if($search['column']!="billing_component"){
                if(!isset($total_column['total']))$total_column['total']=0;
                $total_column['total']+=$total;
                echo "<td class='text-right'>".$total."</td>";
              }
              echo "</tr>";
            }else{
              array_push($rows,$l->{$field[$search['row']]});
              echo "<tr>";
              echo "<td>".($l->{$field[$search['row']]}!="" && $l->{$field[$search['row']]}!=NULL ? $l->{$field[$search['row']]} : $no_mapping)."</td>";
              $total=0;
              $counter=0;
              foreach($columns as $c){
                $check=0;
                foreach($lists as $l2){
                  if($l->{$field[$search['row']]}==$l2->{$field[$search['row']]})
                  {
                    if($c==$l2->{$field[$search['column']]}){
                      if($search['view_by']=="L11"){
                        $value=$l2->SST;
                      }elseif($search['view_by']=="ARPU"){
                        $value=$l2->{$search['billing_component']}/$l2->SST;
                      }else{
                        $value=$l2->{$search['billing_component']};
                      }
                      $total+=$value;
                      if(!isset($total_column[$c]))$total_column[$c]=0;
                      $total_column[$c]+=$value;
                      $counter++;
                      echo "<td class='text-right'>".$value."</td>";
                      $check=1;
                    }
                  }
                }
                if($check==0){
                  echo "<td class='text-right'>0</td>";
                  if(!isset($total_column[$c]))$total_column[$c]=0;
                  $total_column[$c]+=0;
                }
              }
              if($search['column']!="billing_component"){
                if(!isset($total_column['total']))$total_column['total']=0;
                $total_column['total']+=$total;
                echo "<td class='text-right'>".$total."</td>";
              }
              echo "</tr>";
            }
          }
        }
      }
    ?>
  </tbody>
  <tfoot>
  <?php
    if($search['row']!="billing_component"){
      echo "<tr>";
      echo "<td style='padding:8px;'>Total (".sizeof($rows)." item)</td>";
      foreach($columns as $c){
        foreach($total_column as $key=>$t){
          if($c==$key){
            echo "<td class='text-right' style='padding:8px;'>".$t."</td>";
          }
        }
      }
      if($search['column']!="billing_component"){
        echo "<td class='text-right' style='padding:8px;'>".$total_column['total']."</td>";
      }
      echo "</tr>";
    }
  ?>
  </tfoot>
</table>