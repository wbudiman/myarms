<form method="post" action="<?= base_url($path."index") ?>">
  <div class="row">
    <div class="col-md-12">
      <div id="table-search">
        <div class="wrap">
          <table  class="table table-search table-striped">
            <thead>
              <tr>
                <th colspan="8" v-align="middle">Parameter Pencarian Data</th>
              </tr>
            </thead>
            <tbody>
              <tr>                   
                <td>Divisi</td>
                <td>
                  <select name="search[division][]" id="division" multiple class="form-control">
                    <?php 
                      foreach($division_lists as $d){
                        echo "<option value='".$d->DIVISION_CODE."' ".($search['division']!="" && in_array($d->DIVISION_CODE,$search['division']) ? "selected" : "").">".$d->DIVISION_CODE."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Summary Product</td>
                <td>
                  <select name="search[summary_product][]" multiple class="form-control" id="summary_product">
                    <?php 
                      foreach($summary_product_lists as $d){
                        echo "<option value='".$d->SUMMARY_PRODUCT."' ".($search['summary_product']!="" && in_array($d->SUMMARY_PRODUCT,$search['summary_product']) ? "selected" : "").">".$d->SUMMARY_PRODUCT."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Top Customer</td>
                <td>
                  <select name="search[top_customer][]" multiple class="form-control" id="top_customer">
                    <?php 
                      echo '<optgroup label="Top 20">';
                      foreach($top_customer_lists as $r){
                        if($r->PORTOFOLIO_GROUP=="Top 20")
                        {
                          echo "<option value='".$r->PORTOFOLIO_ID."' ".($search['top_customer']!="" && in_array($r->PORTOFOLIO_ID,$search['top_customer']) ? "selected" : "").">".$r->PORTOFOLIO_NAME."</option>";
                        }
                      }
                      echo '</optgroup>';
                      echo '<optgroup label="Non Top 20">';
                      foreach($top_customer_lists as $r){
                        if($r->PORTOFOLIO_GROUP=="Non Top 20")
                        {
                          echo "<option value='".$r->PORTOFOLIO_ID."' ".($search['top_customer']!="" && in_array($r->PORTOFOLIO_ID,$search['top_customer']) ? "selected" : "").">".$r->PORTOFOLIO_NAME."</option>";
                        }
                      }
                      echo '</optgroup>';
                    ?>
                  </select>
                </td>
              <td>Urut/Loncat</td>
                <td>
                  <select name="search[urut_loncat]" id="validasi" class="form-control">
          <option value="" <?php if(isset($search['urut_loncat'])&&($search['urut_loncat']=='')) echo "selected";?>>ALL</option>
          <option value="URUT" <?php if(isset($search['urut_loncat'])&&($search['urut_loncat']=='URUT')) echo "selected=true";?>>URUT</option>
          <option value="LONCAT" <?php if(isset($search['urut_loncat'])&&($search['urut_loncat']=='LONCAT')) echo "selected=true";?>>LONCAT</option>
                  </select>
                </td>  
              </tr>
              <tr>                   
                <td>Witel</td>
                <td>
                  <select name="search[witel][]" id="witel" multiple class="form-control">
                    <?php 
                      foreach($witel_lists as $w){
                        echo "<option value='".$w->WITEL_CODE."' ".($search['witel']!="" && in_array($w->WITEL_CODE,$search['witel']) ? "selected" : "").">".$w->WITEL_DESCRIPTION."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Produk</td>
                <td>
                  <select name="search[product][]" multiple class="form-control" id="product">
                    <?php 
                      foreach($product_lists as $r){
                        echo "<option value='".$r->PRODUCT_ID."' ".($search['product']!="" && in_array($r->PRODUCT_ID,$search['product']) ? "selected" : "").">".$r->PRODUCT_NAME."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Indihome</td>
                <td>
                  <select name="search[indihome]" class="form-control">
                    <option value="">All</option>
                    <?php 
                      foreach($indihome_lists as $r){
                        echo "<option value='".$r->INDIHOME_ID."' ".($search['indihome']==$r->INDIHOME_ID ? "selected" : "").">".$r->INDIHOME_DESCRIPTION."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Status Pelanggan</td>
                <td>
                  <select name="search[status_pelanggan]" id="validasi" class="form-control">
          <option value="" <?php if(isset($search['status_pelanggan'])&&($search['status_pelanggan']=='')) echo "selected";?>>ALL</option>
          <option value="AKTIF" <?php if(isset($search['status_pelanggan'])&&($search['status_pelanggan']=='AKTIF')) echo "selected=true";?>>AKTIF</option>
          <option value="CABUT" <?php if(isset($search['status_pelanggan'])&&($search['status_pelanggan']=='CABUT')) echo "selected=true";?>>CABUT</option>
                  </select>
                </td>  
              </tr>
              <tr>                   
                <td>Datel</td>
                <td>
                  <select name="search[datel][]" id="datel" multiple class="form-control">
                    <?php 
                      foreach($datel_lists as $w){
                        echo "<option value='".$w->DATEL_CODE."' ".($search['datel']!="" && in_array($w->DATEL_CODE,$search['datel']) ? "selected" : "").">".$w->DATEL_DESCRIPTION."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Segmen</td>
                <td>
                  <select name="search[ubis_segment][]" id="ubis_segment" multiple class="form-control">
                    <?php 
                      foreach($segment_lists as $r){
                        echo "<option value='".$r->UBIS_SEGMENT_ID."' ".($search['ubis_segment']!="" && in_array($r->UBIS_SEGMENT_ID,$search['ubis_segment']) ? "selected" : "").">".$r->UBIS_SEGMENT."</option>";
                      }
                    ?>
                  </select>
                </td>
                
                <td>Kategori</td>
                <td>
                  <select name="search[ubis][]" multiple class="form-control" id="ubis">
                    <?php 
                      foreach($ubis_lists as $d){
                        echo "<option value='".$d->UBIS_ID."' ".($search['ubis']!="" && in_array($d->UBIS_ID,$search['ubis']) ? "selected" : "").">".$d->UBIS."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td colspan="2">&nbsp;</td>
              </tr>
              <tr>                   
                <td>Jumlah Kuitansi</td>
                <td>
                  <select name="search[jumlah_kuitansi][]" id="jumlah_kuitansi" multiple class="form-control">
                    <?php 
                      foreach($jumlah_kuitansi_lists as $w){
                        echo "<option value='".$w->JUMLAH_KUITANSI."' ".($search['jumlah_kuitansi']!="" && in_array($w->JUMLAH_KUITANSI,$search['jumlah_kuitansi']) ? "selected" : "").">".$w->JUMLAH_KUITANSI."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Umur Termuda</td>
                <td>
                  <select name="search[umur_termuda][]" id="umur_termuda" multiple class="form-control">
                    <?php 
                      foreach($umur_termuda_lists as $w){
                        echo "<option value='".$w->UMUR_TERMUDA."' ".($search['umur_termuda']!="" && in_array($w->UMUR_TERMUDA,$search['umur_termuda']) ? "selected" : "").">".$w->UMUR_TERMUDA."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td colspan="4"></td>
              </tr>
              <tr>
                <td>Baris</td>
                <td>
                  <select name="search[show_data]" class="form-control" id="show_data">
                    <?php 
                      foreach($row_lists as $key=>$d){
                        echo "<option value='".$key."' ".($search['show_data']==$key ? "selected" : "").">".$d."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Kolom</td>
                <td>
                  <select name="search[kolom][]" id="usage" multiple class="form-control">
                    <?php 
                      foreach($column_lists as $key=>$d){
                        echo "<option value='".$key."' ".($search['kolom']==$key ? "selected" : "").">".$d."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>View By</td>
        <td>
        <select name="search[view_by]" class="form-control" id="view_by">
                    <?php 
                      foreach($view_by_lists as $r){
                        echo "<option value='".$r->VIEW_BY."' ".($search['view_by']==$r->VIEW_BY ? "selected" : "").">".$r->VIEW_BY_DESCRIPTION."</option>";
                      }
                    ?>
                  </select>
        </td>
        <td colspan="2"></td>
              </tr>
              
            </tbody>
            <tfoot>
              <tr>
                <td>Search</td>
                <td><input type="submit" name="search[button]" class="btn btn-search" value="Search"></td>
                <td>To Excel</td>
                <td><input type="submit" name="search[button]" class="btn btn-search" value="Download"></td>
                <td colspan="4"></td>
              </tr>
            </tfoot>
          </table>
        </div>
        <a href="javascript:void(0)" class="showhide"></a>
      </div>
    </div>
  </div> 
</form>
<div class="row">
  <div class="col-md-12">
    <h4 class="title-menu">Group Tunggakan</h4>
    <div class="billing-detail">
      <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered" id="table_c3mr">
        <thead>
          
        </thead>
        <tbody>
          
        </tbody>
        <tfoot>
        
        </tfoot>
      </table>
    </div>
  </div>
</div>
<script type="text/javascript">
  height="378px";
  $(document).ready(function(){
    // $("#table_c3mr").dataTable({
      // "bSort" : true,
      // "bFilter" : false,
      // "bPaginate": false,
      // "info": false,
    // });
    $("#division").multiselect({
      includeSelectAllOption: true,
      numberDisplayed:2
    }).change(function(){
      var divisions=$(this).val();
      $.ajax({
        url:"<?= base_url("ajax/getWitel") ?>",
        dataType:"JSON",
        data:{divisions:divisions},
        success:function(response){
          $("#witel").multiselect("destroy");
          $("#witel").html(response.content);
          $("#witel").multiselect({
            includeSelectAllOption: true,
            maxHeight: 212,
            enableFiltering: true
          }).change();
        }
      });
    });
    $("#summary_product").multiselect({
      includeSelectAllOption: true,
      numberDisplayed:2
    }).change(function(){
      var summary_products=$(this).val();
      $.ajax({
        url:"<?= base_url("ajax/getProducts") ?>",
        dataType:"JSON",
        data:{summary_products:summary_products},
        success:function(response){
          $("#product").multiselect("destroy");
          $("#product").html(response.content);
          $("#product").multiselect({
            includeSelectAllOption: true,
            maxHeight: 212
          });
        }
      });
    });
    $("#ubis").multiselect({
      dropRight: true,
      includeSelectAllOption: true,
      numberDisplayed:2
    }).change(function(){
      var ubis=$(this).val();
      $.ajax({
        url:"<?= base_url("ajax/getUbisSegments") ?>",
        dataType:"JSON",
        data:{ubis:ubis},
        success:function(response){
          $("#ubis_segment").multiselect("destroy");
          $("#ubis_segment").html(response.content);
          $("#ubis_segment").multiselect({
            dropRight: true,
            includeSelectAllOption: true,
            maxHeight: 212,
            enableFiltering: true
          });
        }
      });
    });
    $("#umur_termuda,#jumlah_kuitansi,#witel,#datel,#product,#eksepsi,#tunggakan,#billing_type,#usage").multiselect({numberDisplayed:2,maxHeight: 212,includeSelectAllOption: true,enableFiltering: true});
    $("#ubis_segment").multiselect({dropRight: true,numberDisplayed:2,maxHeight: 212,includeSelectAllOption: true,enableFiltering: true});
    $("#top_customer").multiselect({
      onChange: function(element, checked) {
        if(checked==true){
          value=$(element).val();
          if(value=="SILVER"){
            $("#top_customer option:selected[value='GOLD']").removeAttr("selected");
            $("#top_customer option:selected[value='PLATIN']").removeAttr("selected");
            $("#top_customer option:selected[value='TITAN']").removeAttr("selected");
          }else{
            $("#top_customer option:selected[value='SILVER']").removeAttr("selected");
          }
          $("#top_customer").multiselect('refresh');
        }
      }
    });
  });
  $("#witel").live("change",function(){
    var divisions=$("#division").val();
    var witels=$(this).val();
    $.ajax({
      url:"<?= base_url("ajax/getDatel") ?>",
      dataType:"JSON",
      data:{witels:witels,divisions:divisions},
      success:function(response){
        $("#datel").multiselect("destroy");
        $("#datel").html(response.content);
        $("#datel").multiselect({
          includeSelectAllOption: true,
          maxHeight: 212,
          enableFiltering: true,
          numberDisplayed:2
        }).change();
      }
    });
  });
</script>