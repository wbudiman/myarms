<form method="post" action="<?= base_url($path."index") ?>">
  <div class="row">
    <div class="col-md-12">
      <div id="table-search">
        <div class="wrap">
          <table  class="table table-search table-striped">
            <thead>
              <tr>
                <th colspan="6" v-align="middle">Parameter Pencarian Data</th>
              </tr>
            </thead>
            <tbody>
              <tr>                   
                <td>Divisi</td>
                <td>
                  <select name="search[division][]" id="division" multiple class="form-control">
                    <?php 
                      foreach($division_lists as $d){
                        echo "<option value='".$d->DIVISION_CODE."' ".($search['division']!="" && in_array($d->DIVISION_CODE,$search['division']) ? "selected" : "").">".$d->DIVISION_CODE."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Top Customer</td>
                <td>
                  <select name="search[top_customer][]" multiple class="form-control" id="top_customer">
                    <?php 
                      echo '<optgroup label="Top 20">';
                      foreach($top_customer_lists as $r){
                        if($r->PORTOFOLIO_GROUP=="Top 20")
                        {
                          echo "<option value='".$r->PORTOFOLIO_ID."' ".($search['top_customer']!="" && in_array($r->PORTOFOLIO_ID,$search['top_customer']) ? "selected" : "").">".$r->PORTOFOLIO_NAME."</option>";
                        }
                      }
                      echo '</optgroup>';
                      echo '<optgroup label="Non Top 20">';
                      foreach($top_customer_lists as $r){
                        if($r->PORTOFOLIO_GROUP=="Non Top 20")
                        {
                          echo "<option value='".$r->PORTOFOLIO_ID."' ".($search['top_customer']!="" && in_array($r->PORTOFOLIO_ID,$search['top_customer']) ? "selected" : "").">".$r->PORTOFOLIO_NAME."</option>";
                        }
                      }
                      echo '</optgroup>';
                    ?>
                  </select>
                </td>
                <td>Ubis</td>
                <td>
                  <select name="search[ubis][]" multiple class="form-control" id="ubis">
                    <?php 
                      foreach($ubis_lists as $d){
                        echo "<option value='".$d->UBIS_ID."' ".($search['ubis']!="" && in_array($d->UBIS_ID,$search['ubis']) ? "selected" : "").">".$d->UBIS."</option>";
                      }
                    ?>
                  </select>
                </td>
              </tr>
              <tr>                   
                <td>Witel</td>
                <td>
                  <select name="search[witel][]" id="witel" multiple class="form-control">
                    <?php 
                      foreach($witel_lists as $w){
                        echo "<option value='".$w->WITEL_CODE."' ".($search['witel']!="" && in_array($w->WITEL_CODE,$search['witel']) ? "selected" : "").">".$w->WITEL_DESCRIPTION."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Summary Product</td>
                <td>
                  <select name="search[summary_product][]" multiple class="form-control" id="summary_product">
                    <?php 
                      foreach($summary_product_lists as $d){
                        echo "<option value='".$d->SUMMARY_PRODUCT."' ".($search['summary_product']!="" && in_array($d->SUMMARY_PRODUCT,$search['summary_product']) ? "selected" : "").">".$d->SUMMARY_PRODUCT."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Customer Category</td>
                <td>
                  <select name="search[ubis_segment][]" id="ubis_segment" multiple class="form-control">
                    <?php 
                      foreach($ubis_segment_lists as $r){
                        echo "<option value='".$r->UBIS_SEGMENT_ID."' ".($search['ubis_segment']!="" && in_array($r->UBIS_SEGMENT_ID,$search['customer_category']) ? "selected" : "").">".$r->UBIS_SEGMENT."</option>";
                      }
                    ?>
                  </select>
                </td>
              </tr>
              <tr>                   
                <td>Datel</td>
                <td>
                  <select name="search[datel][]" id="datel" multiple class="form-control">
                    <?php 
                      foreach($datel_lists as $w){
                        echo "<option value='".$w->DATEL_CODE."' ".($search['datel']!="" && in_array($w->DATEL_CODE,$search['datel']) ? "selected" : "").">".$w->DATEL_DESCRIPTION."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Produk</td>
                <td>
                  <select name="search[product][]" multiple class="form-control" id="product">
                    <?php 
                      foreach($product_lists as $r){
                        echo "<option value='".$r->PRODUCT_NAME."' ".($search['product']!="" && in_array($r->PRODUCT_NAME,$search['product']) ? "selected" : "").">".$r->PRODUCT_NAME."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td></td>
                <td></td>
              </tr>
              <tr>                   
                <td>Periode</td>
                <td>
                  <div class="col-md-9" style="padding-left:0px;">
                    <div class="col-md-7" style="padding-left:0px;">
                      <select name="search[month]" class="form-control">
                        <?php 
                          foreach($month_lists as $key=>$d){
                            echo "<option value='".$key."' ".($search['month']==$key ? "selected" : "").">".$d."</option>";
                          }
                        ?>
                      </select>
                    </div>
                    <div class="col-md-5" style="padding-left:0px;">
                      <select name="search[year]" class="form-control">
                        <?php 
                          for($x=date("Y");$x>(date("Y")-10);$x--){
                            echo "<option value='".$x."' ".($search['year']==$x ? "selected" : "").">".$x."</option>";
                          }
                        ?>
                      </select>
                    </div>
                  </div>
                </td>
                <td>Indihome</td>
                <td>
                  <select name="search[indihome]" class="form-control">
                    <option value="">All</option>
                    <?php 
                      foreach($indihome_lists as $r){
                        echo "<option value='".$r->INDIHOME_ID."' ".($search['indihome']==$r->INDIHOME_ID ? "selected" : "").">".$r->INDIHOME_DESCRIPTION."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td></td>
                <td></td>
              </tr>
              <tr class="background-filter">                   
                <td>Baris</td>
                <td>
                  <select name="search[row]" class="form-control" id="row">
                    <?php 
                      foreach($row_lists as $key=>$d){
                        echo "<option value='".$key."' ".($search['view_by']=="L11" && $key=="billing_component" ? "disabled" : "")." ".($search['row']==$key ? "selected" : "").">".$d."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Kolom</td>
                <td>
                  <select name="search[column]" class="form-control" id="column">
                    <?php 
                      foreach($row_lists as $key=>$d){
                        echo "<option value='".$key."' ".($search['view_by']=="L11" && $key=="billing_component" ? "disabled" : "")." ".($search['column']==$key ? "selected" : "").">".$d."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td colspan="2"></td>
              </tr>
            </tbody>
            <tfoot>
              <tr>
                <td>Search</td>
                <td><input type="submit" name="search[button]" class="btn btn-search" value="Search"></td>
                <td>To Excel</td>
                <td><input type="submit" name="search[button]" class="btn btn-search" value="Download"></td>
                <td colspan="2"></td>
              </tr>
            </tfoot>
          </table>
        </div>
        <a href="javascript:void(0)" class="showhide"></a>
      </div>
    </div>
  </div> 
</form>
<div class="row">
  <div class="col-md-12">
    <h4 class="title-menu">Multibill</h4>
    <div class="billing-detail">
      <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered" id="table_revenue">
        <thead>
          <?php 
            echo "<tr>";
            echo "<th class='text-center'>".(is_array($row_lists[$search['row']]) ? "Komponen Billing" : $row_lists[$search['row']])."</th>";
            echo "<th class='text-center'>Total</th>";
            echo "</tr>";
          ?>
        </thead>
        <tbody>
          
        </tbody>
        <tfoot>
        
        </tfoot>
      </table>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    $("#table_revenue").dataTable({
      "bSort" : true,
      "bFilter" : false,
      "bPaginate": false,
      "info": false,
    });
    $("#division").multiselect({
      includeSelectAllOption: true,
      numberDisplayed:2
    }).change(function(){
      var divisions=$(this).val();
      $.ajax({
        url:"<?= base_url("ajax/getWitel") ?>",
        dataType:"JSON",
        data:{divisions:divisions},
        success:function(response){
          $("#witel").multiselect("destroy");
          $("#witel").html(response.content);
          $("#witel").multiselect({
            includeSelectAllOption: true,
            maxHeight: 212,
            enableFiltering: true
          }).change();
        }
      });
    });
    $("#summary_product").multiselect({
      includeSelectAllOption: true,
      numberDisplayed:2
    }).change(function(){
      var summary_products=$(this).val();
      $.ajax({
        url:"<?= base_url("ajax/getProducts") ?>",
        dataType:"JSON",
        data:{summary_products:summary_products},
        success:function(response){
          $("#product").multiselect("destroy");
          $("#product").html(response.content);
          $("#product").multiselect({
            includeSelectAllOption: true,
            maxHeight: 212
          });
        }
      });
    });
    $("#ubis").multiselect({
      dropRight: true,
      includeSelectAllOption: true,
      numberDisplayed:2
    }).change(function(){
      var ubis=$(this).val();
      $.ajax({
        url:"<?= base_url("ajax/getUbisSegments") ?>",
        dataType:"JSON",
        data:{ubis:ubis},
        success:function(response){
          $("#ubis_segment").multiselect("destroy");
          $("#ubis_segment").html(response.content);
          $("#ubis_segment").multiselect({
            dropRight: true,
            includeSelectAllOption: true,
            maxHeight: 212,
            enableFiltering: true
          });
        }
      });
    });
    $("#witel,#datel,#regional,#product").multiselect({numberDisplayed:2,maxHeight: 212,includeSelectAllOption: true,enableFiltering: true});
    $("#ubis_segment").multiselect({dropRight: true,numberDisplayed:2,maxHeight: 212,includeSelectAllOption: true,enableFiltering: true});
    $("#top_customer").multiselect({
      onChange: function(element, checked) {
        if(checked==true){
          value=$(element).val();
          if(value=="SILVER"){
            $("#top_customer option:selected[value='GOLD']").removeAttr("selected");
            $("#top_customer option:selected[value='PLATIN']").removeAttr("selected");
            $("#top_customer option:selected[value='TITAN']").removeAttr("selected");
          }else{
            $("#top_customer option:selected[value='SILVER']").removeAttr("selected");
          }
          $("#top_customer").multiselect('refresh');
        }
      }
    });
    $("#view_by").change(function(){
      var view_by=$(this).val();
      switch(view_by){
        case "AMOUNT":
          $("#row option[value='billing_component']").removeAttr("disabled");
          $("#column option[value='billing_component']").removeAttr("disabled");
          if($("#row").val()=="billing_component" || $("#column").val()=="billing_component"){
            $("#billing_component").attr("disabled","disabled");
          }else{
            $("#billing_component").removeAttr("disabled");
          }
          break;
        case "L11":
          if($("#row").val()=="billing_component")$("#row").val(0);
          if($("#column").val()=="billing_component")$("#column").val(0);
          $("#billing_component").attr("disabled","disabled");
          $("#row option[value='billing_component']").attr("disabled","disabled");
          $("#column option[value='billing_component']").attr("disabled","disabled");
          break;
        case "ARPU":
          $("#row option[value='billing_component']").removeAttr("disabled");
          $("#column option[value='billing_component']").removeAttr("disabled");
          if($("#row").val()=="billing_component" || $("#column").val()=="billing_component"){
            $("#billing_component").attr("disabled","disabled");
          }else{
            $("#billing_component").removeAttr("disabled");
          }
          break;
      }
    });
    $("#row,#column").change(function(){
      if($("#row").val()=="billing_component" || $("#column").val()=="billing_component"){
        $("#billing_component").attr("disabled","disabled");
      }else{
        $("#billing_component").removeAttr("disabled");
      }
    });
  });
  $("#witel").live("change",function(){
    var divisions=$("#division").val();
    var witels=$(this).val();
    $.ajax({
      url:"<?= base_url("ajax/getDatel") ?>",
      dataType:"JSON",
      data:{witels:witels,divisions:divisions},
      success:function(response){
        $("#datel").multiselect("destroy");
        $("#datel").html(response.content);
        $("#datel").multiselect({
          includeSelectAllOption: true,
          maxHeight: 212,
          enableFiltering: true,
          numberDisplayed:2
        }).change();
      }
    });
  });
</script>