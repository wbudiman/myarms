<form method="post" action="<?= base_url($path."index") ?>">
  <div class="row">
    <div class="col-md-12">
      <div id="table-search">
        <div class="wrap">
          <table class="table table-search table-striped">
            <thead>
              <tr>
                <th colspan="8" v-align="middle">Parameter Pencarian Data</th>
              </tr>
            </thead>
            <tbody>
              <tr>                   
                <td>Ubis</td>
                <td>
                  <select name="search[ubis][]" multiple class="form-control" id="ubis">
                    <?php 
                      foreach($ubis_lists as $d){
                        echo "<option value='".$d->UBIS_ID."' ".($search['ubis']!="" && in_array($d->UBIS_ID,$search['ubis']) ? "selected" : "").">".$d->UBIS."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Bisnis Area</td>
                <td>
                  <select name="search[bisnis_area][]" id="bisnis_area" multiple class="form-control">
                            <?php 
                              foreach($bisnis_area_lists as $r){
                                echo "<option value='".$r->BUSINESS_AREA_ID."' ".($search['bisnis_area']!="" && in_array($r->BUSINESS_AREA_ID,$search['bisnis_area']) ? "selected" : "").">".$r->BUSINESS_AREA_DESCRIPTION."</option>";
                              }
                            ?>
                  </select>
                </td>
                <td>Periode Awal</td>
                <td>
                  <div class="col-md-9" style="padding-left:0px;">
                    <div class="col-md-7" style="padding-left:0px;">
                      <select name="search[month_1]" class="form-control" id="month_1">
                        <?php 
                          foreach($month_lists as $key=>$d){
                            echo "<option value='".$key."' ".($search['month_1']==$key ? "selected" : "").">".$d."</option>";
                          }
                        ?>
                      </select>
                    </div>
                    <div class="col-md-5" style="padding-left:0px;">
                      <select name="search[year_1]" class="form-control" id="year_1">
                        <?php 
                          for($x=date("Y");$x>(date("Y")-6);$x--){
                            echo "<option value='".$x."' ".($search['year_1']==$x ? "selected" : "").">".$x."</option>";
                          }
                        ?>
                      </select>
                    </div>
                  </div>
                </td>
              </tr>
              <tr>
                <td>Segmen</td>
                <td>
                  <select name="search[ubis_segment][]" id="ubis_segment" multiple class="form-control">
                            <?php 
                              foreach($ubis_segment_lists as $r){
                                echo "<option value='".$r->UBIS_SEGMENT_ID."' ".($search['ubis_segment']!="" && in_array($r->UBIS_SEGMENT_ID,$search['ubis_segment']) ? "selected" : "").">".$r->UBIS_SEGMENT."</option>";
                              }
                            ?>
                  </select>  
                </td>
                <td></td>
                <td></td>
                
                <td>Periode Akhir</td>
                <td>
                  <div class="col-md-9" style="padding-left:0px;">
                    <div class="col-md-7" style="padding-left:0px;">
                      <select name="search[month_2]" class="form-control" id="month_2">
                        <?php 
                          foreach($month_lists as $key=>$d){
                            echo "<option value='".$key."' ".($search['month_2']==$key ? "selected" : "").">".$d."</option>";
                          }
                        ?>
                      </select>
                    </div>
                    <div class="col-md-5" style="padding-left:0px;">
                      <select name="search[year_2]" class="form-control" id="year_2">
                        <?php 
                          for($x=date("Y");$x>(date("Y")-6);$x--){
                            echo "<option value='".$x."' ".($search['year_2']==$x ? "selected" : "").">".$x."</option>";
                          }
                        ?>
                      </select>
                    </div>
                  </div>
                </td>
              </tr>
              <tr class="background-filter">                   
                <td>Show Data</td>
                <td>
                  <select name="search[show_data]" class="form-control" id="show_data">
                    <?php 
                      foreach($row_lists as $key=>$d){
                        echo "<option value='".$key."' ".($search['show_data']==$key ? "selected" : "").">".$d."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td></td>
                <td></td>
                <td colspan="2"></td>
              </tr>
            </tbody>
            <tfoot>
              <tr>                   
                <td>Search</td>
                <td>
                  <input type="submit" name="search[button]" class="btn btn-search" value="Search">
                </td>
                <td>To Excel</td>
                <td>
                  <input type="submit" name="search[button]" class="btn btn-search" value="Download">
                </td>
                <td colspan="2"></td>
              </tr>
            </tfoot>
          </table>
        </div>
        <a href="javascript:void(0)" class="showhide"></a>
      </div>
    </div>
  </div> 
</form>
<div class="row">
  <div class="col-md-12">
    <h4 class="title-menu">Average Collection Period</h4>
    <div class="billing-detail">
      <div class="clearfix"></div>
      <table class="table table-bordered" id="table_acp">
        <thead>
          <?php
            echo "<tr>";
            echo "<th width='370'class='text-center'>".$row_lists[$search['show_data']]."</th>";
            echo "<th width='170'>a. Saldo Awal ".$search['year_1'].$search['month_1']."</th>";
            echo "<th width='180'>b. Saldo Akhir ".$search['year_2'].$search['month_2']."</th>";
            echo "<th width='230'>c. Billing ".$search['year_1'].$search['month_1']." s/d ".$search['year_2'].$search['month_2']."</th>";
            echo "<th>d. Hari</th>";
            echo "<th>ACP d*((a+b)/2)/c</th>";
            echo "</tr>";
          ?>
        </thead>
        <tbody>
          <?php
            foreach($lists as $l){
              echo "<tr>";
              echo "<td><a href='javascript:void(0);' data-remote-modal='".base_url($path."detail/00?".$param_get)."&id_pack=".$l->ID_PACK."&judul=".$l->DESCRIPTION."' target='_blank'>".$l->DESCRIPTION."</a></td>";
              echo "<td>".date("d/m/Y",strtotime($l->START_DATE))."</td>";
              $START_END=($l->START_END!="" ? date("d/m/Y",strtotime($l->START_END)) : "");
              echo "<td>".$START_END."</td>";
              echo "<td>".$l->DETAILLED_SUM."</td>";
              echo "</tr>";
            }
          ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<script type="text/javascript">
  height=335;
  $(document).ready(function(){
    $("#table_acp").dataTable({
      "bSort" : true,
      "bFilter" : false,
      "bPaginate": false,
      "info": false,
    });
    $("#ubis").multiselect({
      dropRight: true,
      includeSelectAllOption: true,
      numberDisplayed:2
    }).change(function(){
      var ubis=$(this).val();
      $.ajax({
        url:"<?= base_url("ajax/getUbisSegments") ?>",
        dataType:"JSON",
        data:{ubis:ubis},
        success:function(response){
          $("#ubis_segment").multiselect("destroy");
          $("#ubis_segment").html(response.content);
          $("#ubis_segment").multiselect({
            dropRight: true,
            includeSelectAllOption: true,
            maxHeight: 212,
            enableFiltering: true
          });
        }
      });
    });
    $("#ubis_segment").multiselect({
      dropRight: true,
      includeSelectAllOption: true,
      numberDisplayed:2
    }).change(function(){
      var ubis_segment=$(this).val();
      $.ajax({
        url:"<?= base_url("ajax/getBisnisArea") ?>",
        dataType:"JSON",
        data:{ubis_segment:ubis_segment},
        success:function(response){
          $("#bisnis_area").multiselect("destroy");
          $("#bisnis_area").html(response.content);
          $("#bisnis_area").multiselect({
            dropRight: true,
            includeSelectAllOption: true,
            maxHeight: 212,
            enableFiltering: true
          });
        }
      });
    });
    $("#product,#bisnis_area").multiselect({numberDisplayed:2,maxHeight: 212,includeSelectAllOption: true,enableFiltering: true});
    $("form").submit(function(){
      var periode1=$("#year_1").val()+$("#month_1").val();
      var periode2=$("#year_2").val()+$("#month_2").val();
      /*if(periode2=='' && periode1==''){
        main.notification("bottomRight","warning","Peringatan","Silahkan pilih start promo dan end promo dengan benar!");
        return false;
      }*/
    });
  });
</script>