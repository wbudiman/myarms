<h5><?= $header_title ?> ( <?= $header_parameter ?> )</h5>
<table border="1">
  <thead>
          <?php
            echo "<tr>";
            echo "<th width='80'>Witel</th>";
            echo "<th width='80'>Datel</th>";
            echo "<th width='80'>Kode Lokasi</th>";
            echo "<th>Kode Loket</th>";
            echo "<th>Loket</th>";
            echo "<th>Type Loket</th>";
            echo "<th>No BO</th>";
            echo "<th>No Akun BO</th>";
            echo "<th>Akun BO</th>";
            echo "<th>Bisnis Area</th>";
            echo "<th>Area</th>";
            echo "</tr>";
          ?>
        </thead>
        <tbody>
          <?php
            foreach($lists as $l){
              echo "<tr>";
              echo "<td width='80'>".$l->WITEL_DESCRIPTION."</td>";
              echo "<td width='80'>".$l->DATEL_DESCRIPTION."</td>";
              echo "<td width='80'>".$l->KD_LOCATION."</td>";
              echo "<td>".$l->CODE_LOCKET."</td>";
			  echo "<td>".$l->LOCKET_NAME."</td>";
			  echo "<td>".$l->LOCKET_TYPE."</td>";
			  echo "<td>".$l->NO_BO."</td>";
			  echo "<td>".$l->NO_ACCOUNT_BO."</td>";
			  echo "<td>".$l->DESCRIPTION_ACCOUNT_BO."</td>";
			  echo "<td>".$l->BUSINESS_AREA_ID."</td>";
			  echo "<td>".$l->DESCRIPTION_AREA."</td>";
              echo "</tr>";
            }
          ?>
        </tbody>
</table>