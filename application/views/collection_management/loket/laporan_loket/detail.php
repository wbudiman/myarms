<div class="row">
  <div class="col-md-12">
    <h4 class="title-menu">Laporan Loket Summary Detail (<?php echo sizeof($lists)." items" ?>)</h4>
    <div class="billing-detail">
      <div class="clearfix"></div>
      <?php if(sizeof($lists)>0): ?>
      <br>
      <form method="post" action="<?= base_url($path."detail/".$search['param']."?".$param_get) ?>">
        <div class="pull-right">
          <input type="submit" name="search[button]" class="btn btn-search" value="Download">
        </div>
      </form>
      <?php endif; ?>
      <br>
      <table class="table table-bordered" id="table_l251_detail">
        <thead>
          <?php
            echo "<tr>";
            echo "<th width='80'>Witel</th>";
            echo "<th width='80'>Datel</th>";
            echo "<th>Kode Loket</th>";
            echo "<th width='80'>Nama Loket</th>";
            echo "<th>Tahapan</th>";
            echo "<th>Petugas</th>";
            echo "<th>Transaksi</th>";
            echo "<th>Buka Tahapan</th>";
            echo "<th>Tutup Tahapan</th>";
            echo "<th>Saldo</th>";
            echo "<th>Jumlah</th>";
            echo "<th>Validasi</th>";
            echo "<th>Tgl Validasi</th>";
            echo "</tr>";
          ?>
        </thead>
        <tbody>
          <?php
            foreach($lists as $l){
              echo "<tr>";
              echo "<td width='80'>".$l->WITEL_DESCRIPTION."</td>";
              echo "<td width='80'>".$l->DATEL_DESCRIPTION."</td>";
              echo "<td>".$l->CODE_LOCKET."</td>";
        echo "<td width='80'>".$l->LOCKET_NAME."</td>";
              echo "<td>".$l->STEP."</td>";
        echo "<td>".$l->PTGS_CASH_DESK."</td>";
        echo "<td>".$l->TRANSACTION_TYPE."</td>";
        echo "<td>".$l->STEP_OPEN_DATE."</td>";
        echo "<td>".$l->STEP_CLOSE_DATE."</td>";
        echo "<td>".$l->AMOUNT."</td>";
        echo "<td>".$l->SST."</td>";
        echo "<td>".$l->STATUS_VALIDATION."</td>";
        echo "<td>".$l->VALIDATION_DATE."</td>";
              echo "</tr>";
            }
          ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    $("#table_l251_detail").dataTable({
      "bSort" : true,
      "bFilter" : false,
      "bPaginate": false,
      "info": false,
    });
  });
</script>