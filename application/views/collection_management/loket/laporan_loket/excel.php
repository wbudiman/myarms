<h5><?= $header_title ?> ( <?= $header_parameter ?> )</h5>
<table border="1">
<thead>
          <?php 
            echo "<tr>";
            echo "<th class='text-center'>".$row_lists[$search['row']]."</th>";
            $columns=array();
            foreach($lists as $l){
              if(!in_array($l->{$field[$search['column']]},$columns)){
                array_push($columns,$l->{$field[$search['column']]});
                echo "<th class='text-center'>".($l->{$field[$search['column']]}!="" && $l->{$field[$search['column']]}!=NULL ? $l->{$field[$search['column']]} : $no_mapping)."</th>";
              }
            }
            echo "<th class='text-center'>Total</th>";
            echo "</tr>";
          ?>
        </thead>
        <tbody>
			<?php   
            $rows=array();
            $total_column=array();
            $total_column['total']=0;
            foreach($lists as $l){
              if(!in_array($l->{$field[$search['row']]},$rows)){
                array_push($rows,$l->{$field[$search['row']]});
                echo "<tr>";
                echo "<td>".($l->{$field[$search['row']]}!="" && $l->{$field[$search['row']]}!=NULL ? $l->{$field[$search['row']]} : $no_mapping)."</td>";
                $total=0;
                $counter=0;
                foreach($columns as $c){
                  $check=0;
                  foreach($lists as $l2){
                    if($l->{$field[$search['row']]}==$l2->{$field[$search['row']]})
                    {
                      if($c==$l2->{$field[$search['column']]}){
                        if($search['view_by']=="SST"){
                          $value=$l2->SST;
                        }elseif($search['view_by']=="AMOUNT"){
                          $value=$l2->AMOUNT;
                        }
                        $total+=$value;
                        if(!isset($total_column[$c]))$total_column[$c]=0;
                        $total_column[$c]+=$value;
                        $counter++;
                        echo "<td class='text-right'>".number_format($value,0,".",",")."</td>";
                        $check=1;
                      }
                    }
                  }
                  if($check==0){
                    echo "<td class='text-right'>0</td>";
                    if(!isset($total_column[$c]))$total_column[$c]=0;
                    $total_column[$c]+=0;
                  }
                }
                if(!isset($total_column['total']))$total_column['total']=0;
                $total_column['total']+=$total;
                echo "<td class='text-right'>".number_format($total,0,".",",")."</td>";
                echo "</tr>";
              }
            }
          ?>
        </tbody>
		<tfoot>
        <?php
          echo "<tr>";
          echo "<td style='padding:8px;'>Total (".sizeof($rows)." item)</td>";
          foreach($columns as $c){
            foreach($total_column as $key=>$t){
              if($c==$key){
                echo "<td class='text-right' style='padding:8px;'>".number_format($t,0,".",",")."</td>";
              }
            }
          }
          echo "<td class='text-right' style='padding:8px;'>".number_format($total_column['total'],0,".",",")."</td>";
          echo "</tr>";
        ?>
        </tfoot>
</table>