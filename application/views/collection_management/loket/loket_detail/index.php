<form method="post" action="<?= base_url($path."index") ?>">
  <div class="row">
    <div class="col-md-12">
      <div id="table-search">
        <div class="wrap">
          <table class="table table-search table-striped">
            <thead>
              <tr>
                <th colspan="8" v-align="middle">Parameter Pencarian Data</th>
              </tr>
            </thead>
            <tbody>
              <tr>                   
                <td>Divisi</td>
                <td>
                  <select name="search[division][]" id="division" multiple class="form-control">
                    <?php 
                      foreach($division_lists as $d){
                        echo "<option value='".$d->DIVISION_CODE."' ".($search['division']!="" && in_array($d->DIVISION_CODE,$search['division']) ? "selected" : "").">".$d->DIVISION_CODE."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Periode</td>
                <td>
                  <div class="col-md-9" style="padding-left:0px;">
                    <div class="col-md-7" style="padding-left:0px;">
                      <select name="search[month_1]" class="form-control" id="month_1">
                        <?php 
                          foreach($month_lists as $key=>$d){
                            echo "<option value='".$key."' ".($search['month_1']==$key ? "selected" : "").">".$d."</option>";
                          }
                        ?>
                      </select>
                    </div>
                    <div class="col-md-5" style="padding-left:0px;">
                      <select name="search[year_1]" class="form-control" id="year_1">
                        <?php 
                          for($x=date("Y");$x>(date("Y")-5);$x--){
                            echo "<option value='".$x."' ".($search['year_1']==$x ? "selected" : "").">".$x."</option>";
                          }
                        ?>
                      </select>
                    </div>
                  </div>
              </tr>
			  <tr>                   
                <td>Witel</td>
                <td>
                  <select name="search[witel][]" id="witel" multiple class="form-control">
                    <?php 
                      foreach($witel_lists as $w){
                        echo "<option value='".$w->WITEL_CODE."' ".($search['witel']!="" && in_array($w->WITEL_CODE,$search['witel']) ? "selected" : "").">".$w->WITEL_DESCRIPTION."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Status Validasi</td>
                <td>
                  <select name="search[validasi]" id="validasi" class="form-control">
					<option value="" <?php if(isset($search['validasi'])&&($search['validasi']=='')) echo "selected";?>>ALL</option>
					<option value="OK" <?php if(isset($search['validasi'])&&($search['validasi']=='OK')) echo "selected=true";?>>OK</option>
					<option value="NOT OK" <?php if(isset($search['validasi'])&&($search['validasi']=='NOT OK')) echo "selected=true";?>>NOT OK</option>
                  </select>
                </td>
              </tr>
			  <tr>                   
                <td>Datel</td>
                <td>
                  <select name="search[datel][]" id="datel" multiple class="form-control">
                    <?php 
                      foreach($datel_lists as $w){
                        echo "<option value='".$w->DATEL_CODE."' ".($search['datel']!="" && in_array($w->DATEL_CODE,$search['datel']) ? "selected" : "").">".$w->DATEL_DESCRIPTION."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Petugas</td>
                <td>
                  <input type="text" name="search[petugas]" value="<?php if(isset($search['petugas'])) echo $search['petugas'];?>">
              </tr>
              <tr>
                <td>Loket</td>
                <td>
					<select name="search[locket][]" id="locket" multiple class="form-control">
                    <?php 
                      foreach($locket_lists as $w){
                        echo "<option value='".$w->CODE_LOCKET."' ".($search['locket']!="" && in_array($w->CODE_LOCKET,$search['locket']) ? "selected" : "").">".$w->LOCKET_NAME."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td></td>
                <td></td>
              </tr>
            </tbody>
            <tfoot>
              <tr>                   
                <td>Search</td>
                <td>
                  <input type="submit" name="search[button]" class="btn btn-search" value="Search">
                </td>
                <td>To Excel</td>
                <td>
                  <input type="submit" name="search[button]" class="btn btn-search" value="Download">
                </td>
              </tr>
            </tfoot>
          </table>
        </div>
        <a href="javascript:void(0)" class="showhide"></a>
      </div>
    </div>
  </div> 
</form>
<div class="row">
  <div class="col-md-12">
    <h4 class="title-menu">Laporan Loket Detail</h4>
    <div class="billing-detail">
      <div class="clearfix"></div>
      <table class="table table-bordered" id="table_gimmick_catalog">
        <thead>
          <?php
            echo "<tr>";
            echo "<th width='80'>Witel</th>";
            echo "<th width='80'>Datel</th>";
            echo "<th>Kode Loket</th>";
			echo "<th width='80'>Nama Loket</th>";
            echo "<th>Tahapan</th>";
            echo "<th>Petugas</th>";
            echo "<th>Transaksi</th>";
            echo "<th>Buka Tahapan</th>";
            echo "<th>Tutup Tahapan</th>";
            echo "<th>Saldo</th>";
            echo "<th>Jumlah</th>";
            echo "<th>Validasi</th>";
            echo "<th>Tgl Validasi</th>";
            echo "</tr>";
          ?>
        </thead>
        <tbody>
          <?php
            foreach($lists as $l){
              echo "<tr>";
              echo "<td width='80'>".$l->WITEL_DESCRIPTION."</td>";
              echo "<td width='80'>".$l->DATEL_DESCRIPTION."</td>";
              echo "<td>".$l->CODE_LOCKET."</td>";
			  echo "<td width='80'>".$l->LOCKET_NAME."</td>";
              echo "<td>".$l->STEP."</td>";
			  echo "<td>".$l->PTGS_CASH_DESK."</td>";
			  echo "<td>".$l->TRANSACTION_TYPE."</td>";
			  echo "<td>".$l->STEP_OPEN_DATE."</td>";
			  echo "<td>".$l->STEP_CLOSE_DATE."</td>";
			  echo "<td>".$l->AMOUNT."</td>";
			  echo "<td>".$l->SST."</td>";
			  echo "<td>".$l->STATUS_VALIDATION."</td>";
			  echo "<td>".$l->VALIDATION_DATE."</td>";
              echo "</tr>";
            }
          ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<script type="text/javascript">
  height=335;
  $(document).ready(function(){
    $("#table_gimmick_catalog").dataTable({
      "bSort" : true,
      "bFilter" : false,
      "bPaginate": false,
      "info": false,
    });
    $("#division").multiselect({
      includeSelectAllOption: true,
      numberDisplayed:2
    }).change(function(){
      var divisions=$(this).val();
      $.ajax({
        url:"<?= base_url("ajax/getWitel") ?>",
        dataType:"JSON",
        data:{divisions:divisions},
        success:function(response){
          $("#witel").multiselect("destroy");
          $("#witel").html(response.content);
          $("#witel").multiselect({
            includeSelectAllOption: true,
            maxHeight: 212,
            enableFiltering: true
          }).change();
        }
      });
    });
	$("#witel").live("change",function(){
    var divisions=$("#division").val();
    var witels=$(this).val();
    $.ajax({
      url:"<?= base_url("ajax/getDatel") ?>",
      dataType:"JSON",
      data:{witels:witels,divisions:divisions},
      success:function(response){
        $("#datel").multiselect("destroy");
        $("#datel").html(response.content);
        $("#datel").multiselect({
          includeSelectAllOption: true,
          maxHeight: 212,
          enableFiltering: true,
          numberDisplayed:2
        }).change();
      }
    });
  });
    $("#witel,#datel,#locket").multiselect({numberDisplayed:2,maxHeight: 212,includeSelectAllOption: true,enableFiltering: true});
    $("#ubis_segment").multiselect({dropRight: true,numberDisplayed:2,maxHeight: 212,includeSelectAllOption: true,enableFiltering: true});
    $("form").submit(function(){
      var periode1=$("#year_1").val()+$("#month_1").val();
      var periode2=$("#year_2").val()+$("#month_2").val();
      if(periode2<periode1){
        main.notification("bottomRight","warning","Peringatan","Silahkan pilih start promo dan end promo dengan benar!");
        return false;
      }
    });
  });
</script>