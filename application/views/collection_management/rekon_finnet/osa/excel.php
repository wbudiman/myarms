<h5><?= $header_title ?></h5>
<table border="1">
  <thead>
          <?php 
            $total_column=array();
            $total_column['total']=0;
            echo "<tr>";
            echo "<th class='text-center'>".$row_lists[$search['row']]."</th>";
            echo "<th class='text-center'>Trx</th>";
            echo "<th class='text-center'>Bill</th>";
            echo "<th class='text-center'>Amount</th>";
            echo "</tr>";
          ?>
        </thead>
        <tbody>
         <?php   
            $rows=array();
            $total_column=array();
            $total_column['trx']=0;
            $total_column['bill']=0;
            $total_column['amount']=0;

            foreach($lists as $l){
              if(!in_array($l->{$field[$search['row']]},$rows)){
                array_push($rows,$l->{$field[$search['row']]});
                echo "<tr>";
                echo "<td>".($l->{$field[$search['row']]}!="" && $l->{$field[$search['row']]}!=NULL ? $l->{$field[$search['row']]} : $no_mapping)."</td>";
                echo "<td class='text-right'>".number_format($l->TRX,0,".",",")."</td>";
                echo "<td class='text-right'>".number_format($l->BILL,0,".",",")."</td>";
                echo "<td class='text-right'>".number_format($l->AMOUNT,0,".",",")."</td>";
                echo "</tr>";
                $total_column['trx']+=$l->TRX;
                $total_column['bill']+=$l->BILL;
                $total_column['amount']+=$l->AMOUNT;

              }
            }
          ?>
        </tbody>
        <tfoot>
         <?php 
          echo "<tr>";
          echo "<td style='padding:8px;'>Total (".sizeof($rows)." item)</td>";
          echo "<td class='text-right' style='padding:8px;'>".number_format($total_column['trx'],0,".",",")."</td>";
          echo "<td class='text-right' style='padding:8px;'>".number_format($total_column['bill'],0,".",",")."</td>";
          echo "<td class='text-right' style='padding:8px;'>".number_format($total_column['amount'],0,".",",")."</td>";
          echo "</tr>";
        ?>
        </tfoot>
</table>