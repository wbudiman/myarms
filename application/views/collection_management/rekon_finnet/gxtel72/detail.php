<div class="row">
  <div class="col-md-12">
    <h4 class="title-menu">Detail Pelanggan Dapat Gimmick (<?php echo sizeof($lists)." items" ?>)</h4>
    <div class="billing-detail">
      <div class="clearfix"></div>
      <?php if(sizeof($lists)>0): ?>
      <br>
      <form method="post" action="<?= base_url($path."detail/".$search['param']."?".$param_get) ?>">
        <div class="pull-right">
          <input type="submit" name="search[button]" class="btn btn-search" value="Download">
        </div>
      </form>
      <?php endif; ?>
      <br>
      <table class="table table-bordered" id="table_l251_detail">
        <thead>
          <?php 
            echo "<tr>";
            echo "<th class='text-center'>SND</th>";
            echo "<th class='text-center'>NCLI</th>";
            echo "<th class='text-center'>NDOS</th>";
            echo "<th class='text-center'>KTCK</th>";
            echo "<th class='text-center'>Paket</th>";
            echo "<th class='text-center'>Tiket</th>";
            echo "<th class='text-center'>IDBA</th>";
            echo "<th class='text-center'>Amount Call</th>";
            echo "<th class='text-center'>Start Gimmick</th>";
            echo "<th class='text-center'>End Gimmick</th>";
            echo "</tr>";
          ?>
        </thead>
        <tbody>
          <?php
            if(sizeof($lists)>0){
              $total=array("record"=>0,"duration"=>0);
              foreach($lists as $l){
                echo "<tr>";
                echo "<td>".$l->SND."</td>";
                echo "<td>".$l->NCLI."</td>";
                echo "<td>".$l->NDOS."</td>";
                echo "<td>".$l->KTCK."</td>";
                echo "<td>".$l->CITEM."</td>";
                echo "<td>".$l->NTICKET."</td>";
                echo "<td></td>";
                echo "<td>".$l->MNT_TCK."</td>";
                echo "<td>".$l->START_DISC_TARIF."</td>";
                echo "<td>".$l->END_DISC_TARIF."</td>";
                echo "</tr>";
              }
            } 
          ?>
        </tbody>
        <tfoot>
        </tfoot>
      </table>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    $("#table_l251_detail").dataTable({
      "bSort" : true,
      "bFilter" : false,
      "bPaginate": false,
      "info": false,
    });
  });
</script>