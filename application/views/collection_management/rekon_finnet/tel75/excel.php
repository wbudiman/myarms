<h5><?= $header_title ?></h5>
<table border="1">
  <thead>
          <?php 
            $total_column=array();
            $total_column['total']=0;
            echo "<tr>";
            echo "<th class='text-center'>".$row_lists[$search['row']]."</th>";
            echo "<th class='text-center'>Tagihan</th>";
            echo "<th class='text-center'>PPn</th>";
            echo "<th class='text-center'>Cicilan</th>";
            echo "<th class='text-center'>Materai</th>";
            echo "<th class='text-center'>Denda</th>";
            echo "<th class='text-center'>Total Bayar</th>";
            echo "<th class='text-center'>Lembar</th>";
            echo "<th class='text-center'>Transaksi</th>";
            echo "</tr>";
          ?>
        </thead>
        <tbody>
         <?php   
            $rows=array();
            $total_column=array();
            $total_column['tagihan']=0;
            $total_column['ppn']=0;
            $total_column['cicilan']=0;
            $total_column['materai']=0;
            $total_column['denda']=0;
            $total_column['total_bayar']=0;
            $total_column['lembar']=0;
            $total_column['transaksi']=0;

            foreach($lists as $l){
              if(!in_array($l->{$field[$search['row']]},$rows)){
                array_push($rows,$l->{$field[$search['row']]});
                echo "<tr>";
                echo "<td>".($l->{$field[$search['row']]}!="" && $l->{$field[$search['row']]}!=NULL ? $l->{$field[$search['row']]} : $no_mapping)."</td>";
                echo "<td class='text-right'>".number_format($l->TAGIHAN,0,".",",")."</td>";
                echo "<td class='text-right'>".number_format($l->PPN,0,".",",")."</td>";
                echo "<td class='text-right'>".number_format($l->CICILAN,0,".",",")."</td>";
                echo "<td class='text-right'>".number_format($l->MATERAI,0,".",",")."</td>";
                echo "<td class='text-right'>".number_format($l->DENDA,0,".",",")."</td>";
                echo "<td class='text-right'>".number_format($l->TOTAL_BAYAR,0,".",",")."</td>";
                echo "<td class='text-right'>".number_format($l->LEMBAR,0,".",",")."</td>";
                echo "<td class='text-right'>".number_format($l->TRANSAKSI,0,".",",")."</td>";
                echo "</tr>";
                $total_column['tagihan']+=$l->TAGIHAN;
                $total_column['cicilan']+=$l->PPN;
                $total_column['cicilan']+=$l->CICILAN;
                $total_column['materai']+=$l->MATERAI;
                $total_column['denda']+=$l->DENDA;
                $total_column['total_bayar']+=$l->TOTAL_BAYAR;
                $total_column['lembar']+=$l->LEMBAR;
                $total_column['transaksi']+=$l->TRANSAKSI;

              }
            }
          ?>
        </tbody>
        <tfoot>
         <?php 
          echo "<tr>";
          echo "<td style='padding:8px;'>Total (".sizeof($rows)." item)</td>";
          echo "<td class='text-right' style='padding:8px;'>".number_format($total_column['tagihan'],0,".",",")."</td>";
          echo "<td class='text-right' style='padding:8px;'>".number_format($total_column['ppn'],0,".",",")."</td>";
          echo "<td class='text-right' style='padding:8px;'>".number_format($total_column['cicilan'],0,".",",")."</td>";
          echo "<td class='text-right' style='padding:8px;'>".number_format($total_column['materai'],0,".",",")."</td>";
          echo "<td class='text-right' style='padding:8px;'>".number_format($total_column['denda'],0,".",",")."</td>";
          echo "<td class='text-right' style='padding:8px;'>".number_format($total_column['total_bayar'],0,".",",")."</td>";
          echo "<td class='text-right' style='padding:8px;'>".number_format($total_column['lembar'],0,".",",")."</td>";
          echo "<td class='text-right' style='padding:8px;'>".number_format($total_column['transaksi'],0,".",",")."</td>";
          echo "</tr>";
        ?>
        </tfoot>
</table>