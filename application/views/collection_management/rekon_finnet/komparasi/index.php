<form method="post" action="<?= base_url($path."index") ?>">
  <div class="row">
    <div class="col-md-12">
      <div id="table-search">
        <div class="wrap">
          <table class="table table-search table-striped">
            <thead>
              <tr>
                <th colspan="8" v-align="middle">Parameter Pencarian Data</th>
              </tr>
            </thead>
            <tbody>
              <tr>                   
                <td>Loket/Bank</td>
                <td>
                  <select name="search[loket][]" multiple class="form-control" id="loket">
                    <?php 
                      //foreach($product_lists as $r){
                      //  echo "<option value='".$r->PRODUCT_ID."' ".($search['product']!="" && in_array($r->PRODUCT_ID,$search['product']) ? "selected" : "").">".$r->PRODUCT_NAME."</option>";
                      //}
                    ?>
                  </select>
                </td>
                <td>Periode</td>
                <td>
                  <div class="col-md-9">
                    <div class="col-sm-3">
                      <select name="search[day_1]" class="form-control" id="day_1">
                        <option value=""></option>
                        <?php 
                          for($x=1;$x<=31;$x++){
                            echo "<option value='".$x."' ".($search['day_1']==$x ? "selected" : "").">".$x."</option>";
                          }
                        ?>
                      </select>
                    </div>
                    <div class="col-sm-7">
                      <select name="search[month_1]" class="form-control" id="month_1">
                        <option value=""></option>
                        <?php 
                          foreach($month_lists as $key=>$d){
                            echo "<option value='".$key."' ".($search['month_1']==$key ? "selected" : "").">".$d."</option>";
                          }
                        ?>
                      </select>
                    </div>
                    <div class="col-sm-">
                      <select name="search[year_1]" class="form-control" id="year_1">
                        <option value=""></option>
                        <?php 
                          for($x=date("Y");$x>(date("Y")-5);$x--){
                            echo "<option value='".$x."' ".($search['year_1']==$x ? "selected" : "").">".$x."</option>";
                          }
                        ?>
                      </select>
                    </div>
                  </div>
                </td>
              </tr>
              <tr class="background-filter">                   
                <td>Show Data</td>
                <td>
                  <select name="search[row]" class="form-control" id="row">
                    <?php 
                      foreach($row_lists as $key=>$d){
                        echo "<option value='".$key."' ".($search['row']==$key ? "selected" : "").">".$d."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>View Data</td>
                <td>
                  <select name="search[row]" class="form-control" id="row">
                    <?php 
                      foreach($row_lists as $key=>$d){
                        echo "<option value='".$key."' ".($search['row']==$key ? "selected" : "").">".$d."</option>";
                      }
                    ?>
                  </select>
                </td>
              </tr>
            </tbody>
            <tfoot>
              <tr>
                <td>Search</td>
                <td><input type="submit" name="search[button]" class="btn btn-search" value="Search"></td>
                <td>To Excel</td>
                <td><input type="submit" name="search[button]" class="btn btn-search" value="Download"></td>
              </tr>
            </tfoot>
          </table>
        </div>
        <a href="javascript:void(0)" class="showhide"></a>
      </div>
    </div>
  </div> 
</form>
<div class="row">
  <div class="col-md-12">
    <h4 class="title-menu">List Komparasi Tel 75 - Gx Tel 72</h4>
    <div class="billing-detail">
      <table class="table table-bordered" id="table_growth">
        <thead>
          <?php 
            $total_column=array();
            $total_column['total']=0;
            echo "<tr>";
            echo "<th class='text-center'>".$row_lists[$search['row']]."</th>";
            echo "<th class='text-center'>Total</th>";
            echo "</tr>";
          ?>
        </thead>
        <tbody>
         <?php   
            /*$rows=array();
            $total_column=array();
            $total_column['jumlah']=0;
            $total_column['amount']=0;
            foreach($lists as $l){
              if(!in_array($l->{$field[$search['row']]},$rows)){
                array_push($rows,$l->{$field[$search['row']]});
                echo "<tr>";
                echo "<td><a href='".base_url($path."detail/00?".$param_get."&val=".$l->{$field[$search['row']]}."&field=".$field[$search['row']])."' target='_blank'>".($l->{$field[$search['row']]}!="" && $l->{$field[$search['row']]}!=NULL ? $l->{$field[$search['row']]} : $no_mapping)."</a></td>";
                echo "<td class='text-right'>".number_format($l->JUMLAH,0,".",",")."</td>";
                echo "<td class='text-right'>".number_format($l->AMOUNT,0,".",",")."</td>";
                echo "</tr>";
                $total_column['jumlah']+=$l->JUMLAH;
                $total_column['amount']+=$l->AMOUNT;
              }
            }*/
          ?>
        </tbody>
        <tfoot>
         <?php /*
          echo "<tr>";
          echo "<td style='padding:8px;'>Total (".sizeof($rows)." item)</td>";
          echo "<td class='text-right' style='padding:8px;'>".number_format($total_column['jumlah'],0,".",",")."</td>";
          echo "<td class='text-right' style='padding:8px;'>".number_format($total_column['amount'],0,".",",")."</td>";
          echo "</tr>";*/
        ?>
        </tfoot>
      </table>
    </div>
  </div>
</div>
<script type="text/javascript">
  height="290px";
  $(document).ready(function(){
    $("#table_growth").dataTable({
      "bSort" : true,
      "bFilter" : false,
      "bPaginate": false,
      "info": false,
    });
    $("#division").multiselect({
      includeSelectAllOption: true,
      numberDisplayed:2
    }).change(function(){
      var divisions=$(this).val();
      $.ajax({
        url:"<?= base_url("ajax/getWitel") ?>",
        dataType:"JSON",
        data:{divisions:divisions},
        success:function(response){
          $("#witel").multiselect("destroy");
          $("#witel").html(response.content);
          $("#witel").multiselect({
            includeSelectAllOption: true,
            maxHeight: 212,
            enableFiltering: true
          }).change();
        }
      });
    });
    $("#summary_product").multiselect({
      includeSelectAllOption: true,
      numberDisplayed:2
    }).change(function(){
      var summary_products=$(this).val();
      $.ajax({
        url:"<?= base_url("ajax/getProducts") ?>",
        dataType:"JSON",
        data:{summary_products:summary_products},
        success:function(response){
          $("#product").multiselect("destroy");
          $("#product").html(response.content);
          $("#product").multiselect({
            includeSelectAllOption: true,
            maxHeight: 212
          });
        }
      });
    });
    $("#ubis").multiselect({
      includeSelectAllOption: true,
      numberDisplayed:2
    }).change(function(){
      var ubis=$(this).val();
      $.ajax({
        url:"<?= base_url("ajax/getUbisSegments") ?>",
        dataType:"JSON",
        data:{ubis:ubis},
        success:function(response){
          $("#ubis_segment").multiselect("destroy");
          $("#ubis_segment").html(response.content);
          $("#ubis_segment").multiselect({
            includeSelectAllOption: true,
            maxHeight: 212,
            enableFiltering: true
          });
        }
      });
    });
    $("#loket,#ba_pay,#product,#ubis_segment,#bisnis_area").multiselect({numberDisplayed:2,maxHeight: 212,includeSelectAllOption: true,enableFiltering: true});
    $("#top_customer").multiselect({
      numberDisplayed:1,
      onChange: function(element, checked) {
          if(checked==true){
            value=$(element).val();
            if(value=="SILVER"){
              $("#top_customer option:selected[value='GOLD']").removeAttr("selected");
              $("#top_customer option:selected[value='PLATIN']").removeAttr("selected");
              $("#top_customer option:selected[value='TITAN']").removeAttr("selected");
            }else{
              $("#top_customer option:selected[value='SILVER']").removeAttr("selected");
            }
            $("#top_customer").multiselect('refresh');
          }
      }
    });
    $("#view_by").change(function(){
      var view_by=$(this).val();
      switch(view_by){
        case "AMOUNT":
          $("#show_data option[value='billing_component']").removeAttr("disabled");
          if($("#show_data").val()=="billing_component"){
            $("#billing_component").attr("disabled","disabled");
          }else{
            $("#billing_component").removeAttr("disabled");
          }
          break;
        case "L11":
          if($("#show_data").val()=="billing_component")$("#show_data").val(0);
          $("#billing_component").attr("disabled","disabled");
          $("#show_data option[value='billing_component']").attr("disabled","disabled");
          break;
        case "ARPU":
          $("#billing_component").removeAttr("disabled");
          $("#show_data option[value='billing_component']").removeAttr("disabled");
          if($("#show_data").val()=="billing_component"){
            $("#billing_component").attr("disabled","disabled");
          }else{
            $("#billing_component").removeAttr("disabled");
          }
          break;
      }
    });
    $("#show_data").change(function(){
      if($(this).val()=="billing_component"){
        $("#billing_component").attr("disabled","disabled");
      }else{
        $("#billing_component").removeAttr("disabled");
      }
    });
    $("form").submit(function(){
      var periode1=$("#year_1").val()+$("#month_1").val();
      var periode2=$("#year_2").val()+$("#month_2").val();
      var periode3=$("#year_3").val()+$("#month_3").val();
      if(periode3<=periode2 || periode2<=periode1){
        main.notification("bottomRight","warning","Peringatan","Silahkan pilih periode 1,2,3 dengan benar!");
        return false;
      }
    });
  });
</script>