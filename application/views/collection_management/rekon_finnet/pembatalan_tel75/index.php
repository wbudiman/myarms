<form method="post" action="<?= base_url($path."index") ?>">
  <div class="row">
    <div class="col-md-12">
      <div id="table-search">
        <div class="wrap">
          <table class="table table-search table-striped">
            <thead>
              <tr>
                <th colspan="8" v-align="middle">Parameter Pencarian Data</th>
              </tr>
            </thead>
            <tbody>
              <tr>                   
                <td>Ubis</td>
                <td>
                  <select name="search[ubis][]" multiple class="form-control" id="ubis">
                    <?php 
                      foreach($ubis_lists as $d){
                        echo "<option value='".$d->UBIS_ID."' ".($search['ubis']!="" && in_array($d->UBIS_ID,$search['ubis']) ? "selected" : "").">".$d->UBIS."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Bisnis Area</td>
                <td>
                  <select name="search[bisnis_area][]" multiple class="form-control" id="bisnis_area">
                    <?php 
                      foreach($bisnis_area_lists as $d){
                        echo "<option value='".$d->BUSINESS_AREA_ID."' ".($search['bisnis_area']!="" && in_array($d->BUSINESS_AREA_ID,$search['bisnis_area']) ? "selected" : "").">".$d->BUSINESS_AREA_DESCRIPTION."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>Periode</td>
                <td>
                  <div class="col-md-9">
                    <div class="col-sm-3">
                      <select name="search[day_1]" class="form-control" id="day_1">
                        <option value="">-ALL-</option>
                        <?php 
                          for($x=1;$x<=31;$x++){
                            echo "<option value='".$x."' ".($search['day_1']==$x ? "selected" : "").">".$x."</option>";
                          }
                        ?>
                      </select>
                    </div>
                    <div class="col-sm-7">
                      <select name="search[month_1]" class="form-control" id="month_1">
                        <option value=""></option>
                        <?php 
                          foreach($month_lists as $key=>$d){
                            echo "<option value='".$key."' ".($search['month_1']==$key ? "selected" : "").">".$d."</option>";
                          }
                        ?>
                      </select>
                    </div>
                    <div class="col-sm-">
                      <select name="search[year_1]" class="form-control" id="year_1">
                        <option value=""></option>
                        <?php 
                          for($x=date("Y");$x>(date("Y")-6);$x--){
                            echo "<option value='".$x."' ".($search['year_1']==$x ? "selected" : "").">".$x."</option>";
                          }
                        ?>
                      </select>
                    </div>
                  </div>
                </td>
              </tr>
              <tr>                   
               <td>Loket/Bank</td>
                <td>
                  <select name="search[bank][]" multiple class="form-control" id="bank">
                    <?php 
                      foreach($bank_lists as $r){
                        echo "<option value='".$r->BANK_CODE."' ".($search['bank']!="" && in_array($r->BANK_CODE,$search['bank']) ? "selected" : "").">".$r->MAPPING_BANK."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td>BA PAY</td>
                <td><select name="search[ba_pay][]" multiple class="form-control" id="ba_pay">
                    <?php 
                      foreach($ba_pay_lists as $r){
                        echo "<option value='".$r->BUSINESS_AREA_ID."' ".($search['ba_pay']!="" && in_array($r->BUSINESS_AREA_ID,$search['ba_pay']) ? "selected" : "").">".$r->BUSINESS_AREA_DESCRIPTION."</option>";
                      }
                    ?>
                  </select></td>
                  <td></td>
                  <td></td>
              </tr>
              <tr class="background-filter">                   
                <td>Show Data</td>
                <td>
                  <select name="search[row]" class="form-control" id="row">
                    <?php 
                      foreach($row_lists as $key=>$d){
                        echo "<option value='".$key."' ".($search['row']==$key ? "selected" : "").">".$d."</option>";
                      }
                    ?>
                  </select>
                </td>
                <td></td>
                <td>
                </td>
				        <td></td>
                <td></td>
              </tr>
            </tbody>
            <tfoot>
              <tr>
                <td>Search</td>
                <td><input type="submit" name="search[button]" class="btn btn-search" value="Search"></td>
                <td>To Excel</td>
                <td><input type="submit" name="search[button]" class="btn btn-search" value="Download"></td>
                <td colspan="4"></td>
              </tr>
            </tfoot>
          </table>
        </div>
        <a href="javascript:void(0)" class="showhide"></a>
      </div>
    </div>
  </div> 
</form>
<div class="row">
  <div class="col-md-12">
    <h4 class="title-menu">List Pembatalan Tel75</h4>
    <div class="billing-detail">
      <table class="table table-bordered" id="table_growth">
        <thead>
          <?php 
            $total_column=array();
            $total_column['total']=0;
            echo "<tr>";
            echo "<th class='text-center'>".$row_lists[$search['row']]."</th>";
            echo "<th class='text-center'>Tagihan</th>";
            echo "<th class='text-center'>Cicilan</th>";
            echo "<th class='text-center'>Materai</th>";
            echo "<th class='text-center'>Denda</th>";
            echo "<th class='text-center'>Total Cancel</th>";
            echo "<th class='text-center'>Lembar Cancel</th>";
            echo "</tr>";
          ?>
        </thead>
        <tbody>
         <?php   
            $rows=array();
            $total_column=array();
            $total_column['tagihan']=0;
            $total_column['cicilan']=0;
            $total_column['materai']=0;
            $total_column['denda']=0;
            $total_column['total_cancel']=0;
            $total_column['lembar_cancel']=0;

            foreach($lists as $l){
              if(!in_array($l->{$field[$search['row']]},$rows)){
                array_push($rows,$l->{$field[$search['row']]});
                echo "<tr>";
                echo "<td>".($l->{$field[$search['row']]}!="" && $l->{$field[$search['row']]}!=NULL ? $l->{$field[$search['row']]} : $no_mapping)."</td>";
                echo "<td class='text-right'>".number_format($l->TAGIHAN,0,".",",")."</td>";
                echo "<td class='text-right'>".number_format($l->CICILAN,0,".",",")."</td>";
                echo "<td class='text-right'>".number_format($l->MATERAI,0,".",",")."</td>";
                echo "<td class='text-right'>".number_format($l->DENDA,0,".",",")."</td>";
                echo "<td class='text-right'>".number_format($l->TOTAL_CANCEL,0,".",",")."</td>";
                echo "<td class='text-right'>".number_format($l->LEMBAR_CANCEL,0,".",",")."</td>";
                echo "</tr>";
                $total_column['tagihan']+=$l->TAGIHAN;
                $total_column['cicilan']+=$l->CICILAN;
                $total_column['materai']+=$l->MATERAI;
                $total_column['denda']+=$l->DENDA;
                $total_column['total_cancel']+=$l->TOTAL_CANCEL;
                $total_column['lembar_cancel']+=$l->LEMBAR_CANCEL;

              }
            }
          ?>
        </tbody>
        <tfoot>
         <?php 
          echo "<tr>";
          echo "<td style='padding:8px;'>Total (".sizeof($rows)." item)</td>";
          echo "<td class='text-right' style='padding:8px;'>".number_format($total_column['tagihan'],0,".",",")."</td>";
          echo "<td class='text-right' style='padding:8px;'>".number_format($total_column['cicilan'],0,".",",")."</td>";
          echo "<td class='text-right' style='padding:8px;'>".number_format($total_column['materai'],0,".",",")."</td>";
          echo "<td class='text-right' style='padding:8px;'>".number_format($total_column['denda'],0,".",",")."</td>";
          echo "<td class='text-right' style='padding:8px;'>".number_format($total_column['total_cancel'],0,".",",")."</td>";
          echo "<td class='text-right' style='padding:8px;'>".number_format($total_column['lembar_cancel'],0,".",",")."</td>";
          echo "</tr>";
        ?>
        </tfoot>
      </table>
    </div>
  </div>
</div>
<script type="text/javascript">
  height="290px";
  $(document).ready(function(){
    $("#table_growth").dataTable({
      "bSort" : true,
      "bFilter" : false,
      "bPaginate": false,
      "info": false,
    });
    $("#division").multiselect({
      includeSelectAllOption: true,
      numberDisplayed:2
    }).change(function(){
      var divisions=$(this).val();
      $.ajax({
        url:"<?= base_url("ajax/getWitel") ?>",
        dataType:"JSON",
        data:{divisions:divisions},
        success:function(response){
          $("#witel").multiselect("destroy");
          $("#witel").html(response.content);
          $("#witel").multiselect({
            includeSelectAllOption: true,
            maxHeight: 212,
            enableFiltering: true
          }).change();
        }
      });
    });
    $("#summary_product").multiselect({
      includeSelectAllOption: true,
      numberDisplayed:2
    }).change(function(){
      var summary_products=$(this).val();
      $.ajax({
        url:"<?= base_url("ajax/getProducts") ?>",
        dataType:"JSON",
        data:{summary_products:summary_products},
        success:function(response){
          $("#product").multiselect("destroy");
          $("#product").html(response.content);
          $("#product").multiselect({
            includeSelectAllOption: true,
            maxHeight: 212
          });
        }
      });
    });
    $("#ubis").multiselect({
      includeSelectAllOption: true,
      numberDisplayed:2
    }).change(function(){
      var ubis=$(this).val();
      $.ajax({
        url:"<?= base_url("ajax/getUbisSegments") ?>",
        dataType:"JSON",
        data:{ubis:ubis},
        success:function(response){
          $("#ubis_segment").multiselect("destroy");
          $("#ubis_segment").html(response.content);
          $("#ubis_segment").multiselect({
            includeSelectAllOption: true,
            maxHeight: 212,
            enableFiltering: true
          });
        }
      });
    });
    $("#bank,#ba_pay,#product,#ubis_segment,#bisnis_area").multiselect({numberDisplayed:2,maxHeight: 212,includeSelectAllOption: true,enableFiltering: true});
    $("#top_customer").multiselect({
      numberDisplayed:1,
      onChange: function(element, checked) {
          if(checked==true){
            value=$(element).val();
            if(value=="SILVER"){
              $("#top_customer option:selected[value='GOLD']").removeAttr("selected");
              $("#top_customer option:selected[value='PLATIN']").removeAttr("selected");
              $("#top_customer option:selected[value='TITAN']").removeAttr("selected");
            }else{
              $("#top_customer option:selected[value='SILVER']").removeAttr("selected");
            }
            $("#top_customer").multiselect('refresh');
          }
      }
    });
    $("#view_by").change(function(){
      var view_by=$(this).val();
      switch(view_by){
        case "AMOUNT":
          $("#show_data option[value='billing_component']").removeAttr("disabled");
          if($("#show_data").val()=="billing_component"){
            $("#billing_component").attr("disabled","disabled");
          }else{
            $("#billing_component").removeAttr("disabled");
          }
          break;
        case "L11":
          if($("#show_data").val()=="billing_component")$("#show_data").val(0);
          $("#billing_component").attr("disabled","disabled");
          $("#show_data option[value='billing_component']").attr("disabled","disabled");
          break;
        case "ARPU":
          $("#billing_component").removeAttr("disabled");
          $("#show_data option[value='billing_component']").removeAttr("disabled");
          if($("#show_data").val()=="billing_component"){
            $("#billing_component").attr("disabled","disabled");
          }else{
            $("#billing_component").removeAttr("disabled");
          }
          break;
      }
    });
    $("#show_data").change(function(){
      if($(this).val()=="billing_component"){
        $("#billing_component").attr("disabled","disabled");
      }else{
        $("#billing_component").removeAttr("disabled");
      }
    });
    $("form").submit(function(){
      var periode1=$("#year_1").val()+$("#month_1").val();
      var periode2=$("#year_2").val()+$("#month_2").val();
      var periode3=$("#year_3").val()+$("#month_3").val();
      if(periode3<=periode2 || periode2<=periode1){
        main.notification("bottomRight","warning","Peringatan","Silahkan pilih periode 1,2,3 dengan benar!");
        return false;
      }
    });
  });
</script>