<h5><?= $header_title ?></h5>
<table border="1">
  <thead>
          <?php 
            $total_column=array();
            $total_column['total']=0;
            echo "<tr>";
            echo "<th class='text-center'>".$row_lists[$search['row']]."</th>";
            echo "<th class='text-center'>Tagihan</th>";
            echo "<th class='text-center'>Cicilan</th>";
            echo "<th class='text-center'>Materai</th>";
            echo "<th class='text-center'>Denda</th>";
            echo "<th class='text-center'>Total Cancel</th>";
            echo "<th class='text-center'>Lembar Cancel</th>";
            echo "</tr>";
          ?>
        </thead>
        <tbody>
         <?php   
            $rows=array();
            $total_column=array();
            $total_column['tagihan']=0;
            $total_column['cicilan']=0;
            $total_column['materai']=0;
            $total_column['denda']=0;
            $total_column['total_cancel']=0;
            $total_column['lembar_cancel']=0;

            foreach($lists as $l){
              if(!in_array($l->{$field[$search['row']]},$rows)){
                array_push($rows,$l->{$field[$search['row']]});
                echo "<tr>";
                echo "<td>".($l->{$field[$search['row']]}!="" && $l->{$field[$search['row']]}!=NULL ? $l->{$field[$search['row']]} : $no_mapping)."</td>";
                echo "<td class='text-right'>".number_format($l->TAGIHAN,0,".",",")."</td>";
                echo "<td class='text-right'>".number_format($l->CICILAN,0,".",",")."</td>";
                echo "<td class='text-right'>".number_format($l->MATERAI,0,".",",")."</td>";
                echo "<td class='text-right'>".number_format($l->DENDA,0,".",",")."</td>";
                echo "<td class='text-right'>".number_format($l->TOTAL_CANCEL,0,".",",")."</td>";
                echo "<td class='text-right'>".number_format($l->LEMBAR_CANCEL,0,".",",")."</td>";
                echo "</tr>";
                $total_column['tagihan']+=$l->TAGIHAN;
                $total_column['cicilan']+=$l->CICILAN;
                $total_column['materai']+=$l->MATERAI;
                $total_column['denda']+=$l->DENDA;
                $total_column['total_cancel']+=$l->TOTAL_CANCEL;
                $total_column['lembar_cancel']+=$l->LEMBAR_CANCEL;

              }
            }
          ?>
        </tbody>
        <tfoot>
         <?php 
          echo "<tr>";
          echo "<td style='padding:8px;'>Total (".sizeof($rows)." item)</td>";
          echo "<td class='text-right' style='padding:8px;'>".number_format($total_column['tagihan'],0,".",",")."</td>";
          echo "<td class='text-right' style='padding:8px;'>".number_format($total_column['cicilan'],0,".",",")."</td>";
          echo "<td class='text-right' style='padding:8px;'>".number_format($total_column['materai'],0,".",",")."</td>";
          echo "<td class='text-right' style='padding:8px;'>".number_format($total_column['denda'],0,".",",")."</td>";
          echo "<td class='text-right' style='padding:8px;'>".number_format($total_column['total_cancel'],0,".",",")."</td>";
          echo "<td class='text-right' style='padding:8px;'>".number_format($total_column['lembar_cancel'],0,".",",")."</td>";
          echo "</tr>";
        ?>
        </tfoot>
</table>