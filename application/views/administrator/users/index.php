<div class="row">
  <div class="col-md-12">
    <h3 class="title-menu">Otorisasi User</h3>
    <div class="billing-detail">
      <div class="pull-right">
        <a href="<?= base_url("administrator/users/create") ?>" class="btn btn-search">Tambah Baru</a>
      </div>
      <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-search">
        <thead>
          <tr>
            <th>Nama Hak Akses</th>
            <th>Nama</th>
            <th>Username</th>
            <th width="80">Aksi</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach($lists as $l): ?>
          <tr>
            <td><?= $l->ROLE_NAME ?></td>
            <td><?= $l->NAME ?></td>
            <td><?= $l->USERNAME ?></td>
            <td class="center">
              <a href="<?= base_url("administrator/users/edit/".$l->USER_ID) ?>" class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
              <a href="javascript:void(0)" data-confirm-modal="<?= base_url("administrator/users/delete/".$l->USER_ID) ?>" class="btn btn-xs btn-bricky tooltips" data-placement="top" data-original-title="Delete"><i class="fa fa-times fa fa-white"></i></a>
            </td>
          </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>