<div class="row">
  <div class="col-sm-12">
    <h3 class="page-header-2">Edit : Hak Akses</h3>
  </div>
</div>
<div class="row">
  <div class="col-sm-12">
    <div class="panel panel-default">
      <div class="panel-body">
        <form role="form" class="form-horizontal" method="post" action="<?= base_url("administrator/user_roles/edit") ?>">
          <input type="hidden" name="user_role[USER_ROLE_ID]" value="<?= $user_role->USER_ROLE_ID ?>">
          <div class="form-group">
            <label class="col-sm-3 control-label">Nama Hak Akses</label>
            <div class="col-sm-4">
              <input type="text" placeholder="Nama Hak Akses" class="form-control required" name="user_role[ROLE_NAME]" value="<?= $user_role->ROLE_NAME ?>">
              <span class="help-block"></span>
            </div>
          </div>
          <div class="clearfix"></div>
          <table class="table table-bordered">
            <thead>
              <tr>
                <th>Menu Level 1 - 5</th>
                <th width="30">Akses</th>
              </tr>
            </thead>
            <tbody>
              <?php 
                $counter=0;
                foreach($module_lists as $m){
                  echo "<tr>";
                  echo "<td>".$m['data']->NAME."</td>";
                  echo "<td class='text-center'>";
                  if(sizeof($m['childs'])==0){
                    echo "<input type='checkbox' name='detail[]' value='".$m['data']->MENU_ID."' ".($m['data']->IS_EXIST!=NULL ? "checked" : "").">";
                    $counter++;
                  }
                  echo "</td>";
                  echo "</tr>";
                  foreach($m['childs'] as $l2){
                    echo "<tr>";
                    echo "<td>&nbsp;&nbsp;&nbsp;&nbsp;".$l2['data']->NAME."</td>";
                    echo "<td class='text-center'>";
                    if(sizeof($l2['childs'])==0){
                      echo "<input type='checkbox' name='detail[]' value='".$l2['data']->MENU_ID."' ".($l2['data']->IS_EXIST!=NULL ? "checked" : "").">";
                      $counter++;
                    }
                    echo "</td>";
                    echo "</tr>";
                    foreach($l2['childs'] as $l3){
                      echo "<tr>";
                      echo "<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$l3['data']->NAME."</td>";
                      echo "<td class='text-center'>";
                      if(sizeof($l3['childs'])==0){
                        echo "<input type='checkbox' name='detail[]' value='".$l3['data']->MENU_ID."' ".($l3['data']->IS_EXIST!=NULL ? "checked" : "").">";
                        $counter++;
                      }
                      echo "</td>";
                      echo "</tr>";
                      foreach($l3['childs'] as $l4){
                        echo "<tr>";
                        echo "<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$l4['data']->NAME."</td>";
                        echo "<td class='text-center'>";
                        if(sizeof($l4['childs'])==0){
                          echo "<input type='checkbox' name='detail[]' value='".$l4['data']->MENU_ID."' ".($l4['data']->IS_EXIST!=NULL ? "checked" : "").">";
                          $counter++;
                        }
                        echo "</td>";
                        echo "</tr>";
                        foreach($l4['childs'] as $l5){
                          echo "<tr>";
                          echo "<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$l5['data']->NAME."</td>";
                          echo "<td class='text-center'>";
                          if(sizeof($l5['childs'])==0){
                            echo "<input type='checkbox' name='detail[]' value='".$l5['data']->MENU_ID."' ".($l5['data']->IS_EXIST!=NULL ? "checked" : "").">";
                            $counter++;
                          }
                          echo "</td>";
                          echo "</tr>";
                        }
                      }
                    }
                  }
                }
              ?>
            </tbody>
          </table>
          <div class="form-group">
            <div class="text-center">
              <button type="submit" class="btn btn-search" id="user_role_validate">Ubah</button>
              <button type="reset" class="btn btn-default">Reset</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    $("#user_role_validate").click(function(){
      $($(this).parents("form")).validate({
        submitHandler: function(form){
          if($("input[name='detail[]']:checked").length==0){
            app.notification("Peringatan","Cek list salah satu Hak akses","danger");
          }else{
            form.submit();
          }
        },
        errorPlacement: function(error, element){
          if(element.parents(".form-group:first").hasClass("has-error")==false){
            element.parents(".form-group:first").addClass("has-error");
          }
          element.siblings(".help-block").text(error.text());
        },
        success:function(label,element){
          $(element).parents(".form-group:first").removeClass("has-error");
          label.html("");
        }
      });
    });
  });
</script>