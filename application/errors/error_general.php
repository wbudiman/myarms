<!DOCTYPE html>
<html lang="en">
	<head>
		<title>MYARMS</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="<?= base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= base_url(); ?>assets/css/style.css" rel="stylesheet">
    <link href="<?= base_url(); ?>assets/css/custom.css" rel="stylesheet">
	</head>
	<body class="error-full-page">
    <header></header> 
    <div class="container">
      <div class="row" style="transform: translate(0, 25%);">
        <div class="col-sm-12 page-error">
          <div class="error-number bricky">Oops</div><br><br>
          <div class="error-details col-sm-6 col-sm-offset-3">
            <h3>Maaf, Halaman ini tidak tersedia!</h3>
            <p>
              Halaman yang anda cari tidak ditemukan.
              <br>
              Mungkin halaman ini telah dihapus atau tidak tersedia lagi.
              <br>
              Coba periksa URL yang anda tuju jika ada kesalahan dan coba lagi.
              <br>
              <a href="<?= base_url() ?>" class="btn btn-teal btn-return">Kembali ke Halaman Utama</a>
            </p>
          </div>
        </div>
        <div class="clearfix"></div>
        <div class="bricky text-center"><?= $message ?></div>
      </div>
    </div>
    <footer>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          copyright 2014 &copy; MyARMS Applications all rights reserved
        </div>    
      </div>    
    </div>
  </footer>
  </body>
</html>