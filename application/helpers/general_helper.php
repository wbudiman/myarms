<?php 
  function check_menus($location,$current_url){
    $check=false;
    if (stripos($location,$current_url)!== false) {
      $check=true;
    }
    return $check;
  }
  function get_by_url($url="")
  {
    $ci=&get_instance();
    $ci->load->model("menu_model","menu");
    $menu=$ci->menu->find_by_URL($url);
    if(sizeof($menu)>0)return $menu->MENU_ID;
    return 0;
  }
  function menus($user_role="")
  {
    $ci=&get_instance();
    $ci->load->model("user_role_model","user_role");
    $level1=$ci->user_role->get_modules(0,$user_role);
    $result=array();
    foreach($level1 as $l1){
      $result[$l1->MENU_ID]=array("data"=>$l1,"childs"=>array());
      $level2=$ci->user_role->get_modules($l1->MENU_ID,$user_role);
      $counter2=0;
      foreach($level2 as $l2){
        if($l2->IS_EXIST>0){
          $result[$l1->MENU_ID]['data']->IS_EXIST=1;
        }
        $result[$l1->MENU_ID]["childs"][$counter2]=array("data"=>$l2,"childs"=>array());
        $level3=$ci->user_role->get_modules($l2->MENU_ID,$user_role);
        $counter3=0;
        foreach($level3 as $l3){
          if($l3->IS_EXIST>0){
            $result[$l1->MENU_ID]['data']->IS_EXIST=1;
            $result[$l1->MENU_ID]["childs"][$counter2]['data']->IS_EXIST=1;
          }
          $result[$l1->MENU_ID]["childs"][$counter2]['childs'][$counter3]=array("data"=>$l3,"childs"=>array());
          $level4=$ci->user_role->get_modules($l3->MENU_ID,$user_role);
          $counter4=0;
          foreach($level4 as $l4){
            if($l4->IS_EXIST>0){
              $result[$l1->MENU_ID]['data']->IS_EXIST=1;
              $result[$l1->MENU_ID]["childs"][$counter2]['data']->IS_EXIST=1;
              $result[$l1->MENU_ID]["childs"][$counter2]['childs'][$counter3]['data']->IS_EXIST=1;
            }
            $result[$l1->MENU_ID]["childs"][$counter2]['childs'][$counter3]['childs'][$counter4]=array("data"=>$l4,"childs"=>array());
            $level5=$ci->user_role->get_modules($l4->MENU_ID,$user_role);
            $counter5=0;
            foreach($level5 as $l5){
              if($l5->IS_EXIST>0){
                $result[$l1->MENU_ID]['data']->IS_EXIST=1;
                $result[$l1->MENU_ID]["childs"][$counter2]['data']->IS_EXIST=1;
                $result[$l1->MENU_ID]["childs"][$counter2]['childs'][$counter3]['data']->IS_EXIST=1;
                $result[$l1->MENU_ID]["childs"][$counter2]['childs'][$counter3]['childs'][$counter4]['data']->IS_EXIST=1;
              }
              $result[$l1->MENU_ID]["childs"][$counter2]['childs'][$counter3]['childs'][$counter4]['childs'][$counter5]=array("data"=>$l5,"childs"=>array());
              $counter5++;
            }
            $counter4++;
          }
          $counter3++;
        }
        $counter2++;
      }
    }
    return $result;
  }
  function access_log($data=array()){
    $access_type=array(
      "Melihat Data Pada Menu ",
      "Membuat Data Baru Pada Menu ",
      "Edit Data Pada Menu ",
      "Hapus Data Pada Menu ",
      "Download Data Summary Pada Menu ",
      "Download Data Detail Pada Menu "
    );
    $ci=&get_instance();
    $ci->load->model("access_log_model","log");
    $ci->load->model("menu_model","menu");
    $log=$ci->log->find_by_USER_ID_and_MENU_ID_and_TYPE_and_IP_ADDRESS($data['USER_ID'],$data['MENU_ID'],$data['TYPE'],$data['IP_ADDRESS']);
    if(sizeof($log)>0){
      $ci->log->update(array("LAST_ACCESS"=>date("Y-m-d H:i:s")),$log->LOG_ID,"LOG_ID");
    }else{
      $menu=$ci->menu->find_by_MENU_ID($data['MENU_ID']);
      $data['DESCRIPTION']=$access_type[$data['TYPE']].$menu->NAME;
      $log=$ci->log->find_all("LOG_ID","DESC");
      $log_id=(sizeof($log)>0 ? $log[0]->LOG_ID+1 : 1);
      $data['LOG_ID']=$log_id;
      $ci->log->save($data);
    }
  }
?>