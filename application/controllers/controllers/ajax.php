<?php

if (!defined('BASEPATH'))exit('No direct script access allowed');

class Ajax extends MY_Controller {
  public $path="ajax/";
  public function __construct() 
  {
    parent::__construct();
  }
  function getWitel()
  {
    $status=false;
    $division_lists=$this->input->get("divisions");
    $divisions=$this->implode_array($division_lists);
    $this->data['witel_lists']=$this->witel->lists($divisions);
    $content=$this->load->view($this->path."witelData",$this->data,true);
    echo json_encode(array(
      "content" => $content
    ));
  }
  function getDatel()
  {
    $status=false;
    $division_lists=$this->input->get("divisions");
    $witel_lists=$this->input->get("witels");
    $divisions=$this->implode_array($division_lists);
    $witels=$this->implode_array($witel_lists);
    $this->data['datel_lists']=$this->datel->lists($divisions,$witels);
    $content=$this->load->view($this->path."datelData",$this->data,true);
    echo json_encode(array(
      "content" => $content
    ));
  }
  function getProducts()
  {
    $status=false;
    $summary_product_lists=$this->input->get("summary_products");
    $summary_products=$this->implode_array($summary_product_lists);
    $this->data['product_lists']=$this->product->get_product_by_summary_product($summary_products);
    $content=$this->load->view($this->path."productData",$this->data,true);
    echo json_encode(array(
      "content" => $content
    ));
  }
  function getUbisSegments()
  {
    $status=false;
    $ubis_lists=$this->input->get("ubis");
    $ubis=$this->implode_array($ubis_lists);
    if($ubis==""){
      $this->data['ubis_lists']=$this->ubis->find_all("UBIS","ASC");
      $ubis=$this->implode_array($this->data['ubis_lists'],"UBIS_ID");
    }
    $this->data['ubis_segment_lists']=$this->ubis_segment->get_by_ubis_id($ubis);
    $content=$this->load->view($this->path."ubisSegmentData",$this->data,true);
    echo json_encode(array(
      "content" => $content
    ));
  }
  function getBisnisArea()
  {
    $status=false;
    $ubis_segment_lists=$this->input->get("ubis_segment");
    $ubis_segment=$this->implode_array($ubis_segment_lists);
    if($ubis_segment==""){
      $this->data['ubis_segment_lists']=$this->ubis_segment->find_all("UBIS_SEGMENT","ASC");
      $ubis_segment=$this->implode_array($this->data['ubis_segment_lists'],"UBIS_SEGMENT_ID");
    }
    $this->data['bisnis_area_lists']=$this->bisnis_area->get_by_ubis_segment_id($ubis_segment);
    $content=$this->load->view($this->path."bisnisAreaData",$this->data,true);
    echo json_encode(array(
      "content" => $content
    ));
  }
  function getDetailRevenueTree()
  {
    $data=$this->input->post("data");
    $data=$data['search'];
    $data['division']=isset($data['division']) ? $data['division'] : "";
    $data['witel']=isset($data['witel']) ? $data['witel'] : "";
    $data['datel']=isset($data['datel']) ? $data['datel'] : "";
    $data['ubis']=isset($data['ubis']) ? $data['ubis'] : "";
    $data['summary_product']=isset($data['summary_product']) ? $data['summary_product'] : "";
    $data['product']=isset($data['product']) ? $data['product'] : "";
    $data['ubis_segment']=isset($data['ubis_segment']) ? $data['ubis_segment'] : "";
    $data['top_customer']=isset($data['top_customer']) ? $data['top_customer'] : "";
    $table_type=$this->input->post("table_type");
    $table=$this->input->post("content");
    $this->data['data']=$data;
    $this->data['table_type']=$table_type;
    $this->data['content']=$table;
    $content=$this->load->view($this->path."getDetailRevenueTree",$this->data,true);
    echo json_encode(array(
      "status" => true,
      "content" => $content
    ));
  }
  function getImeDescriptions()
  {
    $packet_ime_lists=$this->input->get("packet_imes");
    if($packet_ime_lists==""){
      $packet_imes=$this->implode_array($this->packet_ime->get_packet_ime(),"PACKET_IME");
    }else{
      $packet_imes=$this->implode_array($packet_ime_lists);
    }
    $this->data['ime_description_lists']=$this->packet_ime->get_by_packet_ime($packet_imes);
    $content=$this->load->view($this->path."ImeDescriptionData",$this->data,true);
    echo json_encode(array(
      "content" => $content
    ));
  }
}