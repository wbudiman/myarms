<?php

if (!defined('BASEPATH'))exit('No direct script access allowed');

class Query_Tunggakan extends MY_Controller {
  public $path="collection_management/debt_management/query_tunggakan/";
  public $menu_id=0;
  public function __construct() 
  {
    parent::__construct();
    $this->menu_id=get_by_url("collection_management/debt_management/query_tunggakan");
  }
  
  function index()
  {
    $parameters=array(
      "month_1" => date("m"),
      "year_1" => date("Y"),
      "no_jastel" => "''"
    ); 
    $this->data['search']=array(
      "month_1" => date("m"),
      "year_1" => date("Y"),
      "no_jastel" => ""
    );
    if($this->input->server('REQUEST_METHOD') == 'POST'){
      $data=$this->input->post("search");
      $data['no_jastel']=isset($data['no_jastel']) ? $data['no_jastel'] : "";
      $this->data['search']=$data;
      $parameters=array(
        "month_1" => $data['month_1'],
        "year_1" => $data['year_1'],
    "no_jastel" => $data['no_jastel']
      );
      $this->data['lists']=$this->if_master->Modification($parameters);
      $data_get="no_jastel=".$this->data['search']['no_jastel']."&month_1=".$this->data['search']['month_1']."&year_1=".$this->data['search']['year_1'];
      $this->data['param_get']=$data_get;
      if($data['button']=="Download"){
        access_log(array("MENU_ID"=>$this->menu_id,"USER_ID"=>$this->data['current_user']->USER_ID,"TYPE"=>4,"IP_ADDRESS"=>$_SERVER["REMOTE_ADDR"],"LAST_ACCESS"=>date("Y-m-d H:i:s")));
        $this->data['header_title']="Collection Management - IF Master TREMS - Modification";
        $this->data['header_parameter']=($this->data['month_lists'][$data['month_1']]." ".$data['year_1']);
        $filename="collection_management-if_master-modification_change_number.xlsx";
        $html = $this->load->view($this->path."excel",$this->data,true);
        $html=str_replace('&','&amp;',$html);
        $tmpfile = time().'.html';
        file_put_contents($tmpfile, $html);
        
        $this->load->library('PHPExcel');
        $reader = new PHPExcel_Reader_HTML; 
        $content = $reader->load($tmpfile); 
        $objWriter = PHPExcel_IOFactory::createWriter($content, 'Excel2007');
        ob_end_clean();
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
        unlink($tmpfile);
      }else{
        access_log(array("MENU_ID"=>$this->menu_id,"USER_ID"=>$this->data['current_user']->USER_ID,"TYPE"=>0,"IP_ADDRESS"=>$_SERVER["REMOTE_ADDR"],"LAST_ACCESS"=>date("Y-m-d H:i:s")));
        $this->data['path']=$this->path;
        $this->data['VIEW']=$this->load->view($this->path."index",$this->data,true);
        $this->load->view("templates/".$this->template, $this->data);
      }
    }else{
      $data_get="month_1=".$this->data['search']['month_1']."&year_1=".$this->data['search']['year_1']."&no_jastel=".$this->data['search']['no_jastel'];
      $this->data['param_get']=$data_get;
      $this->data['lists']=$this->if_master->Modification($parameters);
      $this->data['path']=$this->path;
      $this->data['VIEW']=$this->load->view($this->path."index",$this->data,true);
      $this->load->view("templates/".$this->template, $this->data);
    }
  }
  
}