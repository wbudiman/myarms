<?php

if (!defined('BASEPATH'))exit('No direct script access allowed');

class Gxtel72 extends MY_Controller {
  public $path="collection_management/rekon_finnet/gxtel72/";
  public $menu_id=0;
  public function __construct() 
  {
    parent::__construct();
    $this->menu_id=get_by_url("collection_management/rekon_finnet/gxtel72");
    $this->data['billing_component_lists']=$this->billing_component->list_detail_billing();
  }
  
  function index()
  { 
    $row_lists=array();
    $field=array();
    foreach($this->row_column->get_row_gxtel72() as $r)
    {
      $row_lists[$r->ROW_COLUMN_FIELD]=$r->ROW_COLUMN_DESCRIPTION;
      $field[$r->ROW_COLUMN_FIELD]=$r->ROW_COLUMN_FIELD_USED;
      //if($r->ROW_COLUMN_FIELD_USED==NULL)$field[$r->ROW_COLUMN_FIELD]=$this->data['billing_component_lists'];
    } 
    $this->data['row_lists']=$row_lists;
    $this->data['field']=$field;
    $parameters=array(
      "division" => "''",
      "witel" => "''",
      "day_1" => date('d'),
      "month_1" => date('m'),
      "year_1" => date('Y'),
      "row" => "x",
      "ubis" => "''",
      "summary_product" => "''",
      "product" => "''",
      "ubis_segment" => "''",
      "billing_component" => "TAGIHAN",
      "top_customer" => "''",
      "indihome" => "",
      "view_by" => "Amount",
	    "keyword" => "''",
    );
    $this->data['search']=array(
      "division" => "",
      "witel" => "",
      "day_1" => date('d'),
      "month_1" => date('m'),
      "year_1" => date('Y'),
      "row" => "x",
      "ubis" => "",
      "summary_product" => "",
      "product" => "",
      "ubis_segment" => "",
      "billing_component" => "TAGIHAN",
      "top_customer" => "",
      "indihome" => "",
      "view_by" => "Amount",
	  "keyword" => "",
    );
    if($this->input->server('REQUEST_METHOD') == 'POST'){
      $data=$this->input->post("search");
      $data['division']=isset($data['division']) ? $data['division'] : "";
      $data['witel']=isset($data['witel']) ? $data['witel'] : "";
      $data['summary_product']=isset($data['summary_product']) ? $data['summary_product'] : "";
      $data['product']=isset($data['product']) ? $data['product'] : "";
      $data['ubis_segment']=isset($data['ubis_segment']) ? $data['ubis_segment'] : "";
      $data['ubis']=isset($data['ubis']) ? $data['ubis'] : "";
      $this->data['search']=$data;
      $parameters=array(
        "division" => $this->implode_array($data['division']),
        "witel" => $this->implode_array($data['witel']),
        "day_1" => $this->implode_array($data['day_1']),
        "month_1" => $data['month_1'],
        "year_1" => $data['year_1'],
        "row" => $data['row'],
        "summary_product" => $this->implode_array($data['summary_product']),
        "product" => $this->implode_array($data['product']),
        "ubis_segment" => $this->implode_array($data['ubis_segment']),
		    "keyword" => $data['keyword'],
      );
      $this->data['division_lists']=$this->division->find_all("DIVISION_CODE","ASC");
      if($data['division']==""){
        $divisions=$this->implode_array($this->data['division_lists'],"DIVISION_CODE");
      }else{
        $divisions=$this->implode_array($data['division']);
      }
      $this->data['witel_lists']=$this->witel->lists($divisions);
      if($data['witel']==""){
        $witels=$this->implode_array($this->data['witel_lists'],"WITEL_CODE");
      }else{
        $witels=$this->implode_array($data['witel']);
      }
      $this->data['datel_lists']=$this->datel->lists($divisions,$witels);
      $this->data['ubis_lists']=$this->ubis->find_all("UBIS","ASC");
      $this->data['summary_product_lists']=$this->product->lists("'Wireline','Wireless','Speedy'");
      $summary_products=$this->implode_array($this->data['summary_product_lists'],"SUMMARY_PRODUCT");
      $this->data['product_lists']=$this->product->get_product_by_summary_product($summary_products);
      if($data['ubis']==""){
        $ubis=$this->implode_array($this->data['ubis_lists'],"UBIS_ID");
      }else{
        $ubis=$this->implode_array($data['ubis']);
      }
      $this->data['ubis_segment_lists']=$this->ubis_segment->get_by_ubis_id($ubis);
      $this->data['view_by_lists']=$this->view_by->list_detail_billing();
      $this->data['lists']=$this->report_gimmick->PELANGGAN_DPT_GIMMICK($parameters);
      //$data_get="division=".urlencode($this->implode_array($this->data['search']['division']))."&witel=".urlencode($this->implode_array($this->data['search']['witel']))."&datel=".urlencode($this->implode_array($this->data['search']['datel']))."&summary_product=".urlencode($this->implode_array($this->data['search']['summary_product']))."&row=".urlencode($this->data['search']['row'])."&ubis_segment=".urlencode($this->implode_array($this->data['search']['ubis_segment']))."&product=".urlencode($this->implode_array($this->data['search']['product']))."&month_1=".$this->data['search']['month_1']."&year_1=".$this->data['search']['year_1'];
      //$this->data['param_get']=$data_get; 
      if($data['button']=="Download"){
        access_log(array("MENU_ID"=>$this->menu_id,"USER_ID"=>$this->data['current_user']->USER_ID,"TYPE"=>4,"IP_ADDRESS"=>$_SERVER["REMOTE_ADDR"],"LAST_ACCESS"=>date("Y-m-d H:i:s")));
        $this->data['header_title']="Billing - Katalog Gimmick - Pelanggan Dapat Gimmick";
        $this->data['header_parameter']="";
        $filename="billing-katalog_gimmick-pelanggan_dpt_gimmick.xlsx";
        $html = $this->load->view($this->path."excel",$this->data,true);
        $html=str_replace('&','&amp;',$html);
        $tmpfile = time().'.html';
        file_put_contents($tmpfile, $html);
        
        $this->load->library('PHPExcel');
        $reader = new PHPExcel_Reader_HTML; 
        $content = $reader->load($tmpfile); 
        $objWriter = PHPExcel_IOFactory::createWriter($content, 'Excel2007');
        ob_end_clean();
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
        unlink($tmpfile);
      }else{
        access_log(array("MENU_ID"=>$this->menu_id,"USER_ID"=>$this->data['current_user']->USER_ID,"TYPE"=>0,"IP_ADDRESS"=>$_SERVER["REMOTE_ADDR"],"LAST_ACCESS"=>date("Y-m-d H:i:s")));
        $this->data['path']=$this->path;
        $this->data['VIEW']=$this->load->view($this->path."index",$this->data,true);
        $this->load->view("templates/".$this->template, $this->data);
      }
    }else{
      //$data_get="division=".urlencode($this->implode_array($this->data['search']['division']))."&witel=".urlencode($this->implode_array($this->data['search']['witel']))."&datel=".urlencode($this->implode_array($this->data['search']['datel']))."&summary_product=".urlencode($this->implode_array($this->data['search']['summary_product']))."&row=".urlencode($this->data['search']['row'])."&ubis_segment=".urlencode($this->implode_array($this->data['search']['ubis_segment']))."&product=".urlencode($this->implode_array($this->data['search']['product']))."&month_1=".$this->data['search']['month_1']."&year_1=".$this->data['search']['year_1'];
      //$this->data['param_get']=$data_get; 
      $this->data['division_lists']=$this->division->find_all("DIVISION_CODE","ASC");
      $divisions=$this->implode_array($this->data['division_lists'],"DIVISION_CODE");
      $this->data['witel_lists']=$this->witel->lists($divisions);
      $witels=$this->implode_array($this->data['witel_lists'],"WITEL_CODE");
      $this->data['datel_lists']=$this->datel->lists($divisions,$witels);
      $this->data['ubis_lists']=$this->ubis->find_all("UBIS","ASC");
      $this->data['bisnis_area_lists']=$this->bisnis_area->find_all("BUSINESS_AREA_DESCRIPTION","ASC");
      $this->data['summary_product_lists']=$this->product->lists("'Wireline','Wireless','Speedy'");
      $summary_products=$this->implode_array($this->data['summary_product_lists'],"SUMMARY_PRODUCT");
      $this->data['product_lists']=$this->product->get_product_by_summary_product($summary_products);
      $this->data['ubis_segment_lists']=$this->ubis_segment->find_all("UBIS_SEGMENT","ASC");
      $this->data['view_by_lists']=$this->view_by->list_detail_billing();
      $this->data['lists']=$this->rekon_finnet->TEL75($parameters);
      $this->data['path']=$this->path;
      $this->data['VIEW']=$this->load->view($this->path."index",$this->data,true);
      $this->load->view("templates/".$this->template, $this->data);
    }
  }

  function detail($param="")
  {
    $search=$this->input->get();
    $this->data['param_get']=http_build_query($search, '', '&amp;');
    $search['param']=$param;
    $this->data['search']=$search;
    $this->data['lists']=$this->report_gimmick->PELANGGAN_DPT_GIMMICK_detail($search); 
  
    if($this->input->server('REQUEST_METHOD') == 'POST'){
        access_log(array("MENU_ID"=>$this->menu_id,"USER_ID"=>$this->data['current_user']->USER_ID,"TYPE"=>4,"IP_ADDRESS"=>$_SERVER["REMOTE_ADDR"],"LAST_ACCESS"=>date("Y-m-d H:i:s")));
        $this->data['header_title']="Billing - Katalog Gimmick - Pelanggan Dapat Gimmick Detail";
        $this->data['header_parameter']="";
        $filename="billing-katalog_gimmick-pelanggan_dpt_gimmick_detail.xlsx";
        $html = $this->load->view($this->path."detail_excel",$this->data,true);
        $html=str_replace('&','&amp;',$html);
        $tmpfile = time().'.html';
        file_put_contents($tmpfile, $html);
        
        $this->load->library('PHPExcel');
        $reader = new PHPExcel_Reader_HTML; 
        $content = $reader->load($tmpfile); 
        $objWriter = PHPExcel_IOFactory::createWriter($content, 'Excel2007');
        ob_end_clean();
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
        unlink($tmpfile);
    }else{ 
      $this->data['path']=$this->path;
      $this->data['VIEW']=$this->load->view($this->path."detail",$this->data,true);
      $this->load->view("templates/".$this->template, $this->data);
    }
  }
}