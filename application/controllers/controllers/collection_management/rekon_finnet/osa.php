<?php

if (!defined('BASEPATH'))exit('No direct script access allowed');

class Osa extends MY_Controller {
  public $path="collection_management/rekon_finnet/osa/";
  public $menu_id=0;
  public function __construct() 
  {
    parent::__construct();
    $this->menu_id=get_by_url("collection_management/rekon_finnet/osa");
    $this->data['billing_component_lists']=$this->billing_component->list_detail_billing();
  }
  
  function index()
  { 
    $row_lists=array();
    $field=array();
    foreach($this->row_column->get_row_osa() as $r)
    {
      $row_lists[$r->ROW_COLUMN_FIELD]=$r->ROW_COLUMN_DESCRIPTION;
      $field[$r->ROW_COLUMN_FIELD]=$r->ROW_COLUMN_FIELD_USED;
      //if($r->ROW_COLUMN_FIELD_USED==NULL)$field[$r->ROW_COLUMN_FIELD]=$this->data['billing_component_lists'];
    } 
    $this->data['row_lists']=$row_lists;
    $this->data['field']=$field;
    $parameters=array(
      "bisnis_area" => "''",
      "ba_pay" => "''",
      "bank" => "''",
      "day_1" => date('d'),
      "month_1" => date('m'),
      "year_1" => date('Y'),
      "row" => "mb.BANK_CODE,mb.MAPPING_BANK",
      "ubis" => "''",
      "status" => ""
    );
    $this->data['search']=array(
      "bisnis_area" => "",
      "ba_pay" => "",
      "bank" => "",
      "day_1" => date('d'),
      "month_1" => date('m'),
      "year_1" => date('Y'),
      "row" => "mb.BANK_CODE,mb.MAPPING_BANK",
      "ubis" => "",
      "status" => ""
    );
    if($this->input->server('REQUEST_METHOD') == 'POST'){
      $data=$this->input->post("search");
      $data['bank']=isset($data['bank']) ? $data['bank'] : "";
      $data['status']=isset($data['status']) ? $data['status'] : "";
      $data['ubis']=isset($data['ubis']) ? $data['ubis'] : "";
      $data['bisnis_area']=isset($data['bisnis_area']) ? $data['bisnis_area'] : "";
      $data['ba_pay']=isset($data['ba_pay']) ? $data['ba_pay'] : "";
      $this->data['search']=$data;
      $parameters=array(
        "ubis" => $this->implode_array($data['ubis']),
        "bisnis_area" => $this->implode_array($data['bisnis_area']),
        "ba_pay" => $this->implode_array($data['ba_pay']),
        "day_1" => $data['day_1'],
        "month_1" => $data['month_1'],
        "year_1" => $data['year_1'],
        "row" => $data['row'],
        "status" => $data['status'],
        "bank" => $this->implode_array($data['bank'])
      );
      $this->data['ubis_lists']=$this->ubis->find_all("UBIS","ASC");
      $this->data['bank_lists']=$this->mapping_bank->find_all("MAPPING_BANK","ASC");
      $this->data['bisnis_area_lists']=$this->bisnis_area->find_all("BUSINESS_AREA_DESCRIPTION","ASC");
      $this->data['ba_pay_lists']=$this->bisnis_area->find_all("BUSINESS_AREA_DESCRIPTION","ASC");
      $this->data['lists']=$this->rekon_finnet->OSA($parameters);
      //$data_get="division=".urlencode($this->implode_array($this->data['search']['division']))."&witel=".urlencode($this->implode_array($this->data['search']['witel']))."&datel=".urlencode($this->implode_array($this->data['search']['datel']))."&summary_product=".urlencode($this->implode_array($this->data['search']['summary_product']))."&row=".urlencode($this->data['search']['row'])."&ubis_segment=".urlencode($this->implode_array($this->data['search']['ubis_segment']))."&product=".urlencode($this->implode_array($this->data['search']['product']))."&month_1=".$this->data['search']['month_1']."&year_1=".$this->data['search']['year_1'];
      //$this->data['param_get']=$data_get; 
      if($data['button']=="Download"){
        access_log(array("MENU_ID"=>$this->menu_id,"USER_ID"=>$this->data['current_user']->USER_ID,"TYPE"=>4,"IP_ADDRESS"=>$_SERVER["REMOTE_ADDR"],"LAST_ACCESS"=>date("Y-m-d H:i:s")));
        $this->data['header_title']="Collection - Rekon Finnet - OSA";
        $this->data['header_parameter']="";
        $filename="collection-rekon_finnet-osa.xlsx";
        $html = $this->load->view($this->path."excel",$this->data,true);
        $html=str_replace('&','&amp;',$html);
        $tmpfile = time().'.html';
        file_put_contents($tmpfile, $html);
        
        $this->load->library('PHPExcel');
        $reader = new PHPExcel_Reader_HTML; 
        $content = $reader->load($tmpfile); 
        $objWriter = PHPExcel_IOFactory::createWriter($content, 'Excel2007');
        ob_end_clean();
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
        unlink($tmpfile);
      }else{
        access_log(array("MENU_ID"=>$this->menu_id,"USER_ID"=>$this->data['current_user']->USER_ID,"TYPE"=>0,"IP_ADDRESS"=>$_SERVER["REMOTE_ADDR"],"LAST_ACCESS"=>date("Y-m-d H:i:s")));
        $this->data['path']=$this->path;
        $this->data['VIEW']=$this->load->view($this->path."index",$this->data,true);
        $this->load->view("templates/".$this->template, $this->data);
      }
    }else{
      //$data_get="division=".urlencode($this->implode_array($this->data['search']['division']))."&witel=".urlencode($this->implode_array($this->data['search']['witel']))."&datel=".urlencode($this->implode_array($this->data['search']['datel']))."&summary_product=".urlencode($this->implode_array($this->data['search']['summary_product']))."&row=".urlencode($this->data['search']['row'])."&ubis_segment=".urlencode($this->implode_array($this->data['search']['ubis_segment']))."&product=".urlencode($this->implode_array($this->data['search']['product']))."&month_1=".$this->data['search']['month_1']."&year_1=".$this->data['search']['year_1'];
      //$this->data['param_get']=$data_get; 
      
      $this->data['bank_lists']=$this->mapping_bank->find_all("MAPPING_BANK","ASC");
      $this->data['ubis_lists']=$this->ubis->find_all("UBIS","ASC");
      $this->data['bisnis_area_lists']=$this->bisnis_area->find_all("BUSINESS_AREA_DESCRIPTION","ASC");
      $this->data['ba_pay_lists']=$this->bisnis_area->find_all("BUSINESS_AREA_DESCRIPTION","ASC");
      $this->data['lists']=$this->rekon_finnet->OSA($parameters);
      $this->data['path']=$this->path;
      $this->data['VIEW']=$this->load->view($this->path."index",$this->data,true);
      $this->load->view("templates/".$this->template, $this->data);
    }
  }

  function detail($param="")
  {
    $search=$this->input->get();
    $this->data['param_get']=http_build_query($search, '', '&amp;');
    $search['param']=$param;
    $this->data['search']=$search;
    $this->data['lists']=$this->report_gimmick->PELANGGAN_DPT_GIMMICK_detail($search); 
  
    if($this->input->server('REQUEST_METHOD') == 'POST'){
        access_log(array("MENU_ID"=>$this->menu_id,"USER_ID"=>$this->data['current_user']->USER_ID,"TYPE"=>4,"IP_ADDRESS"=>$_SERVER["REMOTE_ADDR"],"LAST_ACCESS"=>date("Y-m-d H:i:s")));
        $this->data['header_title']="Billing - Katalog Gimmick - Pelanggan Dapat Gimmick Detail";
        $this->data['header_parameter']="";
        $filename="billing-katalog_gimmick-pelanggan_dpt_gimmick_detail.xlsx";
        $html = $this->load->view($this->path."detail_excel",$this->data,true);
        $html=str_replace('&','&amp;',$html);
        $tmpfile = time().'.html';
        file_put_contents($tmpfile, $html);
        
        $this->load->library('PHPExcel');
        $reader = new PHPExcel_Reader_HTML; 
        $content = $reader->load($tmpfile); 
        $objWriter = PHPExcel_IOFactory::createWriter($content, 'Excel2007');
        ob_end_clean();
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
        unlink($tmpfile);
    }else{ 
      $this->data['path']=$this->path;
      $this->data['VIEW']=$this->load->view($this->path."detail",$this->data,true);
      $this->load->view("templates/".$this->template, $this->data);
    }
  }
}