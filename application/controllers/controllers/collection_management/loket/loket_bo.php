<?php

if (!defined('BASEPATH'))exit('No direct script access allowed');

class Loket_Bo extends MY_Controller {
  public $path="collection_management/loket/loket_bo/";
  public $menu_id=0;
  public function __construct() 
  {
    parent::__construct();
    $this->menu_id=get_by_url("collection_management/loket/loket_bo");
  }
  
  function index()
  {
    $parameters=array(
      "division" => "''",
      "witel" => "''",
      "datel" => "''",
	  "deskloket" => "",
	  "deskakun" => "",
      "keyword" => "",
      "loket" => "''",
    ); 
    $this->data['search']=array(
      "division" => "",
      "witel" => "",
      "datel" => "",
	  "deskloket" => "",
	  "deskakun" => "",
      "keyword" => "",
      "loket" => "",
    );
    if($this->input->server('REQUEST_METHOD') == 'POST'){
      $data=$this->input->post("search");
      $data['division']=isset($data['division']) ? $data['division'] : "";
      $data['witel']=isset($data['witel']) ? $data['witel'] : "";
      $data['datel']=isset($data['datel']) ? $data['datel'] : "";
      $data['deskakun']=isset($data['deskakun']) ? $data['deskakun'] : "";
      $data['loket']=isset($data['loket']) ? $data['loket'] : "";
	  $data['deskloket']=isset($data['deskloket']) ? $data['deskloket'] : "";
	  $data['keyword']=isset($data['keyword']) ? $data['keyword'] : "";
      $this->data['search']=$data;
      $parameters=array(
		"keyword" => $data['keyword'],
		"deskloket" => $data['deskloket'],
		"deskakun" => $data['deskakun'],
        "division" => $this->implode_array($data['division']),
        "witel" => $this->implode_array($data['witel']),
        "datel" => $this->implode_array($data['datel']),
        "loket" => $this->implode_array($data['loket']),
      );
      $this->data['division_lists']=$this->division->find_all("DIVISION_CODE","ASC");
      if($data['division']==""){
        $divisions=$this->implode_array($this->data['division_lists'],"DIVISION_CODE");
      }else{
        $divisions=$this->implode_array($data['division']);
      }
      $this->data['witel_lists']=$this->witel->lists($divisions);
      if($data['witel']==""){
        $witels=$this->implode_array($this->data['witel_lists'],"WITEL_CODE");
      }else{
        $witels=$this->implode_array($data['witel']);
      }
      $this->data['datel_lists']=$this->datel->lists($divisions,$witels);
	  $this->data['lists']=$this->loket->LOKET_BO($parameters);
      $data_get="division=".urlencode($this->implode_array($data['division']))."&witel=".urlencode($this->implode_array($data['witel']))."&datel=".urlencode($this->implode_array($data['datel']))."&loket=".urlencode($this->implode_array($data['loket']))."&deskakun=".urlencode($data['deskakun'])."&deskloket=".urlencode($data['deskloket']);
      $this->data['param_get']=$data_get;
      if($data['button']=="Download"){
        access_log(array("MENU_ID"=>$this->menu_id,"USER_ID"=>$this->data['current_user']->USER_ID,"TYPE"=>4,"IP_ADDRESS"=>$_SERVER["REMOTE_ADDR"],"LAST_ACCESS"=>date("Y-m-d H:i:s")));
        $this->data['header_title']="Collection - Loket - Laporan Loket BO";
        $this->data['header_parameter']=($this->data['month_lists'][$data['month_1']]." ".$data['year_1']);
        $filename="collection-loket-laporan_loket_bo.xlsx";
        $html = $this->load->view($this->path."excel",$this->data,true);
        $html=str_replace('&','&amp;',$html);
        $tmpfile = time().'.html';
        file_put_contents($tmpfile, $html);
        
        $this->load->library('PHPExcel');
        $reader = new PHPExcel_Reader_HTML; 
        $content = $reader->load($tmpfile); 
        $objWriter = PHPExcel_IOFactory::createWriter($content, 'Excel2007');
        ob_end_clean();
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
        unlink($tmpfile);
      }else{
        access_log(array("MENU_ID"=>$this->menu_id,"USER_ID"=>$this->data['current_user']->USER_ID,"TYPE"=>0,"IP_ADDRESS"=>$_SERVER["REMOTE_ADDR"],"LAST_ACCESS"=>date("Y-m-d H:i:s")));
        $this->data['path']=$this->path;
        $this->data['VIEW']=$this->load->view($this->path."index",$this->data,true);
        $this->load->view("templates/".$this->template, $this->data);
      }
    }else{
      $this->data['division_lists']=$this->division->find_all("DIVISION_CODE","ASC");
	  $divisions=$this->implode_array($this->data['division_lists'],"DIVISION_CODE");
      $this->data['witel_lists']=$this->witel->lists($divisions);
      $witels=$this->implode_array($this->data['witel_lists'],"WITEL_CODE");
      $this->data['datel_lists']=$this->datel->lists($divisions,$witels);
	  $datels=$this->implode_array($this->data['datel_lists'],"DATEL_CODE");
	  $this->data['locket_lists']=$this->loket->lists($divisions,$witels,$datels);
      $this->data['lists']=$this->loket->LOKET_BO($parameters);
      $this->data['path']=$this->path;
      $this->data['VIEW']=$this->load->view($this->path."index",$this->data,true);
      $this->load->view("templates/".$this->template, $this->data);
    }
  }
  
}