<?php

if (!defined('BASEPATH'))exit('No direct script access allowed');

class Katalog_Gimmick extends MY_Controller {
  public $path="billing_management/gimmick/katalog_gimmick/";
  public $menu_id=0;
  public function __construct() 
  {
    parent::__construct();
    $this->menu_id=get_by_url("billing_management/gimmick/katalog_gimmick");
  }
  
  function index()
  {
    $parameters=array(
      "month_1" => date("m"),
      "year_1" => date("Y"),
      "month_2" => date("m"),
      "year_2" => date("Y"),
      "ubis" => "''",
      "ubis_segment" => "''",
      "product" => "''",
    ); 
    $this->data['search']=array(
      "month_1" => date("m"),
      "year_1" => date("Y"),
      "month_2" => date("m"),
      "year_2" => date("Y"),
      "ubis" => "",
      "ubis_segment" => "",
      "product" => "",
    );
    if($this->input->server('REQUEST_METHOD') == 'POST'){
      $data=$this->input->post("search");
      $data['ubis']=isset($data['ubis']) ? $data['ubis'] : "";
      $data['ubis_segment']=isset($data['ubis_segment']) ? $data['ubis_segment'] : "";
      $data['product']=isset($data['product']) ? $data['product'] : "";
      $this->data['search']=$data;
      $parameters=array(
        "month_1" => $data['month_1'],
        "year_1" => $data['year_1'],
        "month_2" => $data['month_2'],
        "year_2" => $data['year_2'],
        "ubis" => $this->implode_array($data['ubis']),
        "ubis_segment" => $this->implode_array($data['ubis_segment']),
        "product" => $this->implode_array($data['product']),
      );
      $this->data['ubis_lists']=$this->ubis->find_all("UBIS","ASC");
      if($data['ubis']==""){
        $ubis=$this->implode_array($this->data['ubis_lists'],"UBIS_ID");
      }else{
        $ubis=$this->implode_array($data['ubis']);
      }
      $this->data['ubis_segment_lists']=$this->ubis_segment->get_by_ubis_id($ubis);
      $this->data['product_lists']=$this->product->find_all();
      $this->data['lists']=$this->report_gimmick->GIMMICK_CATALOG($parameters);
      $data_get="ubis=".urlencode($this->implode_array($data['ubis']))."&ubis_segment=".urlencode($this->implode_array($data['ubis_segment']))."&product=".urlencode($this->implode_array($data['product']))."&month_1=".$this->data['search']['month_1']."&year_1=".$this->data['search']['year_1']."&month_2=".$this->data['search']['month_2']."&year_2=".$this->data['search']['year_2'];
      $this->data['param_get']=$data_get;
      if($data['button']=="Download"){
        access_log(array("MENU_ID"=>$this->menu_id,"USER_ID"=>$this->data['current_user']->USER_ID,"TYPE"=>4,"IP_ADDRESS"=>$_SERVER["REMOTE_ADDR"],"LAST_ACCESS"=>date("Y-m-d H:i:s")));
        $this->data['header_title']="Billing - Gimmick - Katalog Gimmick";
        $this->data['header_parameter']=($this->data['month_lists'][$data['month_1']]." ".$data['year_1']);
        $filename="billing-gimmick-katalog_gimmick.xlsx";
        $html = $this->load->view($this->path."excel",$this->data,true);
        $html=str_replace('&','&amp;',$html);
        $tmpfile = time().'.html';
        file_put_contents($tmpfile, $html);
        
        $this->load->library('PHPExcel');
        $reader = new PHPExcel_Reader_HTML; 
        $content = $reader->load($tmpfile); 
        $objWriter = PHPExcel_IOFactory::createWriter($content, 'Excel2007');
        ob_end_clean();
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
        unlink($tmpfile);
      }else{
        access_log(array("MENU_ID"=>$this->menu_id,"USER_ID"=>$this->data['current_user']->USER_ID,"TYPE"=>0,"IP_ADDRESS"=>$_SERVER["REMOTE_ADDR"],"LAST_ACCESS"=>date("Y-m-d H:i:s")));
        $this->data['path']=$this->path;
        $this->data['VIEW']=$this->load->view($this->path."index",$this->data,true);
        $this->load->view("templates/".$this->template, $this->data);
      }
    }else{
      $data_get="ubis=".urlencode($this->implode_array($this->data['search']['ubis']))."&ubis_segment=".urlencode($this->implode_array($this->data['search']['ubis_segment']))."&product=".urlencode($this->implode_array($this->data['search']['product']))."&month_1=".$this->data['search']['month_1']."&year_1=".$this->data['search']['year_1']."&month_2=".$this->data['search']['month_2']."&year_2=".$this->data['search']['year_2'];
      $this->data['param_get']=$data_get;
      $this->data['ubis_lists']=$this->ubis->find_all("UBIS","ASC");
      $this->data['ubis_segment_lists']=$this->ubis_segment->find_all("UBIS_SEGMENT","ASC");
      $this->data['product_lists']=$this->product->find_all();
      $this->data['lists']=$this->report_gimmick->GIMMICK_CATALOG($parameters);
      $this->data['path']=$this->path;
      $this->data['VIEW']=$this->load->view($this->path."index",$this->data,true);
      $this->load->view("templates/".$this->template, $this->data);
    }
  }
  // function detail($date="")
  // {
    // $search=$this->input->get();
    // $this->data['param_get']=http_build_query($search, '', '&amp;');
    // $search['date']=$date;
    // $this->data['search']=$search;
    // $this->data['lists']=$this->report->WARM_BILLING_by_date_and_time($search);
    // if($this->input->server('REQUEST_METHOD') == 'POST'){
      // access_log(array("MENU_ID"=>$this->menu_id,"USER_ID"=>$this->data['current_user']->USER_ID,"TYPE"=>4,"IP_ADDRESS"=>$_SERVER["REMOTE_ADDR"],"LAST_ACCESS"=>date("Y-m-d H:i:s")));
      // $this->data['header_title']="Billing - Warm Billing - Date : ".$date." By Time";
      // $this->data['header_parameter']="";
      // $filename="billing-warm_billing-by_time.xlsx";
      // $html = $this->load->view($this->path."detail_excel",$this->data,true);
      // $html=str_replace('&nbsp;','',$html);
      // $html=str_replace('\r\n','',$html);
      // $html=str_replace('&','&amp;',$html);
      // $tmpfile = time().'.html';
      // file_put_contents($tmpfile, $html);
      // $this->load->library('PHPExcel');
      // $reader = new PHPExcel_Reader_HTML; 
      // $content = $reader->load($tmpfile); 
      // $objWriter = PHPExcel_IOFactory::createWriter($content, 'Excel2007');
      // ob_end_clean();
      // header('Content-Type: application/vnd.ms-excel');
      // header('Content-Disposition: attachment;filename="'.$filename.'"');
      // header('Cache-Control: max-age=0');
      // $objWriter->save('php://output');
      // unlink($tmpfile);
    // }else{
      // $this->data['path']=$this->path;
      // $this->data['VIEW']=$this->load->view($this->path."detail",$this->data,true);
      // $this->load->view("templates/".$this->template, $this->data);
    // }
  // }
}