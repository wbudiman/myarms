<?php

if (!defined('BASEPATH'))exit('No direct script access allowed');

class Katalog_Gimmick extends MY_Controller {
  public $path="billing_management/gimmick/katalog_gimmick/";
  public $menu_id=0;
  public function __construct() 
  {
    parent::__construct();
    $this->menu_id=get_by_url("billing_management/gimmick/katalog_gimmick");
  }
  
  function index()
  {
    $parameters=array(
      "month_1" => date("m"),
      "year_1" => date("Y"),
      "month_2" => date("m"),
      "year_2" => date("Y"),
      "witel" => "''",
      "datel" => "''",
      "ubis" => "''",
      "summary_product" => "''",
      "product" => "''",
      "ubis_segment" => "''"
    ); 
    $this->data['search']=array(
      "month_1" => date("m"),
      "year_1" => date("Y"),
      "month_2" => date("m"),
      "year_2" => date("Y"),
      "division" => "",
      "witel" => "",
      "datel" => "",
      "ubis" => "",
      "summary_product" => "",
      "product" => "",
      "ubis_segment" => ""
    );
    if($this->input->server('REQUEST_METHOD') == 'POST'){
      $data=$this->input->post("search");
      $data['division']=isset($data['division']) ? $data['division'] : "";
      $data['witel']=isset($data['witel']) ? $data['witel'] : "";
      $data['datel']=isset($data['datel']) ? $data['datel'] : "";
      $data['ubis']=isset($data['ubis']) ? $data['ubis'] : "";
      $data['ubis_segment']=isset($data['ubis_segment']) ? $data['ubis_segment'] : "";
      $data['summary_product']=isset($data['summary_product']) ? $data['summary_product'] : "";
      $data['product']=isset($data['product']) ? $data['product'] : "";
      $this->data['search']=$data;
      $parameters=array(
        "month_1" => $data['month_1'],
        "year_1" => $data['year_1'],
        "month_2" => $data['month_2'],
        "year_2" => $data['year_2'],
        "division" => $this->implode_array($data['division']),
        "witel" => $this->implode_array($data['witel']),
        "datel" => $this->implode_array($data['datel']),
        "ubis" => $this->implode_array($data['ubis']),
        "summary_product" => $this->implode_array($data['summary_product']),
        "product" => $this->implode_array($data['product']),
        "ubis_segment" => $this->implode_array($data['ubis_segment'])
      );
      $this->data['division_lists']=$this->division->find_all("DIVISION_CODE","ASC");
      if($data['division']==""){
        $divisions=$this->implode_array($this->data['division_lists'],"DIVISION_CODE");
      }else{
        $divisions=$this->implode_array($data['division']);
      }
      $this->data['witel_lists']=$this->witel->lists($divisions);
      if($data['witel']==""){
        $witels=$this->implode_array($this->data['witel_lists'],"WITEL_CODE");
      }else{
        $witels=$this->implode_array($data['witel']);
      }
      $this->data['datel_lists']=$this->datel->lists($divisions,$witels);
      $this->data['ubis_lists']=$this->ubis->find_all("UBIS","ASC");
      $this->data['summary_product_lists']=$this->product->lists("'Wireline','Speedy','Wholesale Non Trafik'");
      $summary_products=$this->implode_array($this->data['summary_product_lists'],"SUMMARY_PRODUCT");
      $this->data['product_lists']=$this->product->get_product_by_summary_product($summary_products);
      $this->data['ubis_segment_lists']=$this->ubis_segment->get_by_ubis_id("1");
      $this->data['lists']=$this->report_gimmick->GIMMICK_MGT_KATALOG_GIMMICK($parameters);
	  $this->data['recycle_lists']=$this->report_gimmick->GIMMICK_MGT_KATALOG_GIMMICK_recycle($parameters);
	  $data_get="division=".urlencode($this->implode_array($data['division']))."&product=".urlencode($this->implode_array($data['product']))."&ubis_segment=".urlencode($this->implode_array($data['ubis_segment']))."&month_1=".$this->data['search']['month_1']."&year_1=".$this->data['search']['year_1']."&month_2=".$this->data['search']['month_2']."&year_2=".$this->data['search']['year_2'];
      $this->data['param_get']=$data_get;
      if($data['button']=="Download"){
        access_log(array("MENU_ID"=>$this->menu_id,"USER_ID"=>$this->data['current_user']->USER_ID,"TYPE"=>4,"IP_ADDRESS"=>$_SERVER["REMOTE_ADDR"],"LAST_ACCESS"=>date("Y-m-d H:i:s")));
        $this->data['header_title']="Billing - Gimmick Mangement - Katalog Gimmick";
        $this->data['header_parameter']=($this->data['month_lists'][$data['month_1']]." ".$data['year_1']);
        $filename="billing-gimmick_mgt-katalog_gimmick.xlsx";
        $html = $this->load->view($this->path."excel",$this->data,true);
        $html=str_replace('&','&amp;',$html);
        $tmpfile = time().'.html';
        file_put_contents($tmpfile, $html);
        
        $this->load->library('PHPExcel');
        $reader = new PHPExcel_Reader_HTML; 
        $content = $reader->load($tmpfile); 
        $objWriter = PHPExcel_IOFactory::createWriter($content, 'Excel2007');
        ob_end_clean();
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
        unlink($tmpfile);
      }else{
        access_log(array("MENU_ID"=>$this->menu_id,"USER_ID"=>$this->data['current_user']->USER_ID,"TYPE"=>0,"IP_ADDRESS"=>$_SERVER["REMOTE_ADDR"],"LAST_ACCESS"=>date("Y-m-d H:i:s")));
        $this->data['path']=$this->path;
        $this->data['VIEW']=$this->load->view($this->path."index",$this->data,true);
        $this->load->view("templates/".$this->template, $this->data);
      }
    }else{
	  $data_get="division=".urlencode($this->implode_array(''))."&sto=".urlencode($this->implode_array(''))."&billing_component=".urlencode($this->implode_array(''))."&month_1=".$this->data['search']['month_1']."&year_1=".$this->data['search']['year_1']."&month_2=".$this->data['search']['month_2']."&year_2=".$this->data['search']['year_2'];
      $this->data['param_get']=$data_get;
      $this->data['division_lists']=$this->division->find_all("DIVISION_CODE","ASC");
      $divisions=$this->implode_array($this->data['division_lists'],"DIVISION_CODE");
      $this->data['witel_lists']=$this->witel->lists($divisions);
      $witels=$this->implode_array($this->data['witel_lists'],"WITEL_CODE");
      $this->data['datel_lists']=$this->datel->lists($divisions,$witels);
      $this->data['ubis_lists']=$this->ubis->find_all("UBIS","ASC");
      $this->data['summary_product_lists']=$this->product->lists("'Wireline','Speedy'");
      $summary_products=$this->implode_array($this->data['summary_product_lists'],"SUMMARY_PRODUCT");
      $this->data['product_lists']=$this->product->get_product_by_summary_product($summary_products);
      $this->data['ubis_segment_lists']=$this->ubis_segment->get_by_ubis_id("1");
      $this->data['lists']=$this->report_gimmick->GIMMICK_MGT_KATALOG_GIMMICK($parameters);
      $this->data['path']=$this->path;
      $this->data['VIEW']=$this->load->view($this->path."index",$this->data,true);
      $this->load->view("templates/".$this->template, $this->data);
    }
  }
/*  function index()
  {
	$this->data['summary_product_lists']=$this->product->lists("'Wireline','Speedy'");
	$summary_products=$this->implode_array($this->data['summary_product_lists'],"SUMMARY_PRODUCT");
	$this->data['product_lists']=$this->product->get_product_by_summary_product($summary_products);
	$this->data['segment_lists']=$this->ubis_segment->find_all("UBIS_SEGMENT","ASC");
	$this->data['ubis_lists']=$this->ubis->find_all("UBIS","ASC");
    $parameters=array(
      "ubis" => "''",
      "sub_ubis" => "''",
      "product" => "''",
	  "ubis_segment" => "''",	  
      "month_1" => date("m",strtotime("-2month")),
      "year_1" => date("Y",strtotime("-2month")),
      "month_2" => date("m",strtotime("-1month")),
      "year_2" => date("Y",strtotime("-1month"))
    );
    $this->data['search']=array(
      "ubis" => "''",
      "sub_ubis" => "''",
      "product" => "''",
	  "ubis_segment" => "''",	  
      "month_1" => date("m",strtotime("-2month")),
      "year_1" => date("Y",strtotime("-2month")),
      "month_2" => date("m",strtotime("-1month")),
      "year_2" => date("Y",strtotime("-1month"))
    );
    if($this->input->server('REQUEST_METHOD') == 'POST'){
      $data=$this->input->post("search");
      $this->data['search']=$data;
      $parameters=array(
        "ubis" => $this->implode_array($data['ubis']),
        "sub_ubis" => $this->implode_array($data['sub_ubis']),
        "product" => $this->implode_array($data['product']),
		"ubis_segment" => $this->implode_array($data['ubis_segment']),
        "month_1" => $data['month_1'],
        "year_1" => $data['year_1'],
        "month_2" => $data['month_2'],
        "year_2" => $data['year_2']
      );
	  
      $this->data['lists']=$this->report->WARM_BILLING_by_date($parameters);
      $data_get="ubis=".urlencode($this->implode_array($data['ubis']))."&sub_ubis=".urlencode($this->implode_array($data['sub_ubis']))."&product=".urlencode($this->implode_array($data['product']))."&segment=".urlencode($this->implode_array($data['segment']))."&month_1=".$this->data['search']['month_1']."&year_1=".$this->data['search']['year_1']."&month_2=".$this->data['search']['month_2']."&year_2=".$this->data['search']['year_2'];
      $this->data['param_get']=$data_get;
      $this->data['recycle_lists']=$this->report->WARM_BILLING_by_date_recycle($parameters);
	  
      if($data['button']=="Download"){
        access_log(array("MENU_ID"=>$this->menu_id,"USER_ID"=>$this->data['current_user']->USER_ID,"TYPE"=>4,"IP_ADDRESS"=>$_SERVER["REMOTE_ADDR"],"LAST_ACCESS"=>date("Y-m-d H:i:s")));
        $this->data['header_title']="Billing - Warm Billing - By Date";
        $this->data['header_parameter']="";
        $filename="billing-warm_billing-by_date.xlsx";
        $html = $this->load->view($this->path."excel",$this->data,true);
        $html=str_replace('&','&amp;',$html);
        $tmpfile = time().'.html';
        file_put_contents($tmpfile, $html);
        $this->load->library('PHPExcel');
        $reader = new PHPExcel_Reader_HTML; 
        $content = $reader->load($tmpfile); 
        $objWriter = PHPExcel_IOFactory::createWriter($content, 'Excel2007');
        ob_end_clean();
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
        unlink($tmpfile);
      }else{
        access_log(array("MENU_ID"=>$this->menu_id,"USER_ID"=>$this->data['current_user']->USER_ID,"TYPE"=>0,"IP_ADDRESS"=>$_SERVER["REMOTE_ADDR"],"LAST_ACCESS"=>date("Y-m-d H:i:s")));
        $this->data['path']=$this->path;
        $this->data['VIEW']=$this->load->view($this->path."index",$this->data,true);
        $this->load->view("templates/".$this->template, $this->data);
      } 
    }else{
	
      $data_get="ubis=".urlencode($this->implode_array(''))."&sub_ubis=".urlencode($this->implode_array(''))."&product=".urlencode($this->implode_array(''))."&segment=".urlencode($this->implode_array(''))."&month_1=".$this->data['search']['month_1']."&year_1=".$this->data['search']['year_1']."&month_2=".$this->data['search']['month_2']."&year_2=".$this->data['search']['year_2'];
      $this->data['param_get']=$data_get;
      $this->data['lists']=$this->report_gimmick->WARM_BILLING_by_date($parameters);
      $this->data['recycle_lists']=$this->report_gimmick->WARM_BILLING_by_date_recycle($parameters);
      $this->data['path']=$this->path;
      $this->data['VIEW']=$this->load->view($this->path."index",$this->data,true);
	  $this->data['segment_lists']=$this->ubis_segment->find_all("UBIS_SEGMENT","ASC"); // add by gia
      $this->load->view("templates/".$this->template, $this->data);
	  
    }
  }
  
  */
  function detail($date="")
  {
    $search=$this->input->get();
    $this->data['param_get']=http_build_query($search, '', '&amp;');
    $search['date']=$date;
    $this->data['search']=$search;
    $this->data['lists']=$this->report->WARM_BILLING_by_date_and_time($search);
    if($this->input->server('REQUEST_METHOD') == 'POST'){
      access_log(array("MENU_ID"=>$this->menu_id,"USER_ID"=>$this->data['current_user']->USER_ID,"TYPE"=>4,"IP_ADDRESS"=>$_SERVER["REMOTE_ADDR"],"LAST_ACCESS"=>date("Y-m-d H:i:s")));
      $this->data['header_title']="Billing - Warm Billing - Date : ".$date." By Time";
      $this->data['header_parameter']="";
      $filename="billing-warm_billing-by_time.xlsx";
      $html = $this->load->view($this->path."detail_excel",$this->data,true);
      $html=str_replace('&nbsp;','',$html);
      $html=str_replace('\r\n','',$html);
      $html=str_replace('&','&amp;',$html);
      $tmpfile = time().'.html';
      file_put_contents($tmpfile, $html);
      $this->load->library('PHPExcel');
      $reader = new PHPExcel_Reader_HTML; 
      $content = $reader->load($tmpfile); 
      $objWriter = PHPExcel_IOFactory::createWriter($content, 'Excel2007');
      ob_end_clean();
      header('Content-Type: application/vnd.ms-excel');
      header('Content-Disposition: attachment;filename="'.$filename.'"');
      header('Cache-Control: max-age=0');
      $objWriter->save('php://output');
      unlink($tmpfile);
    }else{
      $this->data['path']=$this->path;
      $this->data['VIEW']=$this->load->view($this->path."detail",$this->data,true);
      $this->load->view("templates/".$this->template, $this->data);
    }
  }
}