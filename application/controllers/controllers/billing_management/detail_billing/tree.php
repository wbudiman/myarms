<?php

if (!defined('BASEPATH'))exit('No direct script access allowed');

class Tree extends MY_Controller {
  public $path="billing_management/detail_billing/tree/";
  public $menu_id=0;
  public function __construct() 
  {
    parent::__construct();
    $this->menu_id=get_by_url("billing_management/detail_billing/tree");
  }
  function index()
  {
    $perpage=100;
    $row_lists=array();
    $field=array();
    foreach($this->row_column->find_all_by_ROW_COLUMN_ID_and_ROW_COLUMN_ID("<=11","!=7","ROW_COLUMN_DESCRIPTION","ASC") as $r)
    {
      $row_lists[$r->ROW_COLUMN_FIELD]=$r->ROW_COLUMN_DESCRIPTION;
      $field[$r->ROW_COLUMN_FIELD]=$r->ROW_COLUMN_FIELD_USED;
      if($r->ROW_COLUMN_FIELD_USED==NULL)$field[$r->ROW_COLUMN_FIELD]=$this->data['billing_component_lists'];
    }
    $this->data['row_lists']=$row_lists;
    $this->data['field']=$field;
    $parameters=array(
      "division" => "''",
      "witel" => "''",
      "datel" => "''",
      "month" => date("m"),
      "year" => date("Y"),
      "show_data" => "di.DIVISION_CODE,di.DIVISION_NAME",
      "ubis" => "''",
      "summary_product" => "''",
      "product" => "''",
      "ubis_segment" => "''",
      "top_customer" => "''",
      "indihome" => "",
      "usage" => "",
      "view" => "summary",
      "packet_speedy" => "",
      "limit" => $perpage
    );
    $this->data['search']=array(
      "division" => "",
      "witel" => "",
      "datel" => "",
      "month" => date("m"),
      "year" => date("Y"),
      "show_data" => "di.DIVISION_CODE,di.DIVISION_NAME",
      "ubis" => "",
      "summary_product" => "",
      "product" => "",
      "ubis_segment" => "",
      "top_customer" => "",
      "indihome" => "",
      "usage" => "",
      "view" => "summary",
      "packet_speedy" => ""
    );
    if($this->input->server('REQUEST_METHOD') == 'POST'){
      access_log(array("MENU_ID"=>$this->menu_id,"USER_ID"=>$this->data['current_user']->USER_ID,"TYPE"=>0,"IP_ADDRESS"=>$_SERVER["REMOTE_ADDR"],"LAST_ACCESS"=>date("Y-m-d H:i:s")));
      $data=$this->input->post("search");
      $data['division']=isset($data['division']) ? $data['division'] : "";
      $data['witel']=isset($data['witel']) ? $data['witel'] : "";
      $data['datel']=isset($data['datel']) ? $data['datel'] : "";
      $data['ubis']=isset($data['ubis']) ? $data['ubis'] : "";
      $data['summary_product']=isset($data['summary_product']) ? $data['summary_product'] : "";
      $data['product']=isset($data['product']) ? $data['product'] : "";
      $data['ubis_segment']=isset($data['ubis_segment']) ? $data['ubis_segment'] : "";
      $data['top_customer']=isset($data['top_customer']) ? $data['top_customer'] : "";
      $this->data['search']=$data;
      $parameters=array(
        "division" => $this->implode_array($data['division']),
        "witel" => $this->implode_array($data['witel']),
        "datel" => $this->implode_array($data['datel']),
        "month" => $data['month'],
        "year" => $data['year'],
        "show_data" => $data['show_data'],
        "ubis" => $this->implode_array($data['ubis']),
        "summary_product" => $this->implode_array($data['summary_product']),
        "product" => $this->implode_array($data['product']),
        "ubis_segment" => $this->implode_array($data['ubis_segment']),
        "top_customer" => $this->implode_array($data['top_customer']),
        "indihome" => $data['indihome'],
        "usage" => $data['usage'],
        "view" => $data['view'],
        "packet_speedy" => $data['packet_speedy'],
        "limit" => $perpage
      );
      if($data['button']=="Download to Txt"){
        $parameters['limit']="";
        $detail_lists=$this->report->BILLING_DETAIL_REVENUEWSW_tree_detail($parameters);
        $parameters['status']="b.STATUS";
        $x_detail_lists=$this->report->BILLING_DETAIL_REVENUEWSW_tree_detail($parameters);
        unset($parameters['status']);
        if($parameters['month']-1==0){
          $parameters['month']=12;
          $parameters['year']-=1;
        }else{
          $parameters['month']-=1;
          if($parameters['month']<10)$parameters['month']="0".$parameters['month'];
        }
        $last_1_detail_lists=$this->report->BILLING_DETAIL_REVENUEWSW_tree_detail($parameters);
        $parameters['status']="b.STATUS";
        $last_1x_detail_lists=$this->report->BILLING_DETAIL_REVENUEWSW_tree_detail($parameters);
        
        unset($parameters['status']);
        $lists=$this->report->BILLING_DETAIL_REVENUEWSW_tree($parameters);
        $detail_lists=$this->report->BILLING_DETAIL_REVENUEWSW_tree_detail($parameters);
        $parameters['status']="b.STATUS";
        $x_lists=$this->report->BILLING_DETAIL_REVENUEWSW_tree($parameters);
        $x_detail_lists=$this->report->BILLING_DETAIL_REVENUEWSW_tree_detail($parameters);
        unset($parameters['status']);
        if($parameters['month']-1==0){
          $parameters['month']=12;
          $parameters['year']-=1;
        }else{
          $parameters['month']-=1;
          if($parameters['month']<10)$parameters['month']="0".$parameters['month'];
        }
        $last_1_lists=$this->report->BILLING_DETAIL_REVENUEWSW_tree($parameters);
        $last_1_detail_lists=$this->report->BILLING_DETAIL_REVENUEWSW_tree_detail($parameters);
        $parameters['status']="b.STATUS";
        $last_1x_lists=$this->report->BILLING_DETAIL_REVENUEWSW_tree($parameters);
        $last_1x_detail_lists=$this->report->BILLING_DETAIL_REVENUEWSW_tree_detail($parameters);
        
        $output="CCA|SND|SND Group|Produk|Tagihan Bulan Lalu|Tagihan Sekarang|Bisnis Area|Umur|Usage|Paket Speedy|Paket FBIP".PHP_EOL;
        $search=$data;
        if($data['table_type']=="TOTAL_REVENUE"){
          foreach($detail_lists as $l){
            $output.=$l->CCA."|".$l->SND."|".$l->SND_GROUP."|".$l->PRODUCT_NAME."|".$l->TOTAL_LALU."|".$l->TOTAL."|".$l->BUSINESS_AREA_DESCRIPTION."|".$l->UMUR_PELANGGAN_DESCRIPTION."|".$l->USAGE_STATUS."|".$l->PACKET_SPEEDY_DESCRIPTION."|".$l->PACKET_FBIP_DESCRIPTION.PHP_EOL;
          }
          $filename="Total Revenue.txt";
        }elseif($data['table_type']=="REVENUE_EXISTING"){
          foreach($detail_lists as $l){
            foreach($last_1_detail_lists as $l2){
              if($l->{$field[$search['show_data']]}==$l2->{$field[$search['show_data']]})
              {
                $output.=$l->CCA."|".$l->SND."|".$l->SND_GROUP."|".$l->PRODUCT_NAME."|".$l->TOTAL_LALU."|".$l->TOTAL."|".$l->BUSINESS_AREA_DESCRIPTION."|".$l->UMUR_PELANGGAN_DESCRIPTION."|".$l->USAGE_STATUS."|".$l->PACKET_SPEEDY_DESCRIPTION."|".$l->PACKET_FBIP_DESCRIPTION.PHP_EOL;
              }
            }
          }
          $filename="Revenue Existing.txt";
        }elseif($data['table_type']=="REVENUE_EXISTING_TAGIHAN_NAIK"){
          foreach($detail_lists as $l){
            foreach($last_1_detail_lists as $l2){
              if($l->{$field[$search['show_data']]}==$l2->{$field[$search['show_data']]} && $l->TAGIHAN>$l2->TAGIHAN)
              {
                $output.=$l->CCA."|".$l->SND."|".$l->SND_GROUP."|".$l->PRODUCT_NAME."|".$l->TOTAL_LALU."|".$l->TOTAL."|".$l->BUSINESS_AREA_DESCRIPTION."|".$l->UMUR_PELANGGAN_DESCRIPTION."|".$l->USAGE_STATUS."|".$l->PACKET_SPEEDY_DESCRIPTION."|".$l->PACKET_FBIP_DESCRIPTION.PHP_EOL;
              }
            }
          }
          $filename="Revenue Existing Tagihan Naik.txt";
        }elseif(substr($data['table_type'],0,30)=="REVENUE_EXISTING_TAGIHAN_NAIK_"){
          $target=substr($data['table_type'],30,strlen($data['table_type']));
          $status_b=array("status"=>array(),"status_name"=>array());
          foreach($lists as $l){
            foreach($last_1x_lists as $l2){
              if($l->{$field[$search['show_data']]}==$l2->{$field[$search['show_data']]} && $l->TAGIHAN>$l2->TAGIHAN)
              {
                if(!in_array($l2->STATUS,$status_b['status'])){
                  array_push($status_b['status'],$l2->STATUS);
                  array_push($status_b['status_name'],$l2->TREE_STATUS_NAME);
                }
              }
            }
          }
          foreach($status_b['status'] as $key=>$s){
            if($target==($key+1)){
              foreach($detail_lists as $l){
                foreach($last_1x_detail_lists as $l2){
                  if($l->{$field[$search['show_data']]}==$l2->{$field[$search['show_data']]} && $l->TAGIHAN>$l2->TAGIHAN && $l2->STATUS==$s)
                  {
                    $output.=$l->CCA."|".$l->SND."|".$l->SND_GROUP."|".$l->PRODUCT_NAME."|".$l->TOTAL_LALU."|".$l->TOTAL."|".$l->BUSINESS_AREA_DESCRIPTION."|".$l->UMUR_PELANGGAN_DESCRIPTION."|".$l->USAGE_STATUS."|".$l->PACKET_SPEEDY_DESCRIPTION."|".$l->PACKET_FBIP_DESCRIPTION.PHP_EOL;
                  }
                }
              }
            }
          }
          $filename="Revenue Existing Tagihan Naik - ".$status_b['status_name'][$target-1].".txt";
        }elseif($data['table_type']=="REVENUE_EXISTING_TAGIHAN_TETAP"){
          foreach($detail_lists as $l){
            foreach($last_1_detail_lists as $l2){
              if($l->{$field[$search['show_data']]}==$l2->{$field[$search['show_data']]} && $l->TAGIHAN==$l2->TAGIHAN)
              {
                $output.=$l->CCA."|".$l->SND."|".$l->SND_GROUP."|".$l->PRODUCT_NAME."|".$l->TOTAL_LALU."|".$l->TOTAL."|".$l->BUSINESS_AREA_DESCRIPTION."|".$l->UMUR_PELANGGAN_DESCRIPTION."|".$l->USAGE_STATUS."|".$l->PACKET_SPEEDY_DESCRIPTION."|".$l->PACKET_FBIP_DESCRIPTION.PHP_EOL;
              }
            }
          }
          $filename="Revenue Exisintg Tagihan Tetap.txt";
        }elseif(substr($data['table_type'],0,31)=="REVENUE_EXISTING_TAGIHAN_TETAP_"){
          $target=substr($data['table_type'],31,strlen($data['table_type']));
          $status_b=array("status"=>array(),"status_name"=>array());
          foreach($lists as $l){
            foreach($last_1x_lists as $l2){
              if($l->{$field[$search['show_data']]}==$l2->{$field[$search['show_data']]} && $l->TAGIHAN==$l2->TAGIHAN)
              {
                if(!in_array($l2->STATUS,$status_b['status'])){
                  array_push($status_b['status'],$l2->STATUS);
                  array_push($status_b['status_name'],$l2->TREE_STATUS_NAME);
                }
              }
            }
          }
          foreach($status_b['status'] as $key=>$s){
            if($target==($key+1)){
              foreach($detail_lists as $l){
                foreach($last_1x_detail_lists as $l2){
                  if($l->{$field[$search['show_data']]}==$l2->{$field[$search['show_data']]} && $l->TAGIHAN==$l2->TAGIHAN && $l2->STATUS==$s)
                  {
                    $output.=$l->CCA."|".$l->SND."|".$l->SND_GROUP."|".$l->PRODUCT_NAME."|".$l->TOTAL_LALU."|".$l->TOTAL."|".$l->BUSINESS_AREA_DESCRIPTION."|".$l->UMUR_PELANGGAN_DESCRIPTION."|".$l->USAGE_STATUS."|".$l->PACKET_SPEEDY_DESCRIPTION."|".$l->PACKET_FBIP_DESCRIPTION.PHP_EOL;
                  }
                }
              }
            }
          }
          $filename="Revenue Existing Tagihan Tetap - ".$status_b['status_name'][$target-1];
        }elseif($data['table_type']=="REVENUE_EXISTING_TAGIHAN_TURUN"){
          foreach($detail_lists as $l){
            foreach($last_1_detail_lists as $l2){
              if($l->{$field[$search['show_data']]}==$l2->{$field[$search['show_data']]} && $l->TAGIHAN<$l2->TAGIHAN)
              {
                $output.=$l->CCA."|".$l->SND."|".$l->SND_GROUP."|".$l->PRODUCT_NAME."|".$l->TOTAL_LALU."|".$l->TOTAL."|".$l->BUSINESS_AREA_DESCRIPTION."|".$l->UMUR_PELANGGAN_DESCRIPTION."|".$l->USAGE_STATUS."|".$l->PACKET_SPEEDY_DESCRIPTION."|".$l->PACKET_FBIP_DESCRIPTION.PHP_EOL;
              }
            }
          }
          $filename="Revenue Existing Tagihan Turun.txt";
        }elseif(substr($data['table_type'],0,31)=="REVENUE_EXISTING_TAGIHAN_TURUN_"){
          $target=substr($data['table_type'],31,strlen($data['table_type']));
          $status_b=array("status"=>array(),"status_name"=>array());
          foreach($lists as $l){
            foreach($last_1x_lists as $l2){
              if($l->{$field[$search['show_data']]}==$l2->{$field[$search['show_data']]} && $l->TAGIHAN<$l2->TAGIHAN)
              {
                if(!in_array($l2->STATUS,$status_b['status'])){
                  array_push($status_b['status'],$l2->STATUS);
                  array_push($status_b['status_name'],$l2->TREE_STATUS_NAME);
                }
              }
            }
          }
          foreach($status_b['status'] as $key=>$s){
            if($target==($key+1)){
              foreach($detail_lists as $l){
                foreach($last_1x_detail_lists as $l2){
                  if($l->{$field[$search['show_data']]}==$l2->{$field[$search['show_data']]} && $l->TAGIHAN<$l2->TAGIHAN && $l2->STATUS==$s)
                  {
                    $output.=$l->CCA."|".$l->SND."|".$l->SND_GROUP."|".$l->PRODUCT_NAME."|".$l->TOTAL_LALU."|".$l->TOTAL."|".$l->BUSINESS_AREA_DESCRIPTION."|".$l->UMUR_PELANGGAN_DESCRIPTION."|".$l->USAGE_STATUS."|".$l->PACKET_SPEEDY_DESCRIPTION."|".$l->PACKET_FBIP_DESCRIPTION.PHP_EOL;
                  }
                }
              }
            }
          }
          $filename="Revenue Existing Tagihan Turun - ".$status_b['status_name'][$target-1].".txt";
        }elseif($data['table_type']=="NEW_BILLING"){
          foreach($detail_lists as $l){
            $check=0;
            foreach($last_1_detail_lists as $l2){
              if($l->{$field[$search['show_data']]}==$l2->{$field[$search['show_data']]})
              {
                $check++;
              }
            }
            if($check==0){
              $output.=$l->CCA."|".$l->SND."|".$l->SND_GROUP."|".$l->PRODUCT_NAME."|".$l->TOTAL_LALU."|".$l->TOTAL."|".$l->BUSINESS_AREA_DESCRIPTION."|".$l->UMUR_PELANGGAN_DESCRIPTION."|".$l->USAGE_STATUS."|".$l->PACKET_SPEEDY_DESCRIPTION."|".$l->PACKET_FBIP_DESCRIPTION.PHP_EOL;
            }
          }
          $filename="New Billing.txt";
        }elseif(substr($data['table_type'],0,12)=="NEW_BILLING_"){
          $target=substr($data['table_type'],12,strlen($data['table_type']));
          $data_new_billing2=array();
          $data_new_billing2_detail=array();
          $status_c=array("status"=>array(),"status_name"=>array());
          foreach($x_lists as $l){
            $check=0;
            foreach($last_1_lists as $l2){
              if($l->{$field[$search['show_data']]}==$l2->{$field[$search['show_data']]})
              {
                $check++;
              }
            }
            if($check==0){
              if(!in_array($l->STATUS,$status_c['status'])){
                array_push($status_c['status'],$l->STATUS);
                array_push($status_c['status_name'],$l->TREE_STATUS_NAME);
              } 
              array_push($data_new_billing2,$l);
            }
          }
          foreach($x_detail_lists as $l){
            $check=0;
            foreach($last_1_detail_lists as $l2){
              if($l->{$field[$search['show_data']]}==$l2->{$field[$search['show_data']]})
              {
                $check++;
              }
            }
            if($check==0){
              array_push($data_new_billing2_detail,$l);
            }
          }
          foreach($status_c['status'] as $key=>$s){
            if($target==($key+1)){
              foreach($data_new_billing2_detail as $l){
                if($l->STATUS==$s){
                  $output.=$l->CCA."|".$l->SND."|".$l->SND_GROUP."|".$l->PRODUCT_NAME."|".$l->TOTAL_LALU."|".$l->TOTAL."|".$l->BUSINESS_AREA_DESCRIPTION."|".$l->UMUR_PELANGGAN_DESCRIPTION."|".$l->USAGE_STATUS."|".$l->PACKET_SPEEDY_DESCRIPTION."|".$l->PACKET_FBIP_DESCRIPTION.PHP_EOL;
                }
              }
            }
          }
          $filename="New Billing - ".$status_b['status_name'][$target-1].".txt";
        }elseif($data['table_type']=="REVENUE_BULAN_LALU_TIDAK_BULAN_INI"){
          foreach($last_1_detail_lists as $l){
            $check=0;
            foreach($detail_lists as $l2){
              if($l->{$field[$search['show_data']]}==$l2->{$field[$search['show_data']]})
              {
                $check++;
              }
            }
            if($check==0){
              $output.=$l->CCA."|".$l->SND."|".$l->SND_GROUP."|".$l->PRODUCT_NAME."|".$l->TOTAL_LALU."|".$l->TOTAL."|".$l->BUSINESS_AREA_DESCRIPTION."|".$l->UMUR_PELANGGAN_DESCRIPTION."|".$l->USAGE_STATUS."|".$l->PACKET_SPEEDY_DESCRIPTION."|".$l->PACKET_FBIP_DESCRIPTION.PHP_EOL;
            }
          }
          $filename="Revenue Bulan Lalu Tidak Bulan Ini.txt";
        }elseif(substr($data['table_type'],0,35)=="REVENUE_BULAN_LALU_TIDAK_BULAN_INI_"){
          $target=substr($data['table_type'],35,strlen($data['table_type']));
          $status_x=array("status"=>array(),"status_name"=>array());
          $data_x=array();
          $data_x_detail=array();
          foreach($last_1x_lists as $l){
            $check=0;
            foreach($lists as $l2){
              if($l->{$field[$search['show_data']]}==$l2->{$field[$search['show_data']]})
              {
                $check++;
              }
            }
            if($check==0){
              if(!in_array($l->STATUS,$status_x['status'])){
                array_push($status_x['status'],$l->STATUS);
                array_push($status_x['status_name'],$l->TREE_STATUS_NAME);
              }
              array_push($data_x,$l);
            }
          }
          foreach($last_1x_detail_lists as $l){
            $check=0;
            foreach($detail_lists as $l2){
              if($l->{$field[$search['show_data']]}==$l2->{$field[$search['show_data']]})
              {
                $check++;
              }
            }
            if($check==0){
              array_push($data_x_detail,$l);
            }
          }
          foreach($status_x['status'] as $key=>$s){
            if($target==($key+1)){
              foreach($data_x_detail as $d){
                if($d->STATUS==$s){
                  $output.=$l->CCA."|".$l->SND."|".$l->SND_GROUP."|".$l->PRODUCT_NAME."|".$l->TOTAL_LALU."|".$l->TOTAL."|".$l->BUSINESS_AREA_DESCRIPTION."|".$l->UMUR_PELANGGAN_DESCRIPTION."|".$l->USAGE_STATUS."|".$l->PACKET_SPEEDY_DESCRIPTION."|".$l->PACKET_FBIP_DESCRIPTION.PHP_EOL;
                }
              }
            }
          }
          $filename="Revenue Bulan Lalu Tidak Bulan Ini - ".$status_x['status_name'][$target-1].".txt";
        }
        header('Content-type: text/plain');
        header('Content-Disposition: attachment; filename="'.$filename.'"');
        echo $output;
      }else{
        $this->data['division_lists']=$this->division->find_all("DIVISION_CODE","ASC");
        if($data['division']==""){
          $divisions=$this->implode_array($this->data['division_lists'],"DIVISION_CODE");
        }else{
          $divisions=$this->implode_array($data['division']);
        }
        $this->data['witel_lists']=$this->witel->lists($divisions);
        if($data['witel']==""){
          $witels=$this->implode_array($this->data['witel_lists'],"WITEL_CODE");
        }else{
          $witels=$this->implode_array($data['witel']);
        }
        $this->data['datel_lists']=$this->datel->lists($divisions,$witels);
        $this->data['ubis_lists']=$this->ubis->find_all("UBIS","ASC");
        $this->data['summary_product_lists']=$this->product->lists("'Wireline','Wireless','Speedy'");
        $summary_products=$this->implode_array($this->data['summary_product_lists'],"SUMMARY_PRODUCT");
        $this->data['product_lists']=$this->product->get_product_by_summary_product($summary_products);
        if($data['ubis']==""){
          $ubis=$this->implode_array($this->data['ubis_lists'],"UBIS_ID");
        }else{
          $ubis=$this->implode_array($data['ubis']);
        }
        $this->data['ubis_segment_lists']=$this->ubis_segment->get_by_ubis_id($ubis);
        $this->data['top_customer_lists']=$this->portofolio->find_all("PORTOFOLIO_NAME","ASC");
        $this->data['indihome_lists']=$this->indihome->find_all("INDIHOME_DESCRIPTION","ASC");
        $this->data['packet_speedy_lists']=$this->packet_speedy->find_all("PACKET_SPEEDY_ID","ASC");
        $this->data['usage_status_lists']=$this->usage_status->find_all("USAGE_STATUS","ASC");
        $this->data['lists']=$this->report->BILLING_DETAIL_REVENUEWSW_tree($parameters);
        $parameters['limit']=40;
        $this->data['detail_lists']=$this->report->BILLING_DETAIL_REVENUEWSW_tree_detail($parameters);
        $parameters['status']="b.STATUS";
        $this->data['x_lists']=$this->report->BILLING_DETAIL_REVENUEWSW_tree($parameters);
        $this->data['x_detail_lists']=$this->report->BILLING_DETAIL_REVENUEWSW_tree_detail($parameters);
        unset($parameters['status']);
        if($parameters['month']-1==0){
          $parameters['month']=12;
          $parameters['year']-=1;
        }else{
          $parameters['month']-=1;
          if($parameters['month']<10)$parameters['month']="0".$parameters['month'];
        }
        $this->data['last_1_lists']=$this->report->BILLING_DETAIL_REVENUEWSW_tree($parameters);
        $this->data['last_1_detail_lists']=$this->report->BILLING_DETAIL_REVENUEWSW_tree_detail($parameters);
        $parameters['status']="b.STATUS";
        $this->data['last_1x_lists']=$this->report->BILLING_DETAIL_REVENUEWSW_tree($parameters);
        $this->data['last_1x_detail_lists']=$this->report->BILLING_DETAIL_REVENUEWSW_tree_detail($parameters);
        $this->data['path']=$this->path;
        $this->data['VIEW']=$this->load->view($this->path."index",$this->data,true);
        $this->load->view("templates/".$this->template, $this->data);
      }
    }else{
      $this->data['division_lists']=$this->division->find_all("DIVISION_CODE","ASC");
      $divisions=$this->implode_array($this->data['division_lists'],"DIVISION_CODE");
      $this->data['witel_lists']=$this->witel->lists($divisions);
      $witels=$this->implode_array($this->data['witel_lists'],"WITEL_CODE");
      $this->data['datel_lists']=$this->datel->lists($divisions,$witels);
      $this->data['ubis_lists']=$this->ubis->find_all("UBIS","ASC");
      $this->data['summary_product_lists']=$this->product->lists("'Wireline','Wireless','Speedy'");
      $summary_products=$this->implode_array($this->data['summary_product_lists'],"SUMMARY_PRODUCT");
      $this->data['product_lists']=$this->product->get_product_by_summary_product($summary_products);
      $this->data['ubis_segment_lists']=$this->ubis_segment->find_all("UBIS_SEGMENT","ASC");
      $this->data['top_customer_lists']=$this->portofolio->find_all("PORTOFOLIO_NAME","ASC");
      $this->data['indihome_lists']=$this->indihome->find_all("INDIHOME_DESCRIPTION","ASC");
      $this->data['packet_speedy_lists']=$this->packet_speedy->find_all("PACKET_SPEEDY_ID","ASC");
      $this->data['usage_status_lists']=$this->usage_status->find_all("USAGE_STATUS","ASC");
      $this->data['lists']=$this->report->BILLING_DETAIL_REVENUEWSW_tree($parameters);
      $this->data['detail_lists']=$this->report->BILLING_DETAIL_REVENUEWSW_tree_detail($parameters);
      $parameters['status']="b.STATUS";
      $this->data['x_lists']=$this->report->BILLING_DETAIL_REVENUEWSW_tree($parameters);
      $this->data['x_detail_lists']=$this->report->BILLING_DETAIL_REVENUEWSW_tree_detail($parameters);
      unset($parameters['status']);
      if($parameters['month']-1==0){
        $parameters['month']=12;
        $parameters['year']-=1;
      }else{
        $parameters['month']-=1;
        if($parameters['month']<10)$parameters['month']="0".$parameters['month'];
      }
      $this->data['last_1_lists']=$this->report->BILLING_DETAIL_REVENUEWSW_tree($parameters);
      $this->data['last_1_detail_lists']=$this->report->BILLING_DETAIL_REVENUEWSW_tree_detail($parameters);
      $parameters['status']="b.STATUS";
      $this->data['last_1x_lists']=$this->report->BILLING_DETAIL_REVENUEWSW_tree($parameters);
      $this->data['last_1x_detail_lists']=$this->report->BILLING_DETAIL_REVENUEWSW_tree_detail($parameters);
      $this->data['path']=$this->path;
      $this->data['VIEW']=$this->load->view($this->path."index",$this->data,true);
      $this->load->view("templates/".$this->template, $this->data);
    }
  }
}