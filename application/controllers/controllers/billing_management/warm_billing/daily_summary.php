<?php

if (!defined('BASEPATH'))exit('No direct script access allowed');

class Daily_Summary extends MY_Controller {
  public $path="billing_management/warm_billing/daily_summary/";
  public $menu_id=0;
  public function __construct() 
  {
    parent::__construct();
    $this->menu_id=get_by_url("billing_management/warm_billing/daily_summary");
  }
  function index()
  {
    $this->data['billing_component_lists']=$this->billing_component->list_warm_billing_by_date();
    $this->data['sto_lists']=$this->sto->warm_billing_by_date();
    $this->data['division_lists']=$this->division->find_all("DIVISION_CODE","ASC");
    $this->data['view_by_lists']=$this->view_by->list_warm_billing();
    $row_lists=array();
    $field=array();
    $rows=array(
      "wb.STO"=>array(
        "ROW_COLUMN_DESCRIPTION"=>"STO",
        "ROW_COLUMN_FIELD_USED"=>"STO"
      ),
      "wb.KOMPONEN_BILLING"=>array(
        "ROW_COLUMN_DESCRIPTION"=>"Komponen Billing",
        "ROW_COLUMN_FIELD_USED"=>"KOMPONEN_BILLING"
      )
    );
    foreach($rows as $key=>$r)
    {
      $row_lists[$key]=$r['ROW_COLUMN_DESCRIPTION'];
      $field[$key]=$r['ROW_COLUMN_FIELD_USED'];
    }
    $this->data['row_lists']=$row_lists;
    $this->data['field']=$field;
    $parameters=array(
      "division" => "''",
      "sto" => "''",
      "billing_component" => "''",
      "month" =>date("m"),
      "year" => date("Y"),
      "group_by" => "wb.STO",
      "view_by" => "TOTAL_RECORD"
    );
    $this->data['search']=array(
      "division" => "",
      "sto" => "",
      "billing_component" => "",
      "month" =>date("m"),
      "year" => date("Y"),
      "group_by" => "wb.STO",
      "view_by" => "TOTAL_RECORD"
    );
    if($this->input->server('REQUEST_METHOD') == 'POST'){
      $data=$this->input->post("search");
      $data['division']=isset($data['division']) ? $data['division'] : "";
      $data['sto']=isset($data['sto']) ? $data['sto'] : "";
      $data['billing_component']=isset($data['billing_component']) ? $data['billing_component'] : "";
      $this->data['search']=$data;
      $parameters=array(
        "division" => $this->implode_array($data['division']),
        "sto" => $this->implode_array($data['sto']),
        "billing_component" => $this->implode_array($data['billing_component']),
        "month" => $data['month'],
        "year" => $data['year'],
        "group_by" => $data['group_by'],
      );
      $lists=$this->report->WARM_BILLING_daily_summary($parameters);
      $result=array();
      foreach($lists as $l){
        if(!isset($result[$l->{$field[$this->data['search']['group_by']]}]))$result[$l->{$field[$this->data['search']['group_by']]}]=array();
        $result[$l->{$field[$this->data['search']['group_by']]}]['data']=$l;
        if(!isset($result[$l->{$field[$this->data['search']['group_by']]}]['detail']))$result[$l->{$field[$this->data['search']['group_by']]}]['detail']=array();
        array_push($result[$l->{$field[$this->data['search']['group_by']]}]['detail'],$l);
      }
      $this->data['lists']=$result;
      $this->data['recycle_lists']=$this->report->WARM_BILLING_daily_summary_recycle($parameters);
      if($data['button']=="Download"){
        access_log(array("MENU_ID"=>$this->menu_id,"USER_ID"=>$this->data['current_user']->USER_ID,"TYPE"=>4,"IP_ADDRESS"=>$_SERVER["REMOTE_ADDR"],"LAST_ACCESS"=>date("Y-m-d H:i:s")));
        $this->data['header_title']="Billing - Warm Billing - Daily Summary";
        $this->data['header_parameter']=($this->data['month_lists'][$data['month']]." ".$data['year']);
        $filename="billing-warm_billing-daily_summary.xlsx";
        $html = $this->load->view($this->path."excel",$this->data,true);
        $html=str_replace('&','&amp;',$html);
        $tmpfile = time().'.html';
        file_put_contents($tmpfile, $html);
        $this->load->library('PHPExcel');
        $reader = new PHPExcel_Reader_HTML; 
        $content = @$reader->load($tmpfile); 
        $objWriter = PHPExcel_IOFactory::createWriter($content, 'Excel2007');
        ob_end_clean();
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
        unlink($tmpfile);
      }else{
        access_log(array("MENU_ID"=>$this->menu_id,"USER_ID"=>$this->data['current_user']->USER_ID,"TYPE"=>0,"IP_ADDRESS"=>$_SERVER["REMOTE_ADDR"],"LAST_ACCESS"=>date("Y-m-d H:i:s")));
        $this->data['path']=$this->path;
        $this->data['VIEW']=$this->load->view($this->path."index",$this->data,true);
        $this->load->view("templates/".$this->template, $this->data);
      }
    }else{
      $lists=$this->report->WARM_BILLING_daily_summary($parameters);
      $result=array();
      foreach($lists as $l){
        if(!isset($result[$l->{$field[$this->data['search']['group_by']]}]))$result[$l->{$field[$this->data['search']['group_by']]}]=array();
        $result[$l->{$field[$this->data['search']['group_by']]}]['data']=$l;
        if(!isset($result[$l->{$field[$this->data['search']['group_by']]}]['detail']))$result[$l->{$field[$this->data['search']['group_by']]}]['detail']=array();
        array_push($result[$l->{$field[$this->data['search']['group_by']]}]['detail'],$l);
      }
      $this->data['lists']=$result;
      $this->data['recycle_lists']=$this->report->WARM_BILLING_daily_summary_recycle($parameters);
      $this->data['path']=$this->path;
      $this->data['VIEW']=$this->load->view($this->path."index",$this->data,true);
      $this->load->view("templates/".$this->template, $this->data);
    }
  }
}