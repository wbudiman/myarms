<?php

if (!defined('BASEPATH'))exit('No direct script access allowed');

class By_Day extends MY_Controller {
  public $path="billing_management/warm_billing/by_day/";
  public $menu_id=0;
  public function __construct() 
  {
    parent::__construct();
    $this->data['path']=$this->path;
    $this->menu_id=get_by_url("billing_management/warm_billing/by_day");
  }
  function index()
  {
    $this->data['billing_component_lists']=$this->billing_component->list_warm_billing_by_date();
    $this->data['sto_lists']=$this->sto->warm_billing_by_date();
    $this->data['division_lists']=$this->division->find_all("DIVISION_CODE","ASC");
    $this->data['view_by_lists']=$this->view_by->list_warm_billing();
    $this->data['day']=array("Monday"=>1,"Tuesday"=>2,"Wednesday"=>3,"Thursday"=>4,"Friday"=>5,"Saturday"=>6,"Sunday"=>7);
    $this->data['day2']=array("SENIN","SELASA","RABU","KAMIS","JUMAT","SABTU","MINGGU");
    $parameters=array(
      "division" => "''",
      "sto" => "''",
      "billing_component" => "''",
      "month_1" => date("m",strtotime("-2month")),
      "year_1" => date("Y",strtotime("-2month")),
      "month_2" => date("m",strtotime("-1month")),
      "year_2" => date("Y",strtotime("-1month")),
      "month_3" =>date("m"),
      "year_3" => date("Y"),
      "view_by" => "TOTAL_RECORD"
    );
    $this->data['search']=array(
      "division" => "",
      "sto" => "",
      "billing_component" => "",
      "month_1" => date("m",strtotime("-2month")),
      "year_1" => date("Y",strtotime("-2month")),
      "month_2" => date("m",strtotime("-1month")),
      "year_2" => date("Y",strtotime("-1month")),
      "month_3" =>date("m"),
      "year_3" => date("Y"),
      "view_by" => "TOTAL_RECORD"
    );
    if($this->input->server('REQUEST_METHOD') == 'POST'){
      $data=$this->input->post("search");
      $data['division']=isset($data['division']) ? $data['division'] : "";
      $data['sto']=isset($data['sto']) ? $data['sto'] : "";
      $data['billing_component']=isset($data['billing_component']) ? $data['billing_component'] : "";
      $this->data['search']=$data;
      $parameters=array(
        "division" => $this->implode_array($data['division']),
        "sto" => $this->implode_array($data['sto']),
        "billing_component" => $this->implode_array($data['billing_component']),
        "month_1" => $data['month_1'],
        "year_1" => $data['year_1'],
        "month_2" => $data['month_2'],
        "year_2" => $data['year_2'],
        "month_3" => $data['month_3'],
        "year_3" => $data['year_3'],
      );
      $this->data['lists']=$this->report->WARM_BILLING_by_day($parameters);
      $data_get="division=".urlencode($this->implode_array($data['division']))."&sto=".urlencode($this->implode_array($data['sto']))."&billing_component=".urlencode($this->implode_array($data['billing_component']))."&month_1=".$this->data['search']['month_1']."&year_1=".$this->data['search']['year_1']."&month_2=".$this->data['search']['month_2']."&year_2=".$this->data['search']['year_2']."&month_3=".$this->data['search']['month_3']."&year_3=".$this->data['search']['year_3']."&view_by=".$this->data['search']['view_by'];
      $this->data['param_get']=$data_get;
      $this->data['recycle_lists']=$this->report->WARM_BILLING_by_day_recycle($parameters);
      $periodes=array($this->data['search']['year_1'].$this->data['search']['month_1'],$this->data['search']['year_2'].$this->data['search']['month_2'],$this->data['search']['year_3'].$this->data['search']['month_3']);
      $result=array();
      foreach($periodes as $p){
        $result[$p]=array();
        $end_of_month=date("t",strtotime(substr($p,0,4)."-".substr($p,4,2)."-01"));
        for($x=1;$x<=$end_of_month;$x++){
          $date=($x<10 ? "0".$x : $x);
          $check=0;
          $temp=array();
          foreach($this->data['lists'] as $l){
            if($l->PERIODE==$p){
              if($l->PERIODE.$date==$l->BY_DATE){
                $temp=$l;
                $check=1;
                break;
              }
            }
          }
          if($check==1){
            $result[$p][$x]=array(
              "PERIODE"=>$temp->PERIODE,
              "BY_DATE"=>$temp->BY_DATE,
              "BY_DAY"=>$temp->BY_DAY,
              "TOTAL_RECORD"=>$temp->TOTAL_RECORD,
              "TOTAL_DURATION"=>$temp->TOTAL_DURATION,
              "TOTAL"=>$temp->TOTAL,
            );
          }else{
            $result[$p][$x]=array(
              "PERIODE"=>$p,
              "BY_DATE"=>$p.$date,
              "BY_DAY"=>$this->data['day2'][$this->data['day'][date("l",strtotime(substr($p,0,4)."-".substr($p,4,2)."-".$date))]-1],
              "TOTAL_RECORD"=>0,
              "TOTAL_DURATION"=>0,
              "TOTAL"=>0,
            );
          }
        }
      }
      $this->data['data']=$result;
      if($data['button']=="Download"){
        access_log(array("MENU_ID"=>$this->menu_id,"USER_ID"=>$this->data['current_user']->USER_ID,"TYPE"=>4,"IP_ADDRESS"=>$_SERVER["REMOTE_ADDR"],"LAST_ACCESS"=>date("Y-m-d H:i:s")));
        $this->data['header_title']="Billing - Warm Billing - By Day";
        $this->data['header_parameter']="";
        $filename="billing-warm_billing-by_day.xlsx";
        $html = $this->load->view($this->path."excel",$this->data,true);
        $html=str_replace('&','&amp;',$html);
        $tmpfile = time().'.html';
        file_put_contents($tmpfile, $html);
        $this->load->library('PHPExcel');
        $reader = new PHPExcel_Reader_HTML; 
        $content = @$reader->load($tmpfile); 
        $objWriter = PHPExcel_IOFactory::createWriter($content, 'Excel2007');
        ob_end_clean();
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
        unlink($tmpfile);
      }else{
        access_log(array("MENU_ID"=>$this->menu_id,"USER_ID"=>$this->data['current_user']->USER_ID,"TYPE"=>0,"IP_ADDRESS"=>$_SERVER["REMOTE_ADDR"],"LAST_ACCESS"=>date("Y-m-d H:i:s")));
        $this->data['path']=$this->path;
        $this->data['VIEW']=$this->load->view($this->path."index",$this->data,true);
        $this->load->view("templates/".$this->template, $this->data);
      }
    }else{
      $data_get="division=".urlencode($this->implode_array(''))."&sto=".urlencode($this->implode_array(''))."&billing_component=".urlencode($this->implode_array(''))."&month_1=".$this->data['search']['month_1']."&year_1=".$this->data['search']['year_1']."&month_2=".$this->data['search']['month_2']."&year_2=".$this->data['search']['year_2']."&month_3=".$this->data['search']['month_3']."&year_3=".$this->data['search']['year_3']."&view_by=".$this->data['search']['view_by'];
      $this->data['param_get']=$data_get;
      $this->data['lists']=$this->report->WARM_BILLING_by_day($parameters);
      $this->data['recycle_lists']=$this->report->WARM_BILLING_by_day_recycle($parameters);
      $periodes=array($this->data['search']['year_1'].$this->data['search']['month_1'],$this->data['search']['year_2'].$this->data['search']['month_2'],$this->data['search']['year_3'].$this->data['search']['month_3']);
      $result=array();
      foreach($periodes as $p){
        $result[$p]=array();
        $end_of_month=date("t",strtotime(substr($p,0,4)."-".substr($p,4,2)."-01"));
        for($x=1;$x<=$end_of_month;$x++){
          $date=($x<10 ? "0".$x : $x);
          $check=0;
          $temp=array();
          foreach($this->data['lists'] as $l){
            if($l->PERIODE==$p){
              if($l->PERIODE.$date==$l->BY_DATE){
                $temp=$l;
                $check=1;
                break;
              }
            }
          }
          if($check==1){
            $result[$p][$x]=array(
              "PERIODE"=>$temp->PERIODE,
              "BY_DATE"=>$temp->BY_DATE,
              "BY_DAY"=>$temp->BY_DAY,
              "TOTAL_RECORD"=>$temp->TOTAL_RECORD,
              "TOTAL_DURATION"=>$temp->TOTAL_DURATION,
              "TOTAL"=>$temp->TOTAL,
            );
          }else{
            $result[$p][$x]=array(
              "PERIODE"=>$p,
              "BY_DATE"=>$p.$date,
              "BY_DAY"=>$this->data['day2'][$this->data['day'][date("l",strtotime(substr($p,0,4)."-".substr($p,4,2)."-".$date))]-1],
              "TOTAL_RECORD"=>0,
              "TOTAL_DURATION"=>0,
              "TOTAL"=>0,
            );
          }
        }
      }
      $this->data['data']=$result;
      $this->data['path']=$this->path;
      $this->data['VIEW']=$this->load->view($this->path."index",$this->data,true);
      $this->load->view("templates/".$this->template, $this->data);
    }
  }
  function detail($day="")
  {
    $search=$this->input->get();
    $this->data['param_get']=http_build_query($search, '', '&amp;');
    $search['day']=$day;
    $this->data['search']=$search;
    $this->data['lists']=$this->report->WARM_BILLING_by_date_and_day($search);
    if($this->input->server('REQUEST_METHOD') == 'POST'){
      access_log(array("MENU_ID"=>$this->menu_id,"USER_ID"=>$this->data['current_user']->USER_ID,"TYPE"=>4,"IP_ADDRESS"=>$_SERVER["REMOTE_ADDR"],"LAST_ACCESS"=>date("Y-m-d H:i:s")));
      $this->data['header_title']="Billing - Warm Billing - Hari : ".$day." By Time";
      $this->data['header_parameter']="";
      $filename="billing-warm_billing-day-by_time.xlsx";
      $html = $this->load->view($this->path."detail_excel",$this->data,true);
      $html=str_replace('&','&amp;',$html);
      $tmpfile = time().'.html';
      file_put_contents($tmpfile, $html);
      $this->load->library('PHPExcel');
      $reader = new PHPExcel_Reader_HTML; 
      $content = $reader->load($tmpfile); 
      $objWriter = PHPExcel_IOFactory::createWriter($content, 'Excel2007');
      ob_end_clean();
      header('Content-Type: application/vnd.ms-excel');
      header('Content-Disposition: attachment;filename="'.$filename.'"');
      header('Cache-Control: max-age=0');
      $objWriter->save('php://output');
      unlink($tmpfile);
    }else{
      $this->data['path']=$this->path;
      $content=$this->load->view($this->path."detail",$this->data,true);
      echo json_encode(array(
        "status" => true,
        "content" =>$content
      ));
    }
  }
}