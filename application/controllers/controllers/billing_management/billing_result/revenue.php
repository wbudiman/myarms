<?php

if (!defined('BASEPATH'))exit('No direct script access allowed');

class Revenue extends MY_Controller {
  public $path="billing_management/billing_result/revenue/";
  public $menu_id=0;
  public function __construct() 
  {
    parent::__construct();
    $this->menu_id=get_by_url("billing_management/billing_result/revenue");
    $this->data['billing_component_lists']=$this->billing_component->list_billing_result();
  }
  function index()
  {
    $row_lists=array();
    $field=array();
    foreach($this->row_column->get_row_billing_result_revenue() as $r)
    {
      $row_lists[$r->ROW_COLUMN_FIELD]=$r->ROW_COLUMN_DESCRIPTION;
      $field[$r->ROW_COLUMN_FIELD]=$r->ROW_COLUMN_FIELD_USED;
      if($r->ROW_COLUMN_FIELD_USED==NULL)$field[$r->ROW_COLUMN_FIELD]="KOMPONEN_BILL";
    }
    $this->data['row_lists']=$row_lists;
    $this->data['field']=$field;
    $parameters=array(
      "division" => "''",
      "witel" => "''",
      "datel" => "''",
      "month" => date("m"),
      "year" => date("Y"),
      "row" => "di.DIVISION_CODE,di.DIVISION_NAME",
      "summary_product" => "''",
      "product" => "''",
      "column" => "p.SUMMARY_PRODUCT",
      "customer_category" => "''",
      "billing_type" => "''",
      "component_type" => "''",
      "billing_component" => "",
    );
    $this->data['search']=array(
      "division" => "",
      "witel" => "",
      "datel" => "",
      "month" => date("m"),
      "year" => date("Y"),
      "row" => "di.DIVISION_CODE,di.DIVISION_NAME",
      "summary_product" => "",
      "product" => "",
      "column" => "p.SUMMARY_PRODUCT",
      "customer_category" => "",
      "billing_type" => "",
      "component_type" => "",
      "billing_component" => "",
    );
    if($this->input->server('REQUEST_METHOD') == 'POST'){
      $data=$this->input->post("search");
      $data['division']=isset($data['division']) ? $data['division'] : "";
      $data['witel']=isset($data['witel']) ? $data['witel'] : "";
      $data['datel']=isset($data['datel']) ? $data['datel'] : "";
      $data['summary_product']=isset($data['summary_product']) ? $data['summary_product'] : "";
      $data['product']=isset($data['product']) ? $data['product'] : "";
      $data['customer_category']=isset($data['customer_category']) ? $data['customer_category'] : "";
      $data['billing_type']=isset($data['billing_type']) ? $data['billing_type'] : "";
      $data['component_type']=isset($data['component_type']) ? $data['component_type'] : "";
      $data['customer_category']=isset($data['customer_category']) ? $data['customer_category'] : "";
      $this->data['search']=$data;
      $parameters=array(
        "division" => $this->implode_array($data['division']),
        "witel" => $this->implode_array($data['witel']),
        "datel" => $this->implode_array($data['datel']),
        "month" => $data['month'],
        "year" => $data['year'],
        "row" => $data['row'],
        "summary_product" => $this->implode_array($data['summary_product']),
        "product" => $this->implode_array($data['product']),
        "column" => $data['column'],
        "customer_category" => $this->implode_array($data['customer_category']),
        "billing_type" => $this->implode_array($data['billing_type']),
        "component_type" => $this->implode_array($data['component_type']),
        "billing_component" => $data['billing_component'],
      );
      $this->data['division_lists']=$this->division->find_all("DIVISION_CODE","ASC");
      if($data['division']==""){
        $divisions=$this->implode_array($this->data['division_lists'],"DIVISION_CODE");
      }else{
        $divisions=$this->implode_array($data['division']);
      }
      $this->data['witel_lists']=$this->witel->lists($divisions);
      if($data['witel']==""){
        $witels=$this->implode_array($this->data['witel_lists'],"WITEL_CODE");
      }else{
        $witels=$this->implode_array($data['witel']);
      }
      $this->data['datel_lists']=$this->datel->lists($divisions,$witels);
      $this->data['summary_product_lists']=$this->product->lists("'Wireline','Speedy'");
      $summary_products=$this->implode_array($this->data['summary_product_lists'],"SUMMARY_PRODUCT");
      $this->data['product_lists']=$this->product->get_product_by_summary_product($summary_products);
      $this->data['customer_category_lists']=$this->customer_category->find_all("CUSTOMER_CATEGORY_DESCRIPTION","ASC");
      $this->data['billing_type_lists']=$this->billing_type->find_all("BILLING_TYPE_DESCRIPTION","ASC");
      $this->data['component_type_lists']=$this->component_type->find_all("COMPONENT_TYPE_DESCRIPTION","ASC");
      $this->data['lists']=$this->report->BILLING_RESULT_revenue($parameters);
      if($data['button']=="Download"){
        access_log(array("MENU_ID"=>$this->menu_id,"USER_ID"=>$this->data['current_user']->USER_ID,"TYPE"=>4,"IP_ADDRESS"=>$_SERVER["REMOTE_ADDR"],"LAST_ACCESS"=>date("Y-m-d H:i:s")));
        $this->data['header_title']="Billing - Billing Result - Revenue Wireline,Speedy";
        $this->data['header_parameter']=($this->data['month_lists'][$data['month']]." ".$data['year']);
        $filename="billing-billing_result-revenue_ws-revenue.xlsx";
        $html = $this->load->view($this->path."excel",$this->data,true);
        $html=str_replace('&','&amp;',$html);
        $tmpfile = time().'.html';
        file_put_contents($tmpfile, $html);
        $this->load->library('PHPExcel');
        $reader = new PHPExcel_Reader_HTML; 
        $content = $reader->load($tmpfile); 
        $objWriter = PHPExcel_IOFactory::createWriter($content, 'Excel2007');
        ob_end_clean();
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
        unlink($tmpfile);
      }else{
        access_log(array("MENU_ID"=>$this->menu_id,"USER_ID"=>$this->data['current_user']->USER_ID,"TYPE"=>0,"IP_ADDRESS"=>$_SERVER["REMOTE_ADDR"],"LAST_ACCESS"=>date("Y-m-d H:i:s")));
        $this->data['path']=$this->path;
        $this->data['VIEW']=$this->load->view($this->path."index",$this->data,true);
        $this->load->view("templates/".$this->template, $this->data);
      }
    }else{
      $this->data['division_lists']=$this->division->find_all("DIVISION_CODE","ASC");
      $divisions=$this->implode_array($this->data['division_lists'],"DIVISION_CODE");
      $this->data['witel_lists']=$this->witel->lists($divisions);
      $witels=$this->implode_array($this->data['witel_lists'],"WITEL_CODE");
      $this->data['datel_lists']=$this->datel->lists($divisions,$witels);
      $this->data['summary_product_lists']=$this->product->lists("'Wireline','Speedy'");
      $summary_products=$this->implode_array($this->data['summary_product_lists'],"SUMMARY_PRODUCT");
      $this->data['product_lists']=$this->product->get_product_by_summary_product($summary_products);
      $this->data['customer_category_lists']=$this->customer_category->find_all("CUSTOMER_CATEGORY_DESCRIPTION","ASC");
      $this->data['billing_type_lists']=$this->billing_type->find_all("BILLING_TYPE_DESCRIPTION","ASC");
      $this->data['component_type_lists']=$this->component_type->find_all("COMPONENT_TYPE_DESCRIPTION","ASC");
      $this->data['lists']=$this->report->BILLING_RESULT_revenue($parameters);
      $this->data['path']=$this->path;
      $this->data['VIEW']=$this->load->view($this->path."index",$this->data,true);
      $this->load->view("templates/".$this->template, $this->data);
    }
  }
}