<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends MY_Controller {
  public function __construct() 
  {
    parent::__construct();
    $this->load->model("user_model","user");
  }
  public function login()
  {
    $back_url=$this->input->get("back_url");
    if($this->data['current_user']==true)redirect(base_url());
    if($this->input->server('REQUEST_METHOD') == 'POST'){
      $user = $this->input->post('user');
      $current_user = $this->user->find_by_USERNAME_and_PASSWORD($user['username'],md5($user['password']));
      if(sizeof($current_user)==0){
        $this->session->set_flashdata('error','Username atau password salah!');
        if($back_url!=""){
          redirect(base_url('auth/login?back_url='.$back_url));
        }else{
          redirect(base_url("auth/login"));
        }
      }else{
        $this->session->set_userdata(array("current_user" => $current_user));
        $this->session->set_flashdata('success','Selamat Datang : '.$current_user->NAME);
        if($back_url!=""){
          redirect($back_url);
        }else{
          redirect(base_url());
        }
      }
    }else{
      $this->data['url']=($back_url!="" ? base_url("auth/login?back_url=".$back_url) : base_url("auth/login"));
      $this->template="auth_template";
      $this->data['VIEW']=$this->load->view("auth/login",$this->data,true);
      $this->load->view("templates/".$this->template, $this->data);
    }
  }
  public function logout()
  {
    $this->session->unset_userdata('current_user');
    redirect(base_url('auth/login'));
  }
  public function profile()
  {
    if($this->input->server('REQUEST_METHOD') == 'POST'){
      $data=$this->input->post("user");
      $data['PASSWORD']=md5($data['PASSWORD']);
      $this->db->trans_begin();
      $this->user->update($data,$data['USER_ID'],"USER_ID");
      $current_user = $this->user->find_by_USER_ID($data['USER_ID']);
      $this->session->set_userdata(array("current_user" => $current_user));
      if ($this->db->trans_status() === FALSE) {
        $this->db->trans_rollback();
        $this->session->set_flashdata("error","Profil gagal disimpan");
      } else {
        $this->db->trans_commit();        
        $this->session->set_flashdata("success","Profil telah disimpan");
      }
      redirect(base_url($this->path));
    }else{
      $user=$this->user->find_by_USER_ID($this->data['current_user']->USER_ID);
      if(sizeof($user)==0)redirect(base_url());
      $this->data['user']=$user;
      $this->data['VIEW']=$this->load->view("auth/profile",$this->data,true);
      $this->load->view("templates/".$this->template, $this->data);
    }
  }
}
