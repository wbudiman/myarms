<?php

if (!defined('BASEPATH'))exit('No direct script access allowed');

class Billing_Committee extends MY_Controller {
  public $path="billing_management/detail_billing/billing_committee/";
  public $menu_id=0;
  public function __construct() 
  {
    parent::__construct();
    $this->menu_id=get_by_url("billing_management/detail_billing/billing_committee");
    $this->data['billing_components']=$this->billing_component->list_detail_billing();
    $this->data['billing_component_lists']=$this->billing_component->all();
  }
  function index()
  {
    $row_lists=array();
    $field=array();
    foreach($this->row_column->find_all_by_ROW_COLUMN_ID_and_ROW_COLUMN_ID("<=11","!=7","ROW_COLUMN_DESCRIPTION","ASC") as $r)
    {
      $row_lists[$r->ROW_COLUMN_FIELD]=$r->ROW_COLUMN_DESCRIPTION;
      $field[$r->ROW_COLUMN_FIELD]=$r->ROW_COLUMN_FIELD_USED;
      if($r->ROW_COLUMN_FIELD_USED==NULL)$field[$r->ROW_COLUMN_FIELD]=$this->data['billing_components'];
    }
    $this->data['row_lists']=$row_lists;
    $this->data['field']=$field;
    $parameters=array(
      "division" => "''",
      "witel" => "''",
      "datel" => "''",
      "month_1" => date("m",strtotime("-2month")),
      "year_1" => date("Y",strtotime("-2month")),
      "month_2" => date("m",strtotime("-1month")),
      "year_2" => date("Y",strtotime("-1month")),
      "month_3" =>date("m"),
      "year_3" => date("Y"),
      "header" => "di.DIVISION_CODE,di.DIVISION_NAME",
      "detail" => "p.SUMMARY_PRODUCT",
      "ubis" => "''",
      "summary_product" => "''",
      "product" => "''",
      "ubis_segment" => "''",
      "billing_component" => "TAGIHAN",
      "top_customer" => "''",
      "indihome" => "",
      "view_by" => "AMOUNT"
    );
    $this->data['search']=array(
      "division" => "",
      "witel" => "",
      "datel" => "",
      "month_1" => date("m",strtotime("-2month")),
      "year_1" => date("Y",strtotime("-2month")),
      "month_2" => date("m",strtotime("-1month")),
      "year_2" => date("Y",strtotime("-1month")),
      "month_3" =>date("m"),
      "year_3" => date("Y"),
      "header" => "di.DIVISION_CODE,di.DIVISION_NAME",
      "detail" => "p.SUMMARY_PRODUCT",
      "ubis" => "",
      "summary_product" => "",
      "product" => "",
      "ubis_segment" => "",
      "billing_component" => "TAGIHAN",
      "top_customer" => "",
      "indihome" => "",
      "view_by" => "AMOUNT"
    );
    if($this->input->server('REQUEST_METHOD') == 'POST'){
      $data=$this->input->post("search");
      $data['division']=isset($data['division']) ? $data['division'] : "";
      $data['witel']=isset($data['witel']) ? $data['witel'] : "";
      $data['datel']=isset($data['datel']) ? $data['datel'] : "";
      $data['ubis']=isset($data['ubis']) ? $data['ubis'] : "";
      $data['summary_product']=isset($data['summary_product']) ? $data['summary_product'] : "";
      $data['product']=isset($data['product']) ? $data['product'] : "";
      $data['ubis_segment']=isset($data['ubis_segment']) ? $data['ubis_segment'] : "";
      $data['top_customer']=isset($data['top_customer']) ? $data['top_customer'] : "";
      $data['billing_component']=(isset($data['billing_component']) ? $data['billing_component'] : "TAGIHAN");
      $this->data['search']=$data;
      $parameters=array(
        "division" => $this->implode_array($data['division']),
        "witel" => $this->implode_array($data['witel']),
        "datel" => $this->implode_array($data['datel']),
        "month_1" => $data['month_1'],
        "year_1" => $data['year_1'],
        "month_2" => $data['month_2'],
        "year_2" => $data['year_2'],
        "month_3" => $data['month_3'],
        "year_3" => $data['year_3'],
        "header" => $data['header'],
        "detail" => $data['detail'],
        "ubis" => $this->implode_array($data['ubis']),
        "summary_product" => $this->implode_array($data['summary_product']),
        "product" => $this->implode_array($data['product']),
        "ubis_segment" => $this->implode_array($data['ubis_segment']),
        "billing_component" => $data['billing_component'],
        "top_customer" => $this->implode_array($data['top_customer']),
        "indihome" => $data['indihome'],
        "view_by" => $data['view_by'],
      );
      $this->data['division_lists']=$this->division->find_all("DIVISION_CODE","ASC");
      if($data['division']==""){
        $divisions=$this->implode_array($this->data['division_lists'],"DIVISION_CODE");
      }else{
        $divisions=$this->implode_array($data['division']);
      }
      $this->data['witel_lists']=$this->witel->lists($divisions);
      if($data['witel']==""){
        $witels=$this->implode_array($this->data['witel_lists'],"WITEL_CODE");
      }else{
        $witels=$this->implode_array($data['witel']);
      }
      $this->data['datel_lists']=$this->datel->lists($divisions,$witels);
      $this->data['ubis_lists']=$this->ubis->find_all("UBIS","ASC");
      $this->data['summary_product_lists']=$this->product->lists("'Wireline','Wireless','Speedy'");
      if($data['summary_product']==""){
        $summary_products=$this->implode_array($this->data['summary_product_lists'],"SUMMARY_PRODUCT");
      }else{
        $summary_products=$this->implode_array($data['summary_product']);
      }
      $this->data['product_lists']=$this->product->get_product_by_summary_product($summary_products);
      if($data['ubis']==""){
        $ubis=$this->implode_array($this->data['ubis_lists'],"UBIS_ID");
      }else{
        $ubis=$this->implode_array($data['ubis']);
      }
      $this->data['ubis_segment_lists']=$this->ubis_segment->get_by_ubis_id($ubis);
      $this->data['top_customer_lists']=$this->portofolio->find_all("PORTOFOLIO_NAME","ASC");
      $this->data['indihome_lists']=$this->indihome->find_all("INDIHOME_DESCRIPTION","ASC");
      $this->data['view_by_lists']=$this->view_by->list_detail_billing();
      $this->data['lists']=$this->report->BILLING_DETAIL_REVENUEWSW_billing_committee($parameters);
      if($data['button']=="Download"){
        access_log(array("MENU_ID"=>$this->menu_id,"USER_ID"=>$this->data['current_user']->USER_ID,"TYPE"=>4,"IP_ADDRESS"=>$_SERVER["REMOTE_ADDR"],"LAST_ACCESS"=>date("Y-m-d H:i:s")));
        $this->data['header_title']="Billing - Detail Billing - Komite Billing Wireline,Speedy,Wireless";
        $this->data['header_parameter']="";
        $filename="billing-detail_billing-revenue_wsw-billing_committee.xlsx";
        $html = $this->load->view($this->path."excel",$this->data,true);
        $html=str_replace('&','&amp;',$html);
        $tmpfile = time().'.html';
        file_put_contents($tmpfile, $html);
        
        $this->load->library('PHPExcel');
        $reader = new PHPExcel_Reader_HTML; 
        $content = $reader->load($tmpfile); 
        $objWriter = PHPExcel_IOFactory::createWriter($content, 'Excel2007');
        ob_end_clean();
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
        unlink($tmpfile);
      }else{  
        access_log(array("MENU_ID"=>$this->menu_id,"USER_ID"=>$this->data['current_user']->USER_ID,"TYPE"=>0,"IP_ADDRESS"=>$_SERVER["REMOTE_ADDR"],"LAST_ACCESS"=>date("Y-m-d H:i:s")));
        $this->data['path']=$this->path;
        $this->data['VIEW']=$this->load->view($this->path."index",$this->data,true);
        $this->load->view("templates/".$this->template, $this->data);
      }
    }else{
      $this->data['division_lists']=$this->division->find_all("DIVISION_CODE","ASC");
      $divisions=$this->implode_array($this->data['division_lists'],"DIVISION_CODE");
      $this->data['witel_lists']=$this->witel->lists($divisions);
      $witels=$this->implode_array($this->data['witel_lists'],"WITEL_CODE");
      $this->data['datel_lists']=$this->datel->lists($divisions,$witels);
      $this->data['ubis_lists']=$this->ubis->find_all("UBIS","ASC");
      $this->data['summary_product_lists']=$this->product->lists("'Wireline','Wireless','Speedy'");
      $summary_products=$this->implode_array($this->data['summary_product_lists'],"SUMMARY_PRODUCT");
      $this->data['product_lists']=$this->product->get_product_by_summary_product($summary_products);
      $this->data['ubis_segment_lists']=$this->ubis_segment->find_all("UBIS_SEGMENT","ASC");
      $this->data['top_customer_lists']=$this->portofolio->find_all("PORTOFOLIO_NAME","ASC");
      $this->data['indihome_lists']=$this->indihome->find_all("INDIHOME_DESCRIPTION","ASC");
      $this->data['view_by_lists']=$this->view_by->list_detail_billing();
      $this->data['lists']=$this->report->BILLING_DETAIL_REVENUEWSW_billing_committee($parameters);
      $this->data['path']=$this->path;
      $this->data['VIEW']=$this->load->view($this->path."index",$this->data,true);
      $this->load->view("templates/".$this->template, $this->data);
    }
  }
}