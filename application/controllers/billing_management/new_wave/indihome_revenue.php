<?php

if (!defined('BASEPATH'))exit('No direct script access allowed');

class Indihome_Revenue extends MY_Controller {
  public $path="billing_management/new_wave/indihome_revenue/";
  public $menu_id=0;
  public function __construct() 
  {
    parent::__construct();
    $this->menu_id=get_by_url("billing_management/new_wave/indihome_revenue");
  }
  function index()
  {
    $row_lists=array();
    $field=array();
    foreach($this->row_column->get_row_new_wave_indihome_revenue() as $r)
    {
      $row_lists[$r->ROW_COLUMN_FIELD]=$r->ROW_COLUMN_DESCRIPTION;
      $field[$r->ROW_COLUMN_FIELD]=$r->ROW_COLUMN_FIELD_USED;
    }
    $this->data['row_lists']=$row_lists;
    $this->data['field']=$field;
    $parameters=array(
      "division" => "''",
      "witel" => "''",
      "datel" => "''",
      "month" => date("m"),
      "year" => date("Y"),
      "row" => "di.DIVISION_CODE,di.DIVISION_NAME",
      "ubis" => "''",
      "column" => "w.WITEL_CODE,w.WITEL_DESCRIPTION",
      "view_by" => "AMOUNT",
      "packet_indihome" => "''",
      "packet_speedy" => "''",
      "ubis_segment" => "''",
      "profile_pelanggan" => "''",
      "umur_pelanggan" => "''",
    );
    $this->data['search']=array(
      "division" => "",
      "witel" => "",
      "datel" => "",
      "month" => date("m"),
      "year" => date("Y"),
      "row" => "di.DIVISION_CODE,di.DIVISION_NAME",
      "ubis" => "",
      "column" => "w.WITEL_CODE,w.WITEL_DESCRIPTION",
      "view_by" => "AMOUNT",
      "packet_indihome" => "",
      "packet_speedy" => "",
      "ubis_segment" => "",
      "profile_pelanggan" => "",
      "umur_pelanggan" => "",
    );
    if($this->input->server('REQUEST_METHOD') == 'POST'){
      $data=$this->input->post("search");
      $data['division']=isset($data['division']) ? $data['division'] : "";
      $data['witel']=isset($data['witel']) ? $data['witel'] : "";
      $data['datel']=isset($data['datel']) ? $data['datel'] : "";
      $data['ubis']=isset($data['ubis']) ? $data['ubis'] : "";
      $data['packet_indihome']=isset($data['packet_indihome']) ? $data['packet_indihome'] : "";
      $data['packet_speedy']=isset($data['packet_speedy']) ? $data['packet_speedy'] : "";
      $data['ubis_segment']=isset($data['ubis_segment']) ? $data['ubis_segment'] : "";
      $data['profile_pelanggan']=isset($data['profile_pelanggan']) ? $data['profile_pelanggan'] : "";
      $data['umur_pelanggan']=isset($data['umur_pelanggan']) ? $data['umur_pelanggan'] : "";
      $this->data['search']=$data;
      $parameters=array(
        "division" => $this->implode_array($data['division']),
        "witel" => $this->implode_array($data['witel']),
        "datel" => $this->implode_array($data['datel']),
        "month" => $data['month'],
        "year" => $data['year'],
        "row" => $data['row'],
        "ubis" => $this->implode_array($data['ubis']),
        "column" => $data['column'],
        "view_by" => $data['view_by'],
        "packet_indihome" => $this->implode_array($data['packet_indihome']),
        "packet_speedy" => $this->implode_array($data['packet_speedy']),
        "ubis_segment" => $this->implode_array($data['ubis_segment']),
        "profile_pelanggan" => $this->implode_array($data['profile_pelanggan']),
        "umur_pelanggan" => $this->implode_array($data['umur_pelanggan']),
      );
      $this->data['division_lists']=$this->division->find_all("DIVISION_CODE","ASC");
      if($data['division']==""){
        $divisions=$this->implode_array($this->data['division_lists'],"DIVISION_CODE");
      }else{
        $divisions=$this->implode_array($data['division']);
      }
      $this->data['witel_lists']=$this->witel->lists($divisions);
      if($data['witel']==""){
        $witels=$this->implode_array($this->data['witel_lists'],"WITEL_CODE");
      }else{
        $witels=$this->implode_array($data['witel']);
      }
      $this->data['datel_lists']=$this->datel->lists($divisions,$witels);
      $this->data['ubis_lists']=$this->ubis->find_all("UBIS","ASC");
      if($data['ubis']==""){
        $ubis=$this->implode_array($this->data['ubis_lists'],"UBIS_ID");
      }else{
        $ubis=$this->implode_array($data['ubis']);
      }
      $this->data['ubis_segment_lists']=$this->ubis_segment->get_by_ubis_id($ubis);
      $this->data['view_by_lists']=$this->view_by->list_new_wave_indihome_revenue();
      $this->data['packet_indihome_lists']=$this->packet_indihome->find_all("PACKET_INDIHOME_DESCRIPTION","ASC");
      $this->data['profile_pelanggan_lists']=$this->profile_pelanggan->find_all("PROFILE_PELANGGAN_DESCRIPTION","ASC");
      $this->data['packet_speedy_lists']=$this->packet_speedy->find_all("PACKET_SPEEDY_DESCRIPTION","ASC");
      $this->data['umur_pelanggan_lists']=$this->umur_pelanggan->find_all("UMUR_PELANGGAN_DESCRIPTION","ASC");
      $this->data['lists']=$this->report->BILLING_DETAIL_NEW_WAVE_indihome_revenue($parameters);
      if($data['button']=="Download"){
        access_log(array("MENU_ID"=>$this->menu_id,"USER_ID"=>$this->data['current_user']->USER_ID,"TYPE"=>4,"IP_ADDRESS"=>$_SERVER["REMOTE_ADDR"],"LAST_ACCESS"=>date("Y-m-d H:i:s")));
        $this->data['header_title']="Billing - Detail Billing - New Wave - Indihome Revenue";
        $this->data['header_parameter']=($this->data['month_lists'][$data['month']]." ".$data['year']);
        $filename="billing-detail_billing-new_wave-indihome_revenue.xlsx";
        $html = $this->load->view($this->path."excel",$this->data,true);
        $html=str_replace('&','&amp;',$html);
        $tmpfile = time().'.html';
        file_put_contents($tmpfile, $html);
        
        $this->load->library('PHPExcel');
        $reader = new PHPExcel_Reader_HTML; 
        $content = $reader->load($tmpfile); 
        $objWriter = PHPExcel_IOFactory::createWriter($content, 'Excel2007');
        ob_end_clean();
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
        unlink($tmpfile);
      }else{
        access_log(array("MENU_ID"=>$this->menu_id,"USER_ID"=>$this->data['current_user']->USER_ID,"TYPE"=>0,"IP_ADDRESS"=>$_SERVER["REMOTE_ADDR"],"LAST_ACCESS"=>date("Y-m-d H:i:s")));
        $this->data['path']=$this->path;
        $this->data['VIEW']=$this->load->view($this->path."index",$this->data,true);
        $this->load->view("templates/".$this->template, $this->data);
      }
    }else{
      $this->data['division_lists']=$this->division->find_all("DIVISION_CODE","ASC");
      $divisions=$this->implode_array($this->data['division_lists'],"DIVISION_CODE");
      $this->data['witel_lists']=$this->witel->lists($divisions);
      $witels=$this->implode_array($this->data['witel_lists'],"WITEL_CODE");
      $this->data['datel_lists']=$this->datel->lists($divisions,$witels);
      $this->data['ubis_lists']=$this->ubis->find_all("UBIS","ASC");
      $this->data['ubis_segment_lists']=$this->ubis_segment->find_all("UBIS_SEGMENT","ASC");
      $this->data['view_by_lists']=$this->view_by->list_new_wave_indihome_revenue();
      $this->data['profile_pelanggan_lists']=$this->profile_pelanggan->find_all("PROFILE_PELANGGAN_DESCRIPTION","ASC");
      $this->data['packet_indihome_lists']=$this->packet_indihome->find_all("PACKET_INDIHOME_DESCRIPTION","ASC");
      $this->data['packet_speedy_lists']=$this->packet_speedy->find_all("PACKET_SPEEDY_DESCRIPTION","ASC");
      $this->data['umur_pelanggan_lists']=$this->umur_pelanggan->find_all("UMUR_PELANGGAN_DESCRIPTION","ASC");
      $this->data['lists']=$this->report->BILLING_DETAIL_NEW_WAVE_indihome_revenue($parameters);
      $this->data['path']=$this->path;
      $this->data['VIEW']=$this->load->view($this->path."index",$this->data,true);
      $this->load->view("templates/".$this->template, $this->data);
    }
  }
}