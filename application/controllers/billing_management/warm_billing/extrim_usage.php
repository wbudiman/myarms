<?php

if (!defined('BASEPATH'))exit('No direct script access allowed');

class Extrim_Usage extends MY_Controller {
  public $path="billing_management/warm_billing/extrim_usage/";
  public $menu_id=0;
  public function __construct() 
  {
    parent::__construct();
    $this->data['path']=$this->path;
    $this->menu_id=get_by_url("billing_management/warm_billing/extrim_usage");
  }
  function index()
  {
    $this->data['range']=array(
      "amount_call" => array(
        "eu.RP_CALL<500000" => "< 500.000",
        "eu.RP_CALL>=500000 and eu.RP_CALL<2500000" => "500.000 - 2.500.000",
        "eu.RP_CALL>2500000" => "> 2.500.000"
      ),
      "amount_sli007" => array(
        "eu.SLI007<500000" => "< 500.000",
        "eu.SLI007>=500000 and eu.SLI007<2500000" => "500.000 - 2.500.000",
        "eu.SLI007<2500000" => "> 2.500.000"
      ),
      "amount_domestic" => array(
        "(eu.LOKAL+eu.SLJJ+eu.STB)<500000" => "< 500.000",
        "(eu.LOKAL+eu.SLJJ+eu.STB)>=500000 and (eu.LOKAL+eu.SLJJ+eu.STB)<2500000" => "500.000 - 2.500.000",
        "(eu.LOKAL+eu.SLJJ+eu.STB)>2500000" => "> 2.500.000"
      ),
      "amount_japati" => array(
        "eu.JAPATI<500000" => "< 500.000",
        "eu.JAPATI>=500000 and eu.JAPATI<2500000" => "500.000 - 2.500.000",
        "eu.JAPATI<2500000" => "> 2.500.000"
      )
    );
    $row_lists=array();
    $field=array();
    foreach($this->row_column->get_row_warm_billing_extrim_usage() as $r)
    {
      $row_lists[$r->ROW_COLUMN_FIELD]=$r->ROW_COLUMN_DESCRIPTION;
      $field[$r->ROW_COLUMN_FIELD]=$r->ROW_COLUMN_FIELD_USED;
    }
    $this->data['row_lists']=$row_lists;
    $this->data['field']=$field;
    $parameters=array(
      "month" => date("m"),
      "year" => date("Y"),
      "division" => "''",
      "witel" => "''",
      "datel" => "''",
      "row" => "di.DIVISION_CODE,di.DIVISION_NAME",
      "ubis" => "''",
      "summary_product" => "''",
      "product" => "''",
      "column" => "p.SUMMARY_PRODUCT",
      "ubis_segment" => "''",
      "amount_call" => "",
      "amount_sli007" => "",
      "amount_domestic" => "",
      "amount_japati" => ""
    );
    $this->data['search']=array(
      "month" => date("m"),
      "year" => date("Y"),
      "division" => "",
      "witel" => "",
      "datel" => "",
      "row" => "di.DIVISION_CODE,di.DIVISION_NAME",
      "ubis" => "",
      "summary_product" => "",
      "product" => "",
      "column" => "p.SUMMARY_PRODUCT",
      "ubis_segment" => "",
      "amount_call" => "",
      "amount_sli007" => "",
      "amount_domestic" => "",
      "amount_japati" => ""
    );
    if($this->input->server('REQUEST_METHOD') == 'POST'){
      $data=$this->input->post("search");
      $data['division']=isset($data['division']) ? $data['division'] : "";
      $data['witel']=isset($data['witel']) ? $data['witel'] : "";
      $data['datel']=isset($data['datel']) ? $data['datel'] : "";
      $data['ubis']=isset($data['ubis']) ? $data['ubis'] : "";
      $data['ubis_segment']=isset($data['ubis_segment']) ? $data['ubis_segment'] : "";
      $data['summary_product']=isset($data['summary_product']) ? $data['summary_product'] : "";
      $data['product']=isset($data['product']) ? $data['product'] : "";
      $this->data['search']=$data;
      $parameters=array(
        "month" => $data['month'],
        "year" => $data['year'],
        "division" => $this->implode_array($data['division']),
        "witel" => $this->implode_array($data['witel']),
        "datel" => $this->implode_array($data['datel']),
        "row" => $data['row'],
        "ubis" => $this->implode_array($data['ubis']),
        "summary_product" => $this->implode_array($data['summary_product']),
        "product" => $this->implode_array($data['product']),
        "column" => $data['column'],
        "ubis_segment" => $this->implode_array($data['ubis_segment']),
        "amount_call" => $data['amount_call'],
        "amount_sli007" => $data['amount_sli007'],
        "amount_domestic" => $data['amount_domestic'],
        "amount_japati" => $data['amount_japati']
      );
      $this->data['division_lists']=$this->division->find_all("DIVISION_CODE","ASC");
      if($data['division']==""){
        $divisions=$this->implode_array($this->data['division_lists'],"DIVISION_CODE");
      }else{
        $divisions=$this->implode_array($data['division']);
      }
      $this->data['witel_lists']=$this->witel->lists($divisions);
      if($data['witel']==""){
        $witels=$this->implode_array($this->data['witel_lists'],"WITEL_CODE");
      }else{
        $witels=$this->implode_array($data['witel']);
      }
      $this->data['datel_lists']=$this->datel->lists($divisions,$witels);
      $this->data['ubis_lists']=$this->ubis->find_all("UBIS","ASC");
      $this->data['summary_product_lists']=$this->product->lists("'Wireline','Speedy'");
      if($data['summary_product']==""){
        $summary_products=$this->implode_array($this->data['summary_product_lists'],"SUMMARY_PRODUCT");
      }else{
        $summary_products=$this->implode_array($data['summary_product']);
      }
      $this->data['product_lists']=$this->product->get_product_by_summary_product($summary_products);
      $this->data['ubis_segment_lists']=$this->ubis_segment->find_all("UBIS_SEGMENT","ASC");
      $data_get="month=".$parameters['month']."&year=".$parameters['year']."&division=".urlencode($parameters['division'])."&witel=".urlencode($parameters['witel'])."&datel=".urlencode($parameters['datel'])."&row=".urlencode($parameters['row'])."&ubis=".urlencode($parameters['ubis'])."&summary_product=".urlencode($parameters['summary_product'])."&product=".urlencode($parameters['product'])."&column=".urlencode($parameters['column'])."&ubis_segment=".urlencode($parameters['ubis_segment'])."&amount_call=".urlencode($parameters['amount_call'])."&amount_sli007=".urlencode($parameters['amount_sli007'])."&amount_domestic=".urlencode($parameters['amount_domestic'])."&amount_japati=".urlencode($parameters['amount_japati']);
      $this->data['param_get']=$data_get;
      $this->data['lists']=$this->report->WARM_BILLING_EXTRIM_USAGE($parameters);
      if($data['button']=="Download"){
        access_log(array("MENU_ID"=>$this->menu_id,"USER_ID"=>$this->data['current_user']->USER_ID,"TYPE"=>4,"IP_ADDRESS"=>$_SERVER["REMOTE_ADDR"],"LAST_ACCESS"=>date("Y-m-d H:i:s")));
        $this->data['header_title']="Billing - Warm Billing - Extrim Usage";
        $this->data['header_parameter']=($this->data['month_lists'][$data['month']]." ".$data['year']);
        $filename="billing-warm_billing-extrim_usage.xlsx";
        $html = $this->load->view($this->path."excel",$this->data,true);
        $html=str_replace('&','&amp;',$html);
        $tmpfile = time().'.html';
        file_put_contents($tmpfile, $html);
        
        $this->load->library('PHPExcel');
        $reader = new PHPExcel_Reader_HTML; 
        $content = $reader->load($tmpfile); 
        $objWriter = PHPExcel_IOFactory::createWriter($content, 'Excel2007');
        ob_end_clean();
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
        unlink($tmpfile);
      }else{
        access_log(array("MENU_ID"=>$this->menu_id,"USER_ID"=>$this->data['current_user']->USER_ID,"TYPE"=>0,"IP_ADDRESS"=>$_SERVER["REMOTE_ADDR"],"LAST_ACCESS"=>date("Y-m-d H:i:s")));
        $this->data['path']=$this->path;
        $this->data['VIEW']=$this->load->view($this->path."index",$this->data,true);
        $this->load->view("templates/".$this->template, $this->data);
      }
    }else{
      $this->data['division_lists']=$this->division->find_all("DIVISION_CODE","ASC");
      $divisions=$this->implode_array($this->data['division_lists'],"DIVISION_CODE");
      $this->data['witel_lists']=$this->witel->lists($divisions);
      $witels=$this->implode_array($this->data['witel_lists'],"WITEL_CODE");
      $this->data['datel_lists']=$this->datel->lists($divisions,$witels);
      $this->data['ubis_lists']=$this->ubis->find_all("UBIS","ASC");
      $this->data['summary_product_lists']=$this->product->lists("'Wireline','Speedy'");
      $summary_products=$this->implode_array($this->data['summary_product_lists'],"SUMMARY_PRODUCT");
      $this->data['product_lists']=$this->product->get_product_by_summary_product($summary_products);
      $this->data['ubis_segment_lists']=$this->ubis_segment->find_all("UBIS_SEGMENT","ASC");
      $data_get="month=".$this->data['search']['month']."&year=".$this->data['search']['year']."&division=".urlencode($this->implode_array(''))."&witel=".urlencode($this->implode_array(''))."&datel=".urlencode($this->implode_array(''))."&row="."di.DIVISION_CODE,di.DIVISION_NAME"."&ubis=".urlencode($this->implode_array(''))."&summary_product=".urlencode($this->implode_array(''))."&product=".urlencode($this->implode_array(''))."&column="."p.SUMMARY_PRODUCT"."&ubis_segment=".urlencode($this->implode_array(''))."&amount_call=&amount_sli007=&amount_domestic=&amount_japati=";
      $this->data['param_get']=$data_get;
      $this->data['lists']=$this->report->WARM_BILLING_EXTRIM_USAGE($parameters);
      $this->data['path']=$this->path;
      $this->data['VIEW']=$this->load->view($this->path."index",$this->data,true);
      $this->load->view("templates/".$this->template, $this->data);
    }
  }
  function detail()
  {
    $search=$this->input->get();
    $this->data['param_get']=http_build_query($search, '', '&amp;');
    $this->data['search']=$search;
    if($this->input->server('REQUEST_METHOD') == 'POST'){
      $search['limit']=0;
      $this->data['lists']=$this->report->WARM_BILLING_EXTRIM_USAGE_detail($search);
      access_log(array("MENU_ID"=>$this->menu_id,"USER_ID"=>$this->data['current_user']->USER_ID,"TYPE"=>4,"IP_ADDRESS"=>$_SERVER["REMOTE_ADDR"],"LAST_ACCESS"=>date("Y-m-d H:i:s")));
      $output="CCA|SND|Nama|Bisnis Area|Produk|Segmen|Call|Min Call|Max Call|Durasi|Usage Current|Usage Last|Amount Last|Selisih usage".PHP_EOL;
      $filename="billing-warm_billing-ekstrim-usage_detail.txt";
      foreach($this->data['lists'] as $l){
        $current_usage=$l->LOKAL+$l->SLJJ+$l->STB+$l->SLI007+$l->JAPATI+$l->USAGE_LAINNYA;
        $selisih=$l->USAGE_LALU-$current_usage;
        $output.="|".$l->CCA."|".$l->ND."|".$l->NAMA."|".$l->BUSINESS_AREA_DESCRIPTION."|".$l->PRODUCT_NAME."|".$l->UBIS_SEGMENT."|".$l->CALL."|".$l->MIN_CALL."|".$l->MAX_CALL."|".$l->DURATION."|".$current_usage."|".$l->USAGE_LALU."|".$l->TAG_LALU."|".$selisih.PHP_EOL;
      }
      header('Content-type: text/plain');
      header('Content-Disposition: attachment; filename="'.$filename.'"');
      echo $output;
    }else{
      $search['limit']=40;
      $this->data['lists']=$this->report->WARM_BILLING_EXTRIM_USAGE_detail($search);
      $this->data['path']=$this->path;
      $content=$this->load->view($this->path."detail",$this->data,true);
      echo json_encode(array(
        "status" => true,
        "content" =>$content
      ));
    }
  }
}