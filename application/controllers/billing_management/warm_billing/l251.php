<?php

if (!defined('BASEPATH'))exit('No direct script access allowed');

class L251 extends MY_Controller {
  public $path="billing_management/warm_billing/l251/";
  public $menu_id=0;
  public function __construct() 
  {
    parent::__construct();
    $this->menu_id=get_by_url("billing_management/warm_billing/l251");
  }
  function index()
  {
    $this->data['division_lists']=$this->division->find_all("DIVISION_CODE","ASC");
    $this->data['sto_lists']=$this->sto->l251();
    $this->data['error_lists']=$this->error->find_all("ERROR_DESCRIPTION","ASC");
    $row_lists=array();
    $field=array();
    $rows=array(
      "l.STO_ID"=>array(
        "ROW_COLUMN_DESCRIPTION"=>"STO",
        "ROW_COLUMN_FIELD_USED"=>"STO_ID"
      ),
      "di.DIVISION_NAME"=>array(
        "ROW_COLUMN_DESCRIPTION"=>"Divisi",
        "ROW_COLUMN_FIELD_USED"=>"DIVISION_NAME"
      ),
      "er.ERROR_DESCRIPTION"=>array(
        "ROW_COLUMN_DESCRIPTION"=>"Error",
        "ROW_COLUMN_FIELD_USED"=>"ERROR_DESCRIPTION"
      )
    );
    foreach($rows as $key=>$r)
    {
      $row_lists[$key]=$r['ROW_COLUMN_DESCRIPTION'];
      $field[$key]=$r['ROW_COLUMN_FIELD_USED'];
    }
    $this->data['row_lists']=$row_lists;
    $this->data['field']=$field;
    $parameters=array(
      "division" => "''",
      "sto" => "''",
      "error" => "''",
      "month" =>date("m"),
      "year" => date("Y"),
      "show_data" => "l.STO_ID",
    );
    $this->data['search']=array(
      "division" => "",
      "sto" => "",
      "error" => "",
      "month" =>date("m"),
      "year" => date("Y"),
      "show_data" => "l.STO_ID",
    );
    if($this->input->server('REQUEST_METHOD') == 'POST'){
      $data=$this->input->post("search");
      $data['division']=isset($data['division']) ? $data['division'] : "";
      $data['sto']=isset($data['sto']) ? $data['sto'] : "";
      $data['error']=isset($data['error']) ? $data['error'] : "";
      $this->data['search']=$data;
      $parameters=array(
        "division" => $this->implode_array($data['division']),
        "sto" => $this->implode_array($data['sto']),
        "error" => $this->implode_array($data['error']),
        "month" => $data['month'],
        "year" => $data['year'],
        "show_data" => $data['show_data'],
      );
      $data_get="division=".urlencode($this->implode_array($data['division']))."&sto=".urlencode($this->implode_array($data['sto']))."&error=".urlencode($this->implode_array($data['error']))."&month=".$this->data['search']['month']."&year=".$this->data['search']['year']."&show_data=".$this->data['search']['show_data'];
      $this->data['param_get']=$data_get;
      $this->data['lists']=$this->report->WARM_BILLING_l251($parameters);
      if($data['button']=="Download"){
        access_log(array("MENU_ID"=>$this->menu_id,"USER_ID"=>$this->data['current_user']->USER_ID,"TYPE"=>4,"IP_ADDRESS"=>$_SERVER["REMOTE_ADDR"],"LAST_ACCESS"=>date("Y-m-d H:i:s")));
        $this->data['header_title']="Billing - Warm Billing - L251";
        $this->data['header_parameter']=($this->data['month_lists'][$data['month']]." ".$data['year']);
        $filename="billing-warm_billing-l251.xlsx";
        $html = $this->load->view($this->path."excel",$this->data,true);
        $html=str_replace('&','&amp;',$html);
        $tmpfile = time().'.html';
        file_put_contents($tmpfile, $html);
        $this->load->library('PHPExcel');
        $reader = new PHPExcel_Reader_HTML; 
        $content = @$reader->load($tmpfile); 
        $objWriter = PHPExcel_IOFactory::createWriter($content, 'Excel2007');
        ob_end_clean();
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
        unlink($tmpfile);
      }else{
        access_log(array("MENU_ID"=>$this->menu_id,"USER_ID"=>$this->data['current_user']->USER_ID,"TYPE"=>0,"IP_ADDRESS"=>$_SERVER["REMOTE_ADDR"],"LAST_ACCESS"=>date("Y-m-d H:i:s")));
        $this->data['path']=$this->path;
        $this->data['VIEW']=$this->load->view($this->path."index",$this->data,true);
        $this->load->view("templates/".$this->template, $this->data);
      }
    }else{
      $data=$this->data['search'];
      $data_get="division=".urlencode($this->implode_array($data['division']))."&sto=".urlencode($this->implode_array($data['sto']))."&error=".urlencode($this->implode_array($data['error']))."&month=".$this->data['search']['month']."&year=".$this->data['search']['year']."&show_data=".$this->data['search']['show_data'];
      $this->data['param_get']=$data_get;
      $this->data['lists']=$this->report->WARM_BILLING_l251($parameters);
      $this->data['path']=$this->path;
      $this->data['VIEW']=$this->load->view($this->path."index",$this->data,true);
      $this->load->view("templates/".$this->template, $this->data);
    }
  }
  function detail($param="")
  {
    $search=$this->input->get();
    $this->data['param_get']=http_build_query($search, '', '&amp;');
    $this->data['search']=$search;
    $this->data['lists']=$this->report->WARM_BILLING_l251_detail($search);
    if($this->input->server('REQUEST_METHOD') == 'POST'){
      access_log(array("MENU_ID"=>$this->menu_id,"USER_ID"=>$this->data['current_user']->USER_ID,"TYPE"=>4,"IP_ADDRESS"=>$_SERVER["REMOTE_ADDR"],"LAST_ACCESS"=>date("Y-m-d H:i:s")));
      $output="Wilayah|Divre|STO|Notel|First Call|Last Call|Total Call|Total Duration|Error".PHP_EOL;
      $filename="billing-warm_billing-l251_detail.txt";
      foreach($this->data['lists'] as $l){
        $output.=$l->TERITORY."|".$l->DIVISION_CODE."|".$l->STO_ID."|".$l->ND."|".$l->FIRST_CALL."|".$l->LAST_CALL."|".$l->TOTAL_CALL."|".$l->TOTAL_DURATION."|".$l->ERROR_DESCRIPTION.PHP_EOL;
      }
      header('Content-type: text/plain');
      header('Content-Disposition: attachment; filename="'.$filename.'"');
      echo $output;
    }else{
      $this->data['path']=$this->path;
      $this->data['VIEW']=$this->load->view($this->path."detail",$this->data,true);
      $this->load->view("templates/".$this->template, $this->data);
    }
  }
}