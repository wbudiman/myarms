<?php

if (!defined('BASEPATH'))exit('No direct script access allowed');

class Group_Tunggakan extends MY_Controller {
  public $path="collection_management/debt_management/group_tunggakan/";
  public $menu_id=0;
  public function __construct() 
  {
    parent::__construct();
    $this->menu_id=get_by_url("collection_management/debt_management/group_tunggakan");
  }
  function index()
  {
    $row_lists=array();
    $field=array();
    foreach($this->row_column->get_row_group_tunggakan() as $r)
    {
      $row_lists[$r->ROW_COLUMN_FIELD]=$r->ROW_COLUMN_DESCRIPTION;
      $field[$r->ROW_COLUMN_FIELD]=$r->ROW_COLUMN_FIELD_USED;
    }
    $this->data['row_lists']=$row_lists;

    foreach($this->row_column->get_column_group_tunggakan() as $r)
    {
      $column_lists[$r->ROW_COLUMN_FIELD]=$r->ROW_COLUMN_DESCRIPTION;
      $field[$r->ROW_COLUMN_FIELD]=$r->ROW_COLUMN_FIELD_USED;
    }
    $this->data['column_lists']=$column_lists;

    $this->data['field']=$field;
    $parameters=array(
      "division" => "''",
      "jumlah_kuitansi" => "''",
      "witel" => "''",
      "datel" => "''",
      "date" => "01",
      "month" => date("m"),
      "year" => date("Y"),
      "show_data" => "di.DIVISION_CODE,di.DIVISION_NAME",
      "summary_product" => "''",
      "product" => "''",
      "ubis" => "''",
      "segment" => "''",
      "billing_type" => "''",
      "top_customer" => "''",
      "indihome" => "",
      "eksepsi" => "''",
      "tunggakan" => "''",
      "usage" => "''",
      "history" => 1,
    );
    $this->data['search']=array(
      "division" => "",
      "jumlah_kuitansi" => "",
      "witel" => "",
      "datel" => "",
      "month" => date("m"),
      "year" => date("Y"),
      "show_data" => "di.DIVISION_CODE,di.DIVISION_NAME",
      "summary_product" => "",
      "product" => "",
      "ubis" => "",
      "segment" => "",
      "billing_type" => "",
      "top_customer" => "",
      "indihome" => "",
      "eksepsi" => "",
      "tunggakan" => "",
      "usage" => "",
      "history" => 1,
    );
    if($this->input->server('REQUEST_METHOD') == 'POST'){
      $data=$this->input->post("search");
      $data['division']=isset($data['division']) ? $data['division'] : "";
      $data['witel']=isset($data['witel']) ? $data['witel'] : "";
      $data['jumlah_kuitansi']=isset($data['jumlah_kuitansi']) ? $data['jumlah_kuitansi'] : "";
      $data['datel']=isset($data['datel']) ? $data['datel'] : "";
      $data['summary_product']=isset($data['summary_product']) ? $data['summary_product'] : "";
      $data['product']=isset($data['product']) ? $data['product'] : "";
      $data['customer_category']=isset($data['customer_category']) ? $data['customer_category'] : "";
      $data['billing_type']=isset($data['billing_type']) ? $data['billing_type'] : "";
      $data['ubis']=isset($data['ubis']) ? $data['ubis'] : "";
      $data['segment']=isset($data['segment']) ? $data['segment'] : "";
      $data['top_customer']=isset($data['top_customer']) ? $data['top_customer'] : "";
      $data['eksepsi']=isset($data['eksepsi']) ? $data['eksepsi'] : "";
      $data['tunggakan']=isset($data['tunggakan']) ? $data['tunggakan'] : "";
      $data['usage']=isset($data['usage']) ? $data['usage'] : "";
      $this->data['search']=$data;
      $parameters=array(
        "division" => $this->implode_array($data['division']),
        "jumlah_kuitansi" => $this->implode_array($data['jumlah_kuitansi']),
        "witel" => $this->implode_array($data['witel']),
        "datel" => $this->implode_array($data['datel']),
        "month" => $data['month'],
        "year" => $data['year'],
        "show_data" => $data['show_data'],
        "summary_product" => $this->implode_array($data['summary_product']),
        "product" => $this->implode_array($data['product']),
        "ubis" => $this->implode_array($data['ubis']),
        "segment" => $this->implode_array($data['segment']),
        "billing_type" => $this->implode_array($data['billing_type']),
        "top_customer" => $this->implode_array($data['top_customer']),
        "eksepsi" => $this->implode_array($data['eksepsi']),
        "tunggakan" => $this->implode_array($data['tunggakan']),
        "usage" => $this->implode_array($data['usage']),
        "indihome" => $data['indihome'],
        "history" => $data['history'],
      );
      $this->data['division_lists']=$this->division->find_all("DIVISION_CODE","ASC");
      if($data['division']==""){
        $divisions=$this->implode_array($this->data['division_lists'],"DIVISION_CODE");
      }else{
        $divisions=$this->implode_array($data['division']);
      }
      $this->data['witel_lists']=$this->witel->lists($divisions);
      if($data['witel']==""){
        $witels=$this->implode_array($this->data['witel_lists'],"WITEL_CODE");
      }else{
        $witels=$this->implode_array($data['witel']);
      }
      $this->data['datel_lists']=$this->datel->lists($divisions,$witels);
      $this->data['summary_product_lists']=$this->product->lists();
      $summary_products=$this->implode_array($this->data['summary_product_lists'],"SUMMARY_PRODUCT");
      $this->data['product_lists']=$this->product->get_product_by_summary_product($summary_products);
      $this->data['ubis_lists']=$this->ubis->find_all("UBIS","ASC");
      if($data['ubis']==""){
        $ubis=$this->implode_array($this->data['ubis_lists'],"UBIS_ID");
      }else{
        $ubis=$this->implode_array($data['ubis']);
      }
      $this->data['segment_lists']=$this->ubis_segment->get_by_ubis_id($ubis);
      $this->data['billing_type_lists']=$this->billing_type_collection->find_all("DESCRIPTION","ASC");
      $this->data['top_customer_lists']=$this->portofolio->find_all("PORTOFOLIO_NAME","ASC");
      $this->data['indihome_lists']=$this->indihome->find_all("INDIHOME_DESCRIPTION","ASC");
      $this->data['eksepsi_lists']=$this->eksepsi->find_all("EKSEPSI_DESCRIPTION","ASC");
      $this->data['tunggakan_lists']=$this->tunggakan->find_all("TUNGGAKAN_DESCRIPTION","ASC");
      $this->data['usage_lists']=$this->usage_status->find_all("USAGE_STATUS","ASC");
      $this->data['jumlah_kuitansi_lists']=$this->debt_management->jumlah_kuitansi_lists();
      $this->data['umur_termuda_lists']=$this->debt_management->umur_termuda_lists();
      $this->data['view_by_lists']=$this->view_by->list_group_tunggakan();
      // $this->data['lists']=$this->report->COLLECTION_c3mr($parameters);
      if($data['button']=="Download"){
        access_log(array("MENU_ID"=>$this->menu_id,"USER_ID"=>$this->data['current_user']->USER_ID,"TYPE"=>4,"IP_ADDRESS"=>$_SERVER["REMOTE_ADDR"],"LAST_ACCESS"=>date("Y-m-d H:i:s")));
        $this->data['header_title']="Collection - Retail - Collection - C3MR";
        $this->data['header_parameter']=($this->data['month_lists'][$data['month']]." ".$data['year']);
        $filename="collection-retail-collection-c3mr.xlsx";
        $html = $this->load->view($this->path."excel",$this->data,true);
        $html=str_replace('&','&amp;',$html);
        $tmpfile = time().'.html';
        file_put_contents($tmpfile, $html);
        $this->load->library('PHPExcel');
        $reader = new PHPExcel_Reader_HTML; 
        $content = $reader->load($tmpfile); 
        $objWriter = PHPExcel_IOFactory::createWriter($content, 'Excel2007');
        ob_end_clean();
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
        unlink($tmpfile);
      }else{
        access_log(array("MENU_ID"=>$this->menu_id,"USER_ID"=>$this->data['current_user']->USER_ID,"TYPE"=>0,"IP_ADDRESS"=>$_SERVER["REMOTE_ADDR"],"LAST_ACCESS"=>date("Y-m-d H:i:s")));
        $this->data['path']=$this->path;
        $this->data['VIEW']=$this->load->view($this->path."index",$this->data,true);
        $this->load->view("templates/".$this->template, $this->data);
      }
    }else{
      $this->data['division_lists']=$this->division->find_all("DIVISION_CODE","ASC");
      $divisions=$this->implode_array($this->data['division_lists'],"DIVISION_CODE");
      $this->data['witel_lists']=$this->witel->lists($divisions);
      $witels=$this->implode_array($this->data['witel_lists'],"WITEL_CODE");
      $this->data['datel_lists']=$this->datel->lists($divisions,$witels);
      $this->data['summary_product_lists']=$this->product->lists();
      $summary_products=$this->implode_array($this->data['summary_product_lists'],"SUMMARY_PRODUCT");
      $this->data['product_lists']=$this->product->get_product_by_summary_product($summary_products);
      $this->data['ubis_lists']=$this->ubis->find_all("UBIS","ASC");
      $this->data['segment_lists']=$this->ubis_segment->find_all("UBIS_SEGMENT","ASC");
      $this->data['billing_type_lists']=$this->billing_type_collection->find_all("DESCRIPTION","ASC");
      $this->data['top_customer_lists']=$this->portofolio->find_all("PORTOFOLIO_NAME","ASC");
      $this->data['indihome_lists']=$this->indihome->find_all("INDIHOME_DESCRIPTION","ASC");
      $this->data['eksepsi_lists']=$this->eksepsi->find_all("EKSEPSI_DESCRIPTION","ASC");
      $this->data['tunggakan_lists']=$this->tunggakan->find_all("TUNGGAKAN_DESCRIPTION","ASC");
      $this->data['usage_lists']=$this->usage_status->find_all("USAGE_STATUS","ASC");
      $this->data['jumlah_kuitansi_lists']=$this->debt_management->jumlah_kuitansi_lists();
      $this->data['umur_termuda_lists']=$this->debt_management->umur_termuda_lists();
      $this->data['view_by_lists']=$this->view_by->list_group_tunggakan();
      // $this->data['lists']=$this->report->COLLECTION_c3mr($parameters);
      $this->data['path']=$this->path;
      $this->data['VIEW']=$this->load->view($this->path."index",$this->data,true);
      $this->load->view("templates/".$this->template, $this->data);
    }
  }
}