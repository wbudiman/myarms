<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_Roles extends MY_Controller {
  public $path="administrator/user_roles/";
  public $menu_id=0;
  public function __construct() 
  {
    parent::__construct();
    $this->menu_id=get_by_url("administrator/user_roles");
    $this->load->model("user_role_model","user_role");
    $this->load->model("user_role_detail_model","user_role_detail");
  }
  public function index($offset=0)
  {
    access_log(array("MENU_ID"=>$this->menu_id,"USER_ID"=>$this->data['current_user']->USER_ID,"TYPE"=>0,"IP_ADDRESS"=>$_SERVER["REMOTE_ADDR"],"LAST_ACCESS"=>date("Y-m-d H:i:s")));
    $perpage=10;
    $pagination = array(
      'base_url' => base_url($this->path."index"),
      'total_rows' => sizeof($this->user_role->lists()),
      'per_page' => $perpage,
      'num_links' => 3,
      'uri_segment' => 4
    );
    $this->data['pagination']=$this->paging($pagination);
    $this->user_role->offset=$offset;
    $this->user_role->limit=$perpage;
    $this->data['lists']=$this->user_role->lists();
    $this->data['VIEW']=$this->load->view($this->path."index",$this->data,true);
    $this->load->view("templates/".$this->template, $this->data);
  }
  public function modules($user_role="")
  {
    $level1=$this->user_role->get_modules(0,$user_role);
    $result=array();
    foreach($level1 as $l1){
      $result[$l1->MENU_ID]=array("data"=>$l1,"childs"=>array());
      $level2=$this->user_role->get_modules($l1->MENU_ID,$user_role);
      $counter2=0;
      foreach($level2 as $l2){
        $result[$l1->MENU_ID]["childs"][$counter2]=array("data"=>$l2,"childs"=>array());
        $level3=$this->user_role->get_modules($l2->MENU_ID,$user_role);
        $counter3=0;
        foreach($level3 as $l3){
          $result[$l1->MENU_ID]["childs"][$counter2]['childs'][$counter3]=array("data"=>$l3,"childs"=>array());
          $level4=$this->user_role->get_modules($l3->MENU_ID,$user_role);
          $counter4=0;
          foreach($level4 as $l4){
            $result[$l1->MENU_ID]["childs"][$counter2]['childs'][$counter3]['childs'][$counter4]=array("data"=>$l4,"childs"=>array());
            $level5=$this->user_role->get_modules($l4->MENU_ID,$user_role);
            $counter5=0;
            foreach($level5 as $l5){
              $result[$l1->MENU_ID]["childs"][$counter2]['childs'][$counter3]['childs'][$counter4]['childs'][$counter5]=array("data"=>$l5,"childs"=>array());
              $counter5++;
            }
            $counter4++;
          }
          $counter3++;
        }
        $counter2++;
      }
    }
    return $result;
  }
  public function create()
  {
    if($this->input->server('REQUEST_METHOD') == 'POST'){
      $data=$this->input->post("user_role");
      $detail=$this->input->post("detail");
      $this->db->trans_begin();
      $user_role=$this->user_role->find_all("USER_ROLE_ID","DESC");
      $id_user_role=1;
      if(sizeof($user_role)>0)$id_user_role=$user_role[0]->USER_ROLE_ID+1;
      $data["USER_ROLE_ID"]=$id_user_role;
      $this->user_role->save($data);
      for($x=0;$x<sizeof($detail);$x++){
        $user_role_detail=$this->user_role_detail->find_all("USER_ROLE_DETAIL_ID","DESC");
        $id_user_role_detail=1;
        if(sizeof($user_role_detail)>0)$id_user_role_detail=$user_role_detail[0]->USER_ROLE_DETAIL_ID+1;
        $this->user_role_detail->save(array(
          "USER_ROLE_DETAIL_ID" => $id_user_role_detail,
          "USER_ROLE_ID" => $id_user_role,
          "MENU_ID" => $detail[$x]
        ));
      }
      if ($this->db->trans_status() === FALSE) {
        $this->db->trans_rollback();
        $this->session->set_flashdata("error","Hak akses gagal disimpan");
      } else {
        $this->db->trans_commit();
        access_log(array("MENU_ID"=>$this->menu_id,"USER_ID"=>$this->data['current_user']->USER_ID,"TYPE"=>1,"IP_ADDRESS"=>$_SERVER["REMOTE_ADDR"],"LAST_ACCESS"=>date("Y-m-d H:i:s")));
        $this->session->set_flashdata("success","Hak akses telah disimpan");
      }
      redirect(base_url($this->path));
    }else{
      $modules=$this->modules();
      $this->data['module_lists']=$modules;
      $this->data['VIEW']=$this->load->view($this->path."create",$this->data,true);
      $this->load->view("templates/".$this->template, $this->data);
    }
  }
  public function delete($id=0)
  {
    $this->db->trans_begin();
    $this->user_role->delete($id,"USER_ROLE_ID");
    $this->user_role_detail->delete($id,"USER_ROLE_ID");
    if ($this->db->trans_status() === FALSE) {
      $this->db->trans_rollback();
      $this->session->set_flashdata("error","Hak akses gagal dihapus");
    } else {
      $this->db->trans_commit();   
      access_log(array("MENU_ID"=>$this->menu_id,"USER_ID"=>$this->data['current_user']->USER_ID,"TYPE"=>3,"IP_ADDRESS"=>$_SERVER["REMOTE_ADDR"],"LAST_ACCESS"=>date("Y-m-d H:i:s")));
      $this->session->set_flashdata("success","Hak akses telah dihapus");
    }
    redirect(base_url($this->path));
  }
  public function edit($id=0)
  {
    if($this->input->server('REQUEST_METHOD') == 'POST'){
      $data=$this->input->post("user_role");
      $detail=$this->input->post("detail");
      $this->db->trans_begin();
      $this->user_role->update($data,$data['USER_ROLE_ID'],"USER_ROLE_ID");
      $this->user_role_detail->delete($data['USER_ROLE_ID'],"USER_ROLE_ID");
      for($x=0;$x<sizeof($detail);$x++){
        $user_role_detail=$this->user_role_detail->find_all("USER_ROLE_DETAIL_ID","DESC");
        $id_user_role_detail=1;
        if(sizeof($user_role_detail)>0)$id_user_role_detail=$user_role_detail[0]->USER_ROLE_DETAIL_ID+1;
        $this->user_role_detail->save(array(
          "USER_ROLE_DETAIL_ID" => $id_user_role_detail,
          "USER_ROLE_ID" => $data['USER_ROLE_ID'],
          "MENU_ID" => $detail[$x]
        ));
      }
      if ($this->db->trans_status() === FALSE) {
        $this->db->trans_rollback();
        $this->session->set_flashdata("error","Hak akses gagal diubah");
      } else {
        $this->db->trans_commit();
        access_log(array("MENU_ID"=>$this->menu_id,"USER_ID"=>$this->data['current_user']->USER_ID,"TYPE"=>2,"IP_ADDRESS"=>$_SERVER["REMOTE_ADDR"],"LAST_ACCESS"=>date("Y-m-d H:i:s")));
        $this->session->set_flashdata("success","Hak akses telah diubah");
      }
      redirect(base_url($this->path));
    }else{
      $user_role=$this->user_role->find_by_USER_ROLE_ID($id);
      if(sizeof($user_role)==0)redirect(base_url($this->path));
      $modules=$this->modules($id);
      $this->data['module_lists']=$modules;
      $this->data['user_role']=$user_role;
      $this->data['VIEW']=$this->load->view($this->path."edit",$this->data,true);
      $this->load->view("templates/".$this->template, $this->data);
    }
  }
}
