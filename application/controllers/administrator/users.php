<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends MY_Controller {
  public $path="administrator/users/";
  public $menu_id=0;
  public function __construct() 
  {
    parent::__construct();
    $this->menu_id=get_by_url("administrator/users");
    $this->load->model("user_model","user");
    $this->load->model("user_role_model","user_role");
  }
  public function index($offset=0)
  {
    access_log(array("MENU_ID"=>$this->menu_id,"USER_ID"=>$this->data['current_user']->USER_ID,"TYPE"=>0,"IP_ADDRESS"=>$_SERVER["REMOTE_ADDR"],"LAST_ACCESS"=>date("Y-m-d H:i:s")));
    $perpage=10;
    $pagination = array(
      'base_url' => base_url($this->path."index"),
      'total_rows' => sizeof($this->user->lists()),
      'per_page' => $perpage,
      'num_links' => 3,
      'uri_segment' => 4
    );
    $this->data['pagination']=$this->paging($pagination);
    $this->user->offset=$offset;
    $this->user->limit=$perpage;
    $this->data['lists']=$this->user->lists();
    $this->data['VIEW']=$this->load->view($this->path."index",$this->data,true);
    $this->load->view("templates/".$this->template, $this->data);
  }
  public function create()
  {
    if($this->input->server('REQUEST_METHOD') == 'POST'){
      $data=$this->input->post("user");
      $data['PASSWORD']=md5($data['PASSWORD']);
      $detail=$this->input->post("detail");
      $this->db->trans_begin();
      $user=$this->user->find_all("USER_ID","DESC");
      $user_id=1;
      if(sizeof($user)>0)$user_id=$user[0]->USER_ID+1;
      $data["USER_ID"]=$user_id;
      $this->user->save($data);
      if ($this->db->trans_status() === FALSE) {
        $this->db->trans_rollback();
        $this->session->set_flashdata("error","Otorisasi user gagal disimpan");
      } else {
        $this->db->trans_commit();        
        access_log(array("MENU_ID"=>$this->menu_id,"USER_ID"=>$this->data['current_user']->USER_ID,"TYPE"=>1,"IP_ADDRESS"=>$_SERVER["REMOTE_ADDR"],"LAST_ACCESS"=>date("Y-m-d H:i:s")));
        $this->session->set_flashdata("success","Otorisasi user telah disimpan");
      }
      redirect(base_url($this->path));
    }else{
      $this->data['user_role_lists']=$this->user_role->find_all("ROLE_NAME","ASC");
      $this->data['VIEW']=$this->load->view($this->path."create",$this->data,true);
      $this->load->view("templates/".$this->template, $this->data);
    }
  }
  public function delete($id=0)
  {
    $this->db->trans_begin();
    $this->user->delete($id,"USER_ID");
    if ($this->db->trans_status() === FALSE) {
      $this->db->trans_rollback();
      $this->session->set_flashdata("error","Otorisasi user gagal dihapus");
    } else {
      $this->db->trans_commit();        
      access_log(array("MENU_ID"=>$this->menu_id,"USER_ID"=>$this->data['current_user']->USER_ID,"TYPE"=>3,"IP_ADDRESS"=>$_SERVER["REMOTE_ADDR"],"LAST_ACCESS"=>date("Y-m-d H:i:s")));
      $this->session->set_flashdata("success","Otorisasi user telah dihapus");
    }
    redirect(base_url($this->path));
  }
  public function edit($id=0)
  {
    if($this->input->server('REQUEST_METHOD') == 'POST'){
      $data=$this->input->post("user");
      $data['PASSWORD']=md5($data['PASSWORD']);
      $this->db->trans_begin();
      $this->user->update($data,$data['USER_ID'],"USER_ID");
      if ($this->db->trans_status() === FALSE) {
        $this->db->trans_rollback();
        $this->session->set_flashdata("error","Otorisasi user gagal diubah");
      } else {
        $this->db->trans_commit();        
        access_log(array("MENU_ID"=>$this->menu_id,"USER_ID"=>$this->data['current_user']->USER_ID,"TYPE"=>2,"IP_ADDRESS"=>$_SERVER["REMOTE_ADDR"],"LAST_ACCESS"=>date("Y-m-d H:i:s")));
        $this->session->set_flashdata("success","Otorisasi user telah diubah");
      }
      redirect(base_url($this->path));
    }else{
      $user=$this->user->find_by_USER_ID($id);
      if(sizeof($user)==0)redirect(base_url($this->path));
      $this->data['user_role_lists']=$this->user_role->find_all("ROLE_NAME","ASC");
      $this->data['user']=$user;
      $this->data['VIEW']=$this->load->view($this->path."edit",$this->data,true);
      $this->load->view("templates/".$this->template, $this->data);
    }
  }
}
