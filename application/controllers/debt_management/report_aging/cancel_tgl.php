<?php

if (!defined('BASEPATH'))exit('No direct script access allowed');

class Cancel_Tgl extends MY_Controller {
  public $path="debt_management/report_aging/cancel_tgl/";
  public $menu_id=0;
  public function __construct() 
  {
    parent::__construct();
    $this->menu_id=get_by_url("debt_management/report_aging/cancel_tgl");
  }
  
  function index()
  {
    $parameters=array(
      "month_1" => date("m"),
      "year_1" => date("Y"),
      "division" => "''",
      "witel" => "''",
      "datel" => "''",
	  "validasi" => "",
	  "petugas" => "''",
      "keyword" => "''",
      "loket" => "''",
    ); 
    $this->data['search']=array(
      "month_1" => date("m"),
      "year_1" => date("Y"),
      "division" => "",
      "witel" => "",
      "datel" => "",
	  "validasi" => "",
	  "petugas" => "",
      "keyword" => "",
      "loket" => "",
    );
    if($this->input->server('REQUEST_METHOD') == 'POST'){
      $data=$this->input->post("search");
      $data['division']=isset($data['division']) ? $data['division'] : "";
      $data['witel']=isset($data['witel']) ? $data['witel'] : "";
      $data['datel']=isset($data['datel']) ? $data['datel'] : "";
      $data['petugas']=isset($data['petugas']) ? $data['petugas'] : "";
      $data['loket']=isset($data['loket']) ? $data['loket'] : "";
	  $data['validasi']=isset($data['validasi']) ? $data['validasi'] : "";
	  $data['keyword']=isset($data['keyword']) ? $data['keyword'] : "";
      $this->data['search']=$data;
      $parameters=array(
        "month_1" => $data['month_1'],
        "year_1" => $data['year_1'],
		"keyword" => $data['keyword'],
		"validasi" => $data['validasi'],
		"petugas" => $data['petugas'],
        "division" => $this->implode_array($data['division']),
        "witel" => $this->implode_array($data['witel']),
        "datel" => $this->implode_array($data['datel']),
        "loket" => $this->implode_array($data['loket']),
      );
      $this->data['division_lists']=$this->division->find_all("DIVISION_CODE","ASC");
      if($data['division']==""){
        $divisions=$this->implode_array($this->data['division_lists'],"DIVISION_CODE");
      }else{
        $divisions=$this->implode_array($data['division']);
      }
      $this->data['witel_lists']=$this->witel->lists($divisions);
      if($data['witel']==""){
        $witels=$this->implode_array($this->data['witel_lists'],"WITEL_CODE");
      }else{
        $witels=$this->implode_array($data['witel']);
      }
      $this->data['datel_lists']=$this->datel->lists($divisions,$witels);
	    $this->data['lists']=$this->report_aging->Cancel_Tgl_Cabut($parameters);
      $data_get="division=".urlencode($this->implode_array($data['division']))."&witel=".urlencode($this->implode_array($data['witel']))."&datel=".urlencode($this->implode_array($data['datel']))."&loket=".urlencode($this->implode_array($data['loket']))."&petugas=".urlencode($data['petugas'])."&validasi=".urlencode($data['validasi'])."&month_1=".$this->data['search']['month_1']."&year_1=".$this->data['search']['year_1'];
      $this->data['param_get']=$data_get;
      if($data['button']=="Download"){
        access_log(array("MENU_ID"=>$this->menu_id,"USER_ID"=>$this->data['current_user']->USER_ID,"TYPE"=>4,"IP_ADDRESS"=>$_SERVER["REMOTE_ADDR"],"LAST_ACCESS"=>date("Y-m-d H:i:s")));
        $this->data['header_title']="Debt Management - Report Aging - Cancel Tgl Cabut";
        $this->data['header_parameter']=($this->data['month_lists'][$data['month_1']]." ".$data['year_1']);
        $filename="debt_management-report_aging-cancel_tgl_cabut.xlsx";
        $html = $this->load->view($this->path."excel",$this->data,true);
        $html=str_replace('&','&amp;',$html);
        $tmpfile = time().'.html';
        file_put_contents($tmpfile, $html);
        
        $this->load->library('PHPExcel');
        $reader = new PHPExcel_Reader_HTML; 
        $content = $reader->load($tmpfile); 
        $objWriter = PHPExcel_IOFactory::createWriter($content, 'Excel2007');
        ob_end_clean();
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
        unlink($tmpfile);
      }else{
        access_log(array("MENU_ID"=>$this->menu_id,"USER_ID"=>$this->data['current_user']->USER_ID,"TYPE"=>0,"IP_ADDRESS"=>$_SERVER["REMOTE_ADDR"],"LAST_ACCESS"=>date("Y-m-d H:i:s")));
        $this->data['path']=$this->path;
        $this->data['VIEW']=$this->load->view($this->path."index",$this->data,true);
        $this->load->view("templates/".$this->template, $this->data);
      }
    }else{
      $data_get="month_1=".$this->data['search']['month_1']."&year_1=".$this->data['search']['year_1'];
      $this->data['param_get']=$data_get;
      $this->data['division_lists']=$this->division->find_all("DIVISION_CODE","ASC");
	  $divisions=$this->implode_array($this->data['division_lists'],"DIVISION_CODE");
      $this->data['witel_lists']=$this->witel->lists($divisions);
      $witels=$this->implode_array($this->data['witel_lists'],"WITEL_CODE");
      $this->data['datel_lists']=$this->datel->lists($divisions,$witels);
	  $datels=$this->implode_array($this->data['datel_lists'],"DATEL_CODE");
	    $this->data['lists']=$this->report_aging->Cancel_Tgl_Cabut($parameters);
      $this->data['path']=$this->path;
      $this->data['VIEW']=$this->load->view($this->path."index",$this->data,true);
      $this->load->view("templates/".$this->template, $this->data);
    }
  }
  
}