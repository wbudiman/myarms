<?php

if (!defined('BASEPATH'))exit('No direct script access allowed');

class Home extends MY_Controller {
  public function __construct() 
  {
    parent::__construct();
  }
  function index()
  {
    $this->data['VIEW']=$this->load->view("home/index",$this->data,true);
    $this->load->view("templates/".$this->template, $this->data);
  }
  function generate_menus()
  {
    $this->load->model("menu_model","menu");
    $json = file_get_contents("assets/generate/MODULE.json");
    $data=json_decode($json,true);
    $counter=1;
    foreach($data as $m){
      $this->menu->save(array(
        "MENU_ID" => $counter,
        "PARENT" => 0,
        "NAME" => $m['NAME'],
        "URL" => $m['URL'],
        "ICON" => $m['ICON'],
        "SORT" => $m['SORT']
      ));
      $menu1_id=$counter;
      $counter++;
      foreach($m['MENUS'] as $m2){
        $this->menu->save(array(
          "MENU_ID" => $counter,
          "PARENT" => $menu1_id,
          "NAME" => $m2['NAME'],
          "URL" => $m2['URL'],
          "ICON" => $m2['ICON'],
          "SORT" => $m2['SORT']
        ));
        $menu2_id=$counter;
        $counter++;
        foreach($m2['MENUS'] as $m3){
          $this->menu->save(array(
            "MENU_ID" => $counter,
            "PARENT" => $menu2_id,
            "NAME" => $m3['NAME'],
            "URL" => $m3['URL'],
            "ICON" => $m3['ICON'],
            "SORT" => $m3['SORT']
          ));
          $menu3_id=$counter;
          $counter++;
          foreach($m3['MENUS'] as $m4){
            $this->menu->save(array(
              "MENU_ID" => $counter,
              "PARENT" => $menu3_id,
              "NAME" => $m4['NAME'],
              "URL" => $m4['URL'],
              "ICON" => $m4['ICON'],
              "SORT" => $m4['SORT']
            ));
            $menu4_id=$counter;
            $counter++;
            foreach($m4['MENUS'] as $m5){
              $this->menu->save(array(
                "MENU_ID" => $counter,
                "PARENT" => $menu4_id,
                "NAME" => $m5['NAME'],
                "URL" => $m5['URL'],
                "ICON" => $m5['ICON'],
                "SORT" => $m5['SORT']
              ));
              $menu5_id=$counter;
              $counter++;
              foreach($m5['MENUS'] as $m6){
                $this->menu->save(array(
                  "MENU_ID" => $counter,
                  "PARENT" => $menu4_id,
                  "NAME" => $m6['NAME'],
                  "URL" => $m6['URL'],
                  "ICON" => $m6['ICON'],
                  "SORT" => $m6['SORT']
                ));
                $menu6_id=$counter;
                $counter++;
              }
            }
          }
        }
      }
    }
  }
}