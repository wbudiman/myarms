<?php

if (!defined('BASEPATH'))exit('No direct script access allowed');

class GrowthL11 extends MY_Controller {
  public $path="manual/growthL11/";
  public $menu_id=0;
  public function __construct() 
  {
    parent::__construct();
    $this->menu_id=get_by_url("manual/growthL11");
  }
  
  function index()
  {
    $this->data['path']=$this->path;
      $this->data['VIEW']=$this->load->view($this->path."index",$this->data,true);
      $this->load->view("templates/".$this->template, $this->data);
  }  
  
}