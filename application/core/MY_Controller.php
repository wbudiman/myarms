<?php

if (!defined('BASEPATH'))exit('No direct script access allowed');

class MY_Controller extends CI_Controller {
  public $data=array();
  public $template="template";
  public $config_pagination=array(
    'full_tag_open' => '<ul class="pagination">',
    'full_tag_close' => '</ul>',
    'first_tag_open' => '<li>',
    'first_tag_close' => '</li>',
    'first_link' => 'First',
    'last_tag_open' => '<li>',
    'last_tag_close' => '</li>',
    'last_link' => 'Last',
    'next_tag_open' => '<li>',
    'next_tag_close' => '</li>',
    'next_link' => 'Next',
    'prev_tag_open' => '<li>',
    'prev_tag_close' => '</li>',
    'prev_link' => 'Prev',
    'cur_tag_open' => '<li class="active"><a href="javascript:void(0)">',
    'cur_tag_close' => '</a></li>',
    'num_tag_open' => '<li>',
    'num_tag_close' => '</li>'
  );
  public function __construct() {
    parent::__construct();
    $this->load->model("division_model","division");
    $this->load->model("witel_model","witel");
    $this->load->model("datel_model","datel");
    $this->load->model("ubis_model","ubis");
    $this->load->model("product_model","product");
    $this->load->model("ubis_segment_model","ubis_segment");
    $this->load->model("indihome_model","indihome");
    $this->load->model("portofolio_model","portofolio");
    $this->load->model("packet_speedy_model","packet_speedy");
    $this->load->model("report_model","report");
    $this->load->model("billing_type_model","billing_type");
    $this->load->model("component_type_model","component_type");
    $this->load->model("customer_category_model","customer_category");
    $this->load->model("billing_component_model","billing_component");
    $this->load->model("view_by_model","view_by");
    $this->load->model("row_column_model","row_column");
    $this->load->model("usage_status_model","usage_status");
    $this->load->model("sto_model","sto");
    $this->load->model("error_model","error");
    $this->load->model("billing_type_collection_model","billing_type_collection");
    $this->load->model("eksepsi_model","eksepsi");
    $this->load->model("tunggakan_model","tunggakan");
    $this->load->model("packet_indihome_model","packet_indihome");
    $this->load->model("umur_pelanggan_model","umur_pelanggan");
    $this->load->model("profile_pelanggan_model","profile_pelanggan");
    $this->load->model("packet_ime_model","packet_ime");
    $this->load->model("payment_status_model","payment_status");
	  $this->load->model("report_gimmick_model","report_gimmick");
	  $this->load->model("loket_model","loket");
    $this->load->model("if_master_model","if_master");
    $this->load->model("debt_management_model","debt_management");
    $this->load->model("bisnis_area_model","bisnis_area");
    $this->load->model("rekon_finnet_model","rekon_finnet");
    $this->load->model("mapping_bank_model","mapping_bank");
    $this->data['no_mapping'] = "No Mapping";
    $this->data['current_user']=$this->session->userdata("current_user");
    $this->data['controller'] = $this->router->fetch_class();
    $this->data['method'] = $this->router->fetch_method();
    $this->data['PAGE_TITLE'] = "Analitycal Revenue Management System";
    $this->data['month_lists']=array(
      "01" => "Januari",
      "02" => "Februari",
      "03" => "Maret",
      "04" => "April",
      "05" => "Mei",
      "06" => "Juni",
      "07" => "Juli",
      "08" => "Agustus",
      "09" => "September",
      "10" => "October",
      "11" => "November",
      "12" => "Desember",
    );
    if($this->data['current_user'] == NULL){
      if($this->data['method'] != 'login' && $this->data['method'] != 'logout'){
        redirect(base_url('auth/login?back_url='.current_url()));
      }
    }else{
      $menus=menus($this->data['current_user']->USER_ROLE_ID);
      $this->data['MENUS']=$menus;
      $check="";
      $active_parent=array();
      $check_access=0;
      $menu_actives=array();
      foreach($menus as $m){
        if($m['data']->IS_EXIST>0){
          if($m['data']->URL!="")array_push($menu_actives,array("value"=>$m['data']->NAME,"url"=>base_url($m['data']->URL)));
          if(check_menus(current_url(),$m['data']->URL)!==false || check_menus($m['data']->URL,current_url())!==false){
            $check_access++;
            $check=$m['data']->MENU_ID;
            $active_parent[0]=$m['data']->MENU_ID;
          }else{
            foreach($m['childs'] as $l2){
              if($l2['data']->IS_EXIST>0){
                if($l2['data']->URL!="")array_push($menu_actives,array("value"=>$m['data']->NAME." - ".$l2['data']->NAME,"url"=>base_url($l2['data']->URL)));
                if(check_menus(current_url(),$l2['data']->URL)!==false || check_menus($l2['data']->URL,current_url())!==false){ 
                  $check_access++;
                  $check=$m['data']->MENU_ID;
                  $active_parent[0]=$m['data']->MENU_ID;
                  $active_parent[1]=$l2['data']->MENU_ID;
                }else{
                  foreach($l2['childs'] as $l3){
                    if($l3['data']->IS_EXIST>0){
                      if($l3['data']->URL!="")array_push($menu_actives,array("value"=>$m['data']->NAME." - ".$l2['data']->NAME." - ".$l3['data']->NAME,"url"=>base_url($l3['data']->URL)));
                      if(check_menus(current_url(),$l3['data']->URL)!==false || check_menus($l3['data']->URL,current_url())!==false){
                        $check_access++;
                        $check=$m['data']->MENU_ID;
                        $active_parent[0]=$m['data']->MENU_ID;
                        $active_parent[1]=$l2['data']->MENU_ID;
                        $active_parent[2]=$l3['data']->MENU_ID;
                      }else{
                        foreach($l3['childs'] as $l4){
                          if($l4['data']->IS_EXIST>0){
                            if($l4['data']->URL!="")array_push($menu_actives,array("value"=>$m['data']->NAME." - ".$l2['data']->NAME." - ".$l3['data']->NAME." - ".$l4['data']->NAME,"url"=>base_url($l4['data']->URL)));
                            if(check_menus(current_url(),$l4['data']->URL)!==false || check_menus($l4['data']->URL,current_url())!==false){
                              $check_access++;
                              $check=$m['data']->MENU_ID;
                              $active_parent[0]=$m['data']->MENU_ID;
                              $active_parent[1]=$l2['data']->MENU_ID;
                              $active_parent[2]=$l3['data']->MENU_ID;
                              $active_parent[3]=$l4['data']->MENU_ID;
                            }else{
                              foreach($l4['childs'] as $l5){
                                if($l5['data']->IS_EXIST>0){
                                  if($l5['data']->URL!="")array_push($menu_actives,array("value"=>$m['data']->NAME." - ".$l2['data']->NAME." - ".$l3['data']->NAME." - ".$l4['data']->NAME." - ".$l5['data']->NAME,"url"=>base_url($l5['data']->URL)));
                                  if(check_menus(current_url(),$l5['data']->URL)!==false || check_menus($l5['data']->URL,current_url())!==false){
                                    $check_access++;
                                    $check=$m['data']->MENU_ID;
                                    $active_parent[0]=$m['data']->MENU_ID;
                                    $active_parent[1]=$l2['data']->MENU_ID;
                                    $active_parent[2]=$l3['data']->MENU_ID;
                                    $active_parent[3]=$l4['data']->MENU_ID;
                                    $active_parent[4]=$l5['data']->MENU_ID;
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      if(!$this->input->is_ajax_request() && !in_array($this->data['controller'],array("home","auth","ajax","manual")))
      {
        if($check_access==0){
          $this->session->set_flashdata("error","Anda tidak punya akses ke halaman tersebut!");
          redirect(base_url());         
        }
      }
      $this->data['LIST_MENU_ACTIVE']=json_encode($menu_actives);
      $this->data['MENU_ACTIVE']=$check;
      $this->data['MENU_PARENT']=$active_parent;
      $this->data['HEADER'] = $this->load->view("shared/header", $this->data, true);
      $this->data['SUB_HEADER'] = $this->load->view("shared/sub_header", $this->data, true);
    }
    $this->data['FOOTER'] = $this->load->view("shared/footer", null, true);
    $this->data['INCLUDES'] = $this->load->view("templates/includes", $this->data, true);
  }
  function paging($param = array()) {
    $this->config_pagination['base_url'] = $param['base_url'];
    $this->config_pagination['total_rows'] = $param['total_rows'];
    $this->config_pagination['per_page'] = $param['per_page'];
    $this->config_pagination['num_links'] = $param['num_links'];
    $this->config_pagination['uri_segment'] = $param['uri_segment'];
    $this->pagination->initialize($this->config_pagination);
    return $this->pagination->create_links();
  }
  function implode_array($data=array(),$id="")
  {
    $result="";
    $counter=1;
    if($data!=""){
      foreach($data as $d){
        if($id!=""){
          $result.="'".$d->{$id}."'";
        }else{
          $result.="'".$d."'";
        }
        if($counter!=sizeof($data))$result.=",";
        $counter++;
      }
    }else{
      $result="''";
    }
    return $result;
  }
}

?>
