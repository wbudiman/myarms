<?php

if (!defined('BASEPATH')){
    exit('No direct script access allowed');
}

class MY_Model extends CI_Model {
    // define attribute table_name
    public $table_name = "";
    public $id = "";
    public $limit="";
    public $offset="";
    private $separator=array("= ","!=","> ",">=","< ","<=");
    /*
    *
    * Constructor method
    *
    * load codeigniter database library
    *
    */
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    /*
    * Overloading php __call method ( http://www.php.net/manual/en/language.oop5.overloading.php#object.call )
    *
    * @method name of method
    * @params array of parameter passed
    */
    public function __call($method, $params) {
      if (substr($method, 0, 8) == 'find_by_') {
        $length = strlen($method);
        $str_query = $this->build_query($method, 8, $length, 'SELECT * FROM "' . $this->table_name.'"', $params);
        $query = $this->db->query($str_query . ' and ROWNUM>0 and ROWNUM<=1');
        return $query->row();
      } elseif (substr($method, 0, 12) == 'find_all_by_'){
        $length = strlen($method);
        $str_query = $this->build_query($method, 12, $length, 'SELECT * FROM "' . $this->table_name.'"', $params);
        $query = $this->db->query($str_query);
        return $query->result();
      } elseif (substr($method, 0, 8) == 'find_all') {
        $length = strlen($method);
        $parameter="";
        if(sizeof($params)>0)$parameter.=" ORDER BY ".$params[0]." ".$params[1];
        $parameter.=$this->limit!="" ? " where ROWNUM>".$this->offset." and ROWNUM<=".$this->limit : "";
        $query = $this->db->query('SELECT * FROM "' . $this->table_name.'"'.$parameter);
        return $query->result();
      } elseif (substr($method, 0, 10) == 'find_like_') {
        $length = strlen($method);
        $str_query = $this->build_query_like($method, 10, $length, 'SELECT * FROM "' . $this->table_name.'"', $params);
        $query = $this->db->query($str_query);
        return $query->result();
      } elseif (substr($method, 0, 10) == 'delete_by_'){
        $length = strlen($method);
        $this->db->query($this->build_query_delete($method, 10, $length, 'DELETE FROM "' . $this->table_name.'"', $params));
      } elseif(substr($method,0,14)=='next_increment'){
        $data=$this->db->query('SHOW TABLE STATUS LIKE "'.$this->table_name.'"')->row();
        return $data->Auto_increment;
      } else {
        return null;
      }
    }
    /*
    * Save method
    *
    * @data array of data to be saved
    *
    */
    public function save($data){
        // $data = array_map("strtoupper", $data);
        $status = $this->db->insert($this->table_name, $data);
        // $this->id = $this->db->insert_id();
        return $status;
    }

    /*
    *
    * Update method
    * @data array of data to be updated
    * @param condition value
    * @updated_by condition key (default will updated by id)
    *
    */
    public function update($data, $param="", $update_by = 'id'){
      // $data = array_map("strtoupper", $data);
      if($param!="")$this->db->where($update_by,$param);
      return $this->db->update($this->table_name,$data);
    }

    /*
    *
    * Update method
    * @data array of data to be updated
    * @param condition value
    * @delete_by condition key (default will deleted by id)
    *
    */
    public function delete($param, $delete_by = 'id'){
        $this->db->where($delete_by,$param);
        return $this->db->delete($this->table_name);
    }
    private function build_query_delete($string, $start, $end, $query, $params){
        $new_str = substr($string, $start, $end);
        $arr_str = $this->find_and_or($new_str);
        $before = 0;

        $field = substr($string, $start, sizeof($arr_str) == 0 ? $end : $arr_str[0] );
        $query = $query . " WHERE " . $this->table_name. "." . $field . " = '" . $params[0] . "'";

        for($i = 0 ; $i < sizeof($arr_str) ; $i ++){
            $next = ($i + 1 < sizeof($arr_str) ? $arr_str[$i + 1] : strlen($string));
            $field = substr($new_str, $arr_str[$i] + 5 + $before, $next);
            $before = $arr_str[$i] + 5;
            $query = $query . " AND " . $this->table_name. "." . $field . " = '" . $params[$i+1] . "'";
        }
        return $query;
    }
    private function build_query($string, $start, $end, $query, $params){
        $new_str = substr($string, $start, $end);
        $arr_str = $this->find_and_or($new_str);
        $before = 0;
        $field = substr($string, $start, sizeof($arr_str) == 0 ? $end : $arr_str[0] );
        $check_separator=substr($params[0],0,2);
        $separator="=";
        if(in_array($check_separator,$this->separator)){
          $separator=$check_separator;
          if(strlen(trim($check_separator))==1)
          {
            $params[0]=substr($params[0],1,strlen($params[0]));
          }else{
            $params[0]=substr($params[0],2,strlen($params[0]));
          }
        }
        $query = $query . " WHERE " . $this->table_name. "." .  $field. $separator."'" . $params[0] . "'";
        for($i = 0 ; $i < sizeof($arr_str) ; $i ++){
            $next = ($i + 1 < sizeof($arr_str) ? $arr_str[$i + 1] : strlen($string));
            $field = substr($new_str, $arr_str[$i] + 5 + $before, $next);
            $before = $before+$arr_str[$i] + 5;
            $check_separator=substr($params[$i+1],0,2);
            $separator="=";
            if(in_array($check_separator,$this->separator)){
              $separator=$check_separator;
              if(strlen(trim($check_separator))==1)
              {
                $params[$i+1]=substr($params[$i+1],1,strlen($params[$i+1]));
              }else{
                $params[$i+1]=substr($params[$i+1],2,strlen($params[$i+1]));
              }
            }
            $query = $query . " AND "  . $this->table_name. "." . $field. $separator."'" . $params[$i+1] . "'";
        }
        if($this->limit!="")$query.=" ROWNUM> ".$this->offset." and ROWNUM<=".$this->limit;
        if(sizeof($params)>sizeof($arr_str)+1)$query.=" ORDER BY ".$params[sizeof($arr_str)+1]." ".$params[sizeof($arr_str)+2];
        return $query;
    }
    private function build_query_like($string, $start, $end, $query, $params){
        $new_str = substr($string, $start, $end);
        $arr_str = $this->find_and_or($new_str);
        $before = 0;
        $field = substr($string, $start, sizeof($arr_str) == 0 ? $end : $arr_str[0] );
        $query = $query . " WHERE " . $this->table_name. "." . $field . " LIKE '%" . $params[0] . "%'";
        for($i = 0 ; $i < sizeof($arr_str) ; $i ++){
            $next = ($i + 1 < sizeof($arr_str) ? $arr_str[$i + 1] : strlen($string));
            $condition= substr($new_str,$arr_str[$i] + $before,5);
            if($condition!="_and_")$condition= substr($new_str,$arr_str[$i] + $before,4);            
            $field = substr($new_str, $arr_str[$i] + strlen($condition) + $before, $next);
            $before = $before+$arr_str[$i] + strlen($condition);
            $query = $query . (strlen($condition)==5 ? " AND " : " OR ").  $this->table_name. "." . $field . " LIKE '%" . $params[$i+1] . "%'";
        }
        if($this->limit!="")$query.=" AND ROWNUM> ".$this->offset." and ROWNUM<=".$this->limit;
        if(sizeof($params)>sizeof($arr_str)+1)$query.=" ORDER BY ".$params[sizeof($arr_str)+1]." ".$params[sizeof($arr_str)+2];
        return $query;
    }
    // private function find_and($string, $arr = array()){
        // $pos = strpos($string, '_and_');
        // if($pos != false){
            // array_push($arr, $pos);
            // $arr = $this->find_and(substr($string, $pos + 5, strlen($string) + 1), $arr);
        // }
        // return $arr;
    // }
    private function find_and_or($string, $arr = array()){
        $pos = strpos($string, '_and_');
        if($pos != false){
          array_push($arr, $pos);
          $arr = $this->find_and_or(substr($string, $pos + 5, strlen($string) + 1), $arr);
        }else{
          $pos = strpos($string, '_or_');
          if($pos != false){
            array_push($arr, $pos);
            $arr = $this->find_and_or(substr($string, $pos + 4, strlen($string) + 1), $arr);
          }
        }
        //TODO create find or in function name
        return $arr;
    }
}
