<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Packet_Speedy_Model extends MY_Model {
  public $table_name = 'MYARMS_PACKET_SPEEDY';
  public function __construct() {
    parent::__construct();
  }
}