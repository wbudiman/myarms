<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_Role_Model extends MY_Model {
  public $table_name = 'MYARMS_USER_ROLES';
  public function __construct() {
    parent::__construct();
  }
  public function lists()
  {
    $parameter=$this->limit!="" ? " WHERE ROWNUM >=".$this->offset." and ROWNUM<=".$this->limit : "";
    return $this->db->query("
      select * from MYARMS_USER_ROLES
    ".$parameter)->result();
  }
  public function get_modules($parent="",$user_role="")
  {
    return $this->db->query("
      select m.*,urd.USER_ROLE_DETAIL_ID,urd.USER_ROLE_DETAIL_ID as is_exist
      from MYARMS_MENU m
      left join MYARMS_USER_ROLE_DETAIL urd on  urd.USER_ROLE_ID='".$user_role."' and urd.MENU_ID=m.MENU_ID
      where m.PARENT='".$parent."'
      order by m.SORT ASC
    ")->result();
  }
}