<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Error_Model extends MY_Model {
  public $table_name = 'MYARMS_ERROR';
  public function __construct() {
    parent::__construct();
  }
}