<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');  

class If_Master_Model extends MY_Model {
  public $table_name = '';
  public function __construct() {
    parent::__construct();
  }
  
  public function lists($divisions="",$witels="",$datels="")
  {
    $query=$this->db->query("
      select distinct(l.CODE_LOCKET),l.CODE_LOCKET,ml.LOCKET_NAME from MYARMS_MAIN_LOCKET l
	  inner join MYARMS_LOCKET ml ON ml.CODE_LOCKET=l.CODE_LOCKET
	  inner join MYARMS_DIVISION_DETAIL dd on l.DATEL_CODE=dd.DATEL_CODE
	  inner join MYARMS_DATEL d on l.DATEL_CODE=d.DATEL_CODE
      inner join MYARMS_WITEL w on l.WITEL_CODE=w.WITEL_CODE
      where dd.DIVISION_CODE in ($divisions) and w.WITEL_CODE in ($witels) and d.DATEL_CODE in ($datels)
	  GROUP BY ml.LOCKET_NAME
      order by ml.LOCKET_NAME ASC
    ");
    return $query->result();
  }
  
  public function BNPDA($params=array())
  { 
    $periode1=$params['year_1'].$params['month_1'];
    $order_by="ORDER BY b.PERIODE DESC";
    $parameter="WHERE b.PERIODE='".$periode1."'";
	
    $parameter.=($params['no_jastel']!="''" && !empty($params['no_jastel']) ? " and ( b.ND LIKE '%".ucfirst($params['no_jastel'])."%' OR b.ND LIKE '%".strtolower($params['no_jastel'])."%' OR b.ND LIKE '%".strtoupper($params['no_jastel'])."%' )" : ""); 
    $query=$this->db->query("
      SELECT *
      FROM MYARMS_BN_PDA b
	  $parameter
      $order_by
    ");
	return $query->result();
  }
  
  public function PSB($params=array())
  { 
    $periode1=$params['year_1'].$params['month_1'];
    $order_by="ORDER BY b.PERIODE DESC";
    $parameter="WHERE b.PERIODE='".$periode1."'";
  
    $parameter.=($params['no_jastel']!="''" && !empty($params['no_jastel']) ? " and ( b.ND LIKE '%".ucfirst($params['no_jastel'])."%' OR b.ND LIKE '%".strtolower($params['no_jastel'])."%' OR b.ND LIKE '%".strtoupper($params['no_jastel'])."%' )" : ""); 
    $query=$this->db->query("
      SELECT *
      FROM MYARMS_PSB b
    $parameter
      $order_by
    ");
  return $query->result();
  }
	
	public function Cancel_Tgl_Cabut($params=array())
  { 
    $periode1=$params['year_1'].$params['month_1'];
    $order_by="ORDER BY b.PERIODE DESC";
    $parameter="WHERE b.PERIODE='".$periode1."'";
  
    $parameter.=($params['no_jastel']!="''" && !empty($params['no_jastel']) ? " and ( b.ND LIKE '%".ucfirst($params['no_jastel'])."%' OR b.ND LIKE '%".strtolower($params['no_jastel'])."%' OR b.ND LIKE '%".strtoupper($params['no_jastel'])."%' )" : ""); 
    $query=$this->db->query("
      SELECT *
      FROM MYARMS_DATE_CANCEL b
    $parameter
      $order_by
    ");
  return $query->result();
  }	
  
  public function Modification($params=array())
  { 
    $periode1=$params['year_1'].$params['month_1'];
    $order_by="ORDER BY b.PERIODE DESC";
    $parameter="WHERE b.PERIODE='".$periode1."'";
  
    $parameter.=($params['no_jastel']!="''" && !empty($params['no_jastel']) ? " and ( b.ND LIKE '%".ucfirst($params['no_jastel'])."%' OR b.ND LIKE '%".strtolower($params['no_jastel'])."%' OR b.ND LIKE '%".strtoupper($params['no_jastel'])."%' )" : ""); 
    $query=$this->db->query("
      SELECT *
      FROM MYARMS_MODIF_NUMBER b
    $parameter
      $order_by
    ");
  return $query->result();
  } 
}