<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Billing_Component_Model extends MY_Model {
  public $table_name = 'MYARMS_BILLING_COMPONENT';
  public function __construct() {
    parent::__construct();
  }
  public function all()
  {
    return $this->db->query("
      select * from MYARMS_BILLING_COMPONENT
      order by BILLING_COMPONENT_DESCRIPTION ASC
    ")->result();
  }
  public function list_detail_billing()
  {
    return $this->db->query("
      select * from MYARMS_BILLING_COMPONENT
      where BILLING_COMPONENT_FIELD NOT IN ('TAGIHAN','TOTAL_TAGIHAN','PPN')
      order by BILLING_COMPONENT_DESCRIPTION ASC
    ")->result();
  }
  public function list_billing_result()
  {
    return $this->db->query("
      select DISTINCT(KOMPONEN_BILL) from MYARMS_BILLING_RESULT_REVENUE where KOMPONEN_BILL is not null order by KOMPONEN_BILL ASC
    ")->result();
  }
  public function list_warm_billing_by_date()
  {
    return $this->db->query("
      select DISTINCT(KOMPONEN_BILLING) from MYARMS_WARM_BILLING where KOMPONEN_BILLING is not null order by KOMPONEN_BILLING ASC
    ")->result();
  }
}