<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usage_Status_Model extends MY_Model {
  public $table_name = 'MYARMS_USAGE_STATUS';
  public function __construct() {
    parent::__construct();
  }
}