<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Indihome_Model extends MY_Model {
  public $table_name = 'MYARMS_INDIHOME';
  public function __construct() {
    parent::__construct();
  }
}