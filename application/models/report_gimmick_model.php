<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');  

class Report_Gimmick_Model extends MY_Model {
  public $table_name = '';
  public function __construct() {
    parent::__construct();
  }
  public function GIMMICK_CATALOG($params=array())
  { 
    $periode1=$params['year_1'].$params['month_1'];
    $periode2=$params['year_2'].$params['month_2'];
    $order_by="ORDER BY GK.START_DATE DESC";
    $parameter=($periode1!="" && $periode2!="" ? " WHERE to_number(to_char(GK.START_DATE, 'YYYYMM'))='".$periode1."' OR (to_number(to_char(GK.START_END, 'YYYYMM')) ='".$periode2."')" : "");
    if($parameter=='')
      $parameter=($periode1!="" && $periode2=="" ? " WHERE to_number(to_char(GK.START_DATE, 'YYYYMM'))='".$periode1."'" : "");
    if($parameter=='')
      $parameter=($periode1=="" && $periode2!="" ? " WHERE to_number(to_char(GK.START_END, 'YYYYMM'))='".$periode2."'" : "");
    if($parameter=='')
      $parameter=($periode1=="" && $periode2=="" ? " WHERE to_number(to_char(GK.START_DATE, 'YYYYMM'))>='200001'" : "");
    $parameter.=($params['ubis']!="''" ? " and us.UBIS_ID in (".$params['ubis'].")" : "");
    $parameter.=($params['product']!="''" ? " and GK.PRODUCT_ID in (".$params['product'].")" : "");
    $parameter.=($params['ubis_segment']!="''" ? " and us.UBIS_SEGMENT_ID in (".$params['ubis_segment'].")" : "");
    $parameter.=($params['bisnis_area']!="''" ? " and GK.BUSINESS_AREA_ID in (".$params['bisnis_area'].")" : "");
	$parameter.=($params['keyword']!="''" ? " and ( GK.DESCRIPTION LIKE '%".ucfirst($params['keyword'])."%' OR GK.DESCRIPTION LIKE '%".strtolower($params['keyword'])."%' OR GK.DETAILLED_SUM LIKE '%".ucfirst($params['keyword'])."%' OR GK.DETAILLED_SUM LIKE '%".strtolower($params['keyword'])."%')" : "");
    $query=$this->db->query("
      SELECT 
        distinct(GK.DESCRIPTION) as DESCRIPTION,GK.START_DATE,GK.START_END,GK.DETAILLED_SUM,u.UBIS_ID,p.PRODUCT_ID,us.UBIS_SEGMENT_ID,GK.ID_PACK
      FROM MYARMS_GIMMICK_CATALOG GK
      LEFT JOIN MYARMS_PRODUCT p ON GK.PRODUCT_ID=p.PRODUCT_ID
      LEFT JOIN MYARMS_BUSINESS_AREA ba ON GK.BUSINESS_AREA_ID=ba.BUSINESS_AREA_ID
      LEFT JOIN MYARMS_UBIS_SEGMENT us ON ba.UBIS_SEGMENT_ID=us.UBIS_SEGMENT_ID
      LEFT JOIN MYARMS_UBIS u ON us.UBIS_ID=u.UBIS_ID
      $parameter
      $order_by
    ");
    return $query->result();
  }

  public function GIMMICK_CATALOG_detail($params=array())
  { 
    $order_by="ORDER BY GKD.PROMO_START DESC";
    $parameter="WHERE GKD.ID_PACK='".$params['id_pack']."'";
    $query=$this->db->query("
      SELECT distinct(GKD.DESCRIPTION) as DESCRIPTION,GKD.DESC_PACK_LINE,GKD.PROMO_START,GKD.PROMOTIONAL_PERIOD,GKD.END_PROMOTIONAL_DATE,GKD.PROMO_DISCOUNT,GKD.PROMO_UNIT
      FROM MYARMS_GIMMICK_CATALOG_DETAIL GKD
      $parameter
      $order_by
    ");
    return $query->result();
  }
  
  public function GIMMICK_AKTIF($params=array())
  { 
    $periode1=$params['year_1'].$params['month_1'];
    $order_by="ORDER BY GK.START_DATE DESC";
    $parameter=($periode1=="" ? " WHERE to_number(to_char(GK.START_DATE, 'YYYYMM'))>='200001'" : " WHERE (to_number(to_char(GK.START_DATE, 'YYYYMM'))<='".$periode1."' AND to_number(to_char(GK.START_END, 'YYYYMM'))>='".$periode1."') OR (to_number(to_char(GK.START_DATE, 'YYYYMM'))<='".$periode1."' AND to_number(to_char(GK.START_END, 'YYYYMM')) IS NULL)");
    
    $parameter.=($params['ubis']!="''" ? " and us.UBIS_ID in (".$params['ubis'].")" : "");
    $parameter.=($params['product']!="''" ? " and GK.PRODUCT_ID in (".$params['product'].")" : "");
    $parameter.=($params['ubis_segment']!="''" ? " and us.UBIS_SEGMENT_ID in (".$params['ubis_segment'].")" : "");
    $parameter.=($params['bisnis_area']!="''" ? " and GK.BUSINESS_AREA_ID in (".$params['bisnis_area'].")" : "");
    $parameter.=($params['keyword']!="''" ? " and ( GK.DESCRIPTION LIKE '%".ucfirst($params['keyword'])."%' OR GK.DESCRIPTION LIKE '%".strtolower($params['keyword'])."%' OR GK.DETAILLED_SUM LIKE '%".ucfirst($params['keyword'])."%' OR GK.DETAILLED_SUM LIKE '%".strtolower($params['keyword'])."%')" : "");
	$query=$this->db->query("
      SELECT 
        distinct(GK.DESCRIPTION) as DESCRIPTION,GK.START_DATE,GK.START_END,GK.DETAILLED_SUM,u.UBIS_ID,p.PRODUCT_ID,us.UBIS_SEGMENT_ID,GK.ID_PACK
      FROM MYARMS_GIMMICK_CATALOG GK
      LEFT JOIN MYARMS_PRODUCT p ON GK.PRODUCT_ID=p.PRODUCT_ID
      LEFT JOIN MYARMS_BUSINESS_AREA ba ON GK.BUSINESS_AREA_ID=ba.BUSINESS_AREA_ID
      LEFT JOIN MYARMS_UBIS_SEGMENT us ON ba.UBIS_SEGMENT_ID=us.UBIS_SEGMENT_ID
      LEFT JOIN MYARMS_UBIS u ON us.UBIS_ID=u.UBIS_ID
      $parameter
      $order_by
    ");
    return $query->result();
  }

  public function GIMMICK_AKTIF_detail($params=array())
  { 
    $order_by="ORDER BY GKD.PROMO_START DESC";
    $parameter="WHERE GKD.ID_PACK='".$params['id_pack']."'";
    $query=$this->db->query("
      SELECT distinct(GKD.DESCRIPTION) as DESCRIPTION,GKD.PROMO_START,GKD.PROMOTIONAL_PERIOD,GKD.END_PROMOTIONAL_DATE,GKD.PROMO_DISCOUNT,GKD.PROMO_UNIT,GKD.DESC_PACK_LINE
      FROM MYARMS_GIMMICK_CATALOG_DETAIL GKD
      $parameter
      $order_by
    ");
    return $query->result();
  }
  
  public function PELANGGAN_DPT_GIMMICK ($params=array())
  {
    $periode=$params['year_1'].$params['month_1'];
    $group_by=($params['row']!="" ? "GROUP BY ".$params['row'] : "");
    $order_by=($params['row']!="" ? "ORDER BY ".$params['row']." ASC" : "");
    $parameter=($periode=="" ? "WHERE to_number(to_char(l.END_DISC_TARIF, 'YYYYMM'))>='200001'" : "WHERE to_number(to_char(l.START_DISC_TARIF, 'YYYYMM'))='".$periode."' OR to_number(to_char(l.END_DISC_TARIF, 'YYYYMM'))='".$periode."'");
    $parameter.=($params['division']!="''" ? " and di.DIVISION_CODE in (".$params['division'].")" : "");
    $parameter.=($params['witel']!="''" ? " and w.WITEL_CODE in (".$params['witel'].")" : "");
    $parameter.=($params['datel']!="''" ? " and d.DATEL_CODE in (".$params['datel'].")" : "");
    $parameter.=($params['summary_product']!="''" ? " and p.SUMMARY_PRODUCT in (".$params['summary_product'].")" : "");
    $parameter.=($params['product']!="''" ? " and p.PRODUCT_ID in (".$params['product'].")" : "");
  
    $query=$this->db->query("
      SELECT
        ".$params['row'].($params['row']!="" ? "," : "")."
        sum(l.MNT_TCK) as AMOUNT, count(l.SND) as JUMLAH
      FROM MYARMS_PELANGGAN_GIMMICK l 
      INNER JOIN MYARMS_DIVISION_DETAIL dd ON dd.BUSINESS_AREA_ID=l.BUSINESS_AREA_ID
      INNER JOIN MYARMS_DIVISION di ON di.DIVISION_CODE=dd.DIVISION_CODE
      INNER JOIN MYARMS_DATEL d ON dd.DATEL_CODE=d.DATEL_CODE
      INNER JOIN MYARMS_WITEL w ON d.WITEL_CODE=w.WITEL_CODE
      LEFT JOIN MYARMS_PRODUCT p ON l.PRODUCT_ID=p.PRODUCT_ID
      $parameter
      $group_by
      $order_by
    ");
    return $query->result();
  }

  public function PELANGGAN_DPT_GIMMICK_detail ($params=array())
  {
    $periode=$params['year_1'].$params['month_1'];
    $group_by=($params['row']!="" ? "GROUP BY ".$params['row'] : "");
    $order_by=($params['row']!="" ? "ORDER BY ".$params['row']." ASC" : "");
    $parameter=($periode=="" ? "WHERE to_number(to_char(l.END_DISC_TARIF, 'YYYYMM'))>='200001'" : "WHERE to_number(to_char(l.START_DISC_TARIF, 'YYYYMM'))='".$periode."' OR to_number(to_char(l.END_DISC_TARIF, 'YYYYMM'))='".$periode."' AND di.".$params['field']."='".$params['val']."'");
    $parameter.=($params['division']!="''" ? " and di.DIVISION_CODE in (".$params['division'].")" : "");
    $parameter.=($params['witel']!="''" ? " and w.WITEL_CODE in (".$params['witel'].")" : "");
    $parameter.=($params['datel']!="''" ? " and d.DATEL_CODE in (".$params['datel'].")" : "");
    $parameter.=($params['summary_product']!="''" ? " and p.SUMMARY_PRODUCT in (".$params['summary_product'].")" : "");
    $parameter.=($params['product']!="''" ? " and p.PRODUCT_ID in (".$params['product'].")" : "");
    $limit=($params['limit']>0 ? "WHERE ROWNUM <=".$params['limit']."" : "");
    $query=$this->db->query("
      
      SELECT * FROM ( SELECT
        l.SND,l.NCLI,l.NDOS,l.KTCK,l.CITEM,l.NTICKET,l.MNT_TCK,l.START_DISC_TARIF,l.END_DISC_TARIF
      FROM MYARMS_PELANGGAN_GIMMICK l 
      INNER JOIN MYARMS_DIVISION_DETAIL dd ON dd.BUSINESS_AREA_ID=l.BUSINESS_AREA_ID
      INNER JOIN MYARMS_DIVISION di ON di.DIVISION_CODE=dd.DIVISION_CODE
      INNER JOIN MYARMS_DATEL d ON dd.DATEL_CODE=d.DATEL_CODE
      INNER JOIN MYARMS_WITEL w ON d.WITEL_CODE=w.WITEL_CODE
      LEFT JOIN MYARMS_PRODUCT p ON l.PRODUCT_ID=p.PRODUCT_ID
      $parameter
      ) $limit
    ");

    return $query->result();
  }

  public function PELANGGAN_BERAKHIR_GIMMICK ($params=array())
  {
    $periode=$params['year_1'].$params['month_1'];
    $group_by=($params['row']!="" ? "GROUP BY ".$params['row'] : "");
    $order_by=($params['row']!="" ? "ORDER BY ".$params['row']." ASC" : "");
    $parameter="WHERE to_number(to_char(l.END_DISC_TARIF, 'YYYYMM'))='".$periode."'";

    $parameter=($periode=="" ? "WHERE to_number(to_char(l.END_DISC_TARIF, 'YYYYMM'))>='200001'" : "WHERE to_number(to_char(l.END_DISC_TARIF, 'YYYYMM'))='".$periode."' ");


    $parameter.=($params['division']!="''" ? " and di.DIVISION_CODE in (".$params['division'].")" : "");
    $parameter.=($params['witel']!="''" ? " and w.WITEL_CODE in (".$params['witel'].")" : "");
    $parameter.=($params['datel']!="''" ? " and d.DATEL_CODE in (".$params['datel'].")" : "");
    $parameter.=($params['summary_product']!="''" ? " and p.SUMMARY_PRODUCT in (".$params['summary_product'].")" : "");
    $parameter.=($params['product']!="''" ? " and p.PRODUCT_ID in (".$params['product'].")" : "");
  
    $query=$this->db->query("
      SELECT
        ".$params['row'].($params['row']!="" ? "," : "")."
        sum(l.MNT_TCK) as AMOUNT, count(l.SND) as JUMLAH
      FROM MYARMS_PELANGGAN_GIMMICK l 
      INNER JOIN MYARMS_DIVISION_DETAIL dd ON dd.BUSINESS_AREA_ID=l.BUSINESS_AREA_ID
      INNER JOIN MYARMS_DIVISION di ON di.DIVISION_CODE=dd.DIVISION_CODE
      INNER JOIN MYARMS_DATEL d ON dd.DATEL_CODE=d.DATEL_CODE
      INNER JOIN MYARMS_WITEL w ON d.WITEL_CODE=w.WITEL_CODE
      LEFT JOIN MYARMS_PRODUCT p ON l.PRODUCT_ID=p.PRODUCT_ID
      $parameter
      $group_by
      $order_by
    ");

    return $query->result();
  }

  public function PELANGGAN_BERAKHIR_GIMMICK_detail ($params=array())
  {
    $periode=$params['year_1'].$params['month_1'];
    $group_by=($params['row']!="" ? "GROUP BY ".$params['row'] : "");
    $order_by=($params['row']!="" ? "ORDER BY ".$params['row']." ASC" : "");
    $parameter=($periode=="" ? "WHERE to_number(to_char(l.END_DISC_TARIF, 'YYYYMM'))>='200001'" : "WHERE to_number(to_char(l.END_DISC_TARIF, 'YYYYMM'))='".$periode."' AND di.".$params['field']."='".$params['val']."'");
    $parameter.=($params['division']!="''" ? " and di.DIVISION_CODE in (".$params['division'].")" : "");
    $parameter.=($params['witel']!="''" ? " and w.WITEL_CODE in (".$params['witel'].")" : "");
    $parameter.=($params['datel']!="''" ? " and d.DATEL_CODE in (".$params['datel'].")" : "");
    $parameter.=($params['summary_product']!="''" ? " and p.SUMMARY_PRODUCT in (".$params['summary_product'].")" : "");
    $parameter.=($params['product']!="''" ? " and p.PRODUCT_ID in (".$params['product'].")" : "");
    $limit=($params['limit']>0 ? "WHERE ROWNUM <=".$params['limit']."" : "");
    $query=$this->db->query("
      SELECT * FROM (
      SELECT
        l.SND,l.NCLI,l.NDOS,l.KTCK,l.CITEM,l.NTICKET,l.MNT_TCK,l.START_DISC_TARIF,l.END_DISC_TARIF
      FROM MYARMS_PELANGGAN_GIMMICK l 
      INNER JOIN MYARMS_DIVISION_DETAIL dd ON dd.BUSINESS_AREA_ID=l.BUSINESS_AREA_ID
      INNER JOIN MYARMS_DIVISION di ON di.DIVISION_CODE=dd.DIVISION_CODE
      INNER JOIN MYARMS_DATEL d ON dd.DATEL_CODE=d.DATEL_CODE
      INNER JOIN MYARMS_WITEL w ON d.WITEL_CODE=w.WITEL_CODE
      LEFT JOIN MYARMS_PRODUCT p ON l.PRODUCT_ID=p.PRODUCT_ID
      $parameter ) $limit
    ");

    return $query->result();
  } 
}