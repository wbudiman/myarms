<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Billing_Type_Model extends MY_Model {
  public $table_name = 'MYARMS_BILLING_TYPE';
  public function __construct() {
    parent::__construct();
  }
}