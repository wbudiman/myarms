<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');  

class Loket_Model extends MY_Model {
  public $table_name = '';
  public function __construct() {
    parent::__construct();
  }
  
  public function lists($divisions="",$witels="",$datels="")
  {
    $query=$this->db->query("
      select ml.CODE_LOCKET,ml.LOCKET_NAME from MYARMS_LOCKET ml INNER JOIN MYARMS_MAIN_LOCKET l ON ml.CODE_LOCKET=l.CODE_LOCKET
	  inner join MYARMS_DIVISION_DETAIL dd on l.DATEL_CODE=dd.DATEL_CODE
	  inner join MYARMS_DATEL d on l.DATEL_CODE=d.DATEL_CODE
      inner join MYARMS_WITEL w on l.WITEL_CODE=w.WITEL_CODE
      where dd.DIVISION_CODE in ($divisions) and w.WITEL_CODE in ($witels) and d.DATEL_CODE in ($datels)
    ");
    return $query->result();
  }
  
  public function LOKET_SUMMARY_detail($params=array())
  { 
    $periode1=$params['year_1'].$params['month_1'];
    $order_by="ORDER BY l.VALIDATION_DATE DESC";
    $parameter="WHERE to_number(to_char(l.VALIDATION_DATE, 'YYYYMM'))='".$periode1."' AND ".$params['field']."='".$params['val']."' AND ".$params['fieldcol']."='".$params['valcol']."'";
	
    $parameter.=($params['division']!="''" ? " and dd.DIVISION_CODE in (".$params['division'].")" : "");
    $parameter.=($params['witel']!="''" ? " and w.WITEL_CODE in (".$params['witel'].")" : "");
    $parameter.=($params['datel']!="''" ? " and d.DATEL_CODE in (".$params['datel'].")" : "");
	$parameter.=($params['loket']!="''" ? " and l.CODE_LOCKET in (".$params['loket'].")" : "");
	$parameter.=($params['validasi']!="''" && !empty($params['validasi']) ? " and l.STATUS_VALIDATION ='".$params['validasi']."'" : ""); 
	$parameter.=($params['petugas']!="''" && !empty($params['petugas']) ? " and (l.PTGS_CASH_DESK LIKE '%".ucfirst($params['petugas'])."%' OR l.PTGS_CASH_DESK LIKE '%".strtolower($params['petugas'])."%' OR l.PTGS_CASH_DESK LIKE '%".strtoupper($params['petugas'])."%') " : "");
	//$parameter.=($params['keyword']!="''" && !empty($params['keyword']) ? " and ( w.WITEL_DESCRIPTION LIKE '%".ucfirst($params['keyword'])."%' OR w.WITEL_DESCRIPTION LIKE '%".strtolower($params['keyword'])."%' OR d.DATEL_DESCRIPTION LIKE '%".ucfirst($params['keyword'])."%' OR d.DATEL_DESCRIPTION LIKE '%".strtolower($params['keyword'])."%' or l.LOCATION LIKE '%".ucfirst($params['keyword'])."%' or l.LOCATION LIKE '%".strtolower($params['keyword'])."%' or l.CODE_LOCKET LIKE '%".ucfirst($params['keyword'])."%' or l.CODE_LOCKET LIKE '%".strtolower($params['keyword'])."%' or ll.LOCKET_NAME LIKE '%".ucfirst($params['keyword'])."%' or ll.LOCKET_NAME LIKE '%".strtolower($params['keyword'])."%' or l.PTGS_CASH_DESK LIKE '%".ucfirst($params['keyword'])."%' or l.PTGS_CASH_DESK LIKE '%".strtolower($params['keyword'])."%' or l.TRANSACTION_TYPE LIKE '%".ucfirst($params['keyword'])."%' or l.TRANSACTION_TYPE LIKE '%".strtolower($params['keyword'])."%')" : ""); 
    $query=$this->db->query("
      SELECT l.WITEL_CODE,w.WITEL_DESCRIPTION,l.DATEL_CODE,d.DATEL_DESCRIPTION,l.CODE_LOCKET,ll.LOCKET_NAME,l.STEP,l.PTGS_CASH_DESK,l.TRANSACTION_TYPE,l.STEP_OPEN_DATE,l.STEP_CLOSE_DATE,l.AMOUNT,l.SST,l.STATUS_VALIDATION,l.VALIDATION_DATE
      FROM MYARMS_MAIN_LOCKET l 
      INNER JOIN MYARMS_WITEL w ON l.WITEL_CODE=w.WITEL_CODE
      INNER JOIN MYARMS_DATEL d ON l.DATEL_CODE=d.DATEL_CODE
      inner join MYARMS_DIVISION_DETAIL dd on l.DATEL_CODE=dd.DATEL_CODE
      LEFT JOIN MYARMS_LOCKET ll ON l.CODE_LOCKET=ll.CODE_LOCKET
	  $parameter
      $order_by
    ");
	return $query->result();
  }
  
  public function LOKET_SUMMARY($params=array())
  {
    $periode=$params['year_1'].$params['month_1'];
    $group_by=($params['row']!="" || $params['column']!="" ? "GROUP BY ".$params['row'].($params['row']!="" && $params['column']!="" ? "," : "").$params['column'] : "");
    $order_by=($params['row']!="" ? "ORDER BY ".$params['row']." ASC" : "");
    $parameter="WHERE to_number(to_char(l.VALIDATION_DATE, 'YYYYMM'))='".$periode."' ";
    $parameter.=($params['division']!="''" ? " and ll.DIVISION_CODE in (".$params['division'].")" : "");
    $parameter.=($params['witel']!="''" ? " and w.WITEL_CODE in (".$params['witel'].")" : "");
    $parameter.=($params['datel']!="''" ? " and d.DATEL_CODE in (".$params['datel'].")" : "");
	$parameter.=($params['loket']!="''" ? " and l.CODE_LOCKET in (".$params['loket'].")" : "");
	$parameter.=($params['validasi']!="''" && !empty($params['validasi']) ? " and l.STATUS_VALIDATION ='".$params['validasi']."'" : ""); 
	$parameter.=($params['petugas']!="''" && !empty($params['petugas']) ? " and (l.PTGS_CASH_DESK LIKE '%".ucfirst($params['petugas'])."%' OR l.PTGS_CASH_DESK LIKE '%".strtolower($params['petugas'])."%' OR l.PTGS_CASH_DESK LIKE '%".strtoupper($params['petugas'])."%') " : "");
	//$parameter.=($params['keyword']!="''" && !empty($params['keyword']) ? " and ( w.WITEL_DESCRIPTION LIKE '%".ucfirst($params['keyword'])."%' OR w.WITEL_DESCRIPTION LIKE '%".strtolower($params['keyword'])."%' OR d.DATEL_DESCRIPTION LIKE '%".ucfirst($params['keyword'])."%' OR d.DATEL_DESCRIPTION LIKE '%".strtolower($params['keyword'])."%' or l.LOCATION LIKE '%".ucfirst($params['keyword'])."%' or l.LOCATION LIKE '%".strtolower($params['keyword'])."%' or l.CODE_LOCKET LIKE '%".ucfirst($params['keyword'])."%' or l.CODE_LOCKET LIKE '%".strtolower($params['keyword'])."%' or ll.LOCKET_NAME LIKE '%".ucfirst($params['keyword'])."%' or ll.LOCKET_NAME LIKE '%".strtolower($params['keyword'])."%' or l.PTGS_CASH_DESK LIKE '%".ucfirst($params['keyword'])."%' or l.PTGS_CASH_DESK LIKE '%".strtolower($params['keyword'])."%' or l.TRANSACTION_TYPE LIKE '%".ucfirst($params['keyword'])."%' or l.TRANSACTION_TYPE LIKE '%".strtolower($params['keyword'])."%')" : ""); 
	$query=$this->db->query("
      SELECT
        ".$params['row'].($params['row']!="" ? "," : "").$params['column'].($params['column']!="" ? "," : "")."
        sum(l.AMOUNT) as AMOUNT,sum(l.SST) as SST
      FROM MYARMS_MAIN_LOCKET l 
      INNER JOIN MYARMS_WITEL w ON l.WITEL_CODE=w.WITEL_CODE
      INNER JOIN MYARMS_DATEL d ON l.DATEL_CODE=d.DATEL_CODE
      LEFT JOIN MYARMS_LOCKET ll ON l.CODE_LOCKET=ll.CODE_LOCKET
	  LEFT JOIN MYARMS_DIVISION_DETAIL  dd ON l.DATEL_CODE=dd.DATEL_CODE
    LEFT JOIN MYARMS_DIVISION  di ON dd.DIVISION_CODE=di.DIVISION_CODE
      $parameter
      $group_by
      $order_by
    ");
    return $query->result();
  }
	
  public function DETAIL_LOKET_SUMMARY($params=array())
  { 
    $periode1=$params['year_1'].$params['month_1'];
    $order_by="ORDER BY l.VALIDATION_DATE DESC";
    $parameter="WHERE to_number(to_char(l.VALIDATION_DATE, 'YYYYMM'))='".$periode1."'";
  
    $parameter.=($params['division']!="''" ? " and dd.DIVISION_CODE in (".$params['division'].")" : "");
    $parameter.=($params['witel']!="''" ? " and w.WITEL_CODE in (".$params['witel'].")" : "");
    $parameter.=($params['datel']!="''" ? " and d.DATEL_CODE in (".$params['datel'].")" : "");
  $parameter.=($params['loket']!="''" ? " and l.CODE_LOCKET in (".$params['loket'].")" : "");
  $parameter.=($params['validasi']!="''" && !empty($params['validasi']) ? " and l.STATUS_VALIDATION ='".$params['validasi']."'" : ""); 
  $parameter.=($params['petugas']!="''" && !empty($params['petugas']) ? " and (l.PTGS_CASH_DESK LIKE '%".ucfirst($params['petugas'])."%' OR l.PTGS_CASH_DESK LIKE '%".strtolower($params['petugas'])."%' OR l.PTGS_CASH_DESK LIKE '%".strtoupper($params['petugas'])."%') " : "");
  //$parameter.=($params['keyword']!="''" && !empty($params['keyword']) ? " and ( w.WITEL_DESCRIPTION LIKE '%".ucfirst($params['keyword'])."%' OR w.WITEL_DESCRIPTION LIKE '%".strtolower($params['keyword'])."%' OR d.DATEL_DESCRIPTION LIKE '%".ucfirst($params['keyword'])."%' OR d.DATEL_DESCRIPTION LIKE '%".strtolower($params['keyword'])."%' or l.LOCATION LIKE '%".ucfirst($params['keyword'])."%' or l.LOCATION LIKE '%".strtolower($params['keyword'])."%' or l.CODE_LOCKET LIKE '%".ucfirst($params['keyword'])."%' or l.CODE_LOCKET LIKE '%".strtolower($params['keyword'])."%' or ll.LOCKET_NAME LIKE '%".ucfirst($params['keyword'])."%' or ll.LOCKET_NAME LIKE '%".strtolower($params['keyword'])."%' or l.PTGS_CASH_DESK LIKE '%".ucfirst($params['keyword'])."%' or l.PTGS_CASH_DESK LIKE '%".strtolower($params['keyword'])."%' or l.TRANSACTION_TYPE LIKE '%".ucfirst($params['keyword'])."%' or l.TRANSACTION_TYPE LIKE '%".strtolower($params['keyword'])."%')" : ""); 
    $query=$this->db->query("
      SELECT l.WITEL_CODE,w.WITEL_DESCRIPTION,l.DATEL_CODE,d.DATEL_DESCRIPTION,l.CODE_LOCKET,ll.LOCKET_NAME,l.STEP,l.PTGS_CASH_DESK,l.TRANSACTION_TYPE,l.STEP_OPEN_DATE,l.STEP_CLOSE_DATE,l.AMOUNT,l.SST,l.STATUS_VALIDATION,l.VALIDATION_DATE
      FROM MYARMS_MAIN_LOCKET l 
      INNER JOIN MYARMS_WITEL w ON l.WITEL_CODE=w.WITEL_CODE
      INNER JOIN MYARMS_DATEL d ON l.DATEL_CODE=d.DATEL_CODE
      inner join MYARMS_DIVISION_DETAIL dd on l.DATEL_CODE=dd.DATEL_CODE
      LEFT JOIN MYARMS_LOCKET ll ON l.CODE_LOCKET=ll.CODE_LOCKET
    $parameter
      $order_by
    ");
  return $query->result();
  }
	public function LOKET_BO($params=array())
  { 
    
    $order_by="ORDER BY l.BUSINESS_AREA_ID ASC";
    $parameter="WHERE l.BUSINESS_AREA_ID ='R104'";
    //$parameter.=($params['division']!="''" ? " and ll.DIVISION_CODE in (".$params['division'].")" : "");
    //$parameter.=($params['witel']!="''" ? " and w.WITEL_CODE in (".$params['witel'].")" : "");
    //$parameter.=($params['datel']!="''" ? " and d.DATEL_CODE in (".$params['datel'].")" : "");
	$parameter.=($params['loket']!="''" ? " and l.CODE_LOCKET in (".$params['loket'].")" : "");
	$parameter.=($params['deskloket']!="''" && !empty($params['deskloket']) ? " and (ll.LOCKET_NAME LIKE '%".ucfirst($params['deskloket'])."%' OR ll.LOCKET_NAME LIKE '%".strtolower($params['deskloket'])."%' OR ll.LOCKET_NAME LIKE '%".strtoupper($params['deskloket'])."%')" : ""); 
	$parameter.=($params['deskakun']!="''" && !empty($params['deskakun']) ? " and (l.DESCRIPTION_ACCOUNT_BO LIKE '%".ucfirst($params['keyword'])."%' OR l.DESCRIPTION_ACCOUNT_BO LIKE '%".strtolower($params['keyword'])."%' OR l.DESCRIPTION_ACCOUNT_BO LIKE '%".strtoupper($params['keyword'])."%')" : "");
	//$parameter.=($params['keyword']!="''" && !empty($params['keyword']) ? " and ( w.WITEL_DESCRIPTION LIKE '%".ucfirst($params['keyword'])."%' OR w.WITEL_DESCRIPTION LIKE '%".strtolower($params['keyword'])."%' OR d.DATEL_DESCRIPTION LIKE '%".ucfirst($params['keyword'])."%' OR d.DATEL_DESCRIPTION LIKE '%".strtolower($params['keyword'])."%' or l.LOCATION LIKE '%".ucfirst($params['keyword'])."%' or l.LOCATION LIKE '%".strtolower($params['keyword'])."%' or l.CODE_LOCKET LIKE '%".ucfirst($params['keyword'])."%' or l.CODE_LOCKET LIKE '%".strtolower($params['keyword'])."%' or ll.LOCKET_NAME LIKE '%".ucfirst($params['keyword'])."%' or ll.LOCKET_NAME LIKE '%".strtolower($params['keyword'])."%' or l.PTGS_CASH_DESK LIKE '%".ucfirst($params['keyword'])."%' or l.PTGS_CASH_DESK LIKE '%".strtolower($params['keyword'])."%' or l.TRANSACTION_TYPE LIKE '%".ucfirst($params['keyword'])."%' or l.TRANSACTION_TYPE LIKE '%".strtolower($params['keyword'])."%')" : ""); 
    $query=$this->db->query("
      SELECT w.WITEL_DESCRIPTION,d.DATEL_DESCRIPTION,l.KD_LOCATION,l.CODE_LOCKET,ll.LOCKET_NAME,l.LOCKET_TYPE,l.NO_BO,l.NO_ACCOUNT_BO,l.DESCRIPTION_ACCOUNT_BO,l.BUSINESS_AREA_ID,l.DESCRIPTION_AREA      
      FROM MYARMS_MAP_BO l 
      LEFT JOIN MYARMS_LOCKET ll ON l.CODE_LOCKET=ll.CODE_LOCKET
      INNER JOIN MYARMS_DIVISION_DETAIL dd ON dd.BUSINESS_AREA_ID=l.BUSINESS_AREA_ID
      INNER JOIN MYARMS_DATEL d ON d.DATEL_CODE=dd.DATEL_CODE
      INNER JOIN MYARMS_WITEL w ON d.WITEL_CODE=w.WITEL_CODE

	  $parameter
      $order_by
    ");
	return $query->result();
  }	
  
  public function DETAIL_PSB($params=array())
  { 
    $periode1=$params['year_1'].$params['month_1'];
    $order_by="ORDER BY l.DATE_OPEN_STEP DESC";
    $parameter="WHERE l.PERIODE='".$periode1."'";
	
    $parameter.=($params['division']!="''" ? " and ll.DIVISION_CODE in (".$params['division'].")" : "");
	$parameter.=($params['product']!="''" ? " and l.PRODUCT_ID in (".$params['product'].")" : "");
  $parameter.=($params['witel']!="''" ? " and w.WITEL_CODE in (".$params['witel'].")" : "");
  $parameter.=($params['datel']!="''" ? " and d.DATEL_CODE in (".$params['datel'].")" : "");
	$parameter.=($params['loket']!="''" ? " and l.CODE_ID in (".$params['loket'].")" : "");
	$parameter.=($params['validasi']!="''" && !empty($params['validasi']) ? " and l.VALIDATION_STATUS ='".$params['validasi']."'" : ""); 
	$parameter.=($params['petugas']!="''" && !empty($params['petugas']) ? " and l.PTGS_CASH_DESK LIKE '%".$params['petugas']."%'" : "");
	//$parameter.=($params['keyword']!="''" && !empty($params['keyword']) ? " and ( w.WITEL_DESCRIPTION LIKE '%".ucfirst($params['keyword'])."%' OR w.WITEL_DESCRIPTION LIKE '%".strtolower($params['keyword'])."%' OR d.DATEL_DESCRIPTION LIKE '%".ucfirst($params['keyword'])."%' OR d.DATEL_DESCRIPTION LIKE '%".strtolower($params['keyword'])."%' or l.LOCATION LIKE '%".ucfirst($params['keyword'])."%' or l.LOCATION LIKE '%".strtolower($params['keyword'])."%' or l.CODE_LOCKET LIKE '%".ucfirst($params['keyword'])."%' or l.CODE_LOCKET LIKE '%".strtolower($params['keyword'])."%' or ll.LOCKET_NAME LIKE '%".ucfirst($params['keyword'])."%' or ll.LOCKET_NAME LIKE '%".strtolower($params['keyword'])."%' or l.PTGS_CASH_DESK LIKE '%".ucfirst($params['keyword'])."%' or l.PTGS_CASH_DESK LIKE '%".strtolower($params['keyword'])."%' or l.TRANSACTION_TYPE LIKE '%".ucfirst($params['keyword'])."%' or l.TRANSACTION_TYPE LIKE '%".strtolower($params['keyword'])."%')" : ""); 
    $query=$this->db->query("
      SELECT w.WITEL_DESCRIPTION,d.DATEL_DESCRIPTION,l.BUSINESS_AREA_ID, l.AREA,l.LOCATION_ID,loc.LOCATION_NAME,l.CODE_LOCKET, ll.LOCKET_NAME,l.NO_TRANSACTION, l.STEP, l.NO_FACTURE,l.NCLI,l.NDOS,l.JUMLAH, l.PTGS_CASH_DESK, l.DATE_OPEN_STEP, l.DATE_CLOSE_STEP, l.VALIDATION_STATUS
      FROM MYARMS_DETAIL_PSB l 
      LEFT JOIN MYARMS_LOCKET ll ON l.CODE_LOCKET=ll.CODE_LOCKET
      LEFT JOIN MYARMS_LOCATION loc ON l.LOCATION_ID=loc.LOCATION_NAME
      INNER JOIN MYARMS_DIVISION_DETAIL dd ON dd.BUSINESS_AREA_ID=l.BUSINESS_AREA_ID
      INNER JOIN MYARMS_DATEL d ON d.DATEL_CODE=dd.DATEL_CODE
      INNER JOIN MYARMS_WITEL w ON d.WITEL_CODE=w.WITEL_CODE
	  $parameter
      $order_by
    ");
	return $query->result();
  }
}