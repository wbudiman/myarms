<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Access_Log_Model extends MY_Model {
  public $table_name = 'MYARMS_ACCESS_LOG';
  /*
    0=index/detail 
    1=create
    2=edit
    3=delete
    4=download data summary
    5=download data detail
  */
  public function __construct() {
    parent::__construct();
  }
}