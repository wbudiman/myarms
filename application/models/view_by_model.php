<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class View_By_Model extends MY_Model {
  public $table_name = 'MYARMS_VIEW_BY';
  public function __construct() {
    parent::__construct();
  }
  public function list_detail_billing()
  {
    return $this->db->query("
      select * from MYARMS_VIEW_BY
      where VIEW_BY IN ('AMOUNT','L11','ARPU')
      order by VIEW_BY_DESCRIPTION ASC
    ")->result();
  }
  public function list_warm_billing()
  {
    return $this->db->query("
      select * from MYARMS_VIEW_BY
      where VIEW_BY IN ('TOTAL_RECORD','TOTAL_DURATION','TOTAL_RUPIAH')
      order by VIEW_BY_DESCRIPTION ASC
    ")->result();
  }
  public function list_new_wave_indihome_revenue()
  {
    return $this->db->query("
      select * from MYARMS_VIEW_BY
      where VIEW_BY IN ('AMOUNT','L11')
      order by VIEW_BY_DESCRIPTION ASC
    ")->result();
  }
  public function list_loket_summary()
  {
    return $this->db->query("
      select * from MYARMS_VIEW_BY
      where VIEW_BY IN ('AMOUNT','SST')
      order by VIEW_BY_DESCRIPTION ASC
    ")->result();
  }

  public function list_group_tunggakan()
  {
    return $this->db->query("
      select * from MYARMS_VIEW_BY
      where VIEW_BY IN ('AMOUNT','L11')
      order by VIEW_BY_DESCRIPTION ASC
    ")->result();
  }
  
}