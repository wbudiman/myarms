<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');  

class Rekon_Finnet_Model extends MY_Model {
  public $table_name = '';
  public function __construct() {
    parent::__construct();
  }
  
  public function TEL75 ($params=array())
  {
    $periode=$params['year_1'].$params['month_1'];
    $group_by=($params['row']!="" ? "GROUP BY ".$params['row'] : "");
    $order_by=($params['row']!="" ? "ORDER BY ".$params['row']." ASC" : "");
    $parameter=($periode!="" ? " WHERE to_number(to_char(l.TGL_ENTRY, 'YYYYMM'))>='".$periode."'" : "");
    $parameter.=($params['ubis']!="''" ? " and u.UBIS_ID in (".$params['ubis'].")" : "");
    $parameter.=($params['bisnis_area']!="''" ? " and ba.BUSINESS_AREA_ID in (".$params['bisnis_area'].")" : "");
    $parameter.=($params['bank']!="''" ? " and l.BANK_CODE in (".$params['bank'].")" : "");
    $parameter.=($params['ba_pay']!="''" ? " and bua.BUSINESS_AREA_ID in (".$params['ba_pay'].")" : "");
    $parameter.=($params['plcl']!="''" && !empty($params['plcl']) ? " and l.PL_CL ='".$params['plcl']."'" : ""); 
  
    $query=$this->db->query("
      SELECT
        ".$params['row'].($params['row']!="" ? "," : "")."
        sum(l.TAGIHAN) as TAGIHAN,sum(l.PPN) as PPN,sum(l.CICILAN) as CICILAN, sum(l.MATERAI) as MATERAI, sum(l.DENDA) as DENDA , sum(l.TOTAL_BAYAR) as TOTAL_BAYAR, sum(l.LEMBAR) as LEMBAR, sum(l.TRANSAKSI) as TRANSAKSI
      FROM MYARMS_TEL75 l 
      INNER JOIN MYARMS_BUSINESS_AREA ba ON ba.BUSINESS_AREA_ID=l.BA_BILL
      LEFT JOIN MYARMS_BUSINESS_AREA bua ON bua.BUSINESS_AREA_ID=l.BA_PAY
      INNER JOIN MYARMS_MAPPING_BANK mb ON mb.BANK_CODE=l.BANK_CODE
      LEFT JOIN MYARMS_UBIS_SEGMENT us ON us.UBIS_SEGMENT_ID=ba.UBIS_SEGMENT_ID
      LEFT JOIN MYARMS_UBIS u on u.UBIS_ID=us.UBIS_ID
      $parameter
      $group_by
      $order_by
    ");
    return $query->result();
  }

  public function PEMBATALAN_TEL75 ($params=array())
  {
    $periode=$params['year_1'].$params['month_1'];
    $group_by=($params['row']!="" ? "GROUP BY ".$params['row'] : "");
    $order_by=($params['row']!="" ? "ORDER BY ".$params['row']." ASC" : "");
    $parameter=($periode=="" ? "WHERE l.PERIODE IS NOT NULL " : "WHERE l.PERIODE='".$periode."' ");
    $parameter.=($params['ubis']!="''" ? " and u.UBIS_ID in (".$params['ubis'].")" : "");
    $parameter.=($params['bisnis_area']!="''" ? " and ba.BUSINESS_AREA_ID in (".$params['bisnis_area'].")" : "");
    $parameter.=($params['bank']!="''" ? " and l.BANK_CODE in (".$params['bank'].")" : "");
    $parameter.=($params['ba_pay']!="''" ? " and bua.BUSINESS_AREA_ID in (".$params['ba_pay'].")" : "");
  
    $query=$this->db->query("
      SELECT
        ".$params['row'].($params['row']!="" ? "," : "")."
        sum(l.TAGIHAN) as TAGIHAN,sum(l.CICILAN) as CICILAN, sum(l.MATERAI) as MATERAI, sum(l.DENDA) as DENDA , sum(l.TOTAL_CANCEL) as TOTAL_CANCEL, sum(l.LEMBAR_CANCEL) as LEMBAR_CANCEL
      FROM MYARMS_PEMBATALAN_TEL75 l 
      INNER JOIN MYARMS_BUSINESS_AREA ba ON ba.BUSINESS_AREA_ID=l.BA_BILL
      LEFT JOIN MYARMS_BUSINESS_AREA bua ON bua.BUSINESS_AREA_ID=l.BA_PAY
      INNER JOIN MYARMS_MAPPING_BANK mb ON mb.BANK_CODE=l.BANK_CODE
      LEFT JOIN MYARMS_UBIS_SEGMENT us ON us.UBIS_SEGMENT_ID=ba.UBIS_SEGMENT_ID
      LEFT JOIN MYARMS_UBIS u on u.UBIS_ID=us.UBIS_ID
      $parameter
      $group_by
      $order_by
    ");
    return $query->result();
  }

  public function GXTEL72 ($params=array())
  {
    $params['day_1']=($params['day_1']<10 ? "0".$params['day_1'] : $params['day_1']); 
    $periode=$params['year_1'].$params['month_1'].$params['day_1'];
    $group_by=($params['row']!="" ? "GROUP BY ".$params['row'] : "");
    $order_by=($params['row']!="" ? "ORDER BY ".$params['row']." ASC" : "");
    $parameter=($periode!="" ? " WHERE to_number(to_char(l.PAYDATE, 'YYYYMMDD'))>='".$periode."'" : "");
    $parameter.=($params['bank']!="''" ? " and l.BANK_CODE in (".$params['bank'].")" : "");
  
    $query=$this->db->query("
      SELECT
        ".$params['row'].($params['row']!="" ? "," : "")."
        sum(l.AMOUNT) as AMOUNT,sum(l.TRANSAKSI) as TRANSAKSI
      FROM MYARMS_GXTEL72 l 
      INNER JOIN MYARMS_MAPPING_BANK mb ON mb.BANK_CODE=l.BANK_CODE
      $parameter
      $group_by
      $order_by
    ");
    return $query->result();
    /*echo "
      SELECT
        ".$params['row'].($params['row']!="" ? "," : "")."
        sum(l.AMOUNT) as AMOUNT,sum(l.TRANSAKSI) as TRANSAKSI
      FROM MYARMS_GXTEL72 l 
      INNER JOIN MYARMS_MAPPING_BANK mb ON mb.BANK_CODE=l.BANK_CODE
      $group_by
      $order_by";*/
  }

  public function OSA ($params=array())
  {
    $params['day_1']=($params['day_1']<10 ? "0".$params['day_1'] : $params['day_1']); 
    $periode=$params['year_1'].$params['month_1'].$params['day_1'];
    $group_by=($params['row']!="" ? "GROUP BY ".$params['row'] : "");
    $order_by=($params['row']!="" ? "ORDER BY ".$params['row']." ASC" : "");
    $parameter=($periode=="" ? "WHERE l.PERIODE IS NOT NULL " : "WHERE l.PERIODE='".$periode."' ");
    $parameter.=($params['bank']!="''" ? " and l.BANK_CODE in (".$params['bank'].")" : "");
    $parameter.=($params['status']!="''" && !empty($params['status']) ? " and l.STATUS ='".$params['status']."'" : ""); 
  
    $query=$this->db->query("
      SELECT
        ".$params['row'].($params['row']!="" ? "," : "")."
        sum(l.TRX) as TRX,sum(l.BILL) as BILL,sum(l.AMOUNT) as AMOUNT
      FROM MYARMS_OSA l 
      INNER JOIN MYARMS_MAPPING_BANK mb ON mb.BANK_CODE=l.BANK_CODE
      $parameter
      $group_by
      $order_by
    ");
    return $query->result();
  }
   
  public function KOMPARASI ($params=array())
  {
    $periode=$params['year_1'].$params['month_1'];
    $group_by="";//$group_by=($params['row']!="" ? "GROUP BY ".$params['row'] : "");
    $order_by="";//$order_by=($params['row']!="" ? "ORDER BY ".$params['row']." ASC" : "");
    $parameter=($periode!="" ? " WHERE to_number(to_char(l.TGL_ENTRY, 'YYYYMM'))>='".$periode."'" : "");
    /*$parameter.=($params['ubis']!="''" ? " and u.UBIS_ID in (".$params['ubis'].")" : "");
    $parameter.=($params['bisnis_area']!="''" ? " and ba.BUSINESS_AREA_ID in (".$params['bisnis_area'].")" : "");
    $parameter.=($params['bank']!="''" ? " and l.BANK_CODE in (".$params['bank'].")" : "");
    $parameter.=($params['ba_pay']!="''" ? " and bua.BUSINESS_AREA_ID in (".$params['ba_pay'].")" : "");
    $parameter.=($params['plcl']!="''" && !empty($params['plcl']) ? " and l.PL_CL ='".$params['plcl']."'" : ""); */
  
    /*$query=$this->db->query("
      SELECT
        ".$params['row'].($params['row']!="" ? "," : "")."
        sum(l.TAGIHAN) as TAGIHAN,sum(l.PPN) as PPN,sum(l.CICILAN) as CICILAN, sum(l.MATERAI) as MATERAI, sum(l.DENDA) as DENDA , sum(l.TOTAL_BAYAR) as TOTAL_BAYAR, sum(l.LEMBAR) as LEMBAR, sum(l.TRANSAKSI) as TRANSAKSI
      FROM MYARMS_TEL75 l 
      INNER JOIN MYARMS_BUSINESS_AREA ba ON ba.BUSINESS_AREA_ID=l.BA_BILL
      LEFT JOIN MYARMS_BUSINESS_AREA bua ON bua.BUSINESS_AREA_ID=l.BA_PAY
      INNER JOIN MYARMS_MAPPING_BANK mb ON mb.BANK_CODE=l.BANK_CODE
      LEFT JOIN MYARMS_UBIS_SEGMENT us ON us.UBIS_SEGMENT_ID=ba.UBIS_SEGMENT_ID
      LEFT JOIN MYARMS_UBIS u on u.UBIS_ID=us.UBIS_ID
      $parameter
      $group_by
      $order_by
    ");*/
      $query=$this->db->query("
      SELECT
      *
      FROM MYARMS_TEL75 l 
      INNER JOIN MYARMS_BUSINESS_AREA ba ON ba.BUSINESS_AREA_ID=l.BA_BILL
      LEFT JOIN MYARMS_BUSINESS_AREA bua ON bua.BUSINESS_AREA_ID=l.BA_PAY
      INNER JOIN MYARMS_MAPPING_BANK mb ON mb.BANK_CODE=l.BANK_CODE
      LEFT JOIN MYARMS_UBIS_SEGMENT us ON us.UBIS_SEGMENT_ID=ba.UBIS_SEGMENT_ID
      LEFT JOIN MYARMS_UBIS u on u.UBIS_ID=us.UBIS_ID
      $parameter
      $group_by
      $order_by
    ");
    return $query->result();
  }

  public function RTGS($params=array())
  {
    $periode=$params['year_1'].$params['month_1'];
    $group_by="";//$group_by=($params['row']!="" ? "GROUP BY ".$params['row'] : "");
    $order_by="";//$order_by=($params['row']!="" ? "ORDER BY ".$params['row']." ASC" : "");
    $parameter=($periode!="" ? " WHERE to_number(to_char(l.TGL_ENTRY, 'YYYYMM'))>='".$periode."'" : "");
    /*$parameter.=($params['ubis']!="''" ? " and u.UBIS_ID in (".$params['ubis'].")" : "");
    $parameter.=($params['bisnis_area']!="''" ? " and ba.BUSINESS_AREA_ID in (".$params['bisnis_area'].")" : "");
    $parameter.=($params['bank']!="''" ? " and l.BANK_CODE in (".$params['bank'].")" : "");
    $parameter.=($params['ba_pay']!="''" ? " and bua.BUSINESS_AREA_ID in (".$params['ba_pay'].")" : "");
    $parameter.=($params['plcl']!="''" && !empty($params['plcl']) ? " and l.PL_CL ='".$params['plcl']."'" : ""); */
  
    /*$query=$this->db->query("
      SELECT
        ".$params['row'].($params['row']!="" ? "," : "")."
        sum(l.TAGIHAN) as TAGIHAN,sum(l.PPN) as PPN,sum(l.CICILAN) as CICILAN, sum(l.MATERAI) as MATERAI, sum(l.DENDA) as DENDA , sum(l.TOTAL_BAYAR) as TOTAL_BAYAR, sum(l.LEMBAR) as LEMBAR, sum(l.TRANSAKSI) as TRANSAKSI
      FROM MYARMS_TEL75 l 
      INNER JOIN MYARMS_BUSINESS_AREA ba ON ba.BUSINESS_AREA_ID=l.BA_BILL
      LEFT JOIN MYARMS_BUSINESS_AREA bua ON bua.BUSINESS_AREA_ID=l.BA_PAY
      INNER JOIN MYARMS_MAPPING_BANK mb ON mb.BANK_CODE=l.BANK_CODE
      LEFT JOIN MYARMS_UBIS_SEGMENT us ON us.UBIS_SEGMENT_ID=ba.UBIS_SEGMENT_ID
      LEFT JOIN MYARMS_UBIS u on u.UBIS_ID=us.UBIS_ID
      $parameter
      $group_by
      $order_by
    ");*/
      $query=$this->db->query("
      SELECT
      *
      FROM MYARMS_TEL75 l 
      INNER JOIN MYARMS_BUSINESS_AREA ba ON ba.BUSINESS_AREA_ID=l.BA_BILL
      LEFT JOIN MYARMS_BUSINESS_AREA bua ON bua.BUSINESS_AREA_ID=l.BA_PAY
      INNER JOIN MYARMS_MAPPING_BANK mb ON mb.BANK_CODE=l.BANK_CODE
      LEFT JOIN MYARMS_UBIS_SEGMENT us ON us.UBIS_SEGMENT_ID=ba.UBIS_SEGMENT_ID
      LEFT JOIN MYARMS_UBIS u on u.UBIS_ID=us.UBIS_ID
      $parameter
      $group_by
      $order_by
    ");
    return $query->result();
  }  
}