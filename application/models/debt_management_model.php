<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');  

class Debt_Management_Model extends MY_Model {
  public $table_name = '';
  public function __construct() {
    parent::__construct();
  }
  
  public function jumlah_kuintasi_lists()
  {
    $query=$this->db->query("
      select l.JUMLAH_KUITANSI from MYARMS_GROUP_TUNGGAKAN l
	  GROUP BY l.JUMLAH_KUITANSI
      order by l.JUMLAH_KUITANSI ASC
    ");
    return $query->result();
  }

  public function umur_termuda_lists()
  {
    $query=$this->db->query("
      select l.UMUR_TERMUDA from MYARMS_GROUP_TUNGGAKAN l
    GROUP BY l.UMUR_TERMUDA
      order by l.UMUR_TERMUDA ASC
    ");
    return $query->result();
  }
  
  public function LOKET_SUMMARY_DETAIL($params=array())
  { 
    $periode1=$params['year_1'].$params['month_1'];
    $order_by="ORDER BY l.VALIDATION_DATE DESC";
    $parameter="WHERE to_number(to_char(l.VALIDATION_DATE, 'YYYYMM'))='".$periode1."'";
	
    $parameter.=($params['division']!="''" ? " and ll.DIVISION_CODE in (".$params['division'].")" : "");
    $parameter.=($params['witel']!="''" ? " and w.WITEL_CODE in (".$params['witel'].")" : "");
    $parameter.=($params['datel']!="''" ? " and d.DATEL_CODE in (".$params['datel'].")" : "");
	$parameter.=($params['loket']!="''" ? " and l.CODE_LOCKET in (".$params['loket'].")" : "");
	$parameter.=($params['validasi']!="''" && !empty($params['validasi']) ? " and l.STATUS_VALIDATION ='".$params['validasi']."'" : ""); 
	$parameter.=($params['petugas']!="''" && !empty($params['petugas']) ? " and (l.PTGS_CASH_DESK LIKE '%".ucfirst($params['petugas'])."%' OR l.PTGS_CASH_DESK LIKE '%".strtolower($params['petugas'])."%' OR l.PTGS_CASH_DESK LIKE '%".strtoupper($params['petugas'])."%') " : "");
	//$parameter.=($params['keyword']!="''" && !empty($params['keyword']) ? " and ( w.WITEL_DESCRIPTION LIKE '%".ucfirst($params['keyword'])."%' OR w.WITEL_DESCRIPTION LIKE '%".strtolower($params['keyword'])."%' OR d.DATEL_DESCRIPTION LIKE '%".ucfirst($params['keyword'])."%' OR d.DATEL_DESCRIPTION LIKE '%".strtolower($params['keyword'])."%' or l.LOCATION LIKE '%".ucfirst($params['keyword'])."%' or l.LOCATION LIKE '%".strtolower($params['keyword'])."%' or l.CODE_LOCKET LIKE '%".ucfirst($params['keyword'])."%' or l.CODE_LOCKET LIKE '%".strtolower($params['keyword'])."%' or ll.LOCKET_NAME LIKE '%".ucfirst($params['keyword'])."%' or ll.LOCKET_NAME LIKE '%".strtolower($params['keyword'])."%' or l.PTGS_CASH_DESK LIKE '%".ucfirst($params['keyword'])."%' or l.PTGS_CASH_DESK LIKE '%".strtolower($params['keyword'])."%' or l.TRANSACTION_TYPE LIKE '%".ucfirst($params['keyword'])."%' or l.TRANSACTION_TYPE LIKE '%".strtolower($params['keyword'])."%')" : ""); 
    $query=$this->db->query("
      SELECT l.WITEL_CODE,w.WITEL_DESCRIPTION,l.DATEL_CODE,d.DATEL_DESCRIPTION,l.LOCATION,l.CODE_LOCKET,ll.LOCKET_NAME,l.STEP,l.PTGS_CASH_DESK,l.TRANSACTION_TYPE,l.STEP_OPEN_DATE,l.STEP_CLOSE_DATE,l.AMOUNT,l.SST,l.STATUS_VALIDATION,l.VALIDATION_DATE
      FROM MYARMS_MAIN_LOCKET l 
      INNER JOIN MYARMS_WITEL w ON l.WITEL_CODE=w.WITEL_CODE
      INNER JOIN MYARMS_DATEL d ON l.DATEL_CODE=d.DATEL_CODE
      LEFT JOIN MYARMS_LOCKET ll ON l.CODE_LOCKET=ll.CODE_LOCKET
	  $parameter
      $order_by
    ");
	return $query->result();
  }
  
  public function LOKET_SUMMARY($params=array())
  {
    $periode=$params['year_1'].$params['month_1'];
    $group_by=($params['row']!="" || $params['column']!="" ? "GROUP BY ".$params['row'].($params['row']!="" && $params['column']!="" ? "," : "").$params['column'] : "");
    $order_by=($params['row']!="" ? "ORDER BY ".$params['row']." ASC" : "");
    $parameter="WHERE to_number(to_char(l.VALIDATION_DATE, 'YYYYMM'))='".$periode."' ";
    $parameter.=($params['division']!="''" ? " and ll.DIVISION_CODE in (".$params['division'].")" : "");
    $parameter.=($params['witel']!="''" ? " and w.WITEL_CODE in (".$params['witel'].")" : "");
    $parameter.=($params['datel']!="''" ? " and d.DATEL_CODE in (".$params['datel'].")" : "");
	$parameter.=($params['loket']!="''" ? " and l.CODE_LOCKET in (".$params['loket'].")" : "");
	$parameter.=($params['validasi']!="''" && !empty($params['validasi']) ? " and l.STATUS_VALIDATION ='".$params['validasi']."'" : ""); 
	$parameter.=($params['petugas']!="''" && !empty($params['petugas']) ? " and (l.PTGS_CASH_DESK LIKE '%".ucfirst($params['petugas'])."%' OR l.PTGS_CASH_DESK LIKE '%".strtolower($params['petugas'])."%' OR l.PTGS_CASH_DESK LIKE '%".strtoupper($params['petugas'])."%') " : "");
	//$parameter.=($params['keyword']!="''" && !empty($params['keyword']) ? " and ( w.WITEL_DESCRIPTION LIKE '%".ucfirst($params['keyword'])."%' OR w.WITEL_DESCRIPTION LIKE '%".strtolower($params['keyword'])."%' OR d.DATEL_DESCRIPTION LIKE '%".ucfirst($params['keyword'])."%' OR d.DATEL_DESCRIPTION LIKE '%".strtolower($params['keyword'])."%' or l.LOCATION LIKE '%".ucfirst($params['keyword'])."%' or l.LOCATION LIKE '%".strtolower($params['keyword'])."%' or l.CODE_LOCKET LIKE '%".ucfirst($params['keyword'])."%' or l.CODE_LOCKET LIKE '%".strtolower($params['keyword'])."%' or ll.LOCKET_NAME LIKE '%".ucfirst($params['keyword'])."%' or ll.LOCKET_NAME LIKE '%".strtolower($params['keyword'])."%' or l.PTGS_CASH_DESK LIKE '%".ucfirst($params['keyword'])."%' or l.PTGS_CASH_DESK LIKE '%".strtolower($params['keyword'])."%' or l.TRANSACTION_TYPE LIKE '%".ucfirst($params['keyword'])."%' or l.TRANSACTION_TYPE LIKE '%".strtolower($params['keyword'])."%')" : ""); 
	$query=$this->db->query("
      SELECT
        ".$params['row'].($params['row']!="" ? "," : "").$params['column'].($params['column']!="" ? "," : "")."
        sum(l.AMOUNT) as AMOUNT,sum(l.SST) as SST
      FROM MYARMS_MAIN_LOCKET l 
      INNER JOIN MYARMS_WITEL w ON l.WITEL_CODE=w.WITEL_CODE
      INNER JOIN MYARMS_DATEL d ON l.DATEL_CODE=d.DATEL_CODE
      LEFT JOIN MYARMS_LOCKET ll ON l.CODE_LOCKET=ll.CODE_LOCKET
	  LEFT JOIN MYARMS_DIVISION di ON ll.DIVISION_CODE=di.DIVISION_CODE
      $parameter
      $group_by
      $order_by
    ");
    return $query->result();
  }
	
	public function LOKET_BO($params=array())
  { 
    
    $order_by="ORDER BY l.BUSINESS_AREA_ID ASC";
    $parameter="WHERE l.BUSINESS_AREA_ID ='R104'";
    //$parameter.=($params['division']!="''" ? " and ll.DIVISION_CODE in (".$params['division'].")" : "");
    //$parameter.=($params['witel']!="''" ? " and w.WITEL_CODE in (".$params['witel'].")" : "");
    //$parameter.=($params['datel']!="''" ? " and d.DATEL_CODE in (".$params['datel'].")" : "");
	$parameter.=($params['loket']!="''" ? " and l.CODE_LOCKET in (".$params['loket'].")" : "");
	$parameter.=($params['deskloket']!="''" && !empty($params['deskloket']) ? " and ll.LOCKET_NAME LIKE '%".$params['deskloket']."%'" : ""); 
	$parameter.=($params['deskakun']!="''" && !empty($params['deskakun']) ? " and l.DESCRIPTION_ACCOUNT_BO LIKE '%".$params['deskakun']."%'" : "");
	//$parameter.=($params['keyword']!="''" && !empty($params['keyword']) ? " and ( w.WITEL_DESCRIPTION LIKE '%".ucfirst($params['keyword'])."%' OR w.WITEL_DESCRIPTION LIKE '%".strtolower($params['keyword'])."%' OR d.DATEL_DESCRIPTION LIKE '%".ucfirst($params['keyword'])."%' OR d.DATEL_DESCRIPTION LIKE '%".strtolower($params['keyword'])."%' or l.LOCATION LIKE '%".ucfirst($params['keyword'])."%' or l.LOCATION LIKE '%".strtolower($params['keyword'])."%' or l.CODE_LOCKET LIKE '%".ucfirst($params['keyword'])."%' or l.CODE_LOCKET LIKE '%".strtolower($params['keyword'])."%' or ll.LOCKET_NAME LIKE '%".ucfirst($params['keyword'])."%' or ll.LOCKET_NAME LIKE '%".strtolower($params['keyword'])."%' or l.PTGS_CASH_DESK LIKE '%".ucfirst($params['keyword'])."%' or l.PTGS_CASH_DESK LIKE '%".strtolower($params['keyword'])."%' or l.TRANSACTION_TYPE LIKE '%".ucfirst($params['keyword'])."%' or l.TRANSACTION_TYPE LIKE '%".strtolower($params['keyword'])."%')" : ""); 
    $query=$this->db->query("
      SELECT l.KD_LOCATION,l.CODE_LOCKET,ll.LOCKET_NAME,l.LOCKET_TYPE,l.NO_BO,l.NO_ACCOUNT_BO,l.DESCRIPTION_ACCOUNT_BO,l.BUSINESS_AREA_ID,l.DESCRIPTION_AREA      FROM MYARMS_MAP_BO l 
      LEFT JOIN MYARMS_LOCKET ll ON l.CODE_LOCKET=ll.CODE_LOCKET
	  $parameter
      $order_by
    ");
	return $query->result();
  }	
  
  public function DETAIL_PSB($params=array())
  { 
    $periode1=$params['year_1'].$params['month_1'];
    $order_by="ORDER BY l.DATE_OPEN_STEP DESC";
    $parameter="WHERE to_number(to_char(l.DATE_OPEN_STEP, 'YYYYMM'))='".$periode1."'";
	
    $parameter.=($params['division']!="''" ? " and ll.DIVISION_CODE in (".$params['division'].")" : "");
	$parameter.=($params['product']!="''" ? " and l.PRODUCT_ID in (".$params['product'].")" : "");
    //$parameter.=($params['witel']!="''" ? " and w.WITEL_CODE in (".$params['witel'].")" : "");
    //$parameter.=($params['datel']!="''" ? " and d.DATEL_CODE in (".$params['datel'].")" : "");
	$parameter.=($params['loket']!="''" ? " and l.CODE_ID in (".$params['loket'].")" : "");
	$parameter.=($params['validasi']!="''" && !empty($params['validasi']) ? " and l.VALIDATION_STATUS ='".$params['validasi']."'" : ""); 
	$parameter.=($params['petugas']!="''" && !empty($params['petugas']) ? " and l.PTGS_CASH_DESK LIKE '%".$params['petugas']."%'" : "");
	//$parameter.=($params['keyword']!="''" && !empty($params['keyword']) ? " and ( w.WITEL_DESCRIPTION LIKE '%".ucfirst($params['keyword'])."%' OR w.WITEL_DESCRIPTION LIKE '%".strtolower($params['keyword'])."%' OR d.DATEL_DESCRIPTION LIKE '%".ucfirst($params['keyword'])."%' OR d.DATEL_DESCRIPTION LIKE '%".strtolower($params['keyword'])."%' or l.LOCATION LIKE '%".ucfirst($params['keyword'])."%' or l.LOCATION LIKE '%".strtolower($params['keyword'])."%' or l.CODE_LOCKET LIKE '%".ucfirst($params['keyword'])."%' or l.CODE_LOCKET LIKE '%".strtolower($params['keyword'])."%' or ll.LOCKET_NAME LIKE '%".ucfirst($params['keyword'])."%' or ll.LOCKET_NAME LIKE '%".strtolower($params['keyword'])."%' or l.PTGS_CASH_DESK LIKE '%".ucfirst($params['keyword'])."%' or l.PTGS_CASH_DESK LIKE '%".strtolower($params['keyword'])."%' or l.TRANSACTION_TYPE LIKE '%".ucfirst($params['keyword'])."%' or l.TRANSACTION_TYPE LIKE '%".strtolower($params['keyword'])."%')" : ""); 
    $query=$this->db->query("
      SELECT l.BUSINESS_AREA_ID, l.AREA,l.LOCATION,l.LOCKET_ID, l.LOCKET_NAME,l.NO_TRANSACTION, l.STEP, l.NO_FACTURE,l.NCLI,l.NDOS,l.JUMLAH, l.PTGS_CASH_DESK, l.DATE_OPEN_STEP, l.DATE_CLOSE_STEP, l.VALIDATION_STATUS
      FROM MYARMS_PSB l 
      LEFT JOIN MYARMS_LOCKET ll ON l.LOCKET_ID=ll.CODE_LOCKET
	  $parameter
      $order_by
    ");
	return $query->result();
  }
}