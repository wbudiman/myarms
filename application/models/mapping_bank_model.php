<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mapping_Bank_Model extends MY_Model {
  public $table_name = 'MYARMS_MAPPING_BANK';
  public function __construct() {
    parent::__construct();
  }
}