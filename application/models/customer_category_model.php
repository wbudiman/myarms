<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Customer_Category_Model extends MY_Model {
  public $table_name = 'MYARMS_CUSTOMER_CATEGORY';
  public function __construct() {
    parent::__construct();
  }
}