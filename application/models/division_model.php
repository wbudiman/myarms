<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Division_Model extends MY_Model {
  public $table_name = 'MYARMS_DIVISION';
  public function __construct() {
    parent::__construct();
  }
}