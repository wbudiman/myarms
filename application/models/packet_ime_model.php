<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Packet_Ime_Model extends MY_Model {
  public $table_name = 'MYARMS_PACKET_IME';
  public function __construct() {
    parent::__construct();
  }
  function get_packet_ime()
  {
    return $this->db->query("
      select PACKET_IME from MYARMS_PACKET_IME
      GROUP BY PACKET_IME
    ")->result();
  }
  function get_by_packet_ime($packet_imes=array())
  {
    return $this->db->query("
      select * from MYARMS_PACKET_IME
      where PACKET_IME in ($packet_imes)
    ")->result();
  }
}