<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Component_Type_Model extends MY_Model {
  public $table_name = 'MYARMS_COMPONENT_TYPE';
  public function __construct() {
    parent::__construct();
  }
}