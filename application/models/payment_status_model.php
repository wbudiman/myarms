<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payment_Status_Model extends MY_Model {
  public $table_name = 'MYARMS_PAYMENT_STATUS';
  public function __construct() {
    parent::__construct();
  }
}