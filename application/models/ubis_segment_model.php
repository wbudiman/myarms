<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ubis_Segment_Model extends MY_Model {
  public $table_name = 'MYARMS_UBIS_SEGMENT';
  public function __construct() {
    parent::__construct();
  }
  public function get_by_ubis_id($ubis){
    $data_ubis=explode(",",str_replace("'","",$ubis));
    $parameter="";
    if(in_array(1,$data_ubis)){
      $parameter="
        UNION 
        select DISTINCT CAST(CUSTOMER_CATEGORY_DESCRIPTION as VARCHAR(50)) as UBIS_SEGMENT
        from MYARMS_CUSTOMER_CATEGORY
      ";
    }else{
      if(($key=array_search(1, $data_ubis)) !== false) {
        unset($data_ubis[$key]);
      }
      $result="";
      $counter=1;
      foreach($data_ubis as $d){
        if($d!=1){
          $result.="'".$d."'";
        }
        if($counter!=sizeof($data_ubis))$result.=",";
        $counter++;
      }
      $ubis=$result;
      $parameter=" WHERE UBIS_ID IN ($ubis)";
    }
    $query=$this->db->query("
      select DISTINCT CAST(UBIS_SEGMENT as VARCHAR(50)) as UBIS_SEGMENT
      from MYARMS_UBIS_SEGMENT
    ".$parameter."
      order by UBIS_SEGMENT ASC
    ");
    return $query->result();
  }
}