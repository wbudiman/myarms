<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Row_Column_Model extends MY_Model {
  public $table_name = 'MYARMS_ROW_COLUMN';
  public function __construct() {
    parent::__construct();
  }
  public function get_row_detail_billing_revenue()
  {
    return $this->db->query("
      select * from MYARMS_ROW_COLUMN
      where ROW_COLUMN_ID in (1,2,3,4,5,7,8,9,10,11,12)
      order by ROW_COLUMN_DESCRIPTION ASC
    ")->result();
  }
  public function get_row_billing_result_revenue()
  {
    return $this->db->query("
      select * from MYARMS_ROW_COLUMN
      where ROW_COLUMN_ID in (1,2,3,4,5,7,12,13,14,15)
      order by ROW_COLUMN_DESCRIPTION ASC
    ")->result();
  }
  public function get_row_billing_result_growth()
  {
    return $this->db->query("
      select * from MYARMS_ROW_COLUMN
      where ROW_COLUMN_ID in (1,2,3,4,5,12)
      order by ROW_COLUMN_DESCRIPTION ASC
    ")->result();
  }
  public function get_row_billing_result_billing_commitee()
  {
    return $this->db->query("
      select * from MYARMS_ROW_COLUMN
      where ROW_COLUMN_ID in (1,2,3,4,5,12,13,14)
      order by ROW_COLUMN_DESCRIPTION ASC
    ")->result();
  }
  public function get_row_billing_result_news_event()
  {
    return $this->db->query("
      select * from MYARMS_ROW_COLUMN
      where ROW_COLUMN_ID in (1,2,3,4,5,12)
      order by ROW_COLUMN_DESCRIPTION ASC
    ")->result();
  }
  public function get_row_warm_billing_extrim_usage()
  {
    return $this->db->query("
      select * from MYARMS_ROW_COLUMN
      where ROW_COLUMN_ID in (1,2,3,4,5,9,12)
      order by ROW_COLUMN_DESCRIPTION ASC
    ")->result();
  }
  public function get_row_c3mr()
  {
    return $this->db->query("
      select * from MYARMS_ROW_COLUMN
      where ROW_COLUMN_ID in (1,2,3,4,5,6,8,9,10,11,16,17,18)
      order by ROW_COLUMN_DESCRIPTION ASC
    ")->result();
  }
  public function get_row_new_wave_indihome_revenue()
  {
    return $this->db->query("
      select * from MYARMS_ROW_COLUMN
      where ROW_COLUMN_ID in (1,2,3,6,9,19,20,21,22)
      order by ROW_COLUMN_DESCRIPTION ASC
    ")->result();
  }
  public function get_row_new_wave_ime_revenue()
  {
    return $this->db->query("
      select * from MYARMS_ROW_COLUMN
      where ROW_COLUMN_ID in (1,2,3,6,9,19,23,24,25)
      order by ROW_COLUMN_DESCRIPTION ASC
    ")->result();
  }
  
  public function get_row_gimmick_pelanggan()
  {
    return $this->db->query("
      select * from MYARMS_ROW_COLUMN
      where ROW_COLUMN_ID in (1,2,3,4,5,26)
      order by ROW_COLUMN_DESCRIPTION ASC
    ")->result();
  }
  
  public function get_row_loket_summary()
  {
    return $this->db->query("
      select * from MYARMS_ROW_COLUMN
      where ROW_COLUMN_ID in (1,2,3,28,29,30,31,32)
      order by ROW_COLUMN_DESCRIPTION ASC
    ")->result();
  }

  public function get_row_group_tunggakan()
  {
    return $this->db->query("
      select * from MYARMS_ROW_COLUMN
      where ROW_COLUMN_ID in (33,34)
      order by ROW_COLUMN_DESCRIPTION ASC
    ")->result();
  }

  public function get_column_group_tunggakan()
  {
    return $this->db->query("
      select * from MYARMS_ROW_COLUMN
      where ROW_COLUMN_ID in (1,4,5)
      order by ROW_COLUMN_DESCRIPTION ASC
    ")->result();
  }

  public function get_row_tel75()
  {
    return $this->db->query("
      select * from MYARMS_ROW_COLUMN
      where ROW_COLUMN_ID in (9,36,37,38)
      order by ROW_COLUMN_DESCRIPTION ASC
    ")->result();
  }

  public function get_row_pembatalan_tel75()
  {
    return $this->db->query("
      select * from MYARMS_ROW_COLUMN
      where ROW_COLUMN_ID in (9,36,37,38,43)
      order by ROW_COLUMN_DESCRIPTION ASC
    ")->result();
  }

  public function get_row_gxtel72()
  {
    return $this->db->query("
      select * from MYARMS_ROW_COLUMN
      where ROW_COLUMN_ID in (37,39,40)
      order by ROW_COLUMN_DESCRIPTION ASC
    ")->result();
  }

  public function get_row_osa()
  {
    return $this->db->query("
      select * from MYARMS_ROW_COLUMN
      where ROW_COLUMN_ID in (37,44,45)
      order by ROW_COLUMN_DESCRIPTION ASC
    ")->result();
  }

  public function get_row_rtgs()
  {
    return $this->db->query("
      select * from MYARMS_ROW_COLUMN
      where ROW_COLUMN_ID in (37,39,42,41)
      order by ROW_COLUMN_DESCRIPTION ASC
    ")->result();
  }

  public function get_row_acp()
  {
    return $this->db->query("
      select * from MYARMS_ROW_COLUMN
      where ROW_COLUMN_ID in (6,9,43)
      order by ROW_COLUMN_DESCRIPTION ASC
    ")->result();
  }

}