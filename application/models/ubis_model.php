<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ubis_Model extends MY_Model {
  public $table_name = 'MYARMS_UBIS';
  public function __construct() {
    parent::__construct();
  }
}