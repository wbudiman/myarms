<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product_Model extends MY_Model {
  public $table_name = 'MYARMS_PRODUCT';
  public function __construct() {
    parent::__construct();
  }
  public function lists($summaries="")
  {
    $parameter=($summaries!="" ? " where SUMMARY_PRODUCT in ($summaries)" : "");
    $query=$this->db->query("
      select distinct(SUMMARY_PRODUCT) from MYARMS_PRODUCT
    ".$parameter."
      order by SUMMARY_PRODUCT ASC
    ");
    return $query->result();
  }
  // public function get_product_by_summary_product($summary_products){
    // $query=$this->db->query("
      // select * from MYARMS_PRODUCT where SUMMARY_PRODUCT in ($summary_products)
      // order by PRODUCT_NAME ASC
    // ");
    // return $query->result();
  // }
  public function get_product_by_summary_product($summary_products){
    $parameter=($summary_products!="''" ? "where SUMMARY_PRODUCT in ($summary_products)" : "");
    $query=$this->db->query("
      select DISTINCT PRODUCT_NAME from MYARMS_PRODUCT 
    ".$parameter."
      order by PRODUCT_NAME ASC
    ");
    return $query->result();
  }
  public function get_product_by_product_name($val){
    $query=$this->db->query("
      select * from MYARMS_PRODUCT where PRODUCT_NAME in ($val)
      order by PRODUCT_NAME ASC
    ");
    return $query->result();
  }


}