<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');  

class Report_aging_Model extends MY_Model {
  public $table_name = '';
  public function __construct() {
    parent::__construct();
  }
  
  public function lists($divisions="",$witels="",$datels="")
  {
    $query=$this->db->query("
      select distinct(l.CODE_LOCKET),l.CODE_LOCKET,ml.LOCKET_NAME from MYARMS_MAIN_LOCKET l
	  inner join MYARMS_LOCKET ml ON ml.CODE_LOCKET=l.CODE_LOCKET
	  inner join MYARMS_DIVISION_DETAIL dd on l.DATEL_CODE=dd.DATEL_CODE
	  inner join MYARMS_DATEL d on l.DATEL_CODE=d.DATEL_CODE
      inner join MYARMS_WITEL w on l.WITEL_CODE=w.WITEL_CODE
      where dd.DIVISION_CODE in ($divisions) and w.WITEL_CODE in ($witels) and d.DATEL_CODE in ($datels)
	  GROUP BY ml.LOCKET_NAME
      order by ml.LOCKET_NAME ASC
    ");
    return $query->result();
  }
  
  public function BNPDA($params=array())
  { 
    $periode1=$params['year_1'].$params['month_1'];
    $order_by="ORDER BY b.PERIODE DESC";
    $parameter="WHERE b.PERIODE='".$periode1."'";
	
    $parameter.=($params['division']!="''" ? " and ll.DIVISION_CODE in (".$params['division'].")" : "");
    $parameter.=($params['witel']!="''" ? " and w.WITEL_CODE in (".$params['witel'].")" : "");
    $parameter.=($params['datel']!="''" ? " and d.DATEL_CODE in (".$params['datel'].")" : "");
	$parameter.=($params['loket']!="''" ? " and l.CODE_LOCKET in (".$params['loket'].")" : "");
	$parameter.=($params['validasi']!="''" && !empty($params['validasi']) ? " and l.STATUS_VALIDATION ='".$params['validasi']."'" : ""); 
	$parameter.=($params['petugas']!="''" && !empty($params['petugas']) ? " and (l.PTGS_CASH_DESK LIKE '%".ucfirst($params['petugas'])."%' OR l.PTGS_CASH_DESK LIKE '%".strtolower($params['petugas'])."%' OR l.PTGS_CASH_DESK LIKE '%".strtoupper($params['petugas'])."%') " : "");
	//$parameter.=($params['keyword']!="''" && !empty($params['keyword']) ? " and ( w.WITEL_DESCRIPTION LIKE '%".ucfirst($params['keyword'])."%' OR w.WITEL_DESCRIPTION LIKE '%".strtolower($params['keyword'])."%' OR d.DATEL_DESCRIPTION LIKE '%".ucfirst($params['keyword'])."%' OR d.DATEL_DESCRIPTION LIKE '%".strtolower($params['keyword'])."%' or l.LOCATION LIKE '%".ucfirst($params['keyword'])."%' or l.LOCATION LIKE '%".strtolower($params['keyword'])."%' or l.CODE_LOCKET LIKE '%".ucfirst($params['keyword'])."%' or l.CODE_LOCKET LIKE '%".strtolower($params['keyword'])."%' or ll.LOCKET_NAME LIKE '%".ucfirst($params['keyword'])."%' or ll.LOCKET_NAME LIKE '%".strtolower($params['keyword'])."%' or l.PTGS_CASH_DESK LIKE '%".ucfirst($params['keyword'])."%' or l.PTGS_CASH_DESK LIKE '%".strtolower($params['keyword'])."%' or l.TRANSACTION_TYPE LIKE '%".ucfirst($params['keyword'])."%' or l.TRANSACTION_TYPE LIKE '%".strtolower($params['keyword'])."%')" : ""); 
    $query=$this->db->query("
      SELECT *
      FROM MYARMS_BN_PDA b
	  $parameter
      $order_by
    ");
	return $query->result();
  }
  
  public function PSB($params=array())
  { 
    $periode1=$params['year_1'].$params['month_1'];
    $order_by="ORDER BY b.PERIODE DESC";
    $parameter="WHERE b.PERIODE='".$periode1."'";
  
    $parameter.=($params['division']!="''" ? " and ll.DIVISION_CODE in (".$params['division'].")" : "");
    $parameter.=($params['witel']!="''" ? " and w.WITEL_CODE in (".$params['witel'].")" : "");
    $parameter.=($params['datel']!="''" ? " and d.DATEL_CODE in (".$params['datel'].")" : "");
  $parameter.=($params['loket']!="''" ? " and l.CODE_LOCKET in (".$params['loket'].")" : "");
  $parameter.=($params['validasi']!="''" && !empty($params['validasi']) ? " and l.STATUS_VALIDATION ='".$params['validasi']."'" : ""); 
  $parameter.=($params['petugas']!="''" && !empty($params['petugas']) ? " and (l.PTGS_CASH_DESK LIKE '%".ucfirst($params['petugas'])."%' OR l.PTGS_CASH_DESK LIKE '%".strtolower($params['petugas'])."%' OR l.PTGS_CASH_DESK LIKE '%".strtoupper($params['petugas'])."%') " : "");
  //$parameter.=($params['keyword']!="''" && !empty($params['keyword']) ? " and ( w.WITEL_DESCRIPTION LIKE '%".ucfirst($params['keyword'])."%' OR w.WITEL_DESCRIPTION LIKE '%".strtolower($params['keyword'])."%' OR d.DATEL_DESCRIPTION LIKE '%".ucfirst($params['keyword'])."%' OR d.DATEL_DESCRIPTION LIKE '%".strtolower($params['keyword'])."%' or l.LOCATION LIKE '%".ucfirst($params['keyword'])."%' or l.LOCATION LIKE '%".strtolower($params['keyword'])."%' or l.CODE_LOCKET LIKE '%".ucfirst($params['keyword'])."%' or l.CODE_LOCKET LIKE '%".strtolower($params['keyword'])."%' or ll.LOCKET_NAME LIKE '%".ucfirst($params['keyword'])."%' or ll.LOCKET_NAME LIKE '%".strtolower($params['keyword'])."%' or l.PTGS_CASH_DESK LIKE '%".ucfirst($params['keyword'])."%' or l.PTGS_CASH_DESK LIKE '%".strtolower($params['keyword'])."%' or l.TRANSACTION_TYPE LIKE '%".ucfirst($params['keyword'])."%' or l.TRANSACTION_TYPE LIKE '%".strtolower($params['keyword'])."%')" : ""); 
    $query=$this->db->query("
      SELECT *
      FROM MYARMS_PSB b
    $parameter
      $order_by
    ");
  return $query->result();
  }
	
	public function Cancel_Tgl_Cabut($params=array())
  { 
    $periode1=$params['year_1'].$params['month_1'];
    $order_by="ORDER BY b.PERIODE DESC";
    $parameter="WHERE b.PERIODE='".$periode1."'";
  
    $parameter.=($params['division']!="''" ? " and ll.DIVISION_CODE in (".$params['division'].")" : "");
    $parameter.=($params['witel']!="''" ? " and w.WITEL_CODE in (".$params['witel'].")" : "");
    $parameter.=($params['datel']!="''" ? " and d.DATEL_CODE in (".$params['datel'].")" : "");
  $parameter.=($params['loket']!="''" ? " and l.CODE_LOCKET in (".$params['loket'].")" : "");
  $parameter.=($params['validasi']!="''" && !empty($params['validasi']) ? " and l.STATUS_VALIDATION ='".$params['validasi']."'" : ""); 
  $parameter.=($params['petugas']!="''" && !empty($params['petugas']) ? " and (l.PTGS_CASH_DESK LIKE '%".ucfirst($params['petugas'])."%' OR l.PTGS_CASH_DESK LIKE '%".strtolower($params['petugas'])."%' OR l.PTGS_CASH_DESK LIKE '%".strtoupper($params['petugas'])."%') " : "");
  //$parameter.=($params['keyword']!="''" && !empty($params['keyword']) ? " and ( w.WITEL_DESCRIPTION LIKE '%".ucfirst($params['keyword'])."%' OR w.WITEL_DESCRIPTION LIKE '%".strtolower($params['keyword'])."%' OR d.DATEL_DESCRIPTION LIKE '%".ucfirst($params['keyword'])."%' OR d.DATEL_DESCRIPTION LIKE '%".strtolower($params['keyword'])."%' or l.LOCATION LIKE '%".ucfirst($params['keyword'])."%' or l.LOCATION LIKE '%".strtolower($params['keyword'])."%' or l.CODE_LOCKET LIKE '%".ucfirst($params['keyword'])."%' or l.CODE_LOCKET LIKE '%".strtolower($params['keyword'])."%' or ll.LOCKET_NAME LIKE '%".ucfirst($params['keyword'])."%' or ll.LOCKET_NAME LIKE '%".strtolower($params['keyword'])."%' or l.PTGS_CASH_DESK LIKE '%".ucfirst($params['keyword'])."%' or l.PTGS_CASH_DESK LIKE '%".strtolower($params['keyword'])."%' or l.TRANSACTION_TYPE LIKE '%".ucfirst($params['keyword'])."%' or l.TRANSACTION_TYPE LIKE '%".strtolower($params['keyword'])."%')" : ""); 
    $query=$this->db->query("
      SELECT *
      FROM MYARMS_DATE_CANCEL b
    $parameter
      $order_by
    ");
  return $query->result();
  }	
  
  public function Modification($params=array())
  { 
    $periode1=$params['year_1'].$params['month_1'];
    $order_by="ORDER BY b.PERIODE DESC";
    $parameter="WHERE b.PERIODE='".$periode1."'";
  
    $parameter.=($params['division']!="''" ? " and ll.DIVISION_CODE in (".$params['division'].")" : "");
    $parameter.=($params['witel']!="''" ? " and w.WITEL_CODE in (".$params['witel'].")" : "");
    $parameter.=($params['datel']!="''" ? " and d.DATEL_CODE in (".$params['datel'].")" : "");
  $parameter.=($params['loket']!="''" ? " and l.CODE_LOCKET in (".$params['loket'].")" : "");
  $parameter.=($params['validasi']!="''" && !empty($params['validasi']) ? " and l.STATUS_VALIDATION ='".$params['validasi']."'" : ""); 
  $parameter.=($params['petugas']!="''" && !empty($params['petugas']) ? " and (l.PTGS_CASH_DESK LIKE '%".ucfirst($params['petugas'])."%' OR l.PTGS_CASH_DESK LIKE '%".strtolower($params['petugas'])."%' OR l.PTGS_CASH_DESK LIKE '%".strtoupper($params['petugas'])."%') " : "");
  //$parameter.=($params['keyword']!="''" && !empty($params['keyword']) ? " and ( w.WITEL_DESCRIPTION LIKE '%".ucfirst($params['keyword'])."%' OR w.WITEL_DESCRIPTION LIKE '%".strtolower($params['keyword'])."%' OR d.DATEL_DESCRIPTION LIKE '%".ucfirst($params['keyword'])."%' OR d.DATEL_DESCRIPTION LIKE '%".strtolower($params['keyword'])."%' or l.LOCATION LIKE '%".ucfirst($params['keyword'])."%' or l.LOCATION LIKE '%".strtolower($params['keyword'])."%' or l.CODE_LOCKET LIKE '%".ucfirst($params['keyword'])."%' or l.CODE_LOCKET LIKE '%".strtolower($params['keyword'])."%' or ll.LOCKET_NAME LIKE '%".ucfirst($params['keyword'])."%' or ll.LOCKET_NAME LIKE '%".strtolower($params['keyword'])."%' or l.PTGS_CASH_DESK LIKE '%".ucfirst($params['keyword'])."%' or l.PTGS_CASH_DESK LIKE '%".strtolower($params['keyword'])."%' or l.TRANSACTION_TYPE LIKE '%".ucfirst($params['keyword'])."%' or l.TRANSACTION_TYPE LIKE '%".strtolower($params['keyword'])."%')" : ""); 
    $query=$this->db->query("
      SELECT *
      FROM MYARMS_MODIF_NUMBER b
    $parameter
      $order_by
    ");
  return $query->result();
  } 
}