<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sto_Model extends MY_Model {
  public $table_name = 'MYARMS_STO';
  public function __construct() {
    parent::__construct();
  }
  public function warm_billing_by_date()
  {
    return $this->db->query("
      select DISTINCT(STO) from MYARMS_WARM_BILLING where STO is not null order by STO ASC
    ")->result();
  }
  public function l251()
  {
    return $this->db->query("
      select DISTINCT(STO_ID) from MYARMS_L251 where STO_ID is not null order by STO_ID ASC
    ")->result();
  }
}