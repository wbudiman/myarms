<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_Model extends MY_Model {
  public $table_name = 'MYARMS_USERS';
  public function __construct() {
    parent::__construct();
  }
  public function lists()
  {
    $parameter=$this->limit!="" ? " WHERE ROWNUM >=".$this->offset." and ROWNUM<=".$this->limit : "";
    return $this->db->query("
      select u.*,ur.ROLE_NAME
      from MYARMS_USERS u
      left join MYARMS_USER_ROLES ur ON u.USER_ROLE_ID=ur.USER_ROLE_ID
    ".$parameter)->result();
  }
}