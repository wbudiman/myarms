<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report_Model extends MY_Model {
  public $table_name = '';
  public function __construct() {
    parent::__construct();
  }
  //MENU DETAIL BILLING
  public function BILLING_DETAIL_REVENUEWSW_revenue($params=array())
  {
    $data_ubis=explode(",",str_replace("'","",$params['ubis']));
    $periode=$params['year'].$params['month'];
    $params['row']=($params['row']=="billing_component" ? "" : $params['row']);
    $params['column']=($params['column']=="billing_component" ? "" : $params['column']);
    $group_by=($params['row']!="" || $params['column']!="" ? "GROUP BY ".$params['row'].($params['row']!="" && $params['column']!="" ? "," : "").$params['column'] : "");
    $order_by=($params['row']!="" ? "ORDER BY ".$params['row']." ASC" : "");
    $parameter="WHERE b.PERIODE='".$periode."' ";
    $parameter.=($params['division']!="''" ? " and di.DIVISION_CODE in (".$params['division'].")" : "");
    $parameter.=($params['witel']!="''" ? " and d.WITEL_CODE in (".$params['witel'].")" : "");
    $parameter.=($params['datel']!="''" ? " and d.DATEL_CODE in (".$params['datel'].")" : "");
    $parameter.=($params['ubis']!="''" ? " and us.UBIS_ID in (".$params['ubis'].")" : "");
    $parameter.=($params['summary_product']!="''" ? " and p.SUMMARY_PRODUCT in (".$params['summary_product'].")" : " and p.SUMMARY_PRODUCT in ('Wireline','Wireless','Speedy')");
    $parameter.=($params['product']!="''" ? " and p.PRODUCT_NAME in (".$params['product'].")" : "");
    $parameter.=($params['ubis_segment']!="''" ? " and (us.UBIS_SEGMENT in (".$params['ubis_segment'].") or cc.CUSTOMER_CATEGORY_DESCRIPTION in (".$params['ubis_segment']."))" : "");
    $parameter.=($params['top_customer']!="''" ? " and b.PORTOFOLIO in (".$params['top_customer'].")" : "");
    $parameter.=($params['indihome']!="" ? " and b.INDIHOME='".$params['indihome']."'" : "");
    $query=$this->db->query("
      SELECT 
        ".$params['row'].($params['row']!="" ? "," : "").$params['column'].($params['column']!="" ? "," : "")."
        sum(b.SST) as SST,sum(b.ABODEMEN) as ABODEMEN,sum(b.LOKAL) as LOKAL,sum(b.SLJJ) as SLJJ,sum(b.OPERATOR) as OPERATOR,sum(b.STB) as STB,
        sum(b.SLI_TELKOM) as SLI_TELKOM,sum(b.SLI_INDOSAT) as SLI_INDOSAT,sum(b.SLI_BAKRIE) as SLI_BAKRIE,sum(b.TGLOBAL) as TGLOBAL,
        sum(b.TSAVE) as TSAVE,sum(b.TELKOMNET) as TELKOMNET,sum(b.TELKOMSMS) as TELKOMSMS,sum(b.IN_LOKAL) as IN_LOKAL,sum(b.IN_NASIONAL) as IN_NASIONAL,sum(b.EXCESS) as EXCESS,
        sum(b.KONTENT) as KONTENT,sum(b.KREDIT) as KREDIT,sum(b.DEBIT) as DEBIT,sum(b.QUOTA) as QUOTA,sum(b.ISDN_VOICE) as ISDN_VOICE,sum(b.ISDN_DATA) as ISDN_DATA,
        sum(b.LAINNYA) as LAINNYA,sum(b.TAGIHAN) as TAGIHAN,sum(b.PPN) as PPN,sum(b.MATERAI) as MATERAI,sum(b.TOTAL_TAGIHAN) as TOTAL_TAGIHAN,sum(b.OTC) as OTC,sum(b.DISCOUNT) as DISCOUNT,
        sum(b.RESTITUSI) as RESTITUSI,sum(b.ADJUSMENT) as ADJUSMENT,sum(b.BEBAN) as BEBAN,sum(b.PENDAPATAN) as PENDAPATAN
      FROM MYARMS_BILLING b
      LEFT JOIN MYARMS_CUSTOMER_CATEGORY cc ON b.CUSTOMER_CATEGORY_ID=cc.CUSTOMER_CATEGORY_ID
      LEFT JOIN MYARMS_PORTOFOLIO por ON b.PORTOFOLIO=por.PORTOFOLIO_ID
      LEFT JOIN MYARMS_INDIHOME i ON b.INDIHOME=i.INDIHOME_ID
      LEFT JOIN MYARMS_STO s ON s.STO_ID=b.STO_ID
      LEFT JOIN MYARMS_PRODUCT p ON b.PRODUCT_ID=p.PRODUCT_ID
      LEFT JOIN MYARMS_BUSINESS_AREA ba ON b.BUSINESS_AREA_ID=ba.BUSINESS_AREA_ID
      LEFT JOIN MYARMS_UBIS_SEGMENT us ON ba.UBIS_SEGMENT_ID=us.UBIS_SEGMENT_ID
      LEFT JOIN MYARMS_UBIS u ON us.UBIS_ID=u.UBIS_ID
      LEFT JOIN MYARMS_DATEL d ON d.DATEL_CODE=b.DATEL_CODE
      LEFT JOIN MYARMS_WITEL w ON d.WITEL_CODE=w.WITEL_CODE
      LEFT JOIN MYARMS_DIVISION di ON w.DIVISION_CODE=di.DIVISION_CODE
      $parameter
      $group_by
      $order_by
    ");
    return $query->result();
  }
  public function BILLING_DETAIL_REVENUEWSW_growth($params=array())
  {
    $periode_1=$params['year_1'].$params['month_1'];
    $periode_2=$params['year_2'].$params['month_2'];
    $periode_3=$params['year_3'].$params['month_3'];
    $parameter="WHERE b.PERIODE IN ('".$periode_1."','".$periode_2."','".$periode_3."') ";
    $parameter.=($params['division']!="''" ? " and di.DIVISION_CODE in (".$params['division'].")" : "");
    $parameter.=($params['witel']!="''" ? " and d.WITEL_CODE in (".$params['witel'].")" : "");
    $parameter.=($params['datel']!="''" ? " and d.DATEL_CODE in (".$params['datel'].")" : "");
    $parameter.=($params['ubis']!="''" ? " and us.UBIS_ID in (".$params['ubis'].")" : "");
    $parameter.=($params['summary_product']!="''" ? " and p.SUMMARY_PRODUCT in (".$params['summary_product'].")" : " and p.SUMMARY_PRODUCT in ('Wireline','Wireless','Speedy')");
    $parameter.=($params['product']!="''" ? " and p.PRODUCT_NAME in (".$params['product'].")" : "");
    $parameter.=($params['ubis_segment']!="''" ? " and (us.UBIS_SEGMENT in (".$params['ubis_segment'].") or cc.CUSTOMER_CATEGORY_DESCRIPTION in (".$params['ubis_segment']."))" : "");
    $parameter.=($params['top_customer']!="''" ? " and b.PORTOFOLIO in (".$params['top_customer'].")" : "");
    $parameter.=($params['indihome']!="" ? " and b.INDIHOME='".$params['indihome']."'" : "");
    $query=$this->db->query("
      SELECT 
        b.PERIODE,".($params['show_data']!="billing_component" ? $params['show_data']."," : "")."
        sum(b.SST) as SST,sum(b.ABODEMEN) as ABODEMEN,sum(b.LOKAL) as LOKAL,sum(b.SLJJ) as SLJJ,sum(b.OPERATOR) as OPERATOR,sum(b.STB) as STB,
        sum(b.SLI_TELKOM) as SLI_TELKOM,sum(b.SLI_INDOSAT) as SLI_INDOSAT,sum(b.SLI_BAKRIE) as SLI_BAKRIE,sum(b.TGLOBAL) as TGLOBAL,
        sum(b.TSAVE) as TSAVE,sum(b.TELKOMNET) as TELKOMNET,sum(b.TELKOMSMS) as TELKOMSMS,sum(b.IN_LOKAL) as IN_LOKAL,sum(b.IN_NASIONAL) as IN_NASIONAL,sum(b.EXCESS) as EXCESS,
        sum(b.KONTENT) as KONTENT,sum(b.KREDIT) as KREDIT,sum(b.DEBIT) as DEBIT,sum(b.QUOTA) as QUOTA,sum(b.ISDN_VOICE) as ISDN_VOICE,sum(b.ISDN_DATA) as ISDN_DATA,
        sum(b.LAINNYA) as LAINNYA,sum(b.TAGIHAN) as TAGIHAN,sum(b.PPN) as PPN,sum(b.MATERAI) as MATERAI,sum(b.TOTAL_TAGIHAN) as TOTAL_TAGIHAN,sum(b.OTC) as OTC,sum(b.DISCOUNT) as DISCOUNT,
        sum(b.RESTITUSI) as RESTITUSI,sum(b.ADJUSMENT) as ADJUSMENT,sum(b.BEBAN) as BEBAN,sum(b.PENDAPATAN) as PENDAPATAN
      FROM MYARMS_BILLING b
      LEFT JOIN MYARMS_PORTOFOLIO por ON b.PORTOFOLIO=por.PORTOFOLIO_ID
      LEFT JOIN MYARMS_INDIHOME i ON b.INDIHOME=i.INDIHOME_ID
      LEFT JOIN MYARMS_STO s ON s.STO_ID=b.STO_ID
      LEFT JOIN MYARMS_PRODUCT p ON b.PRODUCT_ID=p.PRODUCT_ID
      LEFT JOIN MYARMS_BUSINESS_AREA ba ON b.BUSINESS_AREA_ID=ba.BUSINESS_AREA_ID
      LEFT JOIN MYARMS_UBIS_SEGMENT us ON ba.UBIS_SEGMENT_ID=us.UBIS_SEGMENT_ID
      LEFT JOIN MYARMS_UBIS u ON us.UBIS_ID=u.UBIS_ID
      LEFT JOIN MYARMS_DATEL d ON d.DATEL_CODE=b.DATEL_CODE
      LEFT JOIN MYARMS_WITEL w ON d.WITEL_CODE=w.WITEL_CODE
      LEFT JOIN MYARMS_DIVISION di ON w.DIVISION_CODE=di.DIVISION_CODE
      $parameter
      GROUP BY b.PERIODE".($params['show_data']!="billing_component" ? ",".$params['show_data'] : "")."
      ORDER BY b.PERIODE ASC
    ");
    return $query->result();
  }
  public function BILLING_DETAIL_REVENUEWSW_news_event($params=array())
  {
    $periode_1=$params['year_1'].$params['month_1'];
    $periode_2=$params['year_2'].$params['month_2'];
    $periode_3=$params['year_3'].$params['month_3'];
    $parameter="WHERE b.PERIODE IN ('".$periode_1."','".$periode_2."','".$periode_3."') ";
    $parameter.=($params['division']!="''" ? " and di.DIVISION_CODE in (".$params['division'].")" : "");
    $parameter.=($params['witel']!="''" ? " and d.WITEL_CODE in (".$params['witel'].")" : "");
    $parameter.=($params['datel']!="''" ? " and d.DATEL_CODE in (".$params['datel'].")" : "");
    $parameter.=($params['ubis']!="''" ? " and us.UBIS_ID in (".$params['ubis'].")" : "");
    $parameter.=($params['summary_product']!="''" ? " and p.SUMMARY_PRODUCT in (".$params['summary_product'].")" : " and p.SUMMARY_PRODUCT in ('Wireline','Wireless','Speedy')");
    $parameter.=($params['product']!="''" ? " and p.PRODUCT_NAME in (".$params['product'].")" : "");
    $parameter.=($params['ubis_segment']!="''" ? " and (us.UBIS_SEGMENT in (".$params['ubis_segment'].") or cc.CUSTOMER_CATEGORY_DESCRIPTION in (".$params['ubis_segment']."))" : "");
    $parameter.=($params['top_customer']!="''" ? " and b.PORTOFOLIO in (".$params['top_customer'].")" : "");
    $parameter.=($params['indihome']!="" ? " and b.INDIHOME='".$params['indihome']."'" : "");
    $query=$this->db->query("
      SELECT 
        b.PERIODE,p.SUMMARY_PRODUCT,".($params['show_data']!="billing_component" ? $params['show_data']."," : "")."
        sum(b.SST) as SST,sum(b.ABODEMEN) as ABODEMEN,sum(b.LOKAL) as LOKAL,sum(b.SLJJ) as SLJJ,sum(b.OPERATOR) as OPERATOR,sum(b.STB) as STB,
        sum(b.SLI_TELKOM) as SLI_TELKOM,sum(b.SLI_INDOSAT) as SLI_INDOSAT,sum(b.SLI_BAKRIE) as SLI_BAKRIE,sum(b.TGLOBAL) as TGLOBAL,
        sum(b.TSAVE) as TSAVE,sum(b.TELKOMNET) as TELKOMNET,sum(b.TELKOMSMS) as TELKOMSMS,sum(b.IN_LOKAL) as IN_LOKAL,sum(b.IN_NASIONAL) as IN_NASIONAL,sum(b.EXCESS) as EXCESS,
        sum(b.KONTENT) as KONTENT,sum(b.KREDIT) as KREDIT,sum(b.DEBIT) as DEBIT,sum(b.QUOTA) as QUOTA,sum(b.ISDN_VOICE) as ISDN_VOICE,sum(b.ISDN_DATA) as ISDN_DATA,
        sum(b.LAINNYA) as LAINNYA,sum(b.TAGIHAN) as TAGIHAN,sum(b.PPN) as PPN,sum(b.MATERAI) as MATERAI,sum(b.TOTAL_TAGIHAN) as TOTAL_TAGIHAN,sum(b.OTC) as OTC,sum(b.DISCOUNT) as DISCOUNT,
        sum(b.RESTITUSI) as RESTITUSI,sum(b.ADJUSMENT) as ADJUSMENT,sum(b.BEBAN) as BEBAN,sum(b.PENDAPATAN) as PENDAPATAN
      FROM MYARMS_BILLING b     
      LEFT JOIN MYARMS_PORTOFOLIO por ON b.PORTOFOLIO=por.PORTOFOLIO_ID
      LEFT JOIN MYARMS_INDIHOME i ON b.INDIHOME=i.INDIHOME_ID
      LEFT JOIN MYARMS_STO s ON s.STO_ID=b.STO_ID
      LEFT JOIN MYARMS_PRODUCT p ON b.PRODUCT_ID=p.PRODUCT_ID
      LEFT JOIN MYARMS_BUSINESS_AREA ba ON b.BUSINESS_AREA_ID=ba.BUSINESS_AREA_ID
      LEFT JOIN MYARMS_UBIS_SEGMENT us ON ba.UBIS_SEGMENT_ID=us.UBIS_SEGMENT_ID
      LEFT JOIN MYARMS_UBIS u ON us.UBIS_ID=u.UBIS_ID
      LEFT JOIN MYARMS_DATEL d ON d.DATEL_CODE=b.DATEL_CODE
      LEFT JOIN MYARMS_WITEL w ON d.WITEL_CODE=w.WITEL_CODE
      LEFT JOIN MYARMS_DIVISION di ON w.DIVISION_CODE=di.DIVISION_CODE
      $parameter
      GROUP BY b.PERIODE,p.SUMMARY_PRODUCT ".($params['show_data']!="billing_component" ? ",".$params['show_data'] : "")."
      ORDER BY b.PERIODE ASC
    ");
    return $query->result();
  }
  public function BILLING_DETAIL_REVENUEWSW_billing_committee($params=array())
  {
    $periode_1=$params['year_1'].$params['month_1'];
    $periode_2=$params['year_2'].$params['month_2'];
    $periode_3=$params['year_3'].$params['month_3'];
    $params['header']=($params['header']=="billing_component" ? "" : $params['header']);
    $params['detail']=($params['detail']=="billing_component" ? "" : $params['detail']);
    $group_by=($params['header']!="" ? ",".$params['header'] : "");
    $group_by.=($params['detail']!="" ? ",".$params['detail'] : "");
    $parameter="WHERE b.PERIODE IN ('".$periode_1."','".$periode_2."','".$periode_3."') ";
    $parameter.=($params['division']!="''" ? " and di.DIVISION_CODE in (".$params['division'].")" : "");
    $parameter.=($params['witel']!="''" ? " and d.WITEL_CODE in (".$params['witel'].")" : "");
    $parameter.=($params['datel']!="''" ? " and d.DATEL_CODE in (".$params['datel'].")" : "");
    $parameter.=($params['ubis']!="''" ? " and us.UBIS_ID in (".$params['ubis'].")" : "");
    $parameter.=($params['summary_product']!="''" ? " and p.SUMMARY_PRODUCT in (".$params['summary_product'].")" : " and p.SUMMARY_PRODUCT in ('Wireline','Wireless','Speedy')");
    $parameter.=($params['product']!="''" ? " and p.PRODUCT_NAME in (".$params['product'].")" : "");
    $parameter.=($params['ubis_segment']!="''" ? " and (us.UBIS_SEGMENT in (".$params['ubis_segment'].") or cc.CUSTOMER_CATEGORY_DESCRIPTION in (".$params['ubis_segment']."))" : "");
    $parameter.=($params['top_customer']!="''" ? " and b.PORTOFOLIO in (".$params['top_customer'].")" : "");
    $parameter.=($params['indihome']!="" ? " and b.INDIHOME='".$params['indihome']."'" : "");
    $query=$this->db->query("
      SELECT 
        b.PERIODE,".$params['header'].($params['header']!="" ? "," : "").$params['detail'].($params['detail']!="" ? "," : "")."
        sum(b.SST) as SST,sum(b.ABODEMEN) as ABODEMEN,sum(b.LOKAL) as LOKAL,sum(b.SLJJ) as SLJJ,sum(b.OPERATOR) as OPERATOR,sum(b.STB) as STB,
        sum(b.SLI_TELKOM) as SLI_TELKOM,sum(b.SLI_INDOSAT) as SLI_INDOSAT,sum(b.SLI_BAKRIE) as SLI_BAKRIE,sum(b.TGLOBAL) as TGLOBAL,
        sum(b.TSAVE) as TSAVE,sum(b.TELKOMNET) as TELKOMNET,sum(b.TELKOMSMS) as TELKOMSMS,sum(b.IN_LOKAL) as IN_LOKAL,sum(b.IN_NASIONAL) as IN_NASIONAL,sum(b.EXCESS) as EXCESS,
        sum(b.KONTENT) as KONTENT,sum(b.KREDIT) as KREDIT,sum(b.DEBIT) as DEBIT,sum(b.QUOTA) as QUOTA,sum(b.ISDN_VOICE) as ISDN_VOICE,sum(b.ISDN_DATA) as ISDN_DATA,
        sum(b.LAINNYA) as LAINNYA,sum(b.TAGIHAN) as TAGIHAN,sum(b.PPN) as PPN,sum(b.MATERAI) as MATERAI,sum(b.TOTAL_TAGIHAN) as TOTAL_TAGIHAN,sum(b.OTC) as OTC,sum(b.DISCOUNT) as DISCOUNT,
        sum(b.RESTITUSI) as RESTITUSI,sum(b.ADJUSMENT) as ADJUSMENT,sum(b.BEBAN) as BEBAN,sum(b.PENDAPATAN) as PENDAPATAN
      FROM MYARMS_BILLING b
      LEFT JOIN MYARMS_PORTOFOLIO por ON b.PORTOFOLIO=por.PORTOFOLIO_ID
      LEFT JOIN MYARMS_INDIHOME i ON b.INDIHOME=i.INDIHOME_ID
      LEFT JOIN MYARMS_STO s ON s.STO_ID=b.STO_ID
      LEFT JOIN MYARMS_PRODUCT p ON b.PRODUCT_ID=p.PRODUCT_ID
      LEFT JOIN MYARMS_BUSINESS_AREA ba ON b.BUSINESS_AREA_ID=ba.BUSINESS_AREA_ID
      LEFT JOIN MYARMS_UBIS_SEGMENT us ON ba.UBIS_SEGMENT_ID=us.UBIS_SEGMENT_ID
      LEFT JOIN MYARMS_UBIS u ON us.UBIS_ID=u.UBIS_ID
      LEFT JOIN MYARMS_DATEL d ON d.DATEL_CODE=b.DATEL_CODE
      LEFT JOIN MYARMS_WITEL w ON d.WITEL_CODE=w.WITEL_CODE
      LEFT JOIN MYARMS_DIVISION di ON w.DIVISION_CODE=di.DIVISION_CODE
      $parameter
      GROUP BY b.PERIODE ".$group_by."
      ORDER BY b.PERIODE ASC
    ");
    return $query->result();
  }
  public function BILLING_DETAIL_REVENUEWSW_tree($params=array())
  {
    $periode=$params['year'].$params['month'];
    $group_by=($params['show_data']!="" ? "GROUP BY ".$params['show_data'] : "").(isset($params['status']) ? ",".$params['status'].",ts.TREE_STATUS_NAME" : "");
    $order_by=($params['show_data']!="" ? "ORDER BY ".$params['show_data'].(isset($params['status']) ? ",ts.TREE_STATUS_NAME" : "")." ASC" : "");
    $parameter="WHERE b.PERIODE='".$periode."' ";
    $parameter.=($params['division']!="''" ? " and di.DIVISION_CODE in (".$params['division'].")" : "");
    $parameter.=($params['witel']!="''" ? " and d.WITEL_CODE in (".$params['witel'].")" : "");
    $parameter.=($params['datel']!="''" ? " and d.DATEL_CODE in (".$params['datel'].")" : "");
    $parameter.=($params['ubis']!="''" ? " and us.UBIS_ID in (".$params['ubis'].")" : "");
    $parameter.=($params['summary_product']!="''" ? " and p.SUMMARY_PRODUCT in (".$params['summary_product'].")" : " and p.SUMMARY_PRODUCT in ('Wireline','Wireless','Speedy')");
    $parameter.=($params['product']!="''" ? " and p.PRODUCT_NAME in (".$params['product'].")" : "");
    $parameter.=($params['ubis_segment']!="''" ? " and (us.UBIS_SEGMENT in (".$params['ubis_segment'].") or cc.CUSTOMER_CATEGORY_DESCRIPTION in (".$params['ubis_segment']."))" : "");
    $parameter.=($params['top_customer']!="''" ? " and b.PORTOFOLIO in (".$params['top_customer'].")" : "");
    $parameter.=($params['indihome']!="" ? " and b.INDIHOME='".$params['indihome']."'" : "");
    $parameter.=($params['usage']!="" ? " and b.USAGE='".$params['usage']."'" : "");
    $parameter.=($params['packet_speedy']!="" ? " and b.PACKET_SPEEDY_ID='".$params['packet_speedy']."'" : "");
    $query=$this->db->query("
      SELECT 
        ".$params['show_data'].($params['show_data']!="" ? "," : "").(isset($params['status']) ? $params['status'].",ts.TREE_STATUS_NAME," : "")."
        sum(b.SST) as SST,sum(b.ABODEMEN) as ABODEMEN,sum(b.LOKAL) as LOKAL,sum(b.SLJJ) as SLJJ,sum(b.OPERATOR) as OPERATOR,sum(b.STB) as STB,
        sum(b.SLI_TELKOM) as SLI_TELKOM,sum(b.SLI_INDOSAT) as SLI_INDOSAT,sum(b.SLI_BAKRIE) as SLI_BAKRIE,sum(b.TGLOBAL) as TGLOBAL,
        sum(b.TSAVE) as TSAVE,sum(b.TELKOMNET) as TELKOMNET,sum(b.TELKOMSMS) as TELKOMSMS,sum(b.IN_LOKAL) as IN_LOKAL,sum(b.IN_NASIONAL) as IN_NASIONAL,sum(b.EXCESS) as EXCESS,
        sum(b.KONTENT) as KONTENT,sum(b.KREDIT) as KREDIT,sum(b.DEBIT) as DEBIT,sum(b.QUOTA) as QUOTA,sum(b.ISDN_VOICE) as ISDN_VOICE,sum(b.ISDN_DATA) as ISDN_DATA,
        sum(b.LAINNYA) as LAINNYA,sum(b.TAGIHAN) as TAGIHAN,sum(b.PPN) as PPN,sum(b.MATERAI) as MATERAI,sum(b.TOTAL_TAGIHAN) as TOTAL_TAGIHAN,sum(b.OTC) as OTC,sum(b.DISCOUNT) as DISCOUNT,
        sum(b.RESTITUSI) as RESTITUSI,sum(b.ADJUSMENT) as ADJUSMENT,sum(b.BEBAN) as BEBAN,sum(b.PENDAPATAN) as PENDAPATAN
      FROM MYARMS_BILLING b
      LEFT JOIN MYARMS_TREE_STATUS ts ON b.STATUS=ts.TREE_STATUS_ID and b.TYPE_POHON=ts.TREE_TYPE_ID
      LEFT JOIN MYARMS_PORTOFOLIO por ON b.PORTOFOLIO=por.PORTOFOLIO_ID
      LEFT JOIN MYARMS_INDIHOME i ON b.INDIHOME=i.INDIHOME_ID
      LEFT JOIN MYARMS_STO s ON s.STO_ID=b.STO_ID
      LEFT JOIN MYARMS_PRODUCT p ON b.PRODUCT_ID=p.PRODUCT_ID
      LEFT JOIN MYARMS_BUSINESS_AREA ba ON b.BUSINESS_AREA_ID=ba.BUSINESS_AREA_ID
      LEFT JOIN MYARMS_UBIS_SEGMENT us ON ba.UBIS_SEGMENT_ID=us.UBIS_SEGMENT_ID
      LEFT JOIN MYARMS_UBIS u ON us.UBIS_ID=u.UBIS_ID
      LEFT JOIN MYARMS_DATEL d ON d.DATEL_CODE=b.DATEL_CODE
      LEFT JOIN MYARMS_WITEL w ON d.WITEL_CODE=w.WITEL_CODE
      LEFT JOIN MYARMS_DIVISION di ON w.DIVISION_CODE=di.DIVISION_CODE
      $parameter
      $group_by
      $order_by
    ");
    return $query->result();
  }
  public function BILLING_DETAIL_REVENUEWSW_tree_detail($params=array())
  {
    $periode=$params['year'].$params['month'];
    $order_by=($params['show_data']!="" ? "ORDER BY ".$params['show_data'].(isset($params['status']) ? ",ts.TREE_STATUS_NAME" : "")." ASC" : "");
    $parameter="WHERE b.PERIODE='".$periode."' ";
    $parameter.=($params['limit']!="" ? " and ROWNUM>0 and ROWNUM<=".$params['limit'] : "");
    $parameter.=($params['division']!="''" ? " and di.DIVISION_CODE in (".$params['division'].")" : "");
    $parameter.=($params['witel']!="''" ? " and d.WITEL_CODE in (".$params['witel'].")" : "");
    $parameter.=($params['datel']!="''" ? " and d.DATEL_CODE in (".$params['datel'].")" : "");
    $parameter.=($params['ubis']!="''" ? " and us.UBIS_ID in (".$params['ubis'].")" : "");
    $parameter.=($params['summary_product']!="''" ? " and p.SUMMARY_PRODUCT in (".$params['summary_product'].")" : " and p.SUMMARY_PRODUCT in ('Wireline','Wireless','Speedy')");
    $parameter.=($params['product']!="''" ? " and p.PRODUCT_NAME in (".$params['product'].")" : "");
    $parameter.=($params['ubis_segment']!="''" ? " and us.UBIS_SEGMENT_ID in (".$params['ubis_segment'].")" : "");
    $parameter.=($params['top_customer']!="''" ? " and b.PORTOFOLIO in (".$params['top_customer'].")" : "");
    $parameter.=($params['indihome']!="" ? " and b.INDIHOME='".$params['indihome']."'" : "");
    $parameter.=($params['usage']!="" ? " and b.USAGE='".$params['usage']."'" : "");
    $parameter.=($params['packet_speedy']!="" ? " and b.PACKET_SPEEDY_ID='".$params['packet_speedy']."'" : "");
    $query=$this->db->query("
      SELECT 
        b.CCA,b.SND,b.SND_GROUP,ba.BUSINESS_AREA_DESCRIPTION,b.TOTAL,b.TOTAL_LALU,up.UMUR_PELANGGAN_DESCRIPTION,p.PRODUCT_NAME,usa.USAGE_STATUS,ps.PACKET_SPEEDY_DESCRIPTION,pf.PACKET_FBIP_DESCRIPTION
      FROM MYARMS_BILLING_DETAIL b
      LEFT JOIN MYARMS_UMUR_PELANGGAN up ON b.UMUR_PELANGGAN_ID=up.UMUR_PELANGGAN_ID
      LEFT JOIN MYARMS_PACKET_FBIP pf ON b.PACKET_FBIP_ID=pf.PACKET_FBIP_ID
      LEFT JOIN MYARMS_PACKET_SPEEDY ps ON b.PACKET_SPEEDY_ID=ps.PACKET_SPEEDY_ID
      LEFT JOIN MYARMS_USAGE_STATUS usa ON b.USAGE=usa.USAGE_ID
      LEFT JOIN MYARMS_TREE_STATUS ts ON b.STATUS=ts.TREE_STATUS_ID and b.TYPE_POHON=ts.TREE_TYPE_ID
      LEFT JOIN MYARMS_PORTOFOLIO por ON b.PORTOFOLIO=por.PORTOFOLIO_ID
      LEFT JOIN MYARMS_INDIHOME i ON b.INDIHOME=i.INDIHOME_ID
      LEFT JOIN MYARMS_STO s ON s.STO_ID=b.STO_ID
      LEFT JOIN MYARMS_PRODUCT p ON b.PRODUCT_ID=p.PRODUCT_ID
      LEFT JOIN MYARMS_BUSINESS_AREA ba ON b.BUSINESS_AREA_ID=ba.BUSINESS_AREA_ID
      LEFT JOIN MYARMS_UBIS_SEGMENT us ON ba.UBIS_SEGMENT_ID=us.UBIS_SEGMENT_ID
      LEFT JOIN MYARMS_UBIS u ON us.UBIS_ID=u.UBIS_ID
      LEFT JOIN MYARMS_DATEL d ON b.DATEL_CODE=d.DATEL_CODE
      LEFT JOIN MYARMS_WITEL w ON d.WITEL_CODE=w.WITEL_CODE
      LEFT JOIN MYARMS_DIVISION di ON w.DIVISION_CODE=di.DIVISION_CODE
      $parameter
      $order_by
    ");
    return $query->result();
  }
  //MENU BILLING RESULT
  public function BILLING_RESULT_revenue($params=array())
  {
    $periode=$params['year'].$params['month'];
    $params['row']=($params['row']=="billing_component" ? "b.KOMPONEN_BILL" : $params['row']);
    $params['column']=($params['column']=="billing_component" ? "b.KOMPONEN_BILL" : $params['column']);
    $group_by=($params['row']!="" || $params['column']!="" ? "GROUP BY ".$params['row'].($params['row']!="" && $params['column']!="" ? "," : "").$params['column'] : "");
    $order_by=($params['row']!="" ? "ORDER BY ".$params['row']." ASC" : "");
    $parameter="WHERE b.PERIODE='".$periode."' ";
    $parameter.=($params['division']!="''" ? " and di.DIVISION_CODE in (".$params['division'].")" : "");
    $parameter.=($params['witel']!="''" ? " and d.WITEL_CODE in (".$params['witel'].")" : "");
    $parameter.=($params['datel']!="''" ? " and d.DATEL_CODE in (".$params['datel'].")" : "");
    $parameter.=($params['summary_product']!="''" ? " and p.SUMMARY_PRODUCT in (".$params['summary_product'].")" : " and p.SUMMARY_PRODUCT in ('Speedy','Wireline')");
    $parameter.=($params['product']!="''" ? " and p.PRODUCT_NAME in (".$params['product'].")" : "");
    $parameter.=($params['billing_type']!="''" ? " and b.BILLING_TYPE in (".$params['billing_type'].")" : "");
    $parameter.=($params['component_type']!="''" ? " and b.COMPONENT_TYPE in (".$params['component_type'].")" : "");
    $parameter.=($params['customer_category']!="''" ? " and b.CUSTOMER_CATEGORY_ID in (".$params['customer_category'].")" : "");
    $parameter.=($params['billing_component']!="" ? " and b.KOMPONEN_BILL = '".$params['billing_component']."'" : "");
    $query=$this->db->query("
      SELECT 
        ".$params['row'].($params['row']!="" ? "," : "").$params['column'].($params['column']!="" ? "," : "")."
        sum(b.NB_ELMT) as NB_ELMT,sum(b.AMOUNT) as AMOUNT
      FROM MYARMS_BILLING_RESULT_REVENUE b
      LEFT JOIN MYARMS_COMPONENT_TYPE ct ON b.COMPONENT_TYPE=ct.COMPONENT_TYPE
      LEFT JOIN MYARMS_BILLING_TYPE bt ON b.BILLING_TYPE=bt.BILLING_TYPE_ID
      LEFT JOIN MYARMS_PRODUCT p ON b.PRODUCT_ID=p.PRODUCT_ID
      LEFT JOIN MYARMS_CUSTOMER_CATEGORY cc ON cc.CUSTOMER_CATEGORY_ID=b.CUSTOMER_CATEGORY_ID
      LEFT JOIN MYARMS_DATEL d ON d.DATEL_CODE=b.DATEL_CODE
      LEFT JOIN MYARMS_WITEL w ON d.WITEL_CODE=w.WITEL_CODE
      LEFT JOIN MYARMS_DIVISION di ON w.DIVISION_CODE=di.DIVISION_CODE
      $parameter
      $group_by
      $order_by
    ");
    return $query->result();
  }
  public function BILLING_RESULT_growth($params=array())
  {
    $periode_1=$params['year_1'].$params['month_1'];
    $periode_2=$params['year_2'].$params['month_2'];
    $periode_3=$params['year_3'].$params['month_3'];
    $params['show_data']=($params['show_data']=="billing_component" ? "b.KOMPONEN_BILL" : $params['show_data']);
    $parameter="WHERE b.PERIODE IN ('".$periode_1."','".$periode_2."','".$periode_3."') ";
    $parameter.=($params['division']!="''" ? " and di.DIVISION_CODE in (".$params['division'].")" : "");
    $parameter.=($params['witel']!="''" ? " and d.WITEL_CODE in (".$params['witel'].")" : "");
    $parameter.=($params['datel']!="''" ? " and d.DATEL_CODE in (".$params['datel'].")" : "");
    $parameter.=($params['summary_product']!="''" ? " and p.SUMMARY_PRODUCT in (".$params['summary_product'].")" : " and p.SUMMARY_PRODUCT in ('Speedy','Wireline')");
    $parameter.=($params['product']!="''" ? " and p.PRODUCT_NAME in (".$params['product'].")" : "");
    $parameter.=($params['customer_category']!="''" ? " and cc.CUSTOMER_CATEGORY_ID in (".$params['customer_category'].")" : "");
    $parameter.=($params['billing_type']!="''" ? " and b.BILLING_TYPE in (".$params['billing_type'].")" : "");
    $parameter.=($params['component_type']!="''" ? " and b.COMPONENT_TYPE in (".$params['component_type'].")" : "");
    $parameter.=($params['billing_component']!="" ? " and b.KOMPONEN_BILL = '".$params['billing_component']."'" : "");
    $query=$this->db->query("
      SELECT 
        b.PERIODE,".($params['show_data']!="billing_component" ? $params['show_data']."," : "")."
        sum(b.NB_ELMT) as NB_ELMT,sum(b.AMOUNT) as AMOUNT
      FROM MYARMS_BILLING_RESULT_REVENUE b
      LEFT JOIN MYARMS_COMPONENT_TYPE ct ON b.COMPONENT_TYPE=ct.COMPONENT_TYPE
      LEFT JOIN MYARMS_BILLING_TYPE bt ON b.BILLING_TYPE=bt.BILLING_TYPE_ID
      LEFT JOIN MYARMS_PRODUCT p ON b.PRODUCT_ID=p.PRODUCT_ID
      LEFT JOIN MYARMS_CUSTOMER_CATEGORY cc ON cc.CUSTOMER_CATEGORY_ID=b.CUSTOMER_CATEGORY_ID
      LEFT JOIN MYARMS_DATEL d ON d.DATEL_CODE=b.DATEL_CODE
      LEFT JOIN MYARMS_WITEL w ON d.WITEL_CODE=w.WITEL_CODE
      LEFT JOIN MYARMS_DIVISION di ON w.DIVISION_CODE=di.DIVISION_CODE
      $parameter
      GROUP BY b.PERIODE".($params['show_data']!="billing_component" ? ",".$params['show_data'] : "")."
      ORDER BY b.PERIODE ASC
    ");
    return $query->result();
  }
  public function BILLING_RESULT_growth_l11($params=array())
  {
    $periode_1=$params['year_1'].$params['month_1'];
    $periode_2=$params['year_2'].$params['month_2'];
    $periode_3=$params['year_3'].$params['month_3'];
    $parameter="WHERE b.PERIODE IN ('".$periode_1."','".$periode_2."','".$periode_3."') ";
    $parameter.=($params['division']!="''" ? " and di.DIVISION_CODE in (".$params['division'].")" : "");
    $parameter.=($params['witel']!="''" ? " and d.WITEL_CODE in (".$params['witel'].")" : "");
    $parameter.=($params['datel']!="''" ? " and d.DATEL_CODE in (".$params['datel'].")" : "");
    $parameter.=($params['summary_product']!="''" ? " and p.SUMMARY_PRODUCT in (".$params['summary_product'].")" : " and p.SUMMARY_PRODUCT in ('Speedy','Wireline')");
    $parameter.=($params['product']!="''" ? " and p.PRODUCT_NAME in (".$params['product'].")" : "");
    $parameter.=($params['customer_category']!="''" ? " and cc.CUSTOMER_CATEGORY_ID in (".$params['customer_category'].")" : "");
    $query=$this->db->query("
      SELECT 
        b.PERIODE,".($params['show_data']!="billing_component" ? $params['show_data']."," : "")."
        sum(b.SST) as L11,sum(b.AMOUNT) as AMOUNT
      FROM MYARMS_BILLING_RESULT_L11 b
      LEFT JOIN MYARMS_PRODUCT p ON b.PRODUCT_ID=p.PRODUCT_ID
      LEFT JOIN MYARMS_CUSTOMER_CATEGORY cc ON cc.CUSTOMER_CATEGORY_ID=b.CUSTOMER_CATEGORY_ID
      LEFT JOIN MYARMS_DATEL d ON d.DATEL_CODE=b.DATEL_CODE
      LEFT JOIN MYARMS_WITEL w ON d.WITEL_CODE=w.WITEL_CODE
      LEFT JOIN MYARMS_DIVISION di ON w.DIVISION_CODE=di.DIVISION_CODE
      $parameter
      GROUP BY b.PERIODE".($params['show_data']!="billing_component" ? ",".$params['show_data'] : "")."
      ORDER BY b.PERIODE ASC
    ");
    return $query->result();
  }
  public function BILLING_RESULT_billing_committee($params=array())
  {
    $periode_1=$params['year_1'].$params['month_1'];
    $periode_2=$params['year_2'].$params['month_2'];
    $periode_3=$params['year_3'].$params['month_3'];
    $params['header']=($params['header']=="billing_component" ? "" : $params['header']);
    $params['detail']=($params['detail']=="billing_component" ? "" : $params['detail']);
    $group_by=($params['header']!="" ? ",".$params['header'] : "");
    $group_by.=($params['detail']!="" ? ",".$params['detail'] : "");
    $parameter="WHERE b.PERIODE IN ('".$periode_1."','".$periode_2."','".$periode_3."') ";
    $parameter.=($params['division']!="''" ? " and di.DIVISION_CODE in (".$params['division'].")" : "");
    $parameter.=($params['witel']!="''" ? " and d.WITEL_CODE in (".$params['witel'].")" : "");
    $parameter.=($params['datel']!="''" ? " and d.DATEL_CODE in (".$params['datel'].")" : "");
    $parameter.=($params['summary_product']!="''" ? " and p.SUMMARY_PRODUCT in (".$params['summary_product'].")" : " and p.SUMMARY_PRODUCT in ('Speedy','Wireline')");
    $parameter.=($params['product']!="''" ? " and p.PRODUCT_NAME in (".$params['product'].")" : "");
    $parameter.=($params['customer_category']!="''" ? " and b.CUSTOMER_CATEGORY_ID in (".$params['customer_category'].")" : "");
    $parameter.=($params['billing_type']!="''" ? " and b.BILLING_TYPE in (".$params['billing_type'].")" : "");
    $parameter.=($params['component_type']!="''" ? " and b.COMPONENT_TYPE in (".$params['component_type'].")" : "");
    $query=$this->db->query("
      SELECT 
        b.PERIODE,".$params['header'].($params['header']!="" ? "," : "").$params['detail'].($params['detail']!="" ? "," : "")."
        sum(b.NB_ELMT) as NB_ELMT,sum(b.AMOUNT) as AMOUNT
      FROM MYARMS_BILLING_RESULT_REVENUE b
      LEFT JOIN MYARMS_COMPONENT_TYPE ct ON b.COMPONENT_TYPE=ct.COMPONENT_TYPE
      LEFT JOIN MYARMS_BILLING_TYPE bt ON b.BILLING_TYPE=bt.BILLING_TYPE_ID
      LEFT JOIN MYARMS_PRODUCT p ON b.PRODUCT_ID=p.PRODUCT_ID
      LEFT JOIN MYARMS_CUSTOMER_CATEGORY cc ON cc.CUSTOMER_CATEGORY_ID=b.CUSTOMER_CATEGORY_ID
      LEFT JOIN MYARMS_DATEL d ON d.DATEL_CODE=b.DATEL_CODE
      LEFT JOIN MYARMS_WITEL w ON d.WITEL_CODE=w.WITEL_CODE
      LEFT JOIN MYARMS_DIVISION di ON w.DIVISION_CODE=di.DIVISION_CODE
      $parameter
      GROUP BY b.PERIODE ".$group_by."
      ORDER BY b.PERIODE ASC
    ");
    return $query->result();
  }
  public function BILLING_RESULT_news_event($params=array())
  {
    $periode_1=$params['year_1'].$params['month_1'];
    $periode_2=$params['year_2'].$params['month_2'];
    $periode_3=$params['year_3'].$params['month_3'];
    $parameter="WHERE b.PERIODE IN ('".$periode_1."','".$periode_2."','".$periode_3."') ";
    $parameter.=($params['division']!="''" ? " and di.DIVISION_CODE in (".$params['division'].")" : "");
    $parameter.=($params['witel']!="''" ? " and d.WITEL_CODE in (".$params['witel'].")" : "");
    $parameter.=($params['datel']!="''" ? " and d.DATEL_CODE in (".$params['datel'].")" : "");
    $parameter.=($params['summary_product']!="''" ? " and p.SUMMARY_PRODUCT in (".$params['summary_product'].")" : " and p.SUMMARY_PRODUCT in ('Wireline','Speedy')");
    $parameter.=($params['product']!="''" ? " and p.PRODUCT_NAME in (".$params['product'].")" : "");
    $parameter.=($params['customer_category']!="''" ? " and b.CUSTOMER_CATEGORY_ID in (".$params['customer_category'].")" : "");
    $query=$this->db->query("
      SELECT 
        b.PERIODE,p.SUMMARY_PRODUCT,".($params['show_data']!="billing_component" ? $params['show_data']."," : "")."
        sum(b.SST) as SST,sum(b.AMOUNT) as AMOUNT
      FROM MYARMS_BILLING_RESULT_L11 b
      LEFT JOIN MYARMS_PRODUCT p ON b.PRODUCT_ID=p.PRODUCT_ID
      LEFT JOIN MYARMS_CUSTOMER_CATEGORY cc ON cc.CUSTOMER_CATEGORY_ID=b.CUSTOMER_CATEGORY_ID
      LEFT JOIN MYARMS_DATEL d ON d.DATEL_CODE=b.DATEL_CODE
      LEFT JOIN MYARMS_WITEL w ON d.WITEL_CODE=w.WITEL_CODE
      LEFT JOIN MYARMS_DIVISION di ON w.DIVISION_CODE=di.DIVISION_CODE
      $parameter
      GROUP BY b.PERIODE,p.SUMMARY_PRODUCT ".($params['show_data']!="billing_component" ? ",".$params['show_data'] : "")."
      ORDER BY b.PERIODE ASC
    ");
    return $query->result();
  }
  public function BILLING_RESULT_news_event_billing($params=array())
  {
    $periode_1=$params['year_1'].$params['month_1'];
    $periode_2=$params['year_2'].$params['month_2'];
    $periode_3=$params['year_3'].$params['month_3'];
    $parameter="WHERE b.PERIODE IN ('".$periode_1."','".$periode_2."','".$periode_3."') ";
    $parameter.=($params['division']!="''" ? " and di.DIVISION_CODE in (".$params['division'].")" : "");
    $parameter.=($params['witel']!="''" ? " and d.WITEL_CODE in (".$params['witel'].")" : "");
    $parameter.=($params['datel']!="''" ? " and d.DATEL_CODE in (".$params['datel'].")" : "");
    $parameter.=($params['summary_product']!="''" ? " and p.SUMMARY_PRODUCT in (".$params['summary_product'].")" : " and p.SUMMARY_PRODUCT in ('Speedy','Wireline')");
    $parameter.=($params['product']!="''" ? " and p.PRODUCT_NAME in (".$params['product'].")" : "");
    $parameter.=($params['customer_category']!="''" ? " and b.CUSTOMER_CATEGORY_ID in (".$params['customer_category'].")" : "");
    $parameter.=($params['billing_type']!="''" ? " and b.BILLING_TYPE in (".$params['billing_type'].")" : "");
    $parameter.=($params['component_type']!="''" ? " and b.COMPONENT_TYPE in (".$params['component_type'].")" : "");
    $query=$this->db->query("
      SELECT 
        b.PERIODE,p.SUMMARY_PRODUCT,b.KOMPONEN_BILL,
        sum(b.NB_ELMT) as NB_ELMT,sum(b.AMOUNT) as AMOUNT
      FROM MYARMS_BILLING_RESULT_REVENUE b
      LEFT JOIN MYARMS_COMPONENT_TYPE ct ON b.COMPONENT_TYPE=ct.COMPONENT_TYPE
      LEFT JOIN MYARMS_BILLING_TYPE bt ON b.BILLING_TYPE=bt.BILLING_TYPE_ID
      LEFT JOIN MYARMS_PRODUCT p ON b.PRODUCT_ID=p.PRODUCT_ID
      LEFT JOIN MYARMS_CUSTOMER_CATEGORY cc ON cc.CUSTOMER_CATEGORY_ID=b.CUSTOMER_CATEGORY_ID
      LEFT JOIN MYARMS_DATEL d ON d.DATEL_CODE=b.DATEL_CODE
      LEFT JOIN MYARMS_WITEL w ON d.WITEL_CODE=w.WITEL_CODE
      LEFT JOIN MYARMS_DIVISION di ON w.DIVISION_CODE=di.DIVISION_CODE
      $parameter
      GROUP BY b.PERIODE,p.SUMMARY_PRODUCT,b.KOMPONEN_BILL
      ORDER BY b.PERIODE ASC
    ");
    return $query->result();
  }
  // WARM BILLING
  public function WARM_BILLING_by_date($params=array())
  {
    $periode_1=$params['year_1'].$params['month_1'];
    $periode_2=$params['year_2'].$params['month_2'];
    $periode_3=$params['year_3'].$params['month_3'];
    $parameter="WHERE concat(wb.PERIODE,'01')<=wb.BY_DATE and wb.PERIODE IN ('".$periode_1."','".$periode_2."','".$periode_3."') ";
    $parameter.=($params['division']!="''" ? " and wb.DIVISION_CODE in (".$params['division'].")" : "");
    $parameter.=($params['sto']!="''" ? " and wb.STO in (".$params['sto'].")" : "");
    $parameter.=($params['billing_component']!="''" ? " and wb.KOMPONEN_BILLING in (".$params['billing_component'].")" : "");
    $query=$this->db->query("
      SELECT 
        wb.PERIODE,wb.BY_DATE,
        sum(wb.TOTAL_RECORD) as TOTAL_RECORD,sum(wb.TOTAL_DURATION) as TOTAL_DURATION,sum(wb.TOTAL) as TOTAL
      FROM MYARMS_WARM_BILLING wb
      $parameter
      GROUP BY wb.PERIODE,wb.BY_DATE
      ORDER BY wb.PERIODE,wb.BY_DATE ASC
    ");
    return $query->result();
  }
  public function WARM_BILLING_by_date_recycle($params=array())
  {
    $periode_1=$params['year_1'].$params['month_1'];
    $periode_2=$params['year_2'].$params['month_2'];
    $periode_3=$params['year_3'].$params['month_3'];
    $parameter="WHERE concat(wb.PERIODE,'01')>wb.BY_DATE and wb.PERIODE IN ('".$periode_1."','".$periode_2."','".$periode_3."') ";
    $parameter.=($params['division']!="''" ? " and wb.DIVISION_CODE in (".$params['division'].")" : "");
    $parameter.=($params['sto']!="''" ? " and wb.STO in (".$params['sto'].")" : "");
    $parameter.=($params['billing_component']!="''" ? " and wb.KOMPONEN_BILLING in (".$params['billing_component'].")" : "");
    $query=$this->db->query("
      SELECT 
        wb.PERIODE,
        sum(wb.TOTAL_RECORD) as TOTAL_RECORD,sum(wb.TOTAL_DURATION) as TOTAL_DURATION,sum(wb.TOTAL) as TOTAL
      FROM MYARMS_WARM_BILLING wb
      $parameter
      GROUP BY wb.PERIODE
      ORDER BY wb.PERIODE ASC
    ");
    return $query->result();
  }
  public function WARM_BILLING_by_date_and_time($params=array())
  {
    $periode_1=$params['year_1'].$params['month_1'];
    $periode_2=$params['year_2'].$params['month_2'];
    $periode_3=$params['year_3'].$params['month_3'];
    if($params['date']=="00"){
      $parameter="WHERE concat(wb.PERIODE,'01')>wb.BY_DATE and wb.PERIODE IN ('".$periode_1."','".$periode_2."','".$periode_3."') ";
    }else{
      $parameter="WHERE concat(wb.PERIODE,'01')<=wb.BY_DATE and wb.PERIODE IN ('".$periode_1."','".$periode_2."','".$periode_3."') ";
      $parameter.=" and wb.BY_DATE=concat(wb.PERIODE,'".$params['date']."')";
    }
    $parameter.=($params['division']!="''" ? " and wb.DIVISION_CODE in (".$params['division'].")" : "");
    $parameter.=($params['sto']!="''" ? " and wb.STO in (".$params['sto'].")" : "");
    $parameter.=($params['billing_component']!="''" ? " and wb.KOMPONEN_BILLING in (".$params['billing_component'].")" : "");
    $query=$this->db->query("
      SELECT 
        wb.PERIODE,wb.BY_TIME,
        sum(wb.TOTAL_RECORD) as TOTAL_RECORD,sum(wb.TOTAL_DURATION) as TOTAL_DURATION,sum(wb.TOTAL) as TOTAL
      FROM MYARMS_WARM_BILLING wb
      $parameter
      GROUP BY wb.PERIODE,wb.BY_TIME
      ORDER BY wb.PERIODE,wb.BY_TIME ASC
    ");
    return $query->result();
  }
  public function WARM_BILLING_by_day($params=array())
  {
    $periode_1=$params['year_1'].$params['month_1'];
    $periode_2=$params['year_2'].$params['month_2'];
    $periode_3=$params['year_3'].$params['month_3'];
    $parameter="WHERE concat(wb.PERIODE,'01')<=wb.BY_DATE and wb.PERIODE IN ('".$periode_1."','".$periode_2."','".$periode_3."') ";
    $parameter.=($params['division']!="''" ? " and wb.DIVISION_CODE in (".$params['division'].")" : "");
    $parameter.=($params['sto']!="''" ? " and wb.STO in (".$params['sto'].")" : "");
    $parameter.=($params['billing_component']!="''" ? " and wb.KOMPONEN_BILLING in (".$params['billing_component'].")" : "");
    $query=$this->db->query("
      SELECT 
        wb.PERIODE,wb.BY_DATE,wb.BY_DAY,
        sum(wb.TOTAL_RECORD) as TOTAL_RECORD,sum(wb.TOTAL_DURATION) as TOTAL_DURATION,sum(wb.TOTAL) as TOTAL
      FROM MYARMS_WARM_BILLING wb
      $parameter
      GROUP BY wb.PERIODE,wb.BY_DATE,wb.BY_DAY
      ORDER BY wb.PERIODE,wb.BY_DATE ASC
    ");
    return $query->result();
  }
  public function WARM_BILLING_by_day_recycle($params=array())
  {
    $periode_1=$params['year_1'].$params['month_1'];
    $periode_2=$params['year_2'].$params['month_2'];
    $periode_3=$params['year_3'].$params['month_3'];
    $parameter="WHERE concat(wb.PERIODE,'01')>wb.BY_DATE and wb.PERIODE IN ('".$periode_1."','".$periode_2."','".$periode_3."') ";
    $parameter.=($params['division']!="''" ? " and wb.DIVISION_CODE in (".$params['division'].")" : "");
    $parameter.=($params['sto']!="''" ? " and wb.STO in (".$params['sto'].")" : "");
    $parameter.=($params['billing_component']!="''" ? " and wb.KOMPONEN_BILLING in (".$params['billing_component'].")" : "");
    $query=$this->db->query("
      SELECT 
        wb.PERIODE,
        sum(wb.TOTAL_RECORD) as TOTAL_RECORD,sum(wb.TOTAL_DURATION) as TOTAL_DURATION,sum(wb.TOTAL) as TOTAL
      FROM MYARMS_WARM_BILLING wb
      $parameter
      GROUP BY wb.PERIODE
      ORDER BY wb.PERIODE ASC
    ");
    return $query->result();
  }
  public function WARM_BILLING_by_date_and_day($params=array())
  {
    $periode_1=$params['year_1'].$params['month_1'];
    $periode_2=$params['year_2'].$params['month_2'];
    $periode_3=$params['year_3'].$params['month_3'];
    if($params['day']=="00"){
      $parameter="WHERE concat(wb.PERIODE,'01')>wb.BY_DATE and wb.PERIODE IN ('".$periode_1."','".$periode_2."','".$periode_3."') ";
    }else{
      $parameter="WHERE concat(wb.PERIODE,'01')<=wb.BY_DATE and wb.PERIODE IN ('".$periode_1."','".$periode_2."','".$periode_3."') ";
      $date=explode(",",$params['date']);
      $parameter.=" and wb.BY_DATE IN ('".$date[0]."','".$date[1]."','".(isset($date[2]) ? $date[2] : '')."')";
    }
    $parameter.=($params['division']!="''" ? " and wb.DIVISION_CODE in (".$params['division'].")" : "");
    $parameter.=($params['sto']!="''" ? " and wb.STO in (".$params['sto'].")" : "");
    $parameter.=($params['billing_component']!="''" ? " and wb.KOMPONEN_BILLING in (".$params['billing_component'].")" : "");
    $query=$this->db->query("
      SELECT 
        wb.PERIODE,wb.BY_TIME,
        sum(wb.TOTAL_RECORD) as TOTAL_RECORD,sum(wb.TOTAL_DURATION) as TOTAL_DURATION,sum(wb.TOTAL) as TOTAL
      FROM MYARMS_WARM_BILLING wb
      $parameter
      GROUP BY wb.PERIODE,wb.BY_TIME
      ORDER BY wb.PERIODE,wb.BY_TIME ASC
    ");
    return $query->result();
  }
  public function WARM_BILLING_daily_summary($params=array())
  {
    $periode=$params['year'].$params['month'];
    $group_by=($params['group_by']!="" ? ",".$params['group_by'] : "");
    $parameter="WHERE concat(wb.PERIODE,'01')<=wb.BY_DATE and wb.PERIODE='".$periode."' ";
    $parameter.=($params['division']!="''" ? " and wb.DIVISION_CODE in (".$params['division'].")" : "");
    $parameter.=($params['sto']!="''" ? " and wb.STO in (".$params['sto'].")" : "");
    $parameter.=($params['billing_component']!="''" ? " and wb.KOMPONEN_BILLING in (".$params['billing_component'].")" : "");
    $query=$this->db->query("
      SELECT 
        wb.BY_DATE,wb.DIVISION_CODE,".($params['group_by']!="" ? $params['group_by']."," : "")."
        sum(wb.TOTAL_RECORD) as TOTAL_RECORD,sum(wb.TOTAL_DURATION) as TOTAL_DURATION,sum(wb.TOTAL) as TOTAL
      FROM MYARMS_WARM_BILLING wb
      $parameter
      GROUP BY wb.BY_DATE,wb.DIVISION_CODE $group_by
      ORDER BY wb.PERIODE ASC
    ");
    return $query->result();
  }
  public function WARM_BILLING_daily_summary_recycle($params=array())
  {
    $periode=$params['year'].$params['month'];
    $group_by=($params['group_by']!="" ? $params['group_by'] : "");
    $parameter="WHERE concat(wb.PERIODE,'01')>wb.BY_DATE and wb.PERIODE='".$periode."' ";
    $parameter.=($params['division']!="''" ? " and wb.DIVISION_CODE in (".$params['division'].")" : "");
    $parameter.=($params['sto']!="''" ? " and wb.STO in (".$params['sto'].")" : "");
    $parameter.=($params['billing_component']!="''" ? " and wb.KOMPONEN_BILLING in (".$params['billing_component'].")" : "");
    $query=$this->db->query("
      SELECT 
        ".($params['group_by']!="" ? $params['group_by']."," : "")."
        sum(wb.TOTAL_RECORD) as TOTAL_RECORD,sum(wb.TOTAL_DURATION) as TOTAL_DURATION,sum(wb.TOTAL) as TOTAL
      FROM MYARMS_WARM_BILLING wb
      $parameter
      GROUP BY $group_by
    ");
    return $query->result();
  }
  public function WARM_BILLING_l251($params=array())
  {
    $periode=$params['year'].$params['month'];
    $group_by=($params['show_data']!="" ? ",".$params['show_data'] : "");
    $parameter="WHERE l.PERIODE='".$periode."' ";
    $parameter.=($params['division']!="''" ? " and l.DIVISION_CODE in (".$params['division'].")" : "");
    $parameter.=($params['sto']!="''" ? " and l.STO_ID in (".$params['sto'].")" : "");
    $parameter.=($params['error']!="''" ? " and l.ERROR_ID in (".$params['error'].")" : "");
    $query=$this->db->query("
      SELECT 
        l.DIVISION_CODE,".($params['show_data']!="" ? $params['show_data']."," : "")."
        count(l.DIVISION_CODE) as SSL,sum(l.TOTAL_CALL) as TOTAL_CALL,sum(l.TOTAL_DURATION) as TOTAL_DURATION
      FROM MYARMS_L251 l
      LEFT JOIN MYARMS_DIVISION di ON l.DIVISION_CODE=di.DIVISION_CODE
      LEFT JOIN MYARMS_ERROR er ON l.ERROR_ID=er.ERROR_ID
      $parameter
      GROUP BY l.DIVISION_CODE $group_by
      ORDER BY l.DIVISION_CODE,".($params['show_data']!="" ? $params['show_data'] : "")." ASC
    ");
    return $query->result();
  }
  public function WARM_BILLING_l251_detail($params=array())
  {
    $periode=$params['year'].$params['month'];
    $group_by=($params['show_data']!="" ? ",".$params['show_data'] : "");
    $parameter="WHERE l.PERIODE='".$periode."' ";
    $parameter.=($params['ID']!="" ? " and ".$params['BY']."='".$params['ID']."'" : "");
    $parameter.=($params['division']!="''" ? " and l.DIVISION_CODE in (".$params['division'].")" : " and l.DIVISION_CODE='".$params['BY_DIV']."'");
    $parameter.=($params['sto']!="''" ? " and l.STO_ID in (".$params['sto'].")" : "");
    $parameter.=($params['error']!="''" ? " and l.ERROR_ID in (".$params['error'].")" : "");
    $query=$this->db->query("
      SELECT 
        l.TERITORY,l.DIVISION_CODE,l.STO_ID,l.ND,l.FIRST_CALL,l.LAST_CALL,er.ERROR_DESCRIPTION,l.TOTAL_CALL,l.TOTAL_DURATION
      FROM MYARMS_L251 l
      LEFT JOIN MYARMS_DIVISION di ON l.DIVISION_CODE=di.DIVISION_CODE
      LEFT JOIN MYARMS_ERROR er ON l.ERROR_ID=er.ERROR_ID
      $parameter
      ORDER BY l.DIVISION_CODE,".($params['show_data']!="" ? $params['show_data'] : "")." ASC
    ");
    return $query->result();
  }
  public function WARM_BILLING_EXTRIM_USAGE($params=array())
  {
    $periode=$params['year'].$params['month'];
    $group_by=($params['row']!="" || $params['column']!="" ? "GROUP BY ".$params['row'].($params['row']!="" && $params['column']!="" ? "," : "").$params['column'] : "");
    $order_by=($params['row']!="" ? "ORDER BY ".$params['column'].",".$params['row']." ASC" : "");
    $parameter="WHERE eu.PERIODE='".$periode."' ";
    $parameter.=($params['division']!="''" ? " and di.DIVISION_CODE in (".$params['division'].")" : "");
    $parameter.=($params['witel']!="''" ? " and d.WITEL_CODE in (".$params['witel'].")" : "");
    $parameter.=($params['datel']!="''" ? " and d.DATEL_CODE in (".$params['datel'].")" : "");
    $parameter.=($params['ubis']!="''" ? " and us.UBIS_ID in (".$params['ubis'].")" : "");
    $parameter.=($params['summary_product']!="''" ? " and p.SUMMARY_PRODUCT in (".$params['summary_product'].")" : " and p.SUMMARY_PRODUCT in ('Wireline','Speedy')");
    $parameter.=($params['product']!="''" ? " and p.PRODUCT_NAME in (".$params['product'].")" : "");
    $parameter.=($params['ubis_segment']!="''" ? " and us.UBIS_SEGMENT_ID in (".$params['ubis_segment'].")" : "");
    $parameter.=($params['amount_call']!="" ? " and ".$params['amount_call'] : "");
    $parameter.=($params['amount_sli007']!="" ? " and ".$params['amount_sli007'] : "");
    $parameter.=($params['amount_japati']!="" ? " and ".$params['amount_japati'] : "");
    $parameter.=($params['amount_domestic']!="" ? " and ".$params['amount_domestic'] : "");
    $having=" HAVING 1=1";
    $query=$this->db->query("
      SELECT 
        ".$params['row'].($params['row']!="" ? "," : "").$params['column'].($params['column']!="" ? "," : "")."
        count(eu.PERIODE) as RECORD
      FROM MYARMS_EXTRIM_USAGE eu
      LEFT JOIN MYARMS_PRODUCT p ON eu.PRODUCT_ID=p.PRODUCT_ID
      LEFT JOIN MYARMS_BUSINESS_AREA ba ON eu.BUSINESS_AREA_ID=ba.BUSINESS_AREA_ID
      LEFT JOIN MYARMS_UBIS_SEGMENT us ON ba.UBIS_SEGMENT_ID=us.UBIS_SEGMENT_ID
      LEFT JOIN MYARMS_CUSTOMER_CATEGORY cc ON cc.CUSTOMER_CATEGORY_ID=eu.CUSTOMER_CATEGORY_ID
      LEFT JOIN MYARMS_UBIS u ON us.UBIS_ID=u.UBIS_ID
      LEFT JOIN MYARMS_DATEL d ON d.DATEL_CODE=eu.DATEL_CODE
      LEFT JOIN MYARMS_WITEL w ON d.WITEL_CODE=w.WITEL_CODE
      LEFT JOIN MYARMS_DIVISION di ON w.DIVISION_CODE=di.DIVISION_CODE
      $parameter
      $group_by
      $having
      $order_by
    ");
    return $query->result();
  }
  public function WARM_BILLING_EXTRIM_USAGE_detail($params=array())
  {
    $periode=$params['year'].$params['month'];
    if($params['BY']=="DIVISION_CODE"){
      $BY="di.DIVISION_CODE";
    }elseif($params['BY']=="CUSTOMER_CATEGORY_DESCRIPTION"){
      $BY="cc.CUSTOMER_CATEGORY_DESCRIPTION";
    }elseif($params['BY']=="DATEL_CODE"){
      $BY="d.DATEL_CODE";
    }elseif($params['BY']=="PRODUCT_NAME"){
      $BY="p.PRODUCT_NAME";
    }elseif($params['BY']=="SUMMARY_PRODUCT"){
      $BY="p.SUMMARY_PRODUCT";
    }elseif($params['BY']=="UBIS"){
      $BY="u.UBIS";
    }elseif($params['BY']=="WITEL_DESCRIPTION"){
      $BY="w.WITEL_DESCRIPTION";
    }
    $parameter="WHERE eu.PERIODE='".$periode."'";
    $parameter.=($params['ID']!="" ? " and ".$BY."='".$params['ID']."'" : "");
    $parameter.=($params['limit']!=0 ? " and ROWNUM>0 and ROWNUM<=".$params['limit'] : "");
    $parameter.=($params['division']!="''" ? " and di.DIVISION_CODE in (".$params['division'].")" : "");
    $parameter.=($params['witel']!="''" ? " and d.WITEL_CODE in (".$params['witel'].")" : "");
    $parameter.=($params['datel']!="''" ? " and d.DATEL_CODE in (".$params['datel'].")" : "");
    $parameter.=($params['ubis']!="''" ? " and us.UBIS_ID in (".$params['ubis'].")" : "");
    $parameter.=($params['summary_product']!="''" ? " and p.SUMMARY_PRODUCT in (".$params['summary_product'].")" : " and p.SUMMARY_PRODUCT in ('Wireline','Speedy')");
    $parameter.=($params['product']!="''" ? " and p.PRODUCT_NAME in (".$params['product'].")" : "");
    $parameter.=($params['ubis_segment']!="''" ? " and us.UBIS_SEGMENT_ID in (".$params['ubis_segment'].")" : "");
    $parameter.=($params['amount_call']!="" ? " and ".$params['amount_call'] : "");
    $parameter.=($params['amount_sli007']!="" ? " and ".$params['amount_sli007'] : "");
    $parameter.=($params['amount_japati']!="" ? " and ".$params['amount_japati'] : "");
    $parameter.=($params['amount_domestic']!="" ? " and ".$params['amount_domestic'] : "");
    $query=$this->db->query("
      SELECT 
        eu.CCA,eu.ND,eu.NAMA,ba.BUSINESS_AREA_DESCRIPTION,p.PRODUCT_NAME,us.UBIS_SEGMENT,eu.CALL,eu.MIN_CALL,eu.MAX_CALL,eu.DURATION,eu.USAGE_LALU,eu.TAG_LALU,eu.LOKAL,eu.SLJJ,eu.SLI007,eu.STB,eu.USAGE_LAINNYA,eu.JAPATI
      FROM MYARMS_EXTRIM_USAGE eu
      LEFT JOIN MYARMS_PRODUCT p ON eu.PRODUCT_ID=p.PRODUCT_ID
      LEFT JOIN MYARMS_BUSINESS_AREA ba ON eu.BUSINESS_AREA_ID=ba.BUSINESS_AREA_ID
      LEFT JOIN MYARMS_UBIS_SEGMENT us ON ba.UBIS_SEGMENT_ID=us.UBIS_SEGMENT_ID
      LEFT JOIN MYARMS_CUSTOMER_CATEGORY cc ON cc.CUSTOMER_CATEGORY_ID=eu.CUSTOMER_CATEGORY_ID
      LEFT JOIN MYARMS_UBIS u ON us.UBIS_ID=u.UBIS_ID
      LEFT JOIN MYARMS_DATEL d ON d.DATEL_CODE=eu.DATEL_CODE
      LEFT JOIN MYARMS_WITEL w ON d.WITEL_CODE=w.WITEL_CODE
      LEFT JOIN MYARMS_DIVISION di ON w.DIVISION_CODE=di.DIVISION_CODE
      $parameter
    ");
    return $query->result();
  }
  // MENU C3MR
  public function COLLECTION_c3mr($params=array())
  {
    $periode=$params['year'].$params['month'];
    $group_by="GROUP BY ".$params['show_data'];
    $order_by="ORDER BY ".$params['show_data']." ASC";
    $parameter="WHERE b.PERIODE='".$periode."' ";
    $parameter.=($params['division']!="''" ? " and dd.DIVISION_CODE in (".$params['division'].")" : "");
    $parameter.=($params['witel']!="''" ? " and d.WITEL_CODE in (".$params['witel'].")" : "");
    $parameter.=($params['datel']!="''" ? " and d.DATEL_CODE in (".$params['datel'].")" : "");
    $parameter.=($params['ubis']!="''" ? " and us.UBIS_ID in (".$params['ubis'].")" : "");
    $parameter.=($params['summary_product']!="''" ? " and p.SUMMARY_PRODUCT in (".$params['summary_product'].")" : "");
    $parameter.=($params['product']!="''" ? " and b.PRODUCT_ID in (".$params['product'].")" : "");
    $parameter.=($params['segment']!="''" ? " and us.UBIS_SEGMENT_ID in (".$params['segment'].")" : "");
    $parameter.=($params['top_customer']!="''" ? " and b.PORTOFOLIO_ID in (".$params['top_customer'].")" : "");
    $parameter.=($params['indihome']!="" ? " and b.INDIHOME_ID='".$params['indihome']."'" : "");
    $parameter.=($params['usage']!="''" ? " and b.USAGE in (".$params['usage'].")" : "");
    $parameter.=($params['billing_type']!="''" ? " and b.BILLING_TYPE_COLLECTION_ID in (".$params['billing_type'].")" : "");
    $parameter.=($params['tunggakan']!="''" ? " and b.TUNGGAKAN_ID in (".$params['tunggakan'].")" : "");
    $parameter.=($params['eksepsi']!="''" ? " and b.EKSEPSI_ID in (".$params['eksepsi'].")" : "");
    $parameter2=$parameter;
    $parameter2=" and JENIS='N'";
    $query=$this->db->query("
      SELECT 
        ".$params['show_data']."
      FROM MYARMS_BILLING_COLLECTION b
      LEFT JOIN MYARMS_PORTOFOLIO por ON b.PORTOFOLIO_ID=por.PORTOFOLIO_ID
      LEFT JOIN MYARMS_INDIHOME i ON b.INDIHOME_ID=i.INDIHOME_ID
      LEFT JOIN MYARMS_DIVISION_DETAIL dd ON b.BUSINESS_AREA_ID=dd.BUSINESS_AREA_ID
      LEFT JOIN MYARMS_DIVISION di ON dd.DIVISION_CODE=di.DIVISION_CODE
      LEFT JOIN MYARMS_STO s ON s.STO_ID=b.STO_ID
      LEFT JOIN MYARMS_PRODUCT p ON b.PRODUCT_ID=p.PRODUCT_ID
      LEFT JOIN MYARMS_BUSINESS_AREA ba ON dd.BUSINESS_AREA_ID=ba.BUSINESS_AREA_ID
      LEFT JOIN MYARMS_UBIS_SEGMENT us ON ba.UBIS_SEGMENT_ID=us.UBIS_SEGMENT_ID
      LEFT JOIN MYARMS_UBIS u ON us.UBIS_ID=u.UBIS_ID
      LEFT JOIN MYARMS_DATEL d ON d.DATEL_CODE=dd.DATEL_CODE
      LEFT JOIN MYARMS_WITEL w ON d.WITEL_CODE=w.WITEL_CODE
      LEFT JOIN MYARMS_USAGE_STATUS usa ON b.USAGE=usa.USAGE_ID
      LEFT JOIN MYARMS_BILLING_TYPE_COLLECTION btc ON b.BILLING_TYPE_COLLECTION_ID=btc.BILLING_TYPE_COLLECTION_ID
      LEFT JOIN MYARMS_TUNGGAKAN t ON b.TUNGGAKAN_ID=t.TUNGGAKAN_ID
      LEFT JOIN MYARMS_EKSEPSI e ON b.EKSEPSI_ID=e.EKSEPSI_ID
      $parameter2
      $group_by
      UNION
      SELECT 
        ".$params['show_data']."
      FROM MYARMS_COLLECTION b
      LEFT JOIN MYARMS_PORTOFOLIO por ON b.PORTOFOLIO_ID=por.PORTOFOLIO_ID
      LEFT JOIN MYARMS_INDIHOME i ON b.INDIHOME_ID=i.INDIHOME_ID
      LEFT JOIN MYARMS_DIVISION_DETAIL dd ON b.BUSINESS_AREA_ID=dd.BUSINESS_AREA_ID
      LEFT JOIN MYARMS_DIVISION di ON dd.DIVISION_CODE=di.DIVISION_CODE
      LEFT JOIN MYARMS_STO s ON s.STO_ID=b.STO_ID
      LEFT JOIN MYARMS_PRODUCT p ON b.PRODUCT_ID=p.PRODUCT_ID
      LEFT JOIN MYARMS_BUSINESS_AREA ba ON dd.BUSINESS_AREA_ID=ba.BUSINESS_AREA_ID
      LEFT JOIN MYARMS_UBIS_SEGMENT us ON ba.UBIS_SEGMENT_ID=us.UBIS_SEGMENT_ID
      LEFT JOIN MYARMS_UBIS u ON us.UBIS_ID=u.UBIS_ID
      LEFT JOIN MYARMS_DATEL d ON d.DATEL_CODE=dd.DATEL_CODE
      LEFT JOIN MYARMS_WITEL w ON d.WITEL_CODE=w.WITEL_CODE
      LEFT JOIN MYARMS_USAGE_STATUS usa ON b.USAGE=usa.USAGE_ID
      LEFT JOIN MYARMS_BILLING_TYPE_COLLECTION btc ON b.BILLING_TYPE_COLLECTION_ID=btc.BILLING_TYPE_COLLECTION_ID
      LEFT JOIN MYARMS_TUNGGAKAN t ON b.TUNGGAKAN_ID=t.TUNGGAKAN_ID
      LEFT JOIN MYARMS_EKSEPSI e ON b.EKSEPSI_ID=e.EKSEPSI_ID
      $parameter
      $group_by
    ");
    return $query->result();
  }
  public function BILLING_DETAIL_NEW_WAVE_indihome_revenue($params=array())
  {
    $periode=$params['year'].$params['month'];
    $group_by=($params['row']!="" || $params['column']!="" ? "GROUP BY ".$params['row'].($params['row']!="" && $params['column']!="" ? "," : "").$params['column'] : "");
    $order_by=($params['row']!="" ? "ORDER BY ".$params['row']." ASC" : "");
    $parameter="WHERE b.PERIODE='".$periode."' ";
    $parameter.=($params['division']!="''" ? " and di.DIVISION_CODE in (".$params['division'].")" : "");
    $parameter.=($params['witel']!="''" ? " and d.WITEL_CODE in (".$params['witel'].")" : "");
    $parameter.=($params['datel']!="''" ? " and d.DATEL_CODE in (".$params['datel'].")" : "");
    $parameter.=($params['ubis']!="''" ? " and us.UBIS_ID in (".$params['ubis'].")" : "");
    $parameter.=($params['ubis_segment']!="''" ? " and us.UBIS_SEGMENT_ID in (".$params['ubis_segment'].")" : "");
    $parameter.=($params['packet_speedy']!="''" ? " and b.PACKET_SPEEDY_ID in (".$params['packet_speedy'].")" : "");
    $parameter.=($params['packet_indihome']!="''" ? " and b.PACKET_INDIHOME_ID in (".$params['packet_indihome'].")" : "");
    $parameter.=($params['profile_pelanggan']!="''" ? " and b.PROFILE_PELANGGAN_ID in (".$params['profile_pelanggan'].")" : "");
    $parameter.=($params['umur_pelanggan']!="''" ? " and b.UMUR_PELANGGAN_ID in (".$params['umur_pelanggan'].")" : "");
    $query=$this->db->query("
      SELECT 
        ".$params['row'].($params['row']!="" ? "," : "").$params['column'].($params['column']!="" ? "," : "")."
        sum(b.TAGIHAN) as TAGIHAN,sum(b.PPN) as PPN,sum(b.ABODEMEN) as ABODEMEN,sum(b.EXCESS) as EXCESS,sum(b.KONTENT) as KONTENT,
        sum(b.KREDIT) as KREDIT,sum(b.DEBIT) as DEBIT,sum(b.SST) as SST
      FROM MYARMS_MAIN_INDIHOME b
      LEFT JOIN MYARMS_PACKET_SPEEDY ps ON b.PACKET_SPEEDY_ID=ps.PACKET_SPEEDY_ID
      LEFT JOIN MYARMS_PACKET_INDIHOME pi ON b.PACKET_INDIHOME_ID=pi.PACKET_INDIHOME_ID
      LEFT JOIN MYARMS_PROFILE_PELANGGAN pp ON b.PROFILE_PELANGGAN_ID=pp.PROFILE_PELANGGAN_ID
      LEFT JOIN MYARMS_UMUR_PELANGGAN up ON b.UMUR_PELANGGAN_ID=up.UMUR_PELANGGAN_ID
      LEFT JOIN MYARMS_STO s ON s.STO_ID=b.STO_ID
      LEFT JOIN MYARMS_BUSINESS_AREA ba ON b.BUSINESS_AREA_ID=ba.BUSINESS_AREA_ID
      LEFT JOIN MYARMS_UBIS_SEGMENT us ON ba.UBIS_SEGMENT_ID=us.UBIS_SEGMENT_ID
      LEFT JOIN MYARMS_UBIS u ON us.UBIS_ID=u.UBIS_ID
      LEFT JOIN MYARMS_DATEL d ON d.DATEL_CODE=b.DATEL_CODE
      LEFT JOIN MYARMS_WITEL w ON d.WITEL_CODE=w.WITEL_CODE
      LEFT JOIN MYARMS_DIVISION di ON w.DIVISION_CODE=di.DIVISION_CODE
      $parameter
      $group_by
      $order_by
    ");
    return $query->result();
  }
  public function BILLING_DETAIL_NEW_WAVE_indihome_growth($params=array())
  {
    $periode_1=$params['year_1'].$params['month_1'];
    $periode_2=$params['year_2'].$params['month_2'];
    $periode_3=$params['year_3'].$params['month_3'];
    $parameter="WHERE b.PERIODE IN ('".$periode_1."','".$periode_2."','".$periode_3."') ";
    $parameter.=($params['division']!="''" ? " and di.DIVISION_CODE in (".$params['division'].")" : "");
    $parameter.=($params['witel']!="''" ? " and d.WITEL_CODE in (".$params['witel'].")" : "");
    $parameter.=($params['datel']!="''" ? " and d.DATEL_CODE in (".$params['datel'].")" : "");
    $parameter.=($params['ubis']!="''" ? " and us.UBIS_ID in (".$params['ubis'].")" : "");
    $parameter.=($params['ubis_segment']!="''" ? " and us.UBIS_SEGMENT_ID in (".$params['ubis_segment'].")" : "");
    $parameter.=($params['packet_speedy']!="''" ? " and b.PACKET_SPEEDY_ID in (".$params['packet_speedy'].")" : "");
    $parameter.=($params['packet_indihome']!="''" ? " and b.PACKET_INDIHOME_ID in (".$params['packet_indihome'].")" : "");
    $parameter.=($params['profile_pelanggan']!="''" ? " and b.PROFILE_PELANGGAN_ID in (".$params['profile_pelanggan'].")" : "");
    $parameter.=($params['umur_pelanggan']!="''" ? " and b.UMUR_PELANGGAN_ID in (".$params['umur_pelanggan'].")" : "");
    $query=$this->db->query("
      SELECT 
        b.PERIODE,".$params['show_data'].",
        sum(b.TAGIHAN) as TAGIHAN,sum(b.PPN) as PPN,sum(b.ABODEMEN) as ABODEMEN,sum(b.EXCESS) as EXCESS,sum(b.KONTENT) as KONTENT,
        sum(b.KREDIT) as KREDIT,sum(b.DEBIT) as DEBIT,sum(b.SST) as SST
      FROM MYARMS_MAIN_INDIHOME b
      LEFT JOIN MYARMS_PACKET_SPEEDY ps ON b.PACKET_SPEEDY_ID=ps.PACKET_SPEEDY_ID
      LEFT JOIN MYARMS_PACKET_INDIHOME pi ON b.PACKET_INDIHOME_ID=pi.PACKET_INDIHOME_ID
      LEFT JOIN MYARMS_PROFILE_PELANGGAN pp ON b.PROFILE_PELANGGAN_ID=pp.PROFILE_PELANGGAN_ID
      LEFT JOIN MYARMS_UMUR_PELANGGAN up ON b.UMUR_PELANGGAN_ID=up.UMUR_PELANGGAN_ID
      LEFT JOIN MYARMS_STO s ON s.STO_ID=b.STO_ID
      LEFT JOIN MYARMS_BUSINESS_AREA ba ON b.BUSINESS_AREA_ID=ba.BUSINESS_AREA_ID
      LEFT JOIN MYARMS_UBIS_SEGMENT us ON ba.UBIS_SEGMENT_ID=us.UBIS_SEGMENT_ID
      LEFT JOIN MYARMS_UBIS u ON us.UBIS_ID=u.UBIS_ID
      LEFT JOIN MYARMS_DATEL d ON d.DATEL_CODE=b.DATEL_CODE
      LEFT JOIN MYARMS_WITEL w ON d.WITEL_CODE=w.WITEL_CODE
      LEFT JOIN MYARMS_DIVISION di ON w.DIVISION_CODE=di.DIVISION_CODE
      $parameter
      GROUP BY b.PERIODE".",".$params['show_data']."
      ORDER BY b.PERIODE ASC
    ");
    return $query->result();
  }
  public function BILLING_DETAIL_NEW_WAVE_ime_revenue($params=array())
  {
    $periode=$params['year'].$params['month'];
    $group_by=($params['row']!="" || $params['column']!="" ? "GROUP BY ".$params['row'].($params['row']!="" && $params['column']!="" ? "," : "").$params['column'] : "");
    $order_by=($params['row']!="" ? "ORDER BY ".$params['row']." ASC" : "");
    $parameter="WHERE b.PERIODE='".$periode."' ";
    $parameter.=($params['division']!="''" ? " and di.DIVISION_CODE in (".$params['division'].")" : "");
    $parameter.=($params['witel']!="''" ? " and d.WITEL_CODE in (".$params['witel'].")" : "");
    $parameter.=($params['datel']!="''" ? " and d.DATEL_CODE in (".$params['datel'].")" : "");
    $parameter.=($params['ubis']!="''" ? " and us.UBIS_ID in (".$params['ubis'].")" : "");
    $parameter.=($params['ubis_segment']!="''" ? " and us.UBIS_SEGMENT_ID in (".$params['ubis_segment'].")" : "");
    $parameter.=($params['packet_ime']!="''" ? " and pim.PACKET_IME in (".$params['packet_ime'].")" : "");
    $parameter.=($params['ime_description']!="''" ? " and b.PACKET_IME_ID in (".$params['ime_description'].")" : "");
    $parameter.=($params['payment_status']!="''" ? " and b.PAYMENT_STATUS_ID in (".$params['payment_status'].")" : "");
    $parameter.=($params['umur_pelanggan']!="''" ? " and b.UMUR_PELANGGAN_ID in (".$params['umur_pelanggan'].")" : "");
    $query=$this->db->query("
      SELECT 
        ".$params['row'].($params['row']!="" ? "," : "").$params['column'].($params['column']!="" ? "," : "")."
        sum(b.TAGIHAN) as TAGIHAN,sum(b.SST) as SST
      FROM MYARMS_MAIN_IME b
      LEFT JOIN MYARMS_PACKET_IME pim ON b.PACKET_IME_ID=pim.PACKET_IME_ID
      LEFT JOIN MYARMS_PAYMENT_STATUS pays ON b.PAYMENT_STATUS_ID=pays.PAYMENT_STATUS_ID
      LEFT JOIN MYARMS_UMUR_PELANGGAN up ON b.UMUR_PELANGGAN_ID=up.UMUR_PELANGGAN_ID
      LEFT JOIN MYARMS_STO s ON s.STO_ID=b.STO_ID
      LEFT JOIN MYARMS_BUSINESS_AREA ba ON b.BUSINESS_AREA_ID=ba.BUSINESS_AREA_ID
      LEFT JOIN MYARMS_UBIS_SEGMENT us ON ba.UBIS_SEGMENT_ID=us.UBIS_SEGMENT_ID
      LEFT JOIN MYARMS_UBIS u ON us.UBIS_ID=u.UBIS_ID
      LEFT JOIN MYARMS_DATEL d ON d.DATEL_CODE=b.DATEL_CODE
      LEFT JOIN MYARMS_WITEL w ON d.WITEL_CODE=w.WITEL_CODE
      LEFT JOIN MYARMS_DIVISION di ON w.DIVISION_CODE=di.DIVISION_CODE
      $parameter
      $group_by
      $order_by
    ");
    return $query->result();
  }
  public function BILLING_DETAIL_NEW_WAVE_ime_growth($params=array())
  {
    $periode_1=$params['year_1'].$params['month_1'];
    $periode_2=$params['year_2'].$params['month_2'];
    $periode_3=$params['year_3'].$params['month_3'];
    $parameter="WHERE b.PERIODE IN ('".$periode_1."','".$periode_2."','".$periode_3."') ";
    $parameter.=($params['division']!="''" ? " and di.DIVISION_CODE in (".$params['division'].")" : "");
    $parameter.=($params['witel']!="''" ? " and d.WITEL_CODE in (".$params['witel'].")" : "");
    $parameter.=($params['datel']!="''" ? " and d.DATEL_CODE in (".$params['datel'].")" : "");
    $parameter.=($params['ubis']!="''" ? " and us.UBIS_ID in (".$params['ubis'].")" : "");
    $parameter.=($params['ubis_segment']!="''" ? " and us.UBIS_SEGMENT_ID in (".$params['ubis_segment'].")" : "");
    $parameter.=($params['packet_ime']!="''" ? " and pim.PACKET_IME in (".$params['packet_ime'].")" : "");
    $parameter.=($params['ime_description']!="''" ? " and b.PACKET_IME_ID in (".$params['ime_description'].")" : "");
    $parameter.=($params['payment_status']!="''" ? " and b.PAYMENT_STATUS_ID in (".$params['payment_status'].")" : "");
    $parameter.=($params['umur_pelanggan']!="''" ? " and b.UMUR_PELANGGAN_ID in (".$params['umur_pelanggan'].")" : "");
    $query=$this->db->query("
      SELECT 
        b.PERIODE,".$params['show_data'].",
        sum(b.TAGIHAN) as TAGIHAN,sum(b.SST) as SST
      FROM MYARMS_MAIN_IME b
      LEFT JOIN MYARMS_PACKET_IME pim ON b.PACKET_IME_ID=pim.PACKET_IME_ID
      LEFT JOIN MYARMS_PAYMENT_STATUS pays ON b.PAYMENT_STATUS_ID=pays.PAYMENT_STATUS_ID
      LEFT JOIN MYARMS_UMUR_PELANGGAN up ON b.UMUR_PELANGGAN_ID=up.UMUR_PELANGGAN_ID
      LEFT JOIN MYARMS_STO s ON s.STO_ID=b.STO_ID
      LEFT JOIN MYARMS_BUSINESS_AREA ba ON b.BUSINESS_AREA_ID=ba.BUSINESS_AREA_ID
      LEFT JOIN MYARMS_UBIS_SEGMENT us ON ba.UBIS_SEGMENT_ID=us.UBIS_SEGMENT_ID
      LEFT JOIN MYARMS_UBIS u ON us.UBIS_ID=u.UBIS_ID
      LEFT JOIN MYARMS_DATEL d ON d.DATEL_CODE=b.DATEL_CODE
      LEFT JOIN MYARMS_WITEL w ON d.WITEL_CODE=w.WITEL_CODE
      LEFT JOIN MYARMS_DIVISION di ON w.DIVISION_CODE=di.DIVISION_CODE
      $parameter
      GROUP BY b.PERIODE".",".$params['show_data']."
      ORDER BY b.PERIODE ASC
    ");
    return $query->result();
  }
}