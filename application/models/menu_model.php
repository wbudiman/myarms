<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Menu_Model extends MY_Model {
  public $table_name = 'MYARMS_MENU';
  public function __construct() {
    parent::__construct();
  }
}