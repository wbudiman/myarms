<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bisnis_Area_Model extends MY_Model {
  public $table_name = 'MYARMS_BUSINESS_AREA';
  public function __construct() {
    parent::__construct();
  }

  public function get_by_ubis_segment_id($ubis_segment){
    $query=$this->db->query("
      select * from MYARMS_BUSINESS_AREA where UBIS_SEGMENT_ID in ($ubis_segment)
      order by BUSINESS_AREA_DESCRIPTION ASC
    ");
    return $query->result();
  }
}  
  
  