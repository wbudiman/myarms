<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_Role_Detail_Model extends MY_Model {
  public $table_name = 'MYARMS_USER_ROLE_DETAIL';
  public function __construct() {
    parent::__construct();
  }
}