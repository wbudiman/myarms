<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Billing_Type_Collection_Model extends MY_Model {
  public $table_name = 'MYARMS_BILLING_TYPE_COLLECTION';
  public function __construct() {
    parent::__construct();
  }
}