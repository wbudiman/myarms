<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Datel_Model extends MY_Model {
  public $table_name = 'MYARMS_DATEL';
  public function __construct() {
    parent::__construct();
  }
  public function lists($divisions="",$witels="")
  {
    $parameter=($divisions!="''" ? "di.DIVISION_CODE in ($divisions)" : "");
    $parameter.=($witels!="''" ? ($parameter!="" ? " and " : "")."w.WITEL_CODE in ($witels)" : "");
    $parameter=($parameter!="" ? "where " : "").$parameter;
    $query=$this->db->query("
      select distinct(d.DATEL_CODE),d.DATEL_CODE,d.DATEL_DESCRIPTION 
      from MYARMS_DATEL d
      inner join MYARMS_WITEL w on d.WITEL_CODE=w.WITEL_CODE
      inner join MYARMS_DIVISION di on w.DIVISION_CODE=di.DIVISION_CODE
    ".$parameter." 
      order by d.DATEL_DESCRIPTION ASC
    ");
    return $query->result();
  }
}