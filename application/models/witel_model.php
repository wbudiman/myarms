<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Witel_Model extends MY_Model {
  public $table_name = 'MYARMS_WITEL';
  public function __construct() {
    parent::__construct();
  }
  public function lists($divisions="")
  {
    $parameter=($divisions!="''" ? "where w.DIVISION_CODE in ($divisions)" : "");
    $query=$this->db->query("
      select distinct(w.WITEL_CODE),w.WITEL_CODE,w.WITEL_DESCRIPTION 
      from MYARMS_WITEL w
    ".$parameter."
      order by w.WITEL_DESCRIPTION ASC
    ");
    return $query->result();
  }
}