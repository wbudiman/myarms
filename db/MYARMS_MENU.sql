/*
Navicat Oracle Data Transfer
Oracle Client Version : 11.2.0.1.0

Source Server         : MYARMS
Source Server Version : 110200
Source Host           : localhost:1521
Source Schema         : MYARMS

Target Server Type    : ORACLE
Target Server Version : 110200
File Encoding         : 65001

Date: 2015-01-18 10:29:22
*/


-- ----------------------------
-- Table structure for "MYARMS"."MYARMS_MENU"
-- ----------------------------
DROP TABLE "MYARMS"."MYARMS_MENU";
CREATE TABLE "MYARMS"."MYARMS_MENU" (
"MENU_ID" NUMBER(8) DEFAULT 1  NOT NULL ,
"PARENT" NUMBER(8) DEFAULT 0  NULL ,
"NAME" NVARCHAR2(40) NULL ,
"URL" NVARCHAR2(100) NULL ,
"ICON" NVARCHAR2(20) NULL ,
"SORT" NUMBER NULL 
)
LOGGING
COMPRESS
CACHE

;

-- ----------------------------
-- Records of MYARMS_MENU
-- ----------------------------
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('1', '0', 'Executive Summary', null, 'ico-nav executive', '0');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('2', '1', 'Revenue', null, null, '0');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('3', '1', 'Collection', null, null, '0');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('4', '1', 'Dunning', null, null, '0');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('5', '0', 'Billing', null, 'ico-nav billing', '0');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('6', '5', 'Revenue', null, null, '0');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('7', '6', 'Retail', null, null, '0');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('8', '7', 'Warm Billing', null, null, '0');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('9', '8', 'By Tanggal', 'billing_management/warm_billing/by_date', null, '1');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('10', '8', 'By Hari', 'billing_management/warm_billing/by_day', null, '2');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('11', '8', 'Rekap Harian', 'billing_management/warm_billing/daily_summary', null, '3');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('12', '8', 'Ekstrim Usage', 'billing_management/warm_billing/extrim_usage', null, '4');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('13', '8', 'L251', 'billing_management/warm_billing/l251', null, '5');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('14', '7', 'Gimmick Mgt', null, null, '0');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('15', '14', 'Katalog Gimmick', 'billing_management/gimmick/katalog_gimmick', null, '1');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('16', '14', 'Gimmick Aktif Per Periode', 'billing_management/gimmick/gimmick_aktif', null, '2');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('17', '14', 'Pelanggan Dpt Gimmick', 'billing_management/gimmick/pelanggan', null, '3');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('18', '14', 'Pelanggan Berakhir Gimmick', 'billing_management/gimmick/pelanggan_berakhir', null, '4');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('19', '7', 'Billing Result', null, null, '0');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('20', '19', 'Revenue', 'billing_management/billing_result/revenue', null, '1');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('21', '19', 'Growth', 'billing_management/billing_result/growth', null, '2');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('22', '19', 'Growth L11', 'billing_management/billing_result/growth_l11', null, '3');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('23', '19', 'Berita Acara', 'billing_management/billing_result/news_event', null, '4');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('24', '19', 'Komite Billing', 'billing_management/billing_result/billing_committee', null, '5');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('25', '7', 'Detail Billing', null, null, '0');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('26', '25', 'Revenue', 'billing_management/detail_billing/revenue', null, '1');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('27', '25', 'Growth', 'billing_management/detail_billing/growth', null, '2');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('28', '25', 'Berita Acara', 'billing_management/detail_billing/news_event', null, '3');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('29', '25', 'Komite Billing', 'billing_management/detail_billing/billing_committee', null, '4');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('30', '25', 'Pohon Revenue', 'billing_management/detail_billing/tree', null, '5');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('31', '7', 'New Wave', null, null, '0');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('32', '31', 'Indihome', null, null, '1');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('33', '31', 'Revenue2', 'billing_management/new_wave/indihome_revenue', null, '1');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('34', '31', 'Growth', 'billing_management/new_wave/indihome_growth', null, '2');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('35', '31', 'IME', null, null, '2');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('36', '31', 'Revenue', 'billing_management/new_wave/ime_revenue', null, '1');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('37', '31', 'Growth', 'billing_management/new_wave/ime_growth', null, '2');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('38', '6', 'Non POTS', null, null, '0');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('39', '38', 'Billing Result', null, null, '0');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('40', '38', 'Detail Billing', null, null, '0');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('41', '38', 'Warm Billing', null, null, '0');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('42', '38', 'Gimmick Mgt', null, null, '0');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('43', '6', 'Interkoneksi', null, null, '0');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('44', '43', 'Incoming (IC)', null, null, '0');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('45', '44', 'Revenue', null, null, '1');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('46', '44', 'Growth', null, null, '2');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('47', '44', 'Traffic', null, null, '3');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('48', '44', 'Skema Bisnis', null, null, '4');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('49', '43', 'IDD Global Partner', null, null, '0');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('50', '49', 'Revenue', null, null, '1');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('51', '49', 'Growth', null, null, '2');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('52', '49', 'Traffic', null, null, '3');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('53', '43', 'IDD OLO', null, null, '0');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('54', '53', 'BD12', null, null, '1');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('55', '53', 'Revenue', null, null, '2');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('56', '53', 'Growth', null, null, '3');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('57', '53', 'Traffic', null, null, '4');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('58', '53', 'Skema Bisnis', null, null, '5');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('59', '43', 'Intelligent Network', null, null, '0');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('60', '59', 'BD12', null, null, '1');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('61', '59', 'Revenue', null, null, '2');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('62', '59', 'Growth', null, null, '3');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('63', '59', 'Traffic', null, null, '4');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('64', '59', 'Skema Bisnis', null, null, '5');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('65', '43', 'Outgoing', null, null, '0');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('66', '65', 'Revenue', null, null, '1');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('67', '65', 'Growth', null, null, '2');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('68', '65', 'Traffic', null, null, '3');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('69', '65', 'Skema Bisnis', null, null, '4');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('70', '43', 'SMS', null, null, '0');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('71', '70', 'Revenue', null, null, '1');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('72', '70', 'Growth', null, null, '2');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('73', '70', 'Traffic', null, null, '3');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('74', '43', 'Telkom Global', null, null, '0');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('75', '74', 'Revenue', null, null, '1');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('76', '74', 'Growth', null, null, '2');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('77', '74', 'Traffic', null, null, '3');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('78', '6', 'Jasa Jaringan', null, null, '0');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('79', '78', 'Revenue', null, null, '1');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('80', '78', 'Growth', null, null, '2');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('81', '0', 'Collection & Settlement', null, 'ico-nav inventory', '0');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('82', '81', 'Retail', null, null, '0');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('83', '82', 'Collection', null, null, '0');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('84', '83', 'C3MR (Harian)', 'collection_management/collection/c3mr', null, '1');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('85', '83', 'CR Ops', null, null, '2');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('86', '83', 'CR Report', null, null, '3');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('87', '83', 'Trend Collection', null, null, '4');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('88', '83', 'ACP', null, null, '5');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('89', '83', 'Multibill', null, null, '6');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('90', '83', 'Extreme Revenue', null, null, '7');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('91', '82', 'Debt Management', null, null, '0');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('92', '91', 'Group Tunggakan', 'collection_management/debt_management/group_tunggakan', null, '1');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('93', '91', 'Query Tunggakan', 'collection_management/debt_management/query_tunggakan', null, '2');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('94', '82', 'IF Master TREMS', null, null, '0');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('95', '94', 'PSB', 'collection_management/if_master/psb', null, '1');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('96', '94', 'Modification / Change Number', 'collection_management/if_master/modification', null, '2');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('97', '94', 'Cancel Tanggal Cabut', 'collection_management/if_master/cancel_tgl', null, '3');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('98', '94', 'BN/PDA', 'collection_management/if_master/bnpda', null, '4');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('99', '82', 'Laporan Loket', null, null, '0');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('100', '99', 'Laporan Loket Detail', 'collection_management/loket/loket_detail', null, '1');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('101', '99', 'Laporan Loket Summary', 'collection_management/loket/laporan_loket', null, '2');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('102', '99', 'Mapping Loket BO', 'collection_management/loket/loket_bo', null, '3');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('103', '99', 'Detail PSB', 'collection_management/loket/detail_psb', null, '4');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('104', '82', 'Rekon Finnet', null, null, '0');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('105', '104', 'Tel75', 'collection_management/rekon_finnet/tel75', null, '1');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('106', '104', 'Pembatalan Tel 75', 'collection_management/rekon_finnet/pembatalan_tel75', null, '1');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('107', '104', 'GX Tel72', 'collection_management/rekon_finnet/gxtel72', null, '1');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('108', '104', 'Komparasi Tel75 - GX Tel 72', 'collection_management/rekon_finnet/komparasi', null, '1');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('110', '104', 'OSA', 'collection_management/rekon_finnet/osa', null, '1');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('111', '104', 'Upload Tel75', 'collection_management/rekon_finnet/upload_tel75', null, '1');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('109', '104', 'RTGS', 'collection_management/rekon_finnet/rtgs', null, '1');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('112', '104', 'Upload Pembatalan Tel75', 'collection_management/rekon_finnet/upload_pembatalan_tel75', null, '1');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('113', '104', 'Upload RTGS', 'collection_management/rekon_finnet/upload_rtgs', null, '1');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('114', '104', 'Upload OSA', 'collection_management/rekon_finnet/upload_osa', null, '1');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('115', '81', 'Non POTS', null, null, '0');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('116', '115', 'Collection Ratio', null, null, '1');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('117', '115', 'Piutang Usaha', null, null, '2');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('118', '115', 'Aging Piutang Usaha', null, null, '3');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('119', '115', 'Penyisihan Piutang', null, null, '3');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('120', '81', 'Wholesale', null, null, '0');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('121', '120', 'Collection', null, null, '1');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('122', '120', 'SOKI Usaha', null, null, '2');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('123', '0', 'Dunning', null, 'ico-nav trolley', '0');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('124', '123', 'Billing Info', null, null, '0');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('125', '124', '109', null, null, '1');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('126', '124', 'SMS', null, null, '2');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('127', '124', 'EBS', null, null, '3');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('128', '124', 'OBC (Billing Perdana)', null, null, '4');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('129', '123', 'Reminding', null, null, '0');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('130', '129', 'OBC', null, null, '1');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('131', '129', 'Email', null, null, '2');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('132', '129', 'OVR', null, null, '3');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('133', '129', 'Visiting', null, null, '4');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('134', '0', 'Administrator', null, 'ico-nav keys', '0');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('135', '134', 'Perubahan', null, null, '0');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('136', '135', 'Hak Akses', 'administrator/user_roles', null, '0');
INSERT INTO "MYARMS"."MYARMS_MENU" VALUES ('137', '135', 'Otorisasi User', 'administrator/users', null, '0');

-- ----------------------------
-- Indexes structure for table MYARMS_MENU
-- ----------------------------

-- ----------------------------
-- Checks structure for table "MYARMS"."MYARMS_MENU"
-- ----------------------------
ALTER TABLE "MYARMS"."MYARMS_MENU" ADD CHECK ("MENU_ID" IS NOT NULL);
ALTER TABLE "MYARMS"."MYARMS_MENU" ADD CHECK ("MENU_ID" IS NOT NULL);
ALTER TABLE "MYARMS"."MYARMS_MENU" ADD CHECK ("MENU_ID" IS NOT NULL);
ALTER TABLE "MYARMS"."MYARMS_MENU" ADD CHECK ("MENU_ID" IS NOT NULL);
ALTER TABLE "MYARMS"."MYARMS_MENU" ADD CHECK ("MENU_ID" IS NOT NULL);
ALTER TABLE "MYARMS"."MYARMS_MENU" ADD CHECK ("MENU_ID" IS NOT NULL);
ALTER TABLE "MYARMS"."MYARMS_MENU" ADD CHECK ("MENU_ID" IS NOT NULL);
ALTER TABLE "MYARMS"."MYARMS_MENU" ADD CHECK ("MENU_ID" IS NOT NULL);
ALTER TABLE "MYARMS"."MYARMS_MENU" ADD CHECK ("MENU_ID" IS NOT NULL);
ALTER TABLE "MYARMS"."MYARMS_MENU" ADD CHECK ("MENU_ID" IS NOT NULL);
ALTER TABLE "MYARMS"."MYARMS_MENU" ADD CHECK ("MENU_ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table "MYARMS"."MYARMS_MENU"
-- ----------------------------
ALTER TABLE "MYARMS"."MYARMS_MENU" ADD PRIMARY KEY ("MENU_ID");
