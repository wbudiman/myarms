/* 
  //DATEPICKER
  main.datepicker(element,minDate,maxDate,element2);
  main.datepicker("#date"); free select date
  main.datepicker("#date","-d"); datepicker with min Date now()-1
  main.datepicker("#date",new Date(),"+20d"); datepicker with range NOW() - NOW() + 20 day
  main.datepicker(".start_date",new Date(),"+20d",".end_date"); datepicker with 2 element range NOW() - NOW()+20day
  
  //MODAL
  echo json_encode(array(
    "status" => true / false,
    "content" => "content modal"
  ));
  <a href="javascript:void(0)" data-remote-modal="<?= base_url("home/view") ?>">Launch Modal</a>
  <button type="button" data-remote-modal="<?= base_url("home/view") ?>">Launch Modal</button>
  
  //LOAD OVERLAY LOADING
  main.showOverlay();
  
  //HIDE OVERLAY LOADING
  main.hideOverlay();
  
  //CALL AJAX WITH OWN CALLBACK FUNCTION 
  main.ajax(url,type,dataType,data,callBackFunction);
  function myOwnFunction(response)
  {
    console.log(response);
  }
  main.ajax("<?= base_url("home/view") ?>","POST","JSON",{id:id,name:name},"myOwnFunction");
  
  //NOTIFICATION 
  main.notification(layout,type,headTitle,message);

  
  //CONFIRM DIALOG
  main.confirmDialog(message,textOK(default="Yes"),textCancel(default="Cancel"));
  <a href="javascript:void(0)" data-confirm-modal="<?= base_url("item/delete/7") ?>">Delete</a>
  main.confirmDialog("Are you sure want delete this data ?");
  if Yes is clicked then redirect to content attribute -> data-confirm-modal
  
  //VALIDATE FORM
  with this tag form like this <form validate>, when submit is click the form will validate
  or if you want validate your own like this
  function callbackfunction()
  {
    alert("sukses");
  }
  main.validate("form[validate]","callbackfunction");
  
  // SELECT / COMBOX BOX
  main.select("select#source");

*/
$.fn.serializeObject = function() {
  var data = { };
  $.each( this.serializeArray(), function( key, obj ) {
      var a = obj.name.match(/(.*?)\[(.*?)\]/);
      if(a !== null)
      {
          var subName = new String(a[1]);
          var subKey = new String(a[2]);
          if( !data[subName] ) data[subName] = { };
          if( data[subName][subKey] ) {
              if( $.isArray( data[subName][subKey] ) ) {
                  data[subName][subKey].push( obj.value );
              } else {
                  data[subName][subKey] = { };
                  data[subName][subKey].push( obj.value );
              };
          } else {
              data[subName][subKey] = obj.value;
          };  
      } else {
          var keyName = new String(obj.name);
          if( data[keyName] ) {
              if( $.isArray( data[keyName] ) ) {
                  data[keyName].push( obj.value );
              } else {
                  data[keyName] = { };
                  data[keyName].push( obj.value );
              };
          } else {
              data[keyName] = obj.value;
          };
      };
  });
  return data;
};
var main = {
  // datepicker:function(element,minDate,maxDate,element2){
    // if(element==undefined)element="";
    // if(minDate==undefined)minDate="";
    // if(maxDate==undefined)maxDate="";
    // if(element2==undefined)element2="";
    // if(element2!=""){
      // var datepicker=$(element).datepicker({
        // format:"dd/mm/yyyy",
        // autoclose: true,
        // todayHighlight: true,
        // startDate:minDate,
        // endDate:maxDate
      // });
      // $(element2).datepicker({
        // format:"dd/mm/yyyy",
        // autoclose: true,
        // todayHighlight: true,
        // startDate:minDate,
        // endDate:maxDate
      // });
      // datepicker.on('changeDate', function(ev){
        // if($(element2).val()!=""){
          // var end_date=$(element2).val().split('/');
          // var end =new Date(end_date[2]+"-"+end_date[1]+"-"+end_date[0]);
          // $(element2).datepicker('remove');
          // if(ev.date.valueOf()>end.valueOf()){
            // $(element2).datepicker({
              // format:"dd/mm/yyyy",
              // autoclose: true,
              // todayHighlight: true,
              // startDate: $(element).val(),
              // endDate:maxDate
            // });
            // $(element2).datepicker("update", $(element).val());
          // }else{
            // $(element2).datepicker({
              // format:"dd/mm/yyyy",
              // autoclose: true,
              // todayHighlight: true,
              // startDate: new Date(ev.date),
              // endDate:maxDate
            // });
          // }
        // }else{
          // $(element2).datepicker('remove');
          // $(element2).datepicker({
            // format:"dd/mm/yyyy",
            // autoclose: true,
            // todayHighlight: true,
            // startDate: new Date(ev.date),
            // endDate:maxDate
          // });
          // $(element2).datepicker("update", new Date(ev.date));
        // }
      // });
    // }else{
      // $(element).datepicker({
        // format:"dd/mm/yyyy",
        // autoclose: true,
        // todayHighlight: true,
        // startDate:minDate,
        // endDate:maxDate
      // });   
    // }
  // },
  // triggerModal:function(){
    // $(document).on("click", "[data-remote-modal]", function() {
      // var url=$(this).attr("data-remote-modal");
      // var width=$(this).attr("data-width");
      // if(width!=undefined){
        // $(".modal-dialog").css("width",width);
      // }
      // main.showModal(url,"#mainModal",".modal-content",width);
      // return false;
    // });
  // },
  // showModal:function(url,targetModal,targetElement,data,width){
    // alert(width)
    
    // if(data==undefined)data={};
    // $.ajax({
      // url:url,
      // dataType:"JSON",
      // data:data,
      // beforeSend:function(){
        // main.showOverlay();
      // },
      // success:function(response){
        // main.hideOverlay();
        // if(response.status==false)
        // {
          // alert("Page request not found!.");
        // }else if(response.status==true){
          // $(targetModal+" "+targetElement).html(response.content);
          // $(targetModal).modal();
        // }
      // }
    // });
  // },
  ajax:function(url,type,dataType,data,callBackFunction){
    $.ajax({
      url:url,
      type:type,
      dataType:dataType,
      data:data,
      beforeSend:function(){
        main.showOverlay();
      },
      success:function(response){
        main.dataResponse=response;
        main.hideOverlay();
        if(callBackFunction!=""){
          var fn = window[callBackFunction];
          if (typeof fn === "function") fn(response);
        }
      }
    });
  },
    
  triggerModal:function(){
    $(document).on("click", "[data-remote-modal]", function() {
      var url=$(this).attr("data-remote-modal");
      var width=$(this).attr("data-width");
      if(width!=undefined){
        $(".modal-dialog").css("width",width);
      }
      main.showModal(url,"#mainModal",".modal-content","GET");
      return false;
    });
  },
  showModal:function(url,targetModal,targetElement,type,data){
    if(type==undefined)type="GET";
    if(data==undefined)data={};
    $.ajax({
      url:url,
      type:type,
      dataType:"JSON",
      data:data,
      success:function(response){
        if(response.status==false)
        {
          alert("Page request not found!.");
        }else if(response.status==true){
          $(targetModal+" "+targetElement).html(response.content);
          $(targetModal).modal();
          main.validate(targetModal+" "+targetElement+" form[validate]");
          main.select(targetModal+" "+targetElement+" select");
          main.tooltip(targetModal+" "+targetElement+" [title]");
        }
      }
    });
  },
  ajax:function(url,type,dataType,data,callBackFunction){
    if(callBackFunction==undefined)callBackFunction="";
    $.ajax({
      url:url,
      type:type,
      dataType:dataType,
      data:data,
      success:function(response){
        main.dataResponse=response;
        if(response.status==false)
        {
          alert("Page request not found!.");
        }else if(response.status==true){
          if(callBackFunction!=""){
            var fn = window[callBackFunction];
            if (typeof fn === "function") fn(response);
          }
        }
      }
    });
  },
  notification:function(layout,type,headTitle,message){
    noty({
      dismissQueue: true, 
      layout: layout, 
      theme: 'defaultTheme', 
      text: "<strong>"+headTitle+"</strong><br>"+message, 
      type: type
    });
  },
  validate:function(element){
    $(element).validate({
      submitHandler: function(form){
        form.submit();
      },
      errorPlacement: function(error, element){
        if(element.parents(".form-group:first").hasClass("has-error")==false){
          element.parents(".form-group:first").addClass("has-error");
        }
        element.siblings(".help-block").text(error.text());
      },
      success:function(label,element){
        $(element).parents(".form-group:first").removeClass("has-error");
        label.html("");
      }
    });	
  },
  confirmDialog:function(message,textOK,textCancel){
    $(document).on("click", "[data-confirm-modal]", function() {
      var url=$(this).attr("data-confirm-modal");
      if(textOK==undefined)textOK="Yes";
      if(textCancel==undefined)textCancel="Cancel";
      noty({
        dismissQueue: true, 
        layout: "center", 
        theme: 'defaultTheme', 
        text: message, 
        type: "warning",
        modal: true,
        buttons: [
        {
          addClass: 'btn btn-primary', 
          text: textOK, 
          onClick: function ($noty) {
            window.location.href=url;
          }
        },{
          addClass: 'btn btn-danger', 
          text: textCancel, 
          onClick: function ($noty) {
            $noty.close();
          }
        }
        ]
      });
      return false;
    });
  },
  select:function(element){
    $(element).multiselect();
  },
  tooltip:function(element){
    $(element).tooltip();
  },
  init:function(){
    this.triggerModal();
    this.validate("form[validate]");
    this.tooltip('nav a,.tooltips');
    this.confirmDialog("Apakah anda yakin ingin hapus data ini ?","Ya","Batal");
  }
}
var height="330px";
$(document).ready(function(){
  main.init();
  if($.cookie('showhide')==undefined){
    $.cookie('showhide', "true",{ path: '/' });
  }
  if($.cookie('showhide')=="true"){
    $("#table-search .wrap").css({
      "min-height":height,
      "height":height
    });
    $('#table-search').removeClass('collapsed');
    $("#table-search .wrap").removeAttr("style");
  }else{
    $("#table-search .wrap").css({
      "min-height":"40px",
      "height":"40px"
    });
    $('#table-search').addClass('collapsed');
  }
  $('.showhide').click(function(){
    $(this).toggleClass('active');
    if($('#table-search').hasClass('collapsed')){
      $("#table-search .wrap").animate({
        "min-height":height,
        "height":height
      }, 700, function() {
        $('#table-search').removeClass('collapsed');
        $("#table-search .wrap").removeAttr("style")
      });
      $.removeCookie('showhide');
      $.cookie('showhide', true,{ path: '/' });
    }else{
      $("#table-search .wrap").animate({
        "min-height":"40px",
        "height":"40px"
      }, 700, function() {
        $('#table-search').addClass('collapsed');
      });
      $.removeCookie('showhide');
      $.cookie('showhide', false,{ path: '/' });
    }
  });
  if($.cookie('zoom')==undefined){
    $.cookie('zoom', 1,{ path: '/' });
  }else{
    var zoom=$.cookie('zoom');
    if(zoom==1){
      $("#zoom_in").attr("disabled","disabled");
    }
    if(zoom==0.7){
      $("#zoom_out").attr("disabled","disabled");
    }
  }
  $("body section").css("zoom",$.cookie("zoom"));
  if($.cookie("zoom")<1){
    $(".tooltip.bottom").css("left","0px !important");
  }
  $("#zoom_in").click(function(){
    var zoom=$.cookie('zoom');
    if(zoom!=1){
      zoom=Math.ceil((parseFloat(zoom)+0.1)*100)/100;
      $("body section").css("zoom",zoom);
      $.removeCookie('zoom');
      $.cookie('zoom', (zoom),{ path: '/' });
      if(zoom==1){
        $("#zoom_in").attr("disabled","disabled");
      }else{
        $("#zoom_in").removeAttr("disabled");
      }
      if(zoom==0.7){
        $("#zoom_out").attr("disabled","disabled");
      }else{
        $("#zoom_out").removeAttr("disabled");
      }
    }
  });
  $("#zoom_out").click(function(){
    var zoom=$.cookie('zoom');
    if(zoom!=0.7){
      zoom=Math.ceil((parseFloat(zoom)-0.1)*100)/100;
      $("body section").css("zoom",zoom);
      $.removeCookie('zoom');
      $.cookie('zoom', (zoom),{ path: '/' });
      if(zoom==1){
        $("#zoom_in").attr("disabled","disabled");
      }else{
        $("#zoom_in").removeAttr("disabled");
      }
      if(zoom==0.7){
        $("#zoom_out").attr("disabled","disabled");
      }else{
        $("#zoom_out").removeAttr("disabled");
      }
    }
  });
  $("ul.nav-tab > li").click(function(){
    $('ul.nav-tab > li').removeClass('active');
    $('ul.nav-tab').css('height','auto');
    $(this).addClass('active');
    if( $(this).hasClass('has_menu_two') ){
      $('ul.nav-tab').css('height','55px');
    }else if( $(this).hasClass('has_menu') ){
      $('ul.nav-tab').css('height','90px');
    }
  });
  $("ul.sub-one li.has_menu_two").click(function(){
    $("li.has_menu_two ul.sub-two").css("display","none");
    $(this).find(".sub-two").css("display","block");
  });
  $(".level1").click(function(){
    var menu_id=$(this).data("menu_id");
    var menu_name=$(this).data("original-title");
    $(".billing-tabs").removeClass("hide");
    $(".level2").addClass("hide");
    $(".level2[data-parent='"+menu_id+"']").removeClass("hide");
    return false;
  });
});